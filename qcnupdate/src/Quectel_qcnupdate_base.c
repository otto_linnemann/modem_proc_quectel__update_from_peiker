
/*!  
  @file
  Quectel_qcnupdate_base.c

  @brief
  This file defines interfaces for QCN-UPDATE BASE use

  @see
  Create this file by QCM9X00212C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/06/2018   vicent        Init
11/15/2018   vicent        Streamlined code design
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"

#include "quectel_qcnupdate_base.h"

#if defined(QUECTEL_QCNUPDATE_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Extern functions
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_GetItem
=========================================================================*/
/*! @brief 
     Get the pointer of 'QCNUPDATE_ItemStruct' data matched by <iItemType>

    @param
     <pItemList>  [IN] The pointer of 'QCNUPDATE_ItemStruct' data
     <iItemType>  [IN] Item type, same as 'QCNUPDATE_TypeEnum'

    @return
     The pointer of 'QCNUPDATE_ItemStruct' data

    @dependencies
     NONE

    @see
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
QCNUPDATE_ItemStruct* QL_QCNUPDATE_GetItem(QCNUPDATE_ItemStruct *pItemList,int iItemType)
{
    uint16 i = 0;
    QCNUPDATE_ItemStruct *pItem = NULL;

    if(NULL == pItemList)
    {
        QCNUPDATE_TRACE_2("FAIL! pItemList is NULL.",0,0);
        return FALSE;
    }

    i = 0;
    pItem = &(pItemList[i]);

    while((pItem->uType) != QCNUPDATE_TYPE_MAX)
    {
        if((pItem->uType) == iItemType)
        {
            break;
        }
    
        i += 1;
        pItem = &(pItemList[i]);
    }

    if(QCNUPDATE_TYPE_MAX == (pItem->uType))
    {
        return NULL;
    }

    return pItem;
}

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_GetVersionFromEfs
=========================================================================*/
/*! @brief 
     Get version from EFS matched by <iProject>

    @param
     <iProject>  [IN] 'QCNUPDATE_ProjectEnum' data

    @return
     version from EFS

    @dependencies
     NONE

    @see
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
uint32 QL_QCNUPDATE_GetVersionFromEfs(int iProject)
{
    QUECEFS_ErrEnum eErr = QUEC_EFS_ERR_SUCC;
    QCNUPDATE_VersionStruct sVersion;
    uint32 uVersion = 0;

    //Initialize
    memset(&sVersion, 0x00, sizeof(sVersion));

    eErr = QL_EFS_Read(QUEC_EFS_QCNUPDATE_VERSION, &sVersion, sizeof(sVersion));
    if(eErr != QUEC_EFS_ERR_SUCC)
    {
        QCNUPDATE_TRACE_2("WARNING! QL_EFS_Read(QCNUPDATE_VERSION) FAIL! eErr:%d,use default value.", eErr, 0);
        memset(&sVersion, 0x00, sizeof(sVersion)); //Reset
    }

    if(QCNUPDATE_PROJECT_ALL == iProject)
    {
        uVersion = sVersion.uAll;
    }
    else
    {
        uVersion = sVersion.uSingle;
    }

    return uVersion;
}

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_SaveVersionToEfs
=========================================================================*/
/*! @brief 
     Save version to EFS matched by <iProject>

    @param
     <iProject>  [IN] 'QCNUPDATE_ProjectEnum' data

    @return
     version from EFS

    @dependencies
     NONE

    @see
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
boolean QL_QCNUPDATE_SaveVersionToEfs(int iProject,uint32 uVersion)
{
    QUECEFS_ErrEnum eErr = QUEC_EFS_ERR_SUCC;
    QCNUPDATE_VersionStruct sVersion;

    //Initialize
    memset(&sVersion, 0x00, sizeof(sVersion));

    eErr = QL_EFS_Read(QUEC_EFS_QCNUPDATE_VERSION, &sVersion, sizeof(sVersion));
    if(eErr != QUEC_EFS_ERR_SUCC)
    {
        QCNUPDATE_TRACE_2("WARNING! QL_EFS_Read(QCNUPDATE_VERSION) FAIL! eErr:%d,use default value.", eErr, 0);
        memset(&sVersion, 0x00, sizeof(sVersion)); //Reset
    }

    if(QCNUPDATE_PROJECT_ALL == iProject)
    {
        sVersion.uAll = uVersion;
    }
    else
    {
        sVersion.uSingle = uVersion;
    }

    eErr = QL_EFS_Write(QUEC_EFS_QCNUPDATE_VERSION, &sVersion, sizeof(sVersion));
    if(eErr != QUEC_EFS_ERR_SUCC)
    {
        QCNUPDATE_TRACE_2("FAIL! QL_EFS_Write(QCNUPDATE_VERSION) FAIL! eErr:%d.", eErr, 0);
        return FALSE;
    }

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_GetCurVersion
=========================================================================*/
/*! @brief 
     Get current version from <pItemList>

    @param
     <pItemList>  [IN] The pointer of 'QCNUPDATE_ItemStruct' data

    @return
     current version

    @dependencies
     NONE

    @see
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
uint32 QL_QCNUPDATE_GetCurVersion(QCNUPDATE_ItemStruct *pItemList)
{
    uint32 uVersion = 0;
    QCNUPDATE_ItemStruct *pItem = NULL;

    if(NULL == pItemList)
    {
        QCNUPDATE_TRACE_2("FAIL! pItemList is NULL.", 0, 0);
        return 0;
    }

    pItem = QL_QCNUPDATE_GetItem(pItemList,QCNUPDATE_TYPE_VERSION);
    if(NULL == pItem)
    {
        QCNUPDATE_TRACE_2("FAIL! QL_QCNUPDATE_GetItem(TYPE_VERSION) FAIL!", 0, 0);
        return 0;
    }

    uVersion = (uint32)pItem->pData;

    return uVersion;
}

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_GetProject
=========================================================================*/
/*! @brief 
     Get project type from <pItemList>

    @param
     <pItemList>  [IN] The pointer of 'QCNUPDATE_ItemStruct' data

    @return
     project type

    @dependencies
     NONE

    @see
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
int QL_QCNUPDATE_GetProject(QCNUPDATE_ItemStruct *pItemList)
{
    int iProject = 0;

    if(NULL == pItemList)
    {
        QCNUPDATE_TRACE_2("FAIL! pItemList is NULL.", 0, 0);
        return QCNUPDATE_PROJECT_SINGLE;
    }

    if(pItemList == g_asQCNUPDATEItemListAll)
    {
        iProject = QCNUPDATE_PROJECT_ALL;
    }
    else
    {
        iProject = QCNUPDATE_PROJECT_SINGLE;
    }

    return iProject;
}

#endif //#if defined(QUECTEL_QCNUPDATE_FEATURE)
