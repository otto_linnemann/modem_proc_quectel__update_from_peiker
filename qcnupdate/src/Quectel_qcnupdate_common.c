
/*!  
  @file
  Quectel_qcnupdate_common.c

  @brief
  This file defines interfaces for QCN-UPDATE COMMON use

  @see
  Create this file by QCM9X00212C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/06/2018   vicent        Init
11/15/2018   vicent        Streamlined code design
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"
//<2019/02/01-QCM9X00212C001-P02-Vicent.Gao, <[QCNUPDATE] Segment 2==> Base code submit.>
#include "mcfg_fs.h"
//>2019/02/01-QCM9X00212C001-P02-Vicent.Gao

#include "quectel_func_net_oper.h"
#include "quectel_efs_default.h"

#include "quectel_qcnupdate_common.h"
#include "quectel_qcnupdate_config.h"

#if defined(QUECTEL_QCNUPDATE_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_sInitWrite
=========================================================================*/
/*! @brief 
     Write item list for MCFG Refresh

    @param
     <pItemList>  [IN] The pointer of 'QCNUPDATE_ItemStruct' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see    
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
static boolean QL_QCNUPDATE_sInitWrite(QCNUPDATE_ItemStruct *pItemList)
{
    uint16 i = 0;
    boolean bResult = FALSE;
    QCNUPDATE_ItemStruct *pItem = NULL;
    mcfg_fs_status_e_type eMcfgStatus = MCFG_FS_STATUS_OK;

    if(NULL == pItemList)
    {
        QCNUPDATE_TRACE_2("FAIL! pItemList is NULL.",0,0);
        return FALSE;
    }

    i = 0;
    pItem = &(pItemList[i]);
    
    while(pItem->uType != QCNUPDATE_TYPE_MAX)
    {
        if(QCNUPDATE_TYPE_NV_ADD == pItem->uType)
        {
            bResult = QL_EFSDEF_WriteNvItemByMcfgStr((nv_items_enum_type)pItem->pData, pItem->pStr);
            if(FALSE == bResult)
            {
                QCNUPDATE_TRACE_2("FAIL! QL_EFSDEF_WriteNvItemByMcfgStr(%d) FAIL! i:%d", (nv_items_enum_type)pItem->pData,i);
            }
        }
        else if(QCNUPDATE_TYPE_NV_DEL == pItem->uType)
        {
            bResult = QL_EFSDEF_DeleteNvItemNum((nv_items_enum_type)pItem->pData);
            if(FALSE == bResult)
            {
                QCNUPDATE_TRACE_2("FAIL! QL_EFSDEF_DeleteNvItemNum(%d) FAIL! i:%d", (nv_items_enum_type)pItem->pData,i);
            }
        }
        else if(QCNUPDATE_TYPE_EFS_ADD == pItem->uType)
        {
            bResult = QL_EFSDEF_WriteFileStr((char*)pItem->pData, pItem->pStr);
            if(FALSE == bResult)
            {
                QCNUPDATE_TRACE_2("FAIL! QL_EFSDEF_WriteFileStr(%s) FAIL! i:%d", (char*)pItem->pData, i);
            }
        }
        else if(QCNUPDATE_TYPE_EFS_DEL == pItem->uType)
        {
            eMcfgStatus = mcfg_fs_delete((char*)pItem->pData,MCFG_FS_TYPE_EFS,MCFG_FS_SUBID_0);
            if(eMcfgStatus != MCFG_FS_STATUS_OK)
            {
                QCNUPDATE_TRACE_3("FAIL! mcfg_fs_delete(%s) FAIL! i:%d,eMcfgStatus:%d", (char*)pItem->pData, i, eMcfgStatus);
            }
        }
        else
        {
            QCNUPDATE_TRACE_2("FAIL! uType:%d is INVALID. i:%d,ignore it.", pItem->uType, i);
        }

        i += 1;
        pItem = &(pItemList[i]);
    }

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_sInitItemList
=========================================================================*/
/*! @brief 
     Init item list - QCN UPDATE

    @param
     <pItemList>  [IN] The pointer of 'QCNUPDATE_ItemStruct' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
static boolean QL_QCNUPDATE_sInitItemList(QCNUPDATE_ItemStruct *pItemList)
{
    boolean bResult = FALSE;
    boolean bCurVersionIsNewer = FALSE;
    uint32 uCurVersion = 0;
    uint32 uEfsVersion = 0;
    int iProject = 0;

    if(NULL == pItemList)
    {
        QCNUPDATE_TRACE_2("FAIL! pItemList is NULL.", 0, 0);
        return FALSE;
    }

    uCurVersion = QL_QCNUPDATE_GetCurVersion(pItemList);
    iProject = QL_QCNUPDATE_GetProject(pItemList);
    uEfsVersion = QL_QCNUPDATE_GetVersionFromEfs(iProject);
    
    if(uCurVersion <= uEfsVersion)
    {
        QCNUPDATE_TRACE_2("FAIL! uCurVersion:%d <= uEfsVersion:%d,ignore it.", uCurVersion, uEfsVersion);
        return FALSE;
    }

    bResult = QL_QCNUPDATE_sInitWrite(pItemList);
    if(FALSE == bResult)
    {
        QCNUPDATE_TRACE_2("FAIL! QL_QCNUPDATE_sInitWrite FAIL!", 0,0);
        return FALSE;
    }

    bResult = QL_QCNUPDATE_SaveVersionToEfs(iProject,uCurVersion);
    if(FALSE == bResult)
    {
        QCNUPDATE_TRACE_2("FAIL! QL_QCNUPDATE_SaveVersionToEfs FAIL! iProject:%d,uCurVersion:%d", iProject, uCurVersion);
        return FALSE;
    }
        
    return TRUE;
}

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Extern functions
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_QCNUPDATE_Init
=========================================================================*/
/*! @brief 
     Init for QCN UPDATE

    @param
     VOID

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     Refer to [Req-Depot].[RQ0000204][Submitter:2018/11/14,Date:2018/11/14]
*/
/*=======================================================================*/
boolean QL_QCNUPDATE_Init(void)
{
    boolean bResult = FALSE;

    bResult = QL_QCNUPDATE_sInitItemList(g_asQCNUPDATEItemListAll);
    if(FALSE == bResult)
    {
        QCNUPDATE_TRACE_2("WARNING! QL_QCNUPDATE_sInitItemList(ALL) FAIL!", 0, 0);
    }

    bResult = QL_QCNUPDATE_sInitItemList(g_asQCNUPDATEItemListSingle);
    if(FALSE == bResult)
    {
        QCNUPDATE_TRACE_2("WARNING! QL_QCNUPDATE_sInitItemList(SINGLE) FAIL!", 0, 0);
    }
        
    return TRUE;
}

#endif //#if defined(QUECTEL_QCNUPDATE_FEATURE)

