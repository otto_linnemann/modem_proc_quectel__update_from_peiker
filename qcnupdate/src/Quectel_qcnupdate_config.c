
/*!  
  @file
  Quectel_qcnupdate_config.c

  @brief
  This file defines interfaces for QCN-UPDATE CONFIG use

  @see
  Create this file by QCM9X00212C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/14/2018   vicent        Init
11/15/2018   vicent        Streamlined code design
12/11/2018   Aaron         Turn off network EDRX related functions
12/24/2018   Aaron         Improve low temperature 5.7/5.7A
03/16/2019   Aaron         Wcdma release version default configuration
04/10/2019   Aaron         QCN update for AG35NAVAM and AG35EVAM project.
04/26/2019   Aaron         QCN update for AG35NAVAM RF.
04/26/2019   Tery          QCN update for AG35NAVAM SUPL.
04/28/2019   Tery          QCN update for AG35EVAM SUPL.
04/30/2019   Aaron         QCN update for AG35EVAM RF.
05/06/2019   Tery          QCN update for AG35NAVAM SUPL.
05/09/2019   Tery          QCN update for AG35EVAM SUPL.
05/14/2019   Aaron         QCN update for AG35EVAM project.
05/14/2019   Floyd         QCN update for AG35EVAM project.
15/06/2019   Tery          add NV05596 default value for all projecct
09/07/2019   Tery          delete update_band_range EFS NV config for all project
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"

#include "quectel_efs_default.h"
#include "quectel_qcnupdate_config.h"

#if defined(QUECTEL_QCNUPDATE_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////

QCNUPDATE_ItemStruct g_asQCNUPDATEItemListAll[] = 
{
    {QCNUPDATE_TYPE_VERSION, (void*)201909041, NULL},

	/*<</Tery 20190709 delete update_band_range EFS  NV config*/
	{QCNUPDATE_TYPE_EFS_DEL, (void*)"/nv/item_files/modem/lte/ML1/update_band_range", NULL},
	
	/*Tery 20190615 add DPO control default value for dpo */
	{QCNUPDATE_TYPE_NV_ADD, (void*)5596, "00"},
	
    //<2019/03/16-QCM96A00012-P01-Aaron.Li, <[QCNUPDATE] Segment 1==> Wcdma release version default configuration.>
    {QCNUPDATE_TYPE_NV_ADD, (void*)3649, "05"},
    //>2019/03/16-QCM96A00012-P01-Aaron.Li
    //Warning!==>Please add new element upper this line.
    {QCNUPDATE_TYPE_MAX, NULL, NULL}
};

QCNUPDATE_ItemStruct g_asQCNUPDATEItemListSingle[] = 
{
#if defined(__QUECTEL_PROJECT_DEMO__)

    {QCNUPDATE_TYPE_VERSION, (void*)201811141, NULL},
    {QCNUPDATE_TYPE_NV_ADD, (void*)850, "000001"},
    {QCNUPDATE_TYPE_NV_DEL, (void*)855, NULL},
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/ims/IMS_enable", "00"},
    {QCNUPDATE_TYPE_EFS_DEL, (void*)"/nv/item_files/modem/mmode/ue_usage_setting", NULL},
    
//<2018/12/11-QCM9X00212C003-P01-Aaron.Li, <MBN] Segment 1==> Turn off network EDRX related functions.>
#elif defined(__QUECTEL_PROJECT_EC25AF__)
    
    {QCNUPDATE_TYPE_VERSION, (void*)201812241, NULL},
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/nas/nas_edrx_config", "00000000000000000000000000000000000000000000000000000000000000"},
    //<2018/12/24-QCM9X00212C003-P02-Aaron.Li, <MBN] Segment 2==> Improve low temperature 5.7/5.7A.>
    {QCNUPDATE_TYPE_NV_ADD, (void*)4030, "000000000000000F"},
    {QCNUPDATE_TYPE_NV_ADD, (void*)1178, "000000000000000F"},
    {QCNUPDATE_TYPE_NV_ADD, (void*)1849, "000000000000000F"},
    //>2018/12/24-QCM9X00212C003-P02-Aaron.Li
//>2018/12/11-QCM9X00212C003-P01-Aaron.Li

//<2019/02/19-QCM9X00212C005-P01-Vicent.Gao, <[QCNUPDATE] Segment 1==> QCN update for EG25G.>
#elif defined(__QUECTEL_PROJECT_EG25G__)
    {QCNUPDATE_TYPE_VERSION, (void*)201902251, NULL},
    {QCNUPDATE_TYPE_NV_ADD, (void*)3649, "05"},
    //<2019/02/25-QCM9X00212C005-P02-Vicent.Gao, <[QCNUPDATE] Segment 2==> QCN update for EG25G.>
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/lte/rrc/cap/diff_fdd_tdd_fgi_enable", "01"},
    //>2019/02/25-QCM9X00212C005-P02-Vicent.Gao
//>2019/02/19-QCM9X00212C005-P01-Vicent.Gao

//<2019/04/10-QCM96A00013C002-P01-Aaron.Li, <[QCNUPDATE] Segment 1==> QCN update for AG35NAVAM and AG35EVAM project.>
#elif defined(__QUECTEL_PROJECT_AG35_NA_VAM__)
    {QCNUPDATE_TYPE_VERSION, (void*)201905141, NULL},
    {QCNUPDATE_TYPE_NV_ADD, (void*)6850, "0F"}, //NV6850
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/qmi/cat/qmi_cat_mode", "00"}, //NV65683
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/ds/atcop/atcop_cops_auto_mode.txt", "01"},
//<2019/04/10-QCM96A00013C002-P02-Aaron.Li, <[QCNUPDATE] Segment 2==> QCN update for AG35NAVAM and AG35EVAM project.>
    {QCNUPDATE_TYPE_NV_ADD, (void*)1849, "00000000000F0F0F"}, //NV1849
    {QCNUPDATE_TYPE_NV_ADD, (void*)4157, "00"}, //NV4157
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/rfnv/00024980", "BEFF070002000200FFFFE2FF0100"}, //NV70749
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/rfnv/00022155", "F6F6F60000000000"}, //NV66894
    /*Tery 20190426 add NV01920 default value*/
    {QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920
	{QCNUPDATE_TYPE_NV_ADD, (void*)7165, "18000000"},//NV07165
	{QCNUPDATE_TYPE_NV_ADD, (void*)6760, "01"},//NV06760
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/gps/cgps/me/gnss_config", "0F000000"},//NV70326
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/gps/cgps/sm/gnss_lpp_enable", "03"},//NV67225
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/gps/cgps/sm/gnss_assisted_glo_protocol_select", "06050000"},//NV70192
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/wcdma/rrc/rrc_ganss_support_nv", "01"},//NV71564

//>2019/04/10-QCM96A00013C002-P02-Aaron.Li

//<2019/04/26-QCM96A00013C002-P03-Aaron.Li, <[QCNUPDATE] Segment 3==> QCN update for AG35NAVAM and AG35EVAM project.>
    {QCNUPDATE_TYPE_NV_ADD, (void*)4158, "00"}, //NV4158
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/uim/gstk/cat_version", "09"}, //NV67331
//>2019/04/26-QCM96A00013C002-P03-Aaron.Li

//>2019/04/10-QCM96A00013C002-P01-Aaron.Li
	//<2019/04/28-QCM96A00013C002-P04-Vicent.Gao, <[QCNUPDATE] Segment 4==> QCN update for AG35NAVAM and AG35EVAM project.>
	{QCNUPDATE_TYPE_NV_ADD, (void*)6828, "5A180000000000000000000000000000"},
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/mmode/lte_bandpref", "5A18000000000000"}, //NV65633
	//>2019/04/28-QCM96A00013C002-P04-Vicent.Gao

//<2019/04/10-QCM96A00009C002-P01-Aaron.Li, <[QCNUPDATE] Segment 1==> QCN update for AG35NAVAM and AG35EVAM project.>
#elif defined(__QUECTEL_PROJECT_AG35_E_VAM__)
    {QCNUPDATE_TYPE_VERSION, (void*)201905201, NULL},
    {QCNUPDATE_TYPE_NV_ADD, (void*)6850, "0F"}, //NV6850
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/qmi/cat/qmi_cat_mode", "00"}, //NV65683
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/ds/atcop/atcop_cops_auto_mode.txt", "01"},
//<2019/04/30-QCM96A00009C002-P04-Aaron.Li, <[QCNUPDATE] Segment 4==> QCN update for AG35NAVAM and AG35EVAM project.>
    {QCNUPDATE_TYPE_NV_ADD, (void*)4157, "00"}, //NV4157
    {QCNUPDATE_TYPE_NV_ADD, (void*)4158, "00"}, //NV4158
//>2019/04/30-QCM96A00009C002-P04-Aaron.Li
//<2019/05/14-QCM96A00009C002-P05-Aaron.Li, <[QCNUPDATE] Segment 5==> QCN update for AG35NAVAM and AG35EVAM project.>
    {QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/uim/gstk/cat_version", "09"}, //NV67331
//>2019/05/14-QCM96A00009C002-P05-Aaron.Li
//>2019/04/10-QCM96A00009C002-P01-Aaron.Li
	/*Tery 20190428 add NV01920 default value*/
	{QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920
	{QCNUPDATE_TYPE_NV_ADD, (void*)7165, "18000000"},//NV07165
	{QCNUPDATE_TYPE_NV_ADD, (void*)6760, "01"},//NV06760
//>2019/05/20-QCM96A00009C002-P06-Floyd.Wang
	{QCNUPDATE_TYPE_NV_ADD, (void*)5107, "0100"},//NV05107
	{QCNUPDATE_TYPE_NV_ADD, (void*)4117, "01"},//NV04117
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/geran/vamos_support", "01"},//NV67228
//>2019/05/20-QCM96A00009C002-P06-Floyd.Wang
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/gps/cgps/me/gnss_config", "0F000000"},//NV70326
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/gps/cgps/sm/gnss_lpp_enable", "03"},//NV67225
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/gps/cgps/sm/gnss_assisted_glo_protocol_select", "06050000"},//NV70192
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/wcdma/rrc/rrc_ganss_support_nv", "01"},//NV71564
#elif defined(__QUECTEL_PROJECT_AG35_J_VAM__)
    {QCNUPDATE_TYPE_VERSION, (void*)201907121, NULL},
	{QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920
#elif defined(__QUECTEL_PROJECT_AG35_LA_VAM__)
    {QCNUPDATE_TYPE_VERSION, (void*)201908201, NULL},
	{QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920
	/*<</Lanny 20190820 add nv 70334 default value */
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/data/3gpp/ps/remove_unused_pdn", "00"}, //70334
	/*<</Lanny 20190820 */
	
#elif defined(__QUECTEL_PROJECT_AG35_NA_VAM__)
    {QCNUPDATE_TYPE_VERSION, (void*)201907121, NULL},
	{QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920
#elif defined(__QUECTEL_PROJECT_AG35_LA_VA__)
    {QCNUPDATE_TYPE_VERSION, (void*)201908201, NULL},
	{QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920
	
	/*<</Lanny 20190820 add nv 70334 default value */
	{QCNUPDATE_TYPE_EFS_ADD, (void*)"/nv/item_files/modem/data/3gpp/ps/remove_unused_pdn", "00"}, //70334
	/*<</Lanny 20190820 */
	
#elif defined(__QUECTEL_PROJECT_AG35_J_VA__)
	{QCNUPDATE_TYPE_VERSION, (void*)201909041, NULL},
	{QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920
#elif defined(__QUECTEL_PROJECT_AG35_E_VA__)
	{QCNUPDATE_TYPE_VERSION, (void*)201909041, NULL},
	{QCNUPDATE_TYPE_NV_ADD, (void*)1920, "7FFFFE01"},//NV01920	
#else
    {QCNUPDATE_TYPE_VERSION, (void*)201811141, NULL},

#endif
    
    //Warning!==>Please add new element upper this line.
    {QCNUPDATE_TYPE_MAX, NULL, NULL}
};

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////

#endif //#if defined(QUECTEL_QCNUPDATE_FEATURE)

