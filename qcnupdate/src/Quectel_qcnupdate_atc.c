
/*!  
  @file
  Quectel_qcnupdate_atc.c

  @brief
  This file defines interfaces for QCN-UPDATE ATC use

  @see
  Create this file by QCM9X00212C002-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/28/2018   vicent        Init
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"

#include "quectel_qcnupdate_atc.h"

#if defined(QUECTEL_QCNUPDATE_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_QQCNUPDATE_sCmdHdlrTest
=========================================================================*/
/*! @brief 
     Command handler of AT+QQCNUPDATE=?

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QQCNUPDATE_sCmdHdlrTest
(
    dsat_mode_enum_type eMode,
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok,
    dsm_item_type *pResBuf
)
{
    pResBuf->used += sprintf((char*)(pResBuf->data_ptr + pResBuf->used),"%s: \"list\"",pTok->name);

    return DSAT_OK;
}

/*========================================================================
  FUNCTION:  QL_QQCNUPDATE_sCmdHdlrSetListSubParamItem
=========================================================================*/
/*! @brief 
     Command handler of AT+QQCNUPDATE="list",<sub_param> - list item

    @param
     <pItemList>  [IN] The pointer of 'QCNUPDATE_ItemStruct' data

    @return
     'dsat_result_enum_type' value

    @dependencies
     NONE

    @see    
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QQCNUPDATE_sCmdHdlrSetListSubParamItem(QCNUPDATE_ItemStruct *pItemList)
{
    #define QQCNUPDATE_LIST_BUF_LEN  1024

    uint16 i = 0;
    uint16 j = 0;
    QCNUPDATE_ItemStruct *pItem = NULL;
    char *pBuf = NULL;
    uint16 uBufLen = 0;

    if(NULL == pItemList)
    {
        QCNUPDATE_TRACE_2("FAIL! pItemList is NULL.",0,0);
        return DSAT_ERROR;
    }

    pBuf = (char *)dsat_alloc_memory(QQCNUPDATE_LIST_BUF_LEN,FALSE);
    if(NULL == pBuf)
    {
        FUNC_TRACE_2("SMS_FAIL! dsat_alloc_memory FAIL! size:%d",QQCNUPDATE_LIST_BUF_LEN,0);
        return DSAT_ERROR;
    }

    i = 0;
    pItem = &(pItemList[i]);
    
    while(pItem->uType != QCNUPDATE_TYPE_MAX)
    {
        if(QCNUPDATE_TYPE_NV_ADD == pItem->uType)
        {
            snprintf(
                pBuf,
                QQCNUPDATE_LIST_BUF_LEN,
                "+QQCNUPDATE: \"list\",%d,\"nv_add\",%d,\"%s\"\r\n",
                j,
                (nv_items_enum_type)pItem->pData,
                pItem->pStr
            );
            j += 1;
        }
        else if(QCNUPDATE_TYPE_NV_DEL == pItem->uType)
        {
            snprintf(
                pBuf,
                QQCNUPDATE_LIST_BUF_LEN,
                "+QQCNUPDATE: \"list\",%d,\"nv_del\",%d\r\n",
                j,
                (nv_items_enum_type)pItem->pData
            );
            j += 1;
        }
        else if(QCNUPDATE_TYPE_EFS_ADD == pItem->uType)
        {
            snprintf(
                pBuf,
                QQCNUPDATE_LIST_BUF_LEN,
                "+QQCNUPDATE: \"list\",%d,\"efs_add\",\"%s\",\"%s\"\r\n",
                j,
                (char*)pItem->pData,
                pItem->pStr
            );
            j += 1;
        }
        else if(QCNUPDATE_TYPE_EFS_DEL == pItem->uType)
        {
            snprintf(
                pBuf,
                QQCNUPDATE_LIST_BUF_LEN,
                "+QQCNUPDATE: \"list\",%d,\"efs_del\",\"%s\"\r\n",
                j,
                (char*)pItem->pData
            );
            j += 1;
        }
        else
        {
            QCNUPDATE_TRACE_2("FAIL! uType:%d is INVALID. i:%d,ignore it.", pItem->uType, i);
            pBuf[0] = '\0';
        }

        uBufLen = strlen(pBuf);
        if(uBufLen != 0)
        {
            QL_FUNC_SendAtResponse(pBuf);
        }

        i += 1;
        pItem = &(pItemList[i]);
    }

    dsatutil_free_memory((void *)pBuf); pBuf = NULL;

    return DSAT_OK;
}

/*========================================================================
  FUNCTION:  QL_QQCNUPDATE_sCmdHdlrSetListSubParam
=========================================================================*/
/*! @brief 
     Command handler of AT+QQCNUPDATE="list",<sub_param>

    @param
     <pSubParam>    [IN] The pointer of sub param string

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QQCNUPDATE_sCmdHdlrSetListSubParam(char *pSubParam)
{
    QCNUPDATE_ItemStruct *pItemList = NULL;
    uint32 uCurVersion = 0;
    char aBuf[120] = {0,};

    if(NULL == pSubParam)
    {
        QCNUPDATE_TRACE_2("FAIL! pSubParam is NULL.",0,0);
        return DSAT_ERROR;
    }

    if(0 == dsatutil_strcmp_ig_sp_case((byte*)pSubParam, (byte *)"all"))
    {
        pItemList = g_asQCNUPDATEItemListAll;
    }
    else if(0 == dsatutil_strcmp_ig_sp_case((byte*)pSubParam, (byte *)"single"))
    {
        pItemList = g_asQCNUPDATEItemListSingle;
    }
    else
    {
        QCNUPDATE_TRACE_2("FAIL! pSubParam:%s NOT support.",pSubParam,0);
        return DSAT_ERROR;
    }

    uCurVersion = QL_QCNUPDATE_GetCurVersion(pItemList);

    snprintf(aBuf,sizeof(aBuf),"+QQCNUPDATE: \"list\",\"%s\",%d\r\n",pSubParam,uCurVersion);
    QL_FUNC_SendAtResponse(aBuf);

    QL_QQCNUPDATE_sCmdHdlrSetListSubParamItem(pItemList);

    return DSAT_OK;
}

/*========================================================================
  FUNCTION:  QL_QQCNUPDATE_sCmdHdlrSetList
=========================================================================*/
/*! @brief 
     Command handler of AT+QQCNUPDATE="list"

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QQCNUPDATE_sCmdHdlrSetList
(
    dsat_mode_enum_type eMode,
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok,
    dsm_item_type *pResBuf
)
{
    QCNUPDATE_ItemStruct *pItemListAll = g_asQCNUPDATEItemListAll;
    QCNUPDATE_ItemStruct *pItemListSingle = g_asQCNUPDATEItemListSingle;
    uint32 uCurVersionAll = QL_QCNUPDATE_GetCurVersion(pItemListAll);
    uint32 uCurVersionSingle = QL_QCNUPDATE_GetCurVersion(pItemListSingle);

    char aSubParam[40] = {0,};
    boolean bResult = FALSE;
    dsat_result_enum_type eResult = DSAT_OK;

    if(pTok->args_found < 2) //Syntax: AT+QQCNUPDATE="list"
    {
        pResBuf->used += snprintf(
                            (char*)(pResBuf->data_ptr + pResBuf->used),
                            (pResBuf->size - pResBuf->used),
                            "%s: \"list\",\"all\",%d",
                            pTok->name,
                            uCurVersionAll
                         );

        pResBuf->used += snprintf(
                            (char*)(pResBuf->data_ptr + pResBuf->used),
                            (pResBuf->size - pResBuf->used),
                            "\r\n%s: \"list\",\"single\",%d",
                            pTok->name,
                            uCurVersionSingle
                         );
        
        return DSAT_OK;
    }

    bResult = dsatutil_strip_quotes_out((byte*)pTok->arg[1], (byte *)aSubParam,sizeof(aSubParam));
    if(FALSE == bResult)
    {
        return DSAT_ERROR;
    }

    eResult = QL_QQCNUPDATE_sCmdHdlrSetListSubParam(aSubParam);

    return eResult;
}

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Extern functions
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  quectel_exec_qqcnupdate_cmd
=========================================================================*/
/*! @brief 
     Command handler of AT+QQCNUPDATE

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
dsat_result_enum_type quectel_exec_qqcnupdate_cmd
(
    dsat_mode_enum_type eMode,             
    const dsati_cmd_type *pParseTab,   
    const tokens_struct_type *pTok,  
    dsm_item_type *pResBuf         
)
{
	char  aSubCmd[40] = {0,};
	uint32 cmd_index = 0;
    dsat_result_enum_type eResult = DSAT_OK;
    boolean bResult = FALSE;

    QL_FUNC_StrToUpper((uint8*)pTok->name);
    
	if((NA|EQ|QU) == pTok->op) //AT+QQCNUPDATE=?
	{
        eResult = QL_QQCNUPDATE_sCmdHdlrTest(eMode,pParseTab,pTok,pResBuf);
	}
	else if((NA|EQ|AR) == pTok->op)
	{
        if(pTok->args_found < 1)
        {
            return DSAT_ERROR;
        }
        
        bResult = dsatutil_strip_quotes_out((byte*)pTok->arg[0], (byte *)aSubCmd,sizeof(aSubCmd));
        if(FALSE == bResult)
        {
            return DSAT_ERROR;
        }

        if(0 == dsatutil_strcmp_ig_sp_case((byte*)aSubCmd, (byte *)"list"))
        {
            eResult = QL_QQCNUPDATE_sCmdHdlrSetList(eMode,pParseTab,pTok,pResBuf);
        }
        else
        {
            eResult = DSAT_ERROR;
        }
	}    
    else
    {
        eResult = DSAT_ERROR;
    }

	return eResult;
}

#endif //#if defined(QUECTEL_QCNUPDATE_FEATURE)

