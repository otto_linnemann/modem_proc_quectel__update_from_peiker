
/*!  
  @file
  Quectel_qcnupdate_base.h

  @brief
  This file defines declarations for QCN-UPDATE BASE use

  @see
  Create this file by QCM9X00212C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/06/2018   vicent        Init
11/15/2018   vicent        Streamlined code design
=================================================================================*/

#ifndef __QUECTEL_QCNUPDATE_BASE_H__
#define __QUECTEL_QCNUPDATE_BASE_H__

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define QUEC_EFS_QCNUPDATE_VERSION  "qcnupdate_version"

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    QCNUPDATE_PROJECT_ALL,    //QCN update for all project
    QCNUPDATE_PROJECT_SINGLE, //QCN update for one single project
    
    //Warning!==>Please add new QCN-UPDATE PROJECT upper this line
    QCNUPDATE_PROJECT_MAX
} QCNUPDATE_ProjectEnum;

typedef enum
{
    QCNUPDATE_TYPE_NV_ADD,
    QCNUPDATE_TYPE_NV_DEL,
    QCNUPDATE_TYPE_EFS_ADD,
    QCNUPDATE_TYPE_EFS_DEL,
    
    QCNUPDATE_TYPE_VERSION, //QCN Version, as 201811141(The 1th time update on 2018/11/14)

    //Warning!==>Please add new QCN-UPDATE TYPE upper this line.
    QCNUPDATE_TYPE_MAX
} QCNUPDATE_TypeEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef boolean QCNUPDATE_InitFunc(void);

typedef struct
{
    uint8 uType; // 'QCNUPDATE_TypeEnum' data
    void *pData; // NV; EFS;
    char *pStr;
} QCNUPDATE_ItemStruct;

typedef struct
{
    uint32 uAll; //All project
    uint32 uSingle; //Single project, ex: EP06A
    uint32 aRev[10];
} QCNUPDATE_VersionStruct;

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
extern QCNUPDATE_ItemStruct g_asQCNUPDATEItemListAll[];
extern QCNUPDATE_ItemStruct g_asQCNUPDATEItemListSingle[];

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean QL_EFSDEF_WriteNvItemByMcfgStr(nv_items_enum_type eItem,char *pStr);
extern boolean QL_EFSDEF_WriteFileStr(char *pEfs,char *pStr);

extern QCNUPDATE_ItemStruct* QL_QCNUPDATE_GetItem(QCNUPDATE_ItemStruct *pItemList,int iItemType);
extern uint32 QL_QCNUPDATE_GetCurVersion(QCNUPDATE_ItemStruct *pItemList);
extern int QL_QCNUPDATE_GetProject(QCNUPDATE_ItemStruct *pItemList);
extern uint32 QL_QCNUPDATE_GetVersionFromEfs(int iProject);
extern boolean QL_QCNUPDATE_SaveVersionToEfs(int iProject,uint32 uVersion);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define QCNUPDATE_TRACE_0(f)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),QCNUPDATE_" f,__FUNCTION__,__LINE__)
#define QCNUPDATE_TRACE_1(f,g1)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),QCNUPDATE_" f,__FUNCTION__,__LINE__,g1)
#define QCNUPDATE_TRACE_2(f,g1,g2)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),QCNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2)
#define QCNUPDATE_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_5(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),QCNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3)
#define QCNUPDATE_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_6(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),QCNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4)
#define QCNUPDATE_TRACE_5(f,g1,g2,g3,g4,g5)  MSG_SPRINTF_7(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),QCNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5)
#define QCNUPDATE_TRACE_6(f,g1,g2,g3,g4,g5,g6)  MSG_SPRINTF_8(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),QCNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5,g6)

#endif  // #ifndef __QUECTEL_QCNUPDATE_BASE_H__
