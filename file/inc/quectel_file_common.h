#ifndef __QUECTEL_FILE_COMMON__
#define __QUECTEL_FILE_COMMON__

/*===========================================================================

        quectel_file_common.h
                   

        DESCRIPTION
   

        when         who    what, where, why
        --------    ---    ----------------------------------------------------------
        10/06/13    Gibson     Created module

===========================================================================*/

#include "quectel_apptcpip_util.h"

#if defined(QUECTEL_FILE_FUNC)

#define QFILE_FILENAME_MAX_LENGTH       80
#define QFILE_PATH_PREFIX_MAX_LENGTH    30  //文件系统启示路径最大长度，add by running 2016-09-02
#define QFILE_MAX_PATH_LENGTH           (QFILE_FILENAME_MAX_LENGTH * 2)

//next path len can't over QFILE_FS_PREFIX_MAX_LENGTH, add by running 2016-09-02
#define UFS_PATH    "/data/ufs/"    //modify by running.qian, 2016-8-3
#define RAM_PATH    "/ram/ufs/"     //modify by running.qian, 2016-8-5
#define SD_PATH     "/media/sdcard/"  //modify by running.qian, 2016-11-1

typedef enum
{
#ifdef QUECTEL_RAM_FILE
		FILE_RAM,
#endif
    FILE_UFS,
    FILE_SD,
    FILE_MAX
}QF_FILETYPE;


typedef struct storage_info
{
    uint64 free_size;
    uint64 total_size;
    uint64 maxalloc_size;		//For only some partition
}STORAGE_INFO;


typedef struct listfileinfo
{
    Q_INT8 fileName[QFILE_MAX_PATH_LENGTH];
    Q_UINT32 fileSize;
    Q_INT32 ramSize;
}QF_LISTFILEINFO;

//add by running.qian,2016-08-03.it cut from 'quectel_file_at.h'
typedef struct ufs_file_info
{
	Q_UINT64    ufs_file_size;
	Q_UINT32    ufs_file_number;
}UFS_FILE_INFO;
#endif
#endif
