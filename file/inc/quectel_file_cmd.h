#ifndef __QUECTEL_FILE_CMD__
#define __QUECTEL_FILE_CMD__

#include "rex.h"
#include "queue.h"
#include "quectel_apptcpip_util.h"
#include "msgcfg.h"
#include "msg_diag_service.h"
#include "quectel_file_at.h"
#if defined(QUECTEL_FILE_FUNC)

#define QFT_CMD_MAX_NUM   30

typedef  enum
{
    QFILE_MIN_CMD = -1,
    QFILE_MOV_CMD,
    QFILE_GET_OPENFILE_CMD,
    QFILE_OPENONEFILE_CMD,
    QFILE_CLOSEFILE_CMD,
    QFILE_GET_FILEPOSITION_CMD,
    QFILE_FFLUSH_CMD,
    QFILE_FTUCAT_CMD,
    QFILE_FSEEK_CMD,
    QFILE_FREAD_CMD,
    QFILE_FWRITE_CMD,
    QFILE_FDEL_CMD,
    QFILE_FLDS_CMD,
    QFILE_FLST_CMD,
    QFILE_CLEAR_FLAG_CMD,
    QFILE_CLOSEDELFILE_CMD,
    QFILE_MAX_CMD
}qfile_cmd_e_type;


typedef struct
{
    q_link_type                                             link;
    rex_tcb_type                                          *task_ptr;       
    q_type                                                  *done_q_ptr;
    qfile_cmd_e_type                                    cmd_id;
    void                                                      *user_data;
}qfile_cmd_hdr_type;


typedef struct
{
    Q_CHAR src_file_name[QFILE_MAX_PATH_LENGTH];
    Q_CHAR dst_file_name[QFILE_MAX_PATH_LENGTH];
    QF_FILETYPE src_type;
    QF_FILETYPE dst_type;
    Q_INT8 src_copy_f;
    Q_INT8 dst_copy_f;
}QFMOV_PARAM;


typedef struct
{
    Q_INT8 file_name[QFILE_MAX_PATH_LENGTH];
    Q_INT8 mode;
}FILE_INFO;


typedef struct
{
    Q_INT32 fd;
    Q_UINT32 offset;
    Q_UINT32 position;
}FSEEK_PARA;

typedef struct
{
    Q_INT8 fdwl_flag;
    Q_UINT32 bytes_need;
    Q_INT32 fd;
    Q_INT8 file_name[QFILE_MAX_PATH_LENGTH];
    Q_UINT8 end;
    Q_UINT32 bytes_total;//总共请求的数据大小
}FREAD_PARA;

typedef struct
{
    Q_INT8 fupl_flag;
    dsm_item_type *dsm_ptr;
    Q_INT32 fd;
    Q_INT8 file_name[QFILE_MAX_PATH_LENGTH];
    Q_INT32 bytes_left;
    Q_UINT8 end;
    rex_timer_type *rx_input_timer;
    Q_UINT32 rx_input_timeout;
    dsm_watermark_type *rx_wm;
    Q_BOOL    ack_mode;
}FWRITE_PARA;        

typedef struct
{
    Q_UINT8 ufs_def;
    Q_INT8 flds_file_path[QFILE_MAX_PATH_LENGTH];  
}FLDS_PARA;

typedef struct
{
    Q_INT8 flst_file_path[QFILE_MAX_PATH_LENGTH];
    Q_UINT8 type;
}FLST_PARA;

typedef struct
{
    qfile_cmd_hdr_type    hdr;
    dsat_cme_error_e_type code;
    QF_FILETYPE fileType;
    union
    {
        QFMOV_PARAM fmov_param;
        FILE_INFO file_info;
        Q_INT32 fd;
        FSEEK_PARA fseek_para;
        FREAD_PARA fread_para;
        FWRITE_PARA fwrite_para;
        Q_INT8 fdel_file_path[QFILE_MAX_PATH_LENGTH];
        FLDS_PARA flds_para;
        FLST_PARA flst_para;
    }cmd_info;
}QFILE_CMD_TYPE;


typedef  enum
{
     QFILE_CMD_Q_SIG                = 0x0001,
     QFILE_RPT_TIMER_SIG           =  0x0002
}qft_signal_enum;

#define   QFILE_SIGS    (QFILE_CMD_Q_SIG |QFILE_RPT_TIMER_SIG)


/**************************************
*
*   Description:cmd queue init
*
*   Output:none
*
*   Return: none
*
***************************************/
void queue_init(void);

/**************************************
*
*   Description:put cmd queue
*
*   Output:cmd_ptr:cmd data structure
*
*   Return: none
*
***************************************/
void qfile_put_cmd(QFILE_CMD_TYPE *cmd_ptr);




/**************************************
*
*   Description:get cmd from the queue
*
*   Output:none
*
*   Return:Qfile_cmd_type
*
***************************************/
QFILE_CMD_TYPE * qfile_get_next_cmd( void );



/**************************************
*
*   Description:get cmd buffer from the free queue
*
*   Output:none
*
*   Return:Qfile_cmd_type
*
***************************************/
QFILE_CMD_TYPE  *qfile_get_cmd_buf( void );


/**************************************
*
*   Description:release qfile cmd buf
*
*   Output:cmd_pptr:cmd pointer
*
*   Return:void
*
***************************************/
void  qfile_release_cmd_buf(QFILE_CMD_TYPE    **cmd_pptr);
#endif
#endif

