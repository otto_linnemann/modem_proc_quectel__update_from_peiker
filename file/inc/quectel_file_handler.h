/***************************************************************************************
*
*			File Name:quectel_file_handler.h
*
*		       Description:File task
*
*			Author:Gibson Pan(Pan Wei)
*		
*			Created Time: 19 Dec. 2012
*
***************************************************************************************/

#ifndef __QUECTEL_FILE_HANDLER__
#define __QUECTEL_FILE_HANDLER__



#include "comdef.h"
#include "task.h"
#include "dog.h"
//#include "tmc.h"
#include "queue.h"
#include "quectel_apptcpip_util.h"


#include "quectel_file_task.h"
#if defined(QUECTEL_FILE_FUNC)

#define FILE_MAX_NUM 15
#ifdef QUECTEL_FILE_EX_FEATURE
	#define UFS_FP_INDEX_BASE       0
	#define SD_FP_INDEX_BASE	100
#endif


typedef struct 
{
    Q_CHAR file_name[QFILE_MAX_PATH_LENGTH];
    Q_INT32 fd; 
    Q_UINT8 mode;
    Q_UINT16 checksum;
    Q_UINT16 odd;
    Q_UINT8 odd_flag;
    Q_INT8 end;
    Q_UINT32 total_size;
    Q_UINT32 transfer_size;
    QF_FILETYPE fileType;
}QFILE_OPFL_INFO;


/**************************************
*
*   Description:init file info
*
*   Output:none
*
*   Return: none
*
***************************************/
void init_fileinfo(Q_UINT8 i);

#ifdef QUECTEL_UFS_FILE
/**************************************
*
*   Description:qfile_process_mov_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fmov_cmd(QFILE_CMD_TYPE *cmd);
#endif

/**************************************
*
*   Description:qfile_process_getopenfile_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fgetopenfile_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_openfile_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fopenfile_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_closefile_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fclosefile_cmd(QFILE_CMD_TYPE *cmd);



/**************************************
*
*   Description:qfile_process_getposition_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fgetposition_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_fflush_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fflush_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_ftucat_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_ftucat_cmd(QFILE_CMD_TYPE *cmd);

/**************************************
*
*   Description:qfile_process_fseek_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fseek_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_fread_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void  qfile_process_fread_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_fwrite_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fwrite_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_fdel_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_fdel_cmd(QFILE_CMD_TYPE *cmd);

/**************************************
*
*   Description:qfile_process_flds_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_flds_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_flst_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_flst_cmd(QFILE_CMD_TYPE *cmd);


/**************************************
*
*   Description:qfile_process_clearflag_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_clearflag_cmd(QFILE_CMD_TYPE *cmd);




/**************************************
*
*   Description:qfile_process_closedel_cmd
*
*   Output:cmd:cmd info
*
*   Return: none
*
***************************************/
void qfile_process_closedel_cmd(QFILE_CMD_TYPE *cmd);
void qfile_process_fwrite_cmd_test(QFILE_CMD_TYPE *cmd);//jonathan,14/5/6

#endif
#endif


