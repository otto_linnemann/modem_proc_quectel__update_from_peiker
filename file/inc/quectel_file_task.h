/***************************************************************************************
*
*			File Name:quectel_file_task.h
*
*		       Description:File Task
*
*			Author:Gibson Pan(Pan Wei)
*		
*			Creat Time: 18 Dec. 2012
*
***************************************************************************************/


#ifndef __QUECTEL__FILE__TASK__

#define __QUECTEL__FILE__TASK__

#include "QUECoem_feature.h"
#include "quectel_apptcpip_util.h"
#include "msg_diag_service.h"
#include "msgcfg.h"
//#include "dsat_ext_api.h"
#include "ds3gsiolib_ex.h"
#include "fs_sys_types.h"
//#include "fs_vnode.h"
#include "rex.h"
#include "timer.h"
#include "quectel_file_cmd.h"
#include "task.h"
#include "modem_mem.h"
#include "quectel_file_handler.h"

#if defined(QUECTEL_FILE_FUNC)

#define GIBSON_DEBUG_HEAP

#define FT_TAG  "FT_TASK"

typedef struct qfile_struct
{
    rex_crit_sect_type qfile_cmd_queue_crit_sect;
    rex_timer_type  qfile_rpt_timer;
}QFILE_TASK_PARAM;



typedef enum
{
  QFILE_TIMER_MAX             /* maximum value - used for bounds checking   */
} qfile_timer_enum_type;

/*===========================================================================

FUNCTION QFILE_TASK

DESCRIPTION
  This is the entry point for the Quectel File Task. This function contains
  the main processing loop that waits for file events (signals or commands) and
  deal with it.

DEPENDENCIES
  None

RETURN VALUE
  This function does not return.

SIDE EFFECTS
  None

===========================================================================*/
void  file_task(uint32 dummy);

#endif
#endif
