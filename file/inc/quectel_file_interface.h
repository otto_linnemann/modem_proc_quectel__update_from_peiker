/***************************************************************************************
*
*			File Name:quectel_file_interface.h
*
*		       Description:the Common Interface
*
*			Author:Gibson Pan(Pan Wei)
*		
*			Created Time: 2013/06/18
*
***************************************************************************************/


#include "quectel_file_util.h"
#include "fs_public.h"
#include "quectel_file_at.h"
//#include "quectel_efsfile.h"
#include "stringl.h"

#ifdef QUECTEL_RAM_FILE
#include "quectel_ramfile.h"
#endif


#if defined(QUECTEL_FILE_FUNC)

/*===========================================================================
FUNCTION Q_INT32 Quectel_FileOpen(Q_INT8 *path, Q_UINT8 flag);

DESCRIPTION
   Open the File

DEPENDENCIES

RETURN VALUE
  the file description.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 Quectel_FileOpen(Q_INT8 *path, Q_INT32 flag);

/*===========================================================================
FUNCTION Q_INT32 Quectel_FileClose(Q_INT32 fileHandle);

DESCRIPTION
   Close the File

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 Quectel_FileClose(Q_INT32 fileHandle);

/*===========================================================================
FUNCTION Q_INT32 Quectel_FileRead(Q_INT32 fileHandle, Q_INT8 *buf, Q_UINT32 buf_size);

DESCRIPTION
   read the data from  the File

DEPENDENCIES

RETURN VALUE
  the number of the bytes.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 Quectel_FileRead(Q_INT32 fileHandle, Q_INT8 *buf, Q_UINT32 bufSize);

/*===========================================================================
FUNCTION Q_INT32 Quectel_FileWrite(Q_INT32 fileHandle, Q_INT8 *buf, Q_UINT32 buf_size);

DESCRIPTION
   Write the data to the File

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 Quectel_FileWrite(Q_INT32 fileHandle, Q_INT8 *buf, Q_UINT32 bufSize);

/*===========================================================================
FUNCTION Q_INT32 Quectel_FileSeek(Q_INT32 fileHandle, Q_INT32 offset, Q_INT32 whence);

DESCRIPTION
   Seek the File

DEPENDENCIES

RETURN VALUE
  the offset.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 Quectel_FileSeek(Q_INT32 fileHandle, Q_INT32 offset, Q_INT32 whence);

/*===========================================================================
FUNCTION Q_INT32 Quectel_FileDel(Q_INT8 *path);

DESCRIPTION
   Delete the File

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 Quectel_FileDel(Q_INT8 *path);

/*===========================================================================
FUNCTION Q_INT32 Quectel_FileGetSize(Q_INT8 *path, Q_UINT32 *size);

DESCRIPTION
   get file size,if the file not exsit, return -1.

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 Quectel_FileGetSize(Q_INT8 *path, Q_UINT32 *size);
Q_INT32  Quectel_FileSize(Q_INT32 fileHandle, Q_UINT32 *size);
Q_INT32 Quectel_FileTrunk(Q_INT32 fileHandle,  Q_UINT32 length);//jonathan 13/12/6
extern Q_INT32 Quectel_IsFileOpened(Q_INT8 *path);
extern Q_INT8 is_file_open(Q_INT8 *file_name);

#endif
