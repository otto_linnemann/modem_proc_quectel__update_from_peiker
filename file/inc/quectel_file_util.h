/***************************************************************************************
*
*			File Name:quectel_file_util.h
*
*		       Description:File Module function header file
*
*			Author:Gibson Pan(Pan Wei)
*		
*			Created Time: 24 Jan. 2013
*
***************************************************************************************/
#ifndef __QUECTEL_FILE_UTIL__
#define __QUECTEL_FILE_UTIL__

#include <stdio.h>
#include "QuecOEM_feature.h"
#include "quectel_apptcpip_util.h"
#include "fs_sys_types.h"
#include <string.h>
#include "stringl.h"
#include "quectel_file_common.h"
#include "dsat_v.h"
#include "dsati.h"

#if defined(QUECTEL_FILE_FUNC)

#define UFS_SPACE_RESERVED 1000000
#define QAT_LINE_END "\r\n"
#define QAT_END_N "\n"

#define UFSPrefix   "UFS:"
#define RAMPrefix   "RAM:"
#define SDPrefix    "SD:"


//panwei 05/06/2013
typedef enum 
{
    QF_MIN = -1,
    QF_UFS,
    QF_RAM,
    QF_SD,
    QF_MAX
}QF_ST;


typedef Q_INT32 (*openHandler)(Q_INT8 *path, Q_INT32 flag);
typedef Q_INT32 (*readHandler)(Q_INT32 fileHandle, Q_INT8 *buf, Q_UINT32 buf_size);
typedef Q_INT32 (*writeHandler)(Q_INT32 fileHandle, Q_INT8 *buf, Q_UINT32 buf_size);
typedef Q_INT32(*closeHandler)(Q_INT32 fileHandle);
typedef Q_INT32 (*seekHandler)(Q_INT32 fileHandle, Q_INT32 offset, Q_INT32 whence);
typedef Q_INT32 (*delHandler)(Q_INT8 *filename);
//typedef void (*creatHandler)(Q_INT8 *filename, Q_UINT8 flag);
typedef Q_INT32 (*checkFileExistHandler)(Q_INT8 *filename);
typedef uint64 (*getFreeSizeHandler)(void);
typedef Q_INT32 (*getFileSizeHandler)(Q_INT32 fd);
typedef Q_INT32 (*getFileSizeHandlerByName)(Q_INT8 *filename);
typedef Q_INT32 (*getStorageInfoHandler)(STORAGE_INFO *storage_info);
typedef Q_INT8 (*getAllFileInfoHandler)(QF_LISTFILEINFO *fileInfoStruct);
typedef Q_INT32 (*seekFileHandler)(Q_INT32 fileHandle, Q_INT32 offset, Q_INT32 whence);
typedef Q_INT32 (*truncateHandler)(Q_INT32 fd, Q_INT32 length);

//gibson 2013/06/09
typedef struct FileType
{
    QF_FILETYPE fileType;
    openHandler openFunc;
    closeHandler closeFunc;
    readHandler readFunc;
    writeHandler writeFunc;
    delHandler delFunc;
    checkFileExistHandler checkFileExistFunc;
    getFreeSizeHandler getFreeSizeFunc;
    getFileSizeHandler getFileSizeFunc;
    getFileSizeHandlerByName getFileSizeByNameFunc;
    getStorageInfoHandler getStorageInfoFunc;
    getAllFileInfoHandler getAllFileInfoFunc;
    seekFileHandler seekFileFunc;
    truncateHandler truncateFunc;
}QFileStructure;
    
extern QFileStructure qFileStructArray[FILE_MAX];

char *quectel_safe_strncpy (char *d, const char*s, size_t n);    
Q_INT8 quectel_is_dir_exist(const char *path);
Q_INT8 convert_dirpath(char *path, Q_UINT8 len);
Q_INT8 add_dir_to_filepath(char *path, Q_UINT8 len);
Q_INT8 is_file_name_valid(Q_INT8 * file_name);
QF_FILETYPE getFileType(Q_INT8 *filename);
QFileStructure *QFILE_GetFileStruct(QF_FILETYPE fieType);
char *quectel_strchr(const char *s, int c);
char* strrep(char* src, char oldChar, char newChar);
void change_fs_workdir(QF_FILETYPE filetype, char *newpath);
int get_fs_workdir(QF_FILETYPE filetype, char *buff, uint16_t size);
int get_fs_fullpath(char *rpath, char *apath, uint16 apathsize);
Q_INT8 checkStrValid(Q_CHAR *ptr);
atoi_enum_type dsatutil_atow(word *val_arg_ptr, const byte *s, word r);
word file_CalcCheckSum(word *checksum, word *odd_tmp, uint8 *odd_flag, uint8 *buffer, word size, boolean bend);

typedef enum
{
    FTDS_MOVE,
    FTDS_READ,
    FTDS_WRITE,
    FTDS_READ_FINISH,
    FTDS_WRITE_FINISH,
    FTDS_PRINT_RESULT,
    FTDS_FLST,
    FTDS_MAX
}FT_CMD_NO;

typedef struct
{
    Q_UINT32 write_size;
    Q_UINT32 total_size;
    Q_UINT16 checksum;
    Q_UINT16 odd;
    Q_UINT8 odd_flag;
    Q_INT8 fupl_flag;
    Q_INT8 file_name[90];
    Q_INT8 end;
}quec_fupl_para;

typedef struct
{
    Q_INT8 file_name[90];
    Q_INT8 end;
}quec_fdwl_para;


typedef struct
{
    FT_CMD_NO cmd_id;
    dsm_item_type *response;
    Q_INT32 fd;
    dsat_cme_error_e_type code;
    union
    {
        quec_fupl_para fupl_para;
        quec_fdwl_para fdwl_para;
    }ft_param;
}ds_at_ft_cmd_type;

#endif
#endif
