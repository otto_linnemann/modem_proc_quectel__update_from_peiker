/*2014.09.12 Rex add begin*/
/*add a new nv items, you should follow the steps below:
1.define a NV_ITEM_ID
2.define a struct for the new nv_item
3.add the new struct variable to NV_ITEM_TYPE
4.add the new element into nvim_item_info_table
*/

/*1.define a NV_ITEM_ID
NV_MAX_I should not extend  0x7fffffff*/
/************************************************************************************/
#ifdef QUECTEL_OEM_NV_ITEM_ID
/* 2015.11.10 add by Chris  for AT+EGMR NV Item */
NV_EGMR_SN_I,  //= 7233

/*2015.11.11 added by Chris for AT+QURCCGF*/
NV_QURC_PORT_I,//=7234

/*2015.11.11 added by Chris for AT+QINDCGF*/
NV_QINDCFG_URC_IND_FLAG_I,//=7235

/*2015.11.12 added by Chris for Ri*/
NV_QCFG_RI_INFO_I,// = 7236,

/*2014.10.10 added by Rex for ECC*/
NV_QUEC_ECCNUM_WITH_SIM_I,//=7237
NV_QUEC_ECCNUM_WITH_NO_SIM_I,//=7238

/*2014.9.30 added by Rex for AT+CTZR*/
NV_CTZR_I,//=7239

//2015.09.10 add by francis
NV_QUEC_HOTSWAP_I,// = 7240,

/*2014.10.20 added by Francis for fly mode*/
NV_QCFG_AIRPLANECONTROL_I,//=7241

//2015.11.21 add by chris for QGDCNT  //
NV_QGDCNT_PARAM_I, // = 7242

NV_ECALL_EXT_I, //7243

/*2015.01.26 added by Rex for CPMS*/
NV_CPMS_MEMORY_I, //7244

/*2015.09.01 add by chris*/
NV_QUECTEL_STK_CFG_I,    //7245
NV_QUECTEL_TDS_CSQ_I, //7246
/*2015.12.07 added by chris for at+qcfg="remote/ip"*/
NV_QUECTEL_REMOTE_IP_I,    // = 7247

NV_STATUS_LED_MODE_I,//=7248

//2015.10.29 add by francis.tang for bonson  //
NV_STATUS_CUSTOMER_MODE_I, //7249

//2015/11/14 add by wythe for bonson //
NV_CUST_BONSON_QATI_STR_I,   //7250
NV_CUST_BONSON_QGMI_STR_I,   //7251
NV_CUST_BONSON_QGMM_STR_I,   //7252
NV_CUST_BONSON_QGMR_STR_I,   //7253
NV_QUECTEL_NO_TRAILER_I,    // = 7254

NV_CREG_REPORT_I, //7255

/*jacky add 2015.12.21  for GPS*/
NV_GPS_XTRA_TIME_AUTO_SYNC_I,// = 7249
NV_QGPS_NMEASRC_ENABLE_I,//7250
NV_CGPS_ON_DEMAND_READY_I,//7251
NV_QGPS_CFG_OUTPORT_I,//7252
NV_QUECTEL_GPS_AUTO_I,//7253

NV_QUEC_USB_ID_I, //maxcodeflag20160326
/*sundy add 2016.3.28 for volte*/
NV_VOLTE_OPERATOR_I, // 7254  
NV_QUEC_BT_MAC_I,//maxcodeflag20160405
NV_QUEC_WIFI_MAC_I, //maxcodeflag20160405
NV_QUECTEL_CMUX_URCPORT_I,    // 
NV_QUEC_AUTO_BUAD_I,//Jun.Wu.20160526

NV_QUEC_PPP_AUTH_I, //maxcodeflag20160525

NV_SYSINFO_URC_I,

//<2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao, <[CTCC] Segment 1==> smsreg base code submit.>
NV_CTCC_SMS_REG_I,
//>2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao
NV_CTCC_DS_REG_I,

//2016/06/21 add by chao.zhou for QPDPGDCNT  //
NV_QPDPGDCNT_PDP_I,

NV_QUEC_IPV6_MTU_I, //QUECTEL_CERTIFICATION_TELSTRA_MTU

NV_QUEC_STOP_PDP_RETRY_I, //maxcodeflag20160706

NV_QUEC_USB_EE_I, //will.shao, for usb early enable
NV_QUEC_AUTH_SET_FLAG_I, //maxcodeflag20160711
NV_QUEC_GW_CFGT_CFGP_I,

NV_QUEC_OOS_TIMER_I,

NV_MAX_TX_POWER_DEFAULT_I,

NV_QUEC_CDMA_RUIM_CTRL_I, //maxcodeflag20160715

NV_QUEC_MODEM_RESTART_LEVEL_I, //maxcodeflag20160715

//add by chao.zhou 2016/07/22 for porting ppp/termframe & nwoptmz/acq cmd
NV_PPP_TERM_FRAME_I, 
NV_NETWORK_OPTIMIZE_ACQ_I,
//2016/07/29-QCM9X07FZ00002-P01-Frederic.Zhang
//add AT+QCFG="IMS" command 
NV_QUEC_IMS_CMD_CTRL_I,
//2016/07/28-QCM9XFZ00002-P01-Frederic.Zhang

NV_QUEC_W2LFST_INFO_I, //QUEC_3GPP_RRC_MANUAL_RELEASE

//2016/08/19 porting by chao.zhou for landi
NV_QOEM_LANDI_I,

//<2016/09/05-QCM9XTVG00052-P01-Vicent.Gao,<[SIM] Segment 1==>Copy QCFG-sim/recovery from UC20.>
NV_QUECTEL_SIM_RECOVERY_I,
//>2016/09/05-QCM9XTVG00052-P01-Vicent.Gao

NV_QUECTEL_DUAL_SIM_SWITCH_I,  // Ramos20160911 add for QDSIM  commond 

NV_QUEC_USB_FUNCTIONS_I,

//<2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao,<[CDMASMS] Add cmd: qcfg-cdmasms/cmtformat.>
NV_QCFG_CDMA_SMS_CMT_FORMAT_I,
//>2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao
/*2014.11.03 added by Francis for ap ready*/
    NV_QAP_READY_INFO_I,
//lory 2016/09/20 for at+qcfg=sleepind  and wakeupin
NV_QCFG_SLEEPIN_LEVEL_I,
NV_QCFG_WAKEUPIN_LEVEL_I,
//lory add end 2016/09/20 for at+qcfg=sleepind  and wakeupin

//<2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng, <[QCOTA] Segment 1==> Copy AT+QCOTA from UC20.>
NV_QCOTA_I,    //7293
//>2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng

//<2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng, <[SKT] Segment 1==> Copy S30_REQUIREMENT function code from UC20.>
NV_S30_REGISTER_I,    //7294
//>2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng

NV_QCFG_QDSP6_FREQ_I,

//<2016/12/15-QCWXM9X00021-P02-wenxue.sheng, <[QCFG] Segment 2==> Copy AT+QCFG="disrplmn" from EC20.>
NV_QRPLMN_CTRL_MODE_I,    //7296
//>2016/12/15-QCWXM9X00021-P02-wenxue.sheng

//<2016/10/21-QCM9XAW00002-P01-Andvin.Wang, <[QSAR] Segment 1==>Add QSAR common function definition.>
NV_QUECTEL_RF_SAR_I,
//>2016/10/21-QCM9XAW00002-P01-Andvin.Wang

NV_QCFG_THERMAL_LIMIT_RATES_I,

NV_QCFG_THERMAL_TX_LIMIT_I,

NV_QUEC_QMI_SYNC_I,

//Kenneth 2017/01/24
NV_QUECTEL_IP_ROUTE_I,

//<2017/03/20-QCWXM9X00032-P01-wenxue.sheng, <[ATC] Segment 1==>Sync QCOPS CMD from 9215.>
NV_QCOPS_FLAG_DEFAULT_I,
//>2017/03/20-QCWXM9X00032-P01-wenxue.sheng

NV_QCFG_FAST_DORMANCY_I,

NV_QUEC_REP_OOS_I,

#endif //end QUEC_OEM_NV_ITEM_ID


/*2.define NV item struct*/
/************************************************************************************/
#ifdef QUECTEL_OEM_NV_ITEM_STRUCT
/* 2015.11.10 add by Chris  for AT+EGMR NV Item */
typedef PACKED struct PACKED_POST{
    uint8 egmr_sn[64];
}nv_egmr_params_type;

/* 2015.11.10 add by chris  for AT+QURCCFG NV struct*/
typedef PACKED struct PACKED_POST{
	uint32   quectel_urc_port;
}nv_quectel_urc_port_type;

/*2015.11.10 added by chris for AT+QINDCGF*/
typedef PACKED struct PACKED_POST{
    uint32 urc_ind_flag;
}nv_qindcfg_urc_ind_flag_type;

/*2015.11.12 added by chris for RI*/

typedef PACKED struct PACKED_POST{
    uint8  ri_output_type;
    uint8  ri_active_level;
    uint8  ring_auto_wave_no_disturbing;
    uint8  ri_signal_type_ring;
    uint8  ri_signal_type_smsincoming;
    uint8  ri_signal_type_other;
    uint32 pulse_duration_ring;
    uint32 pulse_duration_smsincoming;
    uint32 pulse_duration_other;
    uint32 ring_wave_active_duration;
    uint32 ring_wave_inactive_duration;
    uint8  pulse_count_ring;
    uint8  pulse_count_smsincoming;
    uint8  pulse_count_other;
    uint8  urc_delay;
}nv_qcfg_ri_info_type;

typedef PACKED struct PACKED_POST{
    unsigned char qeccnum_with_sim[20][6];
}nv_quectel_eccnum_with_sim_type;

typedef PACKED struct PACKED_POST{
    unsigned char qeccnum_with_no_sim[20][6];
}nv_quectel_eccnum_with_no_sim_type;

/*2014.9.30 added by Rex for AT+CTZR*/
typedef PACKED struct PACKED_POST{
    uint8 ctzr_flag;
}nv_qctzr_flag_type;

//2015,09,10 add by francis
typedef PACKED struct PACKED_POST{
    uint8 qsimdet_enable[2];
    uint8 qsimdet_inserted_level[2];
    uint8 qsimstat_report_enable[2];
}nv_quectel_hotswap_type;

typedef PACKED struct PACKED_POST{
    uint8 qcfg_airplanecontrol;
}nv_qcfg_airplane_control_type;

/*2015.11.21 add by chris for QGDCNT  */
typedef PACKED struct PACKED_POST {
    uint64 u64RxDataCnt;
    uint64 u64TxDataCnt;
}nv_qgdcnt_param_type;

typedef PACKED struct PACKED_POST {
    uint8  qecall_voc_config;
    uint8 qecall_msd_type;
    uint8 qecall_max_redialnum;
    uint8 qecall_mode;
    uint8 qecall_default_mode;
    uint8 qecall_dsp_mode;
    uint8 qecall_process_info;
}nv_ecall_params_type;

/*2015.01.26 added by Rex*/
typedef PACKED struct PACKED_POST {
    uint8 cpms_value[3];
}nv_cpms_params_type;

/*2015.09.01 add by chris*/
typedef PACKED struct PACKED{
	uint8    stk_mode;
	uint8    stk_alphabet_type;
	uint32  stk_response_timeout;
}nv_stk_cfg_type;

typedef PACKED struct PACKED_POST {
    uint8 tds_csq;
}nv_tds_csq_type;

/*2015.12.07 added by chris for at+qcfg="remote/ip"*/
typedef PACKED struct PACKED_POST {
    uint8 remote_ip_sel;
} nv_remote_ip_type;

typedef PACKED struct PACKED_POST{
    uint8 led_mode;
}nv_status_led_mode;

/*2015,10,29 francis*/
typedef PACKED struct PACKED_POST{
    uint8 customer_mode;
}nv_status_customer_mode;

typedef PACKED struct PACKED_POST {
    uint8 gw_trailer_value;
    uint8 normal_trailer_value;
}nv_no_trailer_type;

//2015/11/14 add by wythe
typedef PACKED struct PACKED_POST{
    unsigned char qati[256];
}nv_cust_bonson_qati_str_type;

typedef PACKED struct PACKED_POST{
    unsigned char qgmi[256];
}nv_cust_bonson_qgmi_str_type;

typedef PACKED struct PACKED_POST{
    unsigned char qgmm[256];
}nv_cust_bonson_qgmm_str_type;

typedef PACKED struct PACKED_POST{
    unsigned char qgmr[256];
}nv_cust_bonson_qgmr_str_type;

typedef PACKED struct PACKED_POST{
    uint16  creg_report_val;
    uint16  cgreg_report_val;
    uint16  cereg_report_val;
}nv_creg_params_type;

/*jacky add 2015.12.21  for GPS*/
typedef PACKED struct PACKED_POST {
    boolean auto_sync;
}nv_gps_xtra_time_auto_sync_type;

typedef PACKED struct PACKED_POST {
    boolean nmeasrc_enable;
}nv_qgps_nmeasrc_enable_type;

typedef PACKED struct PACKED_POST {
    uint8 cgps_on_demand_ready_mode;
}nv_cgps_on_demand_ready_type;

typedef PACKED struct PACKED_POST {
    uint8 qgps_cfg_outport;
}nv_qgps_cfg_outport_type;

typedef PACKED struct PACKED_POST {
    uint8 gps_auto_flag;
}nv_gps_auto_type;

//maxcodeflag20160326
typedef PACKED struct PACKED_POST {
    unsigned int uiPID;
    unsigned int uiVID;
}nv_quec_usb_id_type;

/* 2016.3.28 added by sundy for VoLTE operator config */
typedef PACKED struct PACKED_POST {
    uint8 volte_operator;
}nv_volte_operator_type;

//maxcodeflag20160405
typedef PACKED struct PACKED_POST {
    uint8 u8BTMac[32];
}nv_quec_bt_mac_type;

typedef PACKED struct PACKED_POST {
    uint8 u8WifiMac[32];
}nv_quec_wifi_mac_type;

//2015,05,26 add by francis
typedef PACKED struct PACKED_POST {
    uint8 cmux_urcport_flag;
}nv_cmux_urcport_type;
//end francis

//Jun.Wu.20160526
typedef PACKED struct PACKED_POST {
    uint16 bitrate_type;
}nv_quec_auto_baud_type;

//maxcodeflag20160525
typedef PACKED struct PACKED_POST {
    uint8 u8DefaultAuthType[24];
	uint8 u8LastValidType[24];
}nv_quec_ppp_auth_param;

typedef PACKED struct PACKED_POST {
    int32  sysinfo_urc_flg;
}nv_sysinfo_urc_flg;

//<2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao, <[CTCC] Segment 1==> smsreg base code submit.>
typedef PACKED struct PACKED_POST 
{
    uint8 uRegStat;
    uint8 aImsi[16]; //CTCC_BASE_IMSI_MAX_LEN + 1
}nv_ctcc_sms_reg_type;
//>2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao

typedef PACKED struct PACKED_POST 
{
    //<2016/06/22-QCM9XTCZ00015-P03-Chao.Zhou,<[comment: add new at command for get ueiccid and control enrypt].>
    boolean b_encrypt;
    char    aSvrIP[128];
    uint16  SvrPort;
    //<2016/06/22-QCM9XTCZ00015-P03-Chao.Zhou
    uint8 aUEIccid[20+1];
}nv_ctcc_ds_reg_type;

/*2016/06/21 add by chao.zhou for QPDPGDCNT*/
typedef PACKED struct PACKED_POST{
    uint64 u64RxDataCnt[17];
    uint64 u64TxDataCnt[17];
}nv_qpdpgdcnt_param_type;

//QUECTEL_CERTIFICATION_TELSTRA_MTU
typedef PACKED struct PACKED_POST{
	uint32 uiMTUV6;
}nv_quec_ipv6_mtu_type;

//maxcodeflag20160706
typedef PACKED struct PACKED_POST{
	boolean bEnable;
}nv_quec_stop_pdp_retry_type;

//maxcodeflag20160711
typedef PACKED struct PACKED_POST{
	boolean bAuthSet;
}nv_quec_auth_set_flag_type;

typedef PACKED struct PACKED_POST{
	int iCFGT[6];
	int iCFGP[6];
}nv_quec_gw_cfgt_cfgp_type;


typedef PACKED struct PACKED_POST {
  int time[3];
} pwr_save_pwrup_time_type;

// 2016-04-25 add by scott.hu
typedef PACKED struct PACKED_POST {
	boolean vaild;
	uint16 max_pwr_default[32];
}nv_max_tx_power_type;
//end scott.hu

//maxcodeflag20160715
typedef PACKED struct PACKED_POST{
	boolean bCDMARuimCtrl;
}nv_quec_cdma_ruim_ctrl_type;

//jun20160728
typedef PACKED struct PACKED_POST{
	int restart_level;
}nv_quec_modem_restart_level_type;

//add by chao.zhou 2016/07/22 for porting ppp/termframe cmd
typedef PACKED struct PACKED_POST {
    boolean bEnablePPPTermFrame;
}nv_ppp_term_frame_type;

//add by chao.zhou 2016/07/22 for porting nwoptmz/acq cmd
typedef PACKED struct PACKED_POST {
boolean bEnableTag;
uint32 uiInterval;
}nv_network_optimize_acq_type;

//2016/07/29-QCM9X07FZ00002-P01-Frederic.Zhang
//add AT+QCFG="IMS" command 
typedef PACKED struct PACKED_POST{
	uint16  ims_value;
}nv_quec_ims_ctrl_type;
//2016/07/29-QCM9XFZ00002-P01-Frederic.Zhang

//QUEC_3GPP_RRC_MANUAL_RELEASE
typedef PACKED struct PACKED_POST{
	boolean bFreeNotKeep;
	uint32  uiMaxDropULCheckCnt;
	uint32  rrc_release_counter_max;
}nv_quec_w2lfst_info_type;

//2016/08/19 porting by chao.zhou for landi
typedef PACKED struct PACKED_POST{
boolean bEnableFlag;
}nv_qoem_landipin_type;

//<2016/09/05-QCM9XTVG00052-P01-Vicent.Gao,<[SIM] Segment 1==>Copy QCFG-sim/recovery from UC20.>
typedef PACKED struct PACKED_POST
{
    uint16 recovery_count;
    uint16 auto_detect_period;
    uint16 auto_detect_count;
} nv_qcfg_sim_recovery_type;
//>2016/09/05-QCM9XTVG00052-P01-Vicent.Gao

// Ramos20160911 add for QDSIM  commond 
typedef PACKED struct PACKED_POST{
	int qSimSlot;
}nv_dual_sim_switch_type;

typedef PACKED struct PACKED_POST
{
    uint32 usb_funtions_mask;
} nv_qcfg_usb_funtions_mask_type;

//<2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao,<[CDMASMS] Add cmd: qcfg-cdmasms/cmtformat.>
typedef PACKED struct PACKED_POST
{
    uint8 uCmtFormat;
} nv_qcfg_cdmasms_cmtformat_type;
//>2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao
/*2014,11,3 francis*/
typedef PACKED struct PACKED_POST{
    int32 Active_Level;
    uint32 uiInterval;
    boolean bEnable;
}nv_qap_ready_info_type;

//lory 2016/09/20 for at+qcfg=sleepind  and wakeupin
typedef PACKED struct PACKED_POST{
    uint8 qcfg_sleepind_level;
}nv_qcfg_sleepind_type;

typedef PACKED struct PACKED_POST{
    uint8 qcfg_wakeupin_level;
}nv_qcfg_wakeupin_type;
//lory add end 

//<2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng, <[QCOTA] Segment 1==> Copy AT+QCOTA from UC20.>
typedef PACKED struct PACKED_POST {
    uint8 uQCOTAMode;
}nv_qcota_type;
//>2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng

//<2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng, <[SKT] Segment 1==> Copy S30_REQUIREMENT function code from UC20.> 
typedef PACKED struct PACKED_POST {
   uint8   s30;
}nv_s30_register_type;
//>2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng

//<2016/12/15-QCWXM9X00021-P02-wenxue.sheng, <[QCFG] Segment 2==> Copy AT+QCFG="disrplmn" from EC20.>
typedef PACKED struct PACKED_POST {
    uint8 rplmn_ctrl;
}nv_rplmn_ctrl_type;
//>2016/12/15-QCWXM9X00021-P02-wenxue.sheng

//<2016/10/21-QCM9XAW00002-P01-Andvin.Wang, <[QSAR] Segment 1==>Add QSAR common function definition.>
typedef PACKED struct PACKED_POST{
    uint8 state;
}nv_rf_sar_type;
//>2016/10/21-QCM9XAW00002-P01-Andvin.Wang

typedef PACKED struct PACKED_POST{
    int8 limit[10][LTE_TX_TEMP_COMP_SIZ];
}nv_thermal_tx_limit_type;

typedef PACKED struct PACKED_POST{
    boolean enable;
}nv_qmi_sync_type;

//Kenneth 2017/01/24
typedef PACKED struct PACKED_POST {
    uint8 ip_route;
}nv_ip_route_type;

//<2017/03/20-QCWXM9X00032-P01-wenxue.sheng, <[ATC] Segment 1==>Sync QCOPS CMD from 9215.>
typedef PACKED struct PACKED_POST {
	uint8 qcops_mode;
}nv_qcops_flag_type;
//>2017/03/20-QCWXM9X00032-P01-wenxue.sheng

typedef PACKED struct PACKED_POST{
	uint8  bOnOff;
	uint16 uInterval;
}nv_qcfg_fast_dormancy_type;

typedef PACKED struct PACKED_POST{
	uint8 	enable;
	uint16 	pre_time;
	uint16  sleep_time;
	uint16  acq_time;
}nv_quec_rep_oos_type;
#endif //end QUEC_OEM_NV_ITEM_STRUCT

/*3.add the new struct variable to NV_ITEM_TYPE*/
/************************************************************************************/
#ifdef QUECTEL_OEM_NV_ITEM_TYPE_VAR
/* 2015.11.10 add by Chris  for AT+EGMR NV Item */

nv_egmr_params_type nv_egmr_value;

/* 2015.11.10 add by chris  for AT+QURCCFG NV struct*/

nv_quectel_urc_port_type         nv_urc_port;

/*2015.11.10 added by chris for AT+QINDCGF*/
nv_qindcfg_urc_ind_flag_type nv_urc_ind_flag;

/*2015.11.12 added by chris for RI*/
nv_qcfg_ri_info_type nv_ri_info;

nv_quectel_eccnum_with_sim_type nv_eccnum_with_sim;

nv_quectel_eccnum_with_no_sim_type nv_eccnum_with_no_sim;

nv_qctzr_flag_type nv_ctzr_flag;

//2015.09.10 add by francis
nv_quectel_hotswap_type     nv_hotswap;

nv_qcfg_airplane_control_type nv_airplane_control;
/*2015.11.21 add by chris for QGDCNT  */
nv_qgdcnt_param_type nv_qgdcnt_param;

nv_ecall_params_type nv_ecall_ext;

nv_cpms_params_type nv_cpms_value;

/*2015.09.01 add by chris*/
nv_stk_cfg_type nv_stk_cfg_value;
nv_tds_csq_type nv_tds_csq_value;
/*2015.12.07 added by chris for at+qcfg="remote/ip"*/
nv_remote_ip_type nv_remote_ip_value;

nv_status_led_mode  nv_led_mode;

nv_status_customer_mode  nv_customer_mode;//2015,10,29 francis

nv_cust_bonson_qati_str_type nv_cust_bonson_qati_str; //2015/11/14 wythe

nv_cust_bonson_qgmi_str_type nv_cust_bonson_qgmi_str; //2015/11/14 wythe

nv_cust_bonson_qgmm_str_type nv_cust_bonson_qgmm_str; //2015/11/14 wythe

nv_cust_bonson_qgmr_str_type nv_cust_bonson_qgmr_str; //2015/11/16 wythe
nv_no_trailer_type nv_no_trailer_value;

nv_creg_params_type nv_creg_value;

/*jacky add 2015.12.21  for GPS*/
nv_gps_xtra_time_auto_sync_type nv_gps_xtra_time_auto_snyc;
nv_qgps_nmeasrc_enable_type nv_qgps_nmeasrc_enable;
nv_cgps_on_demand_ready_type nv_cgps_on_demand;
nv_qgps_cfg_outport_type nv_cfg_outport;
nv_gps_auto_type nv_gps_auto_value;

//maxcodeflag20160326
nv_quec_usb_id_type nv_quec_usb_id;

/* 2016.2.2 added by Sundy for VoLTE operator config */
nv_volte_operator_type nv_volte_operator_value;

//maxcodeflag20160405
nv_quec_bt_mac_type  nv_quec_bt_mac;
nv_quec_wifi_mac_type nv_quec_wifi_mac;

nv_cmux_urcport_type nv_cmux_urcport_value;
//Jun.Wu.20160526
nv_quec_auto_baud_type nv_quec_auto_baud;

//maxcodeflag20160525
nv_quec_ppp_auth_param nv_quec_ppp_auth;

nv_sysinfo_urc_flg   nv_sysinfo_urc_value;

//<2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao, <[CTCC] Segment 1==> smsreg base code submit.>
nv_ctcc_sms_reg_type nv_ctcc_sms_reg;
//>2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao
nv_ctcc_ds_reg_type nv_ctcc_ds_reg;

/*2016/06/21 add by chao.zhou for QPDPGDCNT*/
nv_qpdpgdcnt_param_type nv_qpdpgdcnt_param;

//QUECTEL_CERTIFICATION_TELSTRA_MTU
nv_quec_ipv6_mtu_type nv_quec_ipv6_mtu;

//maxcodeflag20160706
nv_quec_stop_pdp_retry_type nv_quec_stop_pdp_retry;

uint8 usb_early_enable; //will.shao, for usb early enable

//maxcodeflag20160711
nv_quec_auth_set_flag_type nv_quec_auth_set_flag;
nv_quec_gw_cfgt_cfgp_type  nv_quec_gw_cfgt_cfgp;

pwr_save_pwrup_time_type nv_pwrup_timer;

// 2016-04-25 add by scott.hu
nv_max_tx_power_type nv_max_tx_pwr;
//end scott.hu

nv_quec_cdma_ruim_ctrl_type nv_quec_cdma_ruim_ctrl; //maxcodeflag20160715

nv_quec_modem_restart_level_type nv_quec_modem_restart_level; //jun20160728

//add by chao.zhou 2016/07/22 for porting ppp/termframe & nwoptmz/acq cmd
nv_ppp_term_frame_type nv_ppp_term_frame_param;
nv_network_optimize_acq_type nv_network_optimize_acq_param;
//2016/07/29-QCM9X07FZ00002-P01-Frederic.Zhang
//add AT+QCFG="IMS" command 
 nv_quec_ims_ctrl_type     nv_quec_ims_ctrl;
//2016/07/29-QCM9XFZ00002-P01-Frederic.Zhang

nv_quec_w2lfst_info_type nv_quec_w2lfst_info; //QUEC_3GPP_RRC_MANUAL_RELEASE

//2016/08/19 porting by chao.zhou for landi
nv_qoem_landipin_type nv_qoem_landipin_param;

//<2016/09/05-QCM9XTVG00052-P01-Vicent.Gao,<[SIM] Segment 1==>Copy QCFG-sim/recovery from UC20.>
nv_qcfg_sim_recovery_type nv_qcfg_sim_recovery;
//>2016/09/05-QCM9XTVG00052-P01-Vicent.Gao

// Ramos20160911 add for QDSIM  commond 
nv_dual_sim_switch_type nv_dual_sim_switch;

nv_qcfg_usb_funtions_mask_type nv_usb_funtions_mask;

//<2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao,<[CDMASMS] Add cmd: qcfg-cdmasms/cmtformat.>
nv_qcfg_cdmasms_cmtformat_type nv_qcfg_cdmasms_cmtformat;
//>2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao
nv_qap_ready_info_type nv_qap_ready_info;//2014,11,3 francis

//lory 2016/09/20 for at+qcfg=sleepind  and wakeupin
nv_qcfg_sleepind_type  nv_sleepind_type;
nv_qcfg_wakeupin_type nv_wakeupin_type;
//lory add end

//<2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng, <[QCOTA] Segment 1==> Copy AT+QCOTA from UC20.>
nv_qcota_type  nv_qcota;
//>2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng

//<2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng, <[SKT] Segment 1==> Copy S30_REQUIREMENT function code from UC20.> 
nv_s30_register_type nv_s30;
//>2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng

//<2016/12/15-QCWXM9X00021-P02-wenxue.sheng, <[QCFG] Segment 2==> Copy AT+QCFG="disrplmn" from EC20.>
nv_rplmn_ctrl_type nv_rplmn_ctrl;
//>2016/12/15-QCWXM9X00021-P02-wenxue.sheng

//<2016/10/21-QCM9XAW00002-P01-Andvin.Wang, <[QSAR] Segment 1==>Add QSAR common function definition.>
nv_rf_sar_type nv_rf_sar;
//>2016/10/21-QCM9XAW00002-P01-Andvin.Wang

nv_qmi_sync_type  nv_qmi_sync;

//Kenneth 2017/01/24
nv_ip_route_type nv_ip_route_value;

//<2017/03/20-QCWXM9X00032-P01-wenxue.sheng, <[ATC] Segment 1==>Sync QCOPS CMD from 9215.>
nv_qcops_flag_type nv_qcops_flag;
//>2017/03/20-QCWXM9X00032-P01-wenxue.sheng

nv_qcfg_fast_dormancy_type nv_qcfg_fast_dormancy;

nv_quec_rep_oos_type nv_quec_rep_oos;

#endif //end QUEC_OEM_NV_ITEM_TYPE_VAR



/*4.add the new struct to nvim_item_info_table*/
/************************************************************************************/
#ifdef QUECTEL_OEM_NV_INFO_TABLE
,{sizeof(nv_egmr_params_type), 0, 1}/* 2015.11.10 add by Chris  for AT+EGMR NV Item */
,{sizeof(nv_quectel_urc_port_type),0,1}/* 2015.11.10 add by chris  for AT+QURCCFG NV struct*/
,{sizeof(nv_qindcfg_urc_ind_flag_type), 0, 1}/*2015.11.10 added by chris for AT+QINDCGF*/
,{sizeof(nv_qcfg_ri_info_type), 0, 1}/*2015.11.12 added by chris for RI*/
,{sizeof(nv_quectel_eccnum_with_sim_type), 0, 1}
,{sizeof(nv_quectel_eccnum_with_no_sim_type), 0, 1}
,{sizeof(nv_qctzr_flag_type), 0, 1}
,{sizeof(nv_quectel_hotswap_type), 0, 1}//2015,09,10  add by francis
,{sizeof(nv_qcfg_airplane_control_type), 0, 1}  //francis.tang
,{sizeof(nv_qgdcnt_param_type), 0, 1}/*2015.11.21 add by chris for QGDCNT  */
,{sizeof(nv_ecall_params_type), 0, 1}//2015,03,14 rex
,{sizeof(nv_cpms_params_type), 0, 1}//2015,01,26 rex
,{sizeof(nv_stk_cfg_type), 0, 1}//2015.09.01 add by chris
,{sizeof(nv_tds_csq_type), 0, 1}//2015.12.04 add by chris
,{sizeof(nv_remote_ip_type),0,1}/*2015.12.07 added by chris for at+qcfg="remote/ip"*/
,{sizeof(nv_status_led_mode), 0, 1}
,{sizeof(nv_status_customer_mode), 0, 1}//2015,10,29 francis
,{sizeof(nv_cust_bonson_qati_str_type), 0, 1}//2015/11/14 wythe
,{sizeof(nv_cust_bonson_qgmi_str_type), 0, 1}//2015/11/14 wythe
,{sizeof(nv_cust_bonson_qgmm_str_type), 0, 1}//2015/11/14 wythe
,{sizeof(nv_cust_bonson_qgmr_str_type), 0, 1}//2015/11/14 wythe
,{sizeof(nv_no_trailer_type), 0, 1}
,{sizeof(nv_creg_params_type), 0, 1}
,{sizeof(nv_gps_xtra_time_auto_sync_type), 0, 1}//jacky add 2015.12.21  for GPS
,{sizeof(nv_qgps_nmeasrc_enable_type), 0, 1}//jacky add 2015.12.21  for GPS
,{sizeof(nv_cgps_on_demand_ready_type), 0, 1}//jacky add 2015.12.21  for GPS
,{sizeof(nv_qgps_cfg_outport_type), 0, 1}//jacky add 2015.12.21  for GPS
,{sizeof(nv_gps_auto_type), 0, 1}//jacky add 2015.12.21  for GPS
,{sizeof(nv_quec_usb_id_type), 0, 1}//maxcodeflag20160326
,{sizeof(nv_volte_operator_type), 0, 1} //sundy add 2016.3.28  for VOLTE

//maxcodeflag20160405
,{sizeof(nv_quec_bt_mac_type), 0, 1}
,{sizeof(nv_quec_wifi_mac_type), 0, 1}
,{sizeof(nv_cmux_urcport_type), 0, 1}//2015,05,26  add by francis
//Jun.Wu.20160526
,{sizeof(nv_quec_auto_baud_type), 0, 1}
,{sizeof(nv_quec_ppp_auth_param), 0, 1} //maxcodeflag20160525
,{sizeof(nv_sysinfo_urc_flg), 0, 1}

//<2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao, <[CTCC] Segment 1==> smsreg base code submit.>
,{sizeof(nv_ctcc_sms_reg_type), 0, 1}//2015,05,26  add by francis
//>2016/05/24-QCM9XTVG00042C001-P01-Vicent.Gao
,{sizeof(nv_ctcc_ds_reg_type), 0, 1}
,{sizeof(nv_qpdpgdcnt_param_type), 0, 1}/*2016.06.21 add by chao.zhou for QPDPGDCNT  */
,{sizeof(nv_quec_ipv6_mtu_type), 0, 1}//QUECTEL_CERTIFICATION_TELSTRA_MTU
,{sizeof(nv_quec_stop_pdp_retry_type), 0, 1}//maxcodeflag20160706

,{sizeof(uint8), 0, 1} //will.shao, for usb early enable

,{sizeof(nv_quec_auth_set_flag_type), 0, 1}//maxcodeflag20160711
,{sizeof(nv_quec_gw_cfgt_cfgp_type), 0, 1}//maxcodeflag20160711

,{sizeof(pwr_save_pwrup_time_type), 0, 1} //will.shao, for oos timer

,{sizeof(nv_max_tx_power_type), 0, 1}// 2016-04-25 add by scott.hu

,{sizeof(nv_quec_cdma_ruim_ctrl_type), 0, 1}//maxcodeflag20160715

,{sizeof(nv_quec_modem_restart_level_type), 0, 1}//jun20160728

,{sizeof(nv_ppp_term_frame_type), 0, 1}//add by chao.zhou 2016/07/22 for porting ppp/termframe cmd
,{sizeof(nv_network_optimize_acq_type), 0, 1}//add by chao.zhou 2016/07/22 for porting nwoptmz/acq cmd
//2016/07/29-QCM9X07FZ00002-P01-Frederic.Zhang
//add AT+QCFG="IMS" command 
,{sizeof(nv_quec_ims_ctrl_type), 0, 1}//maxcodeflag20160715
//2016/07/29-QCM9XFZ00002-P01-Frederic.Zhang

,{sizeof(nv_quec_w2lfst_info_type), 0, 1}//QUEC_3GPP_RRC_MANUAL_RELEASE
//2016/08/19 porting by chao.zhou for landi
,{sizeof(nv_qoem_landipin_type), 0, 1}

//<2016/09/05-QCM9XTVG00052-P01-Vicent.Gao,<[SIM] Segment 1==>Copy QCFG-sim/recovery from UC20.>
,{sizeof(nv_qcfg_sim_recovery_type), 0, 1}
//>2016/09/05-QCM9XTVG00052-P01-Vicent.Gao

// Ramos20160911 add for QDSIM  commond 
,{sizeof(nv_dual_sim_switch_type),0,1}

,{sizeof(nv_qcfg_usb_funtions_mask_type), 0, 1}

//<2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao,<[CDMASMS] Add cmd: qcfg-cdmasms/cmtformat.>
,{sizeof(nv_qcfg_cdmasms_cmtformat_type), 0, 1}
//>2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao
,{sizeof(nv_qap_ready_info_type), 0, 1}//2014,11,3 francis
//lory 2016/09/20 for at+qcfg=sleepind  and wakeupin
,{sizeof(nv_qcfg_sleepind_type), 0, 1}  
,{sizeof(nv_qcfg_wakeupin_type), 0, 1}  
//lory add end


//<2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng, <[QCOTA] Segment 1==> Copy AT+QCOTA from UC20.>
,{sizeof(nv_qcota_type), 0, 1}
//>2016/10/15-QCWXM9X00018C002-P01-wenxue.sheng

//<2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng, <[SKT] Segment 1==> Copy S30_REQUIREMENT function code from UC20.> 
,{sizeof(nv_s30_register_type), 0, 1}
//>2016/10/24-QCWXM9X00018C007-P01-wenxue.sheng

,{sizeof(unsigned int), 0, 1}  //NV_QCFG_QDSP6_FREQ_I

//<2016/12/15-QCWXM9X00021-P02-wenxue.sheng, <[QCFG] Segment 2==> Copy AT+QCFG="disrplmn" from EC20.>
,{sizeof(nv_rplmn_ctrl_type), 0, 1}
//>2016/12/15-QCWXM9X00021-P02-wenxue.sheng

//<2016/10/21-QCM9XAW00002-P01-Andvin.Wang, <[QSAR] Segment 1==>Add QSAR common function definition.>
,{sizeof(nv_rf_sar_type), 0, 1}
//>2016/10/21-QCM9XAW00002-P01-Andvin.Wang

,{sizeof(unsigned int), 0, 1}  //NV_QCFG_THERMAL_LIMIT_RATES_I

,{sizeof(nv_thermal_tx_limit_type), 0, 1} //NV_QCFG_THERMAL_TX_LIMIT_I
,{sizeof(nv_qmi_sync_type), 0, 1} //nv_qmi_sync_type


//Kenneth 2017/01/24
,{sizeof(nv_ip_route_type), 0, 1}

//<2017/03/20-QCWXM9X00032-P01-wenxue.sheng, <[ATC] Segment 1==>Sync QCOPS CMD from 9215.>
,{sizeof(nv_qcops_flag_type), 0, 1}
//>2017/03/20-QCWXM9X00032-P01-wenxue.sheng

,{sizeof(nv_qcfg_fast_dormancy_type),0,1}

,{sizeof(nv_quec_rep_oos_type),0,1}
#endif //end QUECTEL_OEM_NV_INFO_TABLE

