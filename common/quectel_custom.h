#ifndef QUECTEL_CUSTOM_H
#define QUECTEL_CUSTOM_H

//Kenneth 2017/01/24
#ifdef QUECTEL_IP_ROUTE_CONTROL
/*******************************list start****************************************************/
/* 2016/4/7 added by tommy for list (from Linux Kernel (include/linux/list.h)) */
struct list_head {
	struct list_head *next, *prev;
};

#define LIST_HEAD_INIT(name) { &(name), &(name) }

#define LIST_HEAD(name) \
	struct list_head name = LIST_HEAD_INIT(name)

/*
 * Insert a new entry between two known consecutive entries.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static inline void __list_add(struct list_head *new,
		struct list_head *prev,
		struct list_head *next)
{
	next->prev = new;
	new->next = next;
	new->prev = prev;
	prev->next = new;
}

/**
 * list_add - add a new entry
 * @new: new entry to be added
 * @head: list head to add it after
 *
 * Insert a new entry after the specified head.
 * This is good for implementing stacks.
 */
static inline void list_add(struct list_head *new, struct list_head *head)
{
	__list_add(new, head, head->next);
}

/**
 * list_add_tail - add a new entry
 * @new: new entry to be added
 * @head: list head to add it before
 *
 * Insert a new entry before the specified head.
 * This is useful for implementing queues.
 */
static inline void list_add_tail(struct list_head *new, struct list_head *head)
{
	__list_add(new, head->prev, head);
}

/*
 * Delete a list entry by making the prev/next entries
 * point to each other.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static inline void __list_del(struct list_head *prev, struct list_head *next)
{
	next->prev = prev;
	prev->next = next;
}

/**
 * list_del - deletes entry from list.
 * @entry: the element to delete from the list.
 * Note: list_empty on entry does not return true after this, the entry is in an undefined state.
 */
static inline void list_del(struct list_head *entry)
{
	__list_del(entry->prev, entry->next);
	entry->next = (void *) 0;
	entry->prev = (void *) 0;
}


/**
 * list_move - delete from one list and add as another head
 * @list: the entry to move
 * @head: the head that will precede our entry
 */
static inline void list_move(struct list_head *list, struct list_head *head)
{
	__list_del(list->prev, list->next);
	list_add(list, head);
}

/**
 * list_move_tail - delete from one list and add as another tail
 * @list: the entry to move
 * @head: the head that will follow our entry
 */
static inline void list_move_tail(struct list_head *list,
		struct list_head *head)
{
	__list_del(list->prev, list->next);
	list_add_tail(list, head);
}

/**
 * list_empty - tests whether a list is empty
 * @head: the list to test.
 */
static inline int list_empty(struct list_head *head)
{
	return head->next == head;
}

static inline void __list_splice(struct list_head *list,
		struct list_head *head)
{
	struct list_head *first = list->next;
	struct list_head *last = list->prev;
	struct list_head *at = head->next;

	first->prev = head;
	head->next = first;

	last->next = at;
	at->prev = last;
}

/**
 * list_splice - join two lists
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 */
static inline void list_splice(struct list_head *list, struct list_head *head)
{
	if (!list_empty(list))
		__list_splice(list, head);
}


/**
 * list_entry - get the struct for this entry
 * @ptr:    the &struct list_head pointer.
 * @type:    the type of the struct this is embedded in.
 * @member:    the name of the list_struct within the struct.
 */
#define list_entry(ptr, type, member) \
	((type *)((char *)(ptr)-(unsigned long)(&((type *)0)->member)))

/**
 * list_for_each    -    iterate over a list
 * @pos:    the &struct list_head to use as a loop counter.
 * @head:    the head for your list.
 */
#define list_for_each(pos, head) \
	for (pos = (head)->next; pos != (head); \
			pos = pos->next)
/**
 * list_for_each_prev    -    iterate over a list backwards
 * @pos:    the &struct list_head to use as a loop counter.
 * @head:    the head for your list.
 */
#define list_for_each_prev(pos, head) \
	for (pos = (head)->prev; pos != (head); \
			pos = pos->prev)

/**
 * list_for_each_safe    -    iterate over a list safe against removal of list entry
 * @pos:    the &struct list_head to use as a loop counter.
 * @n:        another &struct list_head to use as temporary storage
 * @head:    the head for your list.
 */
#define list_for_each_safe(pos, n, head) \
	for (pos = (head)->next, n = pos->next; pos != (head); \
			pos = n, n = pos->next)

/**
 * list_for_each_entry    -    iterate over list of given type
 * @pos:    the type * to use as a loop counter.
 * @head:    the head for your list.
 * @member:    the name of the list_struct within the struct.
 */
#define list_for_each_entry(pos, head, member)                \
	for (pos = list_entry((head)->next, typeof(*pos), member);    \
			&pos->member != (head);                     \
			pos = list_entry(pos->member.next, typeof(*pos), member))

/**
 * list_for_each_entry_safe - iterate over list of given type safe against removal of list entry
 * @pos:    the type * to use as a loop counter.
 * @n:        another type * to use as temporary storage
 * @head:    the head for your list.
 * @member:    the name of the list_struct within the struct.
 */
#define list_for_each_entry_safe(pos, n, head, member)            \
	for (pos = list_entry((head)->next, typeof(*pos), member),    \
			n = list_entry(pos->member.next, typeof(*pos), member);    \
			&pos->member != (head);                     \
			pos = n, n = list_entry(n->member.next, typeof(*n), member))
/*******************************list end****************************************************/
#endif
//End of Kenneth

#if defined(QUECTEL_AT_COMMON_SUPPORT)
/* 2015/4/21 added by tommy for msg */
#define QUECTEL_MSG_IND                     0X106  
typedef enum {
    QUECTEL_FATAL_ERR_DLOAD_MSG = 1,
    QUECTEL_FATAL_ERR_RESET_MSG,
    QUECTEL_FLOWCTRL_DISABLE_MSG,
    QUECTEL_FLOWCTRL_ENABLE_MSG,
    QUECTEL_MODEM_AT_DATAMODE_MSG,
    QUECTEL_MODEM_MODEM_DATAMODE_MSG,
#ifdef QUECTEL_DETECT_SPI_PLUS
    QUECTEL_MODEM_SPI_DATAMODE_MSG,
#endif
    QUECTEL_MODEM_CMDMODE_MSG,
    QUECTEL_GPS_NONE_PORT_MSG,
    QUECTEL_GPS_USBNMEA_PORT_MSG,
    QUECTEL_GPS_UARTDEBUG_PORT_MSG,
    QUECTEL_DTR_LOW_MSG,
    QUECTEL_MODEM_USB_ID_SET_MSG, //maxcodeflag20160326
    QUECTEL_MODEM_GET_GPIO_STATUS_MSG,	//2016-03-25, add by jun.wu, for GPIO config
    QUECTEL_RESTART_SYSTEM,   //20160505 , add by ivan for restart the system

    /*maxcodeflag2016061-begin*/
    QUECTEL_MODEM_SMEM_INIT_CMP, 
    QUECTEL_MODEM_SMEM_TEST,
    /*maxcodeflag2016061-end*/

    QUECTEL_MODEM_SET_USB_EARLY_ENABLE_MSG, //will.shao, for usb early enable
    
    QUECTEL_SET_GPIO_LOW_MSG,	//2016-03-25, add by jun.wu, for GPIO config
    QUECTEL_SET_GPIO_HIGH_MSG,	//2016-03-25, add by jun.wu, for GPIO config
    QUECTEL_SET_GPIO_HIGH_200_LOW_1800_MSG,	//2016-03-25, add by jun.wu, for GPIO config
    QUECTEL_SET_GPIO_HIGH_1800_LOW_200_MSG,	//2016-03-25, add by jun.wu, for GPIO config
    QUECTEL_SET_GPIO_HIGH_125_LOW_125_MSG,	//2016-03-25, add by jun.wu, for GPIO config
    
    QUECTEL_MAX_MSG
}QUECTEL_MSG_E;

extern void quectel_send_msg(uint16 msg_ind,QUECTEL_MSG_E val);
#ifdef QUECTEL_FLOWCTRL
extern void quectel_send_flowctl(boolean flag);
#endif

#endif
#endif //QUECTEL_CUSTOM_H
