
#ifndef __QUECTEL_VOLTE_H__
#define __QUECTEL_VOLTE_H__

#include "dsat_v.h"
#include "nv_items.h"


typedef enum{
	Default_operator = 0,
	TMobile		= 1,
	Vzw			= 2,
	ATT			= 3,
	CMCC		= 4,
	KDDI		= 5,
	DoCoMo		= 6,
	DT			= 7,
	TEF			= 8,
	RIL			= 9,
	IR92		= 10,
	VOLTE_OPERATOR_MAX
}QUECTEL_VOLTE_OPERATOR_TYPE;

typedef enum{
	QUEC_VOLTE_OK = 0,
	QUEC_VOLTE_ERROR_WITH_NO_CHANGE,
	QUEC_VOLTE_ERROR_CHANGE_FAIL,
	
}QUECTEL_VOLTE_RESULT_CODE;

typedef struct volte_nv_item_type
{
	char *name;
	char *value_str;
//	char *def_str;
}VOLTE_NV_ITEM_TYPE;

extern dsat_result_enum_type quectel_volte_operator_config(dsat_num_item_type operator);



#endif //end __QUECTEL_VOLTE_H__

