#ifndef  QUECTEL_PING_APP_CB_H
#define   QUECTEL_PING_APP_CB_H

#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"
#if defined(QUECTEL_PING_APP_SUPPORT)
typedef  struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            result;
	Q_INT32                            pdp_cid;
	apptcpip_ip_addr_struct      ping_dst_addr;
	Q_INT16                            ping_size;
	Q_UINT16                          ping_time;
	Q_UINT16                          current_ttl;
}quectel_ping_echo_reply_ind_params;

typedef  struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            result;
	Q_INT32                            pdp_cid;
	Q_UINT32                          maxRspMS;
	Q_UINT32                          minRspMS;
	Q_UINT32                          avgRspMS;
	Q_UINT32                          num_pkts_sent;
	Q_UINT32                          num_pkts_recvd;
	Q_UINT32                          num_pkts_lost;
}quectel_ping_finish_ind_params;

void quectel_ping_echo_reply_ind(quectel_ping_echo_reply_ind_params *ind_params);

void quectel_ping_finish_ind(quectel_ping_finish_ind_params  *ind_params);

typedef struct
{
 void (*quectel_ping_echo_reply_ind)(quectel_ping_echo_reply_ind_params *ind_params);

 void (*quectel_ping_finish_ind)(quectel_ping_finish_ind_params  *ind_params);
}quectel_ping_cb_tbl;


#endif
#endif/*QUECTEL_PING_APP_CB_H*/
