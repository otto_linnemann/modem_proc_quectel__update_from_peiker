#ifndef QUECTEL_APPTCPIP_TASK_H
#define  QUECTEL_APPTCPIP_TASK_H

#include "QuecOEM_feature.h"
#include "task.h"
#include "rex.h"
#include "nv.h"

#if defined(QUECTEL_APPTCPIP_SUPPORT)

typedef enum
{
	APPTCPIP_RPT_TIMER_SIG  =  0x00000001,
	APPTCPIP_CMD_Q_SIG         =  0x00000002,
	APPTCPIP_SOC_CLOSE_COMPLETE_SIG = 0x00000004,
	APPTCPIP_NV_CMD_SIG              = 0x00000008,

	// add by herry.Geng on 20190408 for supporting lwm2m
	APPTCPIP_LWM2M_BASE_TIMER_SIG = 0x00000010,
	APPTCPIP_LWM2M_DISABLE_TIMER_SIG = 0x00000020,
}apptcpip_signal_enum;

typedef enum
{
	APPTCPIP_MIN_CMD = 0,
	//common
	APPTCPIP_DSS_NET_CB_CMD,
	APPTCPIP_DSS_SOC_CB_CMD,
	APPTCPIP_DSS_DNS_CB_CMD,
	APPTCPIP_TIMER_EXPIRE_CB_CMD,
	//NTP ATC
	APPTCPIP_QNTP_SET_CMD,
	APPTCPIP_QNTP_READ_CMD,
	//PING ATC
	APPTCPIP_QPING_CMD,
	//PDP ATC
	APPTCPIP_QIACT_CMD,
	APPTCPIP_QIDEACT_CMD,
	APPTCPIP_QIDNSGIP_CMD,
	//Socket ATC
	APPTCPIP_OPEN_SOCKET_CMD,
	APPTCPIP_WRITE_SOCKET_CMD,
	APPTCPIP_READ_SOCKET_CMD,
	APPTCPIP_CLOSE_SOCKET_CMD,
   APPTCPIP_READ_SOCKET_INFO_CMD,
   APPTCPIP_CFG_SOCKET_ACCESS_MODE_CMD,
   APPTCPIP_SCOKET_RX_WM_LOW_IND,
   APPTCPIP_SOCKET_STATE_ASYNC_READ,
#if defined(QUECTEL_HTTP_APP_SUPPORT)
   APPTCPIP_HTTP_URL_CMD,
   APPTCPIP_HTTP_GET_CMD,
   APPTCPIP_HTTP_POST_CMD,
   APPTCPIP_HTTP_READ_CMD,
   APPTCPIP_HTTP_CANCEL_CMD,
   APPTCPIP_HTTP_SOC_WRITE_CMD,
   APPTCPIP_HTTP_SOC_READ_CMD,
#ifdef FEATURE_QUECTEL_LOCATOR_APP
    APPTCPIP_HTTP_CELLLOC_CMD,
    APPTCPIP_HTTP_CELLLOC_TIMER_EX_CMD,
#endif
#endif
#ifdef  QUECTEL_FTP_APP_SUPPORT
        //TODO:
        APPTCPIP_FTP_OPEN_CMD,
        APPTCPIP_FTP_CLOSE_CMD,
        APPTCPIP_FTP_GET_CMD,
        APPTCPIP_FTP_PUT_CMD,
        APPTCPIP_FTP_DATA_ONLINE_TRANSFER_CMD,
        APPTCPIP_FTP_RENAME_CMD,
        APPTCPIP_FTP_MKDIR_CMD,
        APPTCPIP_FTP_RMDIR_CMD,
        APPTCPIP_FTP_DEL_FILE_CMD,
        APPTCPIP_FTP_LIST_CMD,
        APPTCPIP_FTP_NLST_CMD,
        APPTCPIP_FTP_SIZE_CMD,
        APPTCPIP_FTP_CWD_CMD,
        APPTCPIP_FTP_PWD_CMD,
        APPTCPIP_FTP_KEEP_ALIVE_TIMER_EXPIRED_CMD,
        APPTCPIP_FTP_INPUT_INTERNAL_TIMER_EXPIRED_CMD,
        APPTCPIP_FTP_OPEN_TIMER_EXPIRED_CMD,
        APPTCPIP_GWFTP_CLOSE_TIMER_EXPIRED_CMD,
        APPTCPIP_FTP_SOC_SEND_TIMER_EXPIRED_CMD,
        APPTCPIP_FTP_CHECK_ACK_TIMER_EXPIRED_CMD,
        APPTCPIP_FTP_RE_GET_DATA_CMD,//13.11.20,lower cb,input cmd
        APPTCPIP_FTP_PUT_DATA_CMD,
        APPTCPIP_FTP_CLOSE_DATA_CON_CMD,
        APPTCPIP_FTP_DATA_CONNECT_CMD,//38
        APPTCPIP_FTP_READ_FILE_CMD,
        APPTCPIP_FTP_MLSD_CMD,//JONATHAN,14/7/18
        APPTCPIP_FTP_MDTM_CMD,
        APPTCPIP_FTP_ENTER_DATA_MODE_IND_CMD,
        APPTCPIP_FTP_EXIT_DATA_MODE_IND_CMD,
        APPTCPIP_FTP_CANCEL_DATA_TRANSFER_CMD,
 #endif
 #ifdef QUECTEL_SMTP_SUPPORT
       APPTCPIP_SMTP_READ_RCPT_INFO,
       APPTCPIP_SMTP_READ_ATT_INFO,
       APPTCPIP_SMTP_PUT_CMD,
       APPTCPIP_SMTP_BODY_CMD,
       APPTCPIP_SMTP_PUT_TIMER_EXPIRED_CMD,
       APPTCPIP_SMTP_BODY_TIMER_EXPIRED_CMD,
       APPTCPIP_SMTP_SEND_DATA_CMD,
       APPTCPIP_SMTP_ENCODE_DATA_CMD,
 #endif
 #ifdef QUECTEL_MMS_SUPPORT
      APPTCPIP_MMS_READ_TO_ADDR_INFO,
      APPTCPIP_MMS_READ_CC_ADDR_INFO,
      APPTCPIP_MMS_READ_BCC_ADDR_INFO,
      APPTCPIP_MMS_READ_TITLE_INFO,
      APPTCPIP_MMS_READ_BODY_INFO,
      APPTCPIP_MMS_SEND_CMD,
      APPTCPIP_MMS_SEND_TIMER_EXPIRED_CMD,
 #endif
 #if defined(QUECTEL_IOT_RPT_SUPPORT)
     APPTCPIP_IOT_RPT_SEND_CMD,
     APPTCPIP_IOT_RPT_CTX_DEINIT_CMD,
 #endif

// add by herry.Geng on 20190408 for supporting lwm2m
 #if defined(QUECTEL_LWM2M_SUPPORT)
	APPTCPIP_LWM2M_CLIENT_REGISTER,
	APPTCPIP_LWM2M_CLIENT_UPDATE,
	APPTCPIP_LWM2M_CLIENT_DEREGISTER,
	APPTCPIP_LWM2M_CLIENT_OBJ_UPDATE,
#endif

	APPTCPIP_MAX_CMD
}apptcpip_cmd_enum_type;

typedef  struct
{
  q_link_type       link;
  apptcpip_cmd_enum_type  cmd;                    
  void* user_data_ptr;
}apptcpip_cmd_type;


#define APPTCPIP_MAX_CMD_BUF         200      

 typedef void (*apptcpip_cmd_handler_type)
(
  apptcpip_cmd_enum_type cmd,
  void *user_data_ptr
);

// add by herry.Geng on 20190408 for supporting lwm2m
#if defined(QUECTEL_LWM2M_SUPPORT)
#define   APPTCPIP_SIGS    APPTCPIP_RPT_TIMER_SIG | APPTCPIP_CMD_Q_SIG|APPTCPIP_LWM2M_BASE_TIMER_SIG
#else
#define   APPTCPIP_SIGS    APPTCPIP_RPT_TIMER_SIG | APPTCPIP_CMD_Q_SIG
#endif


void  apptcpip_task
( 
uint32 dummy 
);

void  apptcpip_send_cmd
(
apptcpip_cmd_enum_type cmd,
void *user_data_ptr
);

rex_sigs_type  quectel_apptcpip_wait
(
   rex_sigs_type  requested_signal_mask       /* Signal mask to suspend on */
);

apptcpip_cmd_handler_type  apptcpip_set_cmd_handler
(
  apptcpip_cmd_enum_type cmd,            /* Command assocaited with handler  */
  apptcpip_cmd_handler_type cmd_handler /* Command handler function pointer */
);

nv_stat_enum_type apptcpip_set_nv_item  //chaozhoucodeflag20160708
(
  nv_items_enum_type  item,           /* Which item */
  nv_item_type       *data_ptr        /* Pointer to space for item */
);

// add by herry.Geng on 20190408 for supporting lwm2m
nv_stat_enum_type apptcpip_get_nv_item  //chaozhoucodeflag20160708
(
  nv_items_enum_type  item,           /* Which item */
  nv_item_type       *data_ptr        /* Pointer to space for item */
);

#endif/*QUECTEL_APPTCPIP_SUPPORT*/

#endif/*QUECTEL_APPTCPIP_TASK_H*/
