#ifndef  QUECTEL_APPTCPIP_TIMER_H
#define   QUECTEL_APPTCPIP_TIMER_H
#include "comdef.h"
#include "customer.h"
#include "quectel_apptcpip_util.h"


#if defined(QUECTEL_APPTCPIP_SUPPORT)


typedef  Q_INT32  apptcpip_timer_id;

typedef void (*apptcpip_timer_cb_type)(apptcpip_timer_id  timer_id, void *user_data_ptr);

void  apptcpip_timer_init
(
void 
);

apptcpip_timer_id  apptcpip_timer_create
(
apptcpip_timer_cb_type  cb_func,
void *user_data_ptr
);

Q_BOOL  apptcpip_timer_start
(
apptcpip_timer_id  timer_id,
Q_UINT32   duration
);

Q_BOOL  apptcpip_timer_stop
(
apptcpip_timer_id  timer_id
);

void  apptcpip_timer_delete
(
apptcpip_timer_id  timer_id
);

Q_BOOL apptcpip_timer_is_running
(
apptcpip_timer_id  timer_id
);


#endif/*QUECTEL_APPTCPIP_SUPPORT*/

#endif/*QUECTEL_APPTCPIP_TIMER_H*/
