#ifndef QUECTEL_APPTCPIP_SOCMGR_H
#define  QUECTEL_APPTCPIP_SOCMGR_H


#include "dserrno.h"
#include "dssocket.h"
#include "dssocket_defs.h"
#include "dss_netpolicy.h"
#include "ps_iface_defs.h"
#include "dss_iface_ioctl.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"

#if defined(QUECTEL_APPTCPIP_SUPPORT)
typedef enum
{
    APPTCPIP_SOCIFACE_IDLE = 0,
    APPTCPIP_SOCIFACE_INIT = 1,
    APPTCPIP_SOCIFACE_CONNECTING = 2,
    APPTCPIP_SOCIFACE_CONNECTED = 3,
    APPTCPIP_SOCIFACE_LISTEN = 4,
    APPTCPIP_SOCIFACE_CLOSING = 5,
    APPTCPIP_SOCIFACE_CLOSED = APPTCPIP_SOCIFACE_IDLE
}apptcpip_sociface_state;


 typedef enum
 {
    APPTCPIP_AF_INET = 0,
    APPTCPIP_PF_INET,
    APPTCPIP_AF_INET6,
    APPTCPIP_PF_INET6
 }apptcpip_protocol_family_type;

 typedef  enum
 {
     APPTCPIP_TCP_SOCKET = 0,
     APPTCPIP_UDP_SOCKET,
     APPTCPIP_RAW_SOCKET
 }apptcpip_socket_type;


typedef enum
{
    APPTCPIP_IPPROTO_IP = DSS_IPPROTO_IP,
    APPTCPIP_SOL_SOCKET = DSS_SOL_SOCKET,
    APPTCPIP_IPPROTO_TCP = DSS_IPPROTO_TCP,
    APPTCPIP_IPPROTO_ICMP =DSS_IPPROTO_ICMP
}apptcpip_socket_opt_level;


typedef enum
{
    APPTCPIP_SOC_RECV_BUF = DSS_SO_RCVBUF,
    APPTCPIP_SOC_SEND_BUF = DSS_SO_SNDBUF,
    APPTCPIP_SOC_ACK_CB = DSS_SO_SDB_ACK_CB,
    APPTCPIP_SOC_NODELAY = DSS_TCP_NODELAY,
    APPTCPIP_SOC_KEEPALVIE = DSS_SO_KEEPALIVE,
    APPTCPIP_SOC_SACK = DSS_TCP_SACK,
    APPTCPIP_SOC_NREAD = DSS_SO_FIONREAD,
    APPTCPIP_SOC_DELAY_ACK = DSS_TCP_DELAYED_ACK,
    APPTCPIP_SOC_MSS_VAL = DSS_TCP_MAXSEG,
    APPTCPIP_SOC_LINGER = DSS_SO_LINGER,
    APPTCPIP_ICMP_ECHO_ID = DSS_ICMP_ECHO_ID,
    APPTCPIP_IP_TTL = DSS_IP_TTL
  #if defined(QUECTEL_TCP_ACK) 
    ,APPTCPIP_TCP_ACKED_NUM = DSS_TCP_ACKED_NUM
   #endif
}apptcpip_socket_opt_name;


typedef enum
{
  APPTCPIP_ICMP_V4_ECHO_REQ_MSG_TYPE = 8,
  APPTCPIP_ICMP_V4_ECHO_RSP_MSG_TYPE = 0,
  APPTCPIP_ICMP_V6_ECHO_REQ_MSG_TYPE = 128,
  APPTCPIP_ICMP_V6_ECHO_RSP_MSG_TYPE = 0
} apptcpip_icmp_msg_type_val;

typedef enum
{
  APPTCPIP_ICMP_V4_ECHO_REQ_MSG_CODE = 0,
  APPTCPIP_ICMP_V6_ECHO_REQ_MSG_CODE = 0
}apptcpip_icmp_msg_code_val;


#define   APPTCPIP_SOC_READ_EVENT                                    0x0001
#define   APPTCPIP_SOC_WRITE_EVENT                                   0x0002
#define   APPTCPIP_SOC_ACCEPT_EVENT                                 0x0004
#define   APPTCPIP_SOC_CONNECT_EVENT                               0x0008
#define   APPTCPIP_SOC_CLOSE_EVENT                                    0x0010
#define   APPTCPIP_SOC_NET_DOWN_EVENT                            0x0020
#if defined(QUECTEL_CYASSL_SUPPORT)
#define   APPTCPIP_SOC_SSL_HANDSHARK_TIMEOUE_EVENT  0x0040
#endif

typedef dss_so_linger_type   apptcpip_so_linger_type;

typedef void  (*apptcpip_soc_cb_fcn)
(
Q_INT32     pdp_cid,
Q_INT16     sockfd,
Q_UINT32   event_mask,
Q_INT16     err_no
);

typedef struct
{
    q_link_type                             link;
    Q_INT16                                 sockfd;
	 apptcpip_timer_id                   tcp_syn_timer_id;
    Q_INT32                                 pdp_cid;
    apptcpip_sociface_state            if_state;
    apptcpip_protocol_family_type  family_type;
    apptcpip_socket_type               soc_type;
    Q_UINT32                               active_event_set;
    apptcpip_soc_cb_fcn                soc_event_cb;
    Q_UINT32                               total_ack_no;
    Q_UINT16                               local_port;
	Q_UINT16                                peer_port;
	apptcpip_ip_addr_struct           peer_addr;
#if defined(QUECTEL_CYASSL_SUPPORT)
	apptcpip_timer_id                    ssl_handshark_timer_id;
#endif
}apptcpip_sociface_type;


void apptcpip_socmgr_init
(
void
);

Q_INT32   apptcpip_soc_create
(
apptcpip_protocol_family_type  family_type,
apptcpip_socket_type  soc_type,
Q_INT32 protocol_type,
Q_INT32  pdp_cid
);

Q_INT32 apptcpip_soc_disable_rx
(
 Q_INT16 sockfd
);

Q_INT32 apptcpip_soc_enable_rx
(
 Q_INT16 sockfd
);

Q_INT32  apptcpip_soc_select_event
(
 Q_INT16 sockfd,
 apptcpip_soc_cb_fcn  event_cb,
 Q_INT32  event_set
);

Q_INT32  apptcpip_soc_bind
(
Q_INT16 sockfd, 
apptcpip_ip_addr_struct *local_addr, 
Q_UINT16 local_port
);

Q_INT32   apptcpip_soc_listen
(
Q_INT16 sockfd, 
Q_UINT32 backlog
);

Q_INT32   apptcpip_soc_connect
(
Q_INT16 sockfd, 
apptcpip_ip_addr_struct *remote_addr, 
Q_UINT16 remote_port
);

Q_INT32    apptcpip_soc_accept
(
Q_INT16 sockfd, 
apptcpip_ip_addr_struct *remote_addr, 
Q_UINT16 *remote_port
);

Q_INT32   apptcpip_soc_send
(
Q_INT16 sockfd, 
Q_UINT8  *send_buf, 
Q_UINT32 send_len
);

Q_INT32    apptcpip_soc_recv
(
Q_INT16 sockfd, 
Q_UINT8  *recv_buf, 
Q_UINT32 recv_len
);

Q_INT32   apptcpip_soc_send_dsm_chain
(
Q_INT16 sockfd, 
dsm_item_type **write_item
);

Q_INT32    apptcpip_soc_recv_dsm_chain
(
Q_INT16 sockfd, 
dsm_item_type **recv_item
);


Q_INT32    apptcpip_soc_sendto
(
Q_INT16 sockfd, 
apptcpip_ip_addr_struct *remote_addr, 
Q_UINT16 remote_port,
Q_UINT8  *send_buf, 
Q_UINT32 send_len
);

Q_INT32    apptcpip_soc_recvfrom
(
Q_INT32 sockfd, 
Q_UINT16    *remote_port,
apptcpip_ip_addr_struct    *remote_addr,
Q_UINT8  *recv_buf, 
Q_UINT32 recv_len
);

Q_INT32    apptcpip_soc_close
(
Q_INT16 sockfd
);

Q_INT32  apptcpip_soc_getopt
(
Q_INT16 sockfd,
Q_INT32 level,
Q_INT32 opt,
void *param,
Q_UINT32 *param_size
);

Q_INT32  apptcpip_soc_setopt
(
Q_INT16 sockfd,
Q_INT32 level,
Q_INT32 opt,
void *param,
Q_UINT32 param_size
);

Q_INT32  apptcpip_soc_get_peer_name
(
	Q_INT16 sockfd,
	Q_UINT16    *remote_port,
   apptcpip_ip_addr_struct    *remote_addr
);

Q_INT32  apptcpip_soc_get_local_name
(
	Q_INT16 sockfd,
	Q_UINT16    *local_port,
   apptcpip_ip_addr_struct    *local_addr
);

void apptcpip_soc_net_down_event_listener
(
Q_INT32 pdp_cid
);

Q_INT32  apptcpip_inet_atoi
(
Q_UINT8  *addr_buf,
Q_INT32   ip_version,
apptcpip_ip_addr_struct  *ip_addr
);

Q_INT32 apptcpip_soc_get_tcp_retransmit_param
(
Q_UINT32 *max_backoff,
Q_UINT32 *max_rto
);

Q_INT32 apptcpip_soc_set_tcp_retransmit_param
(
Q_UINT32 max_backoff,
Q_UINT32 max_rto
);

#if defined(QUECTEL_CYASSL_SUPPORT)
Q_INT32  apptcpip_soc_start_ssl_handshark_timer
(
Q_INT16 sockfd,
Q_UINT32  timeout
);

Q_INT32  apptcpip_soc_stop_ssl_handshark_timer
(
Q_INT16 sockfd
);
#endif

#endif/*QUECTEL_APPTCPIP_SUPPORT*/
#endif/*QUECTEL_APPTCPIP_SOCMGR_H*/
