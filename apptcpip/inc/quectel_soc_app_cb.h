#ifndef QUECTEL_SOC_APP_CB_H
#define  QUECTEL_SOC_APP_CB_H


#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"
#include "quectel_soc_app.h"

#if defined(QUECTEL_SOC_APP_SUPPORT)
typedef struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            result;
	Q_INT32                            socket_id;
	Q_INT32                            pdp_cid;
	Q_INT32                            service_type;
	Q_INT32                            access_mode;
	Q_INT32                            tcp_mss;
}quectel_socket_open_result_ind_param;

typedef struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            result;
	Q_INT32                            service_type;
	Q_BOOL                             cmd_result;//true: result for qiclose cmd; flase: urc
	Q_INT32                            socket_id;
}quectel_socket_close_result_ind_param;

typedef struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            result;
	Q_INT32                            socket_id;
}quectel_socket_wrtie_result_rsp_param;


typedef struct
{
	ds3g_siolib_port_e_type           cmd_port;
	Q_INT32                                  result;
	Q_INT32                                 socket_id;
	Q_INT32                                 service_type;
	Q_INT32                                 access_mode;
	Q_INT32                                 read_len;
}quectel_socket_read_ind_param;

typedef struct
{
	ds3g_siolib_port_e_type           cmd_port;
	Q_INT32                                  result;
	Q_INT32                                  server_id;
	Q_INT32                                 socket_id;
	apptcpip_ip_addr_struct           accept_ipaddr;
	Q_UINT16                               accept_port;
	Q_INT32                                 service_type;
	Q_INT32                                 access_mode;
}quectel_socket_accept_ind_param;

typedef struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            socket_id;
	Q_INT32                            result;
	Q_INT32                            info_type;
	Q_BOOL                            info_begin;
	union
	{
    	quectel_socket_receive_info_type    read_info;
    	quectel_socket_write_info_type       write_info;
    	quectel_socket_state_type              state_info;
	}info;
#define rd_info             info.read_info
#define wr_info            info.write_info
#define state_info        info.state_info

}quectel_socket_info_result_rsp_param;

typedef struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            result;
	Q_INT32                            socket_id;
	Q_INT32                            access_mode;
}quectel_socket_set_access_mode_result_rsp_param;

void quectel_socket_open_result_ind(quectel_socket_open_result_ind_param *ind_params);

void quectel_socket_close_result_ind(quectel_socket_close_result_ind_param  *ind_params);

void quectel_socket_write_result_rsp(quectel_socket_wrtie_result_rsp_param *rsp_params);

void quectel_socket_read_data_ind(quectel_socket_read_ind_param *ind_params);

void quectel_socket_accept_result_ind(quectel_socket_accept_ind_param  *ind_params);

void quectel_socket_info_result_rsp(quectel_socket_info_result_rsp_param *rsp_params);

void quectel_socket_set_access_mode_result_rsp(quectel_socket_set_access_mode_result_rsp_param *rsp_params);

void  quectel_socket_online_write_ind();

typedef struct
{
 void (*quectel_socket_open_result_ind)(quectel_socket_open_result_ind_param *ind_params);

void (*quectel_socket_close_result_ind)(quectel_socket_close_result_ind_param  *ind_params);

void (*quectel_socket_write_result_rsp)(quectel_socket_wrtie_result_rsp_param *rsp_params);

void (*quectel_socket_read_data_ind)(quectel_socket_read_ind_param *ind_params);

void (*quectel_socket_accept_result_ind)(quectel_socket_accept_ind_param  *ind_params);

void (*quectel_socket_info_result_rsp)(quectel_socket_info_result_rsp_param *rsp_params);

void (*quectel_socket_set_access_mode_result_rsp)(quectel_socket_set_access_mode_result_rsp_param *rsp_params);

void (* quectel_socket_online_write_ind)();
}quectel_sockets_cb_tbl;
#endif/*QUECTEL_SOC_APP_SUPPORT*/

#endif/*QUECTEL_SOC_APP_CB_H*/
