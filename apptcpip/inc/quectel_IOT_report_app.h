#ifndef  _QUECTEL_IOT_REPORT_APP_H
#define  _QUECTEL_IOT_REPORT_APP_H

#if defined(QUECTEL_IOT_RPT_SUPPORT)
#if 0
typedef struct
{
    byte   imei[20];
	byte    iccid[20];
	byte    imsi[20];
	byte    manufacture[20];
	byte    model[20];
	byte    time_stamp[20];
	byte    fgi[32];
	byte    enodeb_id[20];
	byte    cell_id[20];
	byte    net_mode[4];
	byte    band[2];
	byte    earfcn[5];
	byte    gps_longitude[20];
	byte    gps_latitude[20];
	byte    ping_avg_ttl[15];
	byte    rsrp[4];
	byte    sinr[4];
	byte    memo[200];
}quectel_IOT_rpt_info;

void    quectel_IOT_rpt_app_init();

void     quectel_update_IOT_rpt_data(quectel_IOT_rpt_info  *rpt_data);

void     quectel_send_IOT_rpt_cmd();
#endif

#define Q_IOT_RPT_MACID_MAX_LEN	48
#define Q_IOT_RPT_OSVER_MAX_LEN	32
#define Q_IOT_RPT_HWVER_MAX_LEN	32
#define Q_IOT_RPT_MANUFACTURE_MAX_LEN	20
#define Q_IOT_RPT_PNAME_MAX_LEN	16
#define Q_IOT_RPT_MLPLVER_MAX_LEN	8
#define Q_IOT_RPT_MSPLVER_MAX_LEN	8
#define Q_IOT_RPT_CELLID_MAX_LEN	8
#define Q_IOT_RPT_MMEID_MAX_LEN	16
#define Q_IOT_RPT_ACCESSTYPE_MAX_LEN	2
//<2016/09/19-QCM9XTCZ00003-P03-Chao.Zhou, <[CTCC] Segment 3==>Fix CTCC IOT common issue.>
//#define Q_IOT_RPT_REGDATE_MAX_LEN	16
#define Q_IOT_RPT_REGDATE_MAX_LEN	24
//<2016/09/19-QCM9XTCZ00003-P03-Chao.Zhou

#define Q_IOT_RPT_OK					0
#define Q_IOT_RPT_FAIL					-1
#define Q_IOT_RPT_NET_FAIL				-2
#define Q_IOT_RPT_INV_PARAM			-3

#define	Q_IOT_RPT_SVR_RSP_SUCCESS		0
#define	Q_IOT_RPT_SVR_RSP_DECODE_ERR	1
#define	Q_IOT_RPT_SVR_RSP_CHECK_ERR		2
#define	Q_IOT_RPT_SVR_RSP_INVALID		3

//<2016/09/08-QCM9XTCZ00003-P02-Chao.Zhou, <[CTCC] Segment 2==>Fix CTCC IOT common issue.>
#define CTCC_IOT_MEM_ALLOC_SIZE 1024
//<2016/09/08-QCM9XTCZ00003-P02-Chao.Zhou

//<2016/09/05-QCM9XTCZ00005-P01-Chao.Zhou, <[CTCC] Segment 1==>CTCC IOT code structure optimization.>
typedef enum
{
    Q_IOT_MOD_TYPE_2G = 0x1,
    Q_IOT_MOD_TYPE_3G = 0x2,
    Q_IOT_MOD_TYPE_4G_SINGLE = 0x3,
    Q_IOT_MOD_TYPE_4G_MULTI = 0x4,

    //Warning!==>Please add new Q IOT MOD TYPE upper this line
    Q_IOT_MOD_TYPE_INVALID = 0xFFFF
} Q_IOT_ModuleTypeEnum;

typedef enum
{
    Q_IOT_SVR_VER_NUM_1_0 = 1,
    Q_IOT_SVR_VER_NUM_2_0 = 2,
    Q_IOT_SVR_VER_NUM_3_0 = 3,
    Q_IOT_SVR_VER_NUM_4_0 = 4,

    //Warning!==>Please add new Q IOT VER NUM upper this line
    Q_IOT_SVR_VER_NUM_INVALID = 0xFFFF
} Q_IOT_SvrVerNumEnum;
//>2016/09/05-QCM9XTCZ00005-P01-Chao.Zhou

typedef void (*IOT_rpt_rsp_info_cb)(Q_INT32 ctx, Q_INT32 result_code);

typedef void (*IOT_rpt_ctx_deinit_cb)(Q_INT32 ctx);


Q_INT32 quectel_IOT_rpt_ctx_init
(
 Q_UINT16  pdp_cid
);

Q_INT32 quectel_IOT_rpt_add_item
(
Q_INT32  IOT_rpt_ctx,
Q_CHAR *item_name, 
byte *item_value
);

Q_INT32 quectel_IOT_rpt_ctx_data_prepare
(
Q_INT32    IOT_rpt_ctx
);

//<2016/09/02-QCM9XTCZ00005-P01-Chao.Zhou, <[CTCC] Segment 1==>CTCC IOT multi version support.>
Q_INT32 quectel_IOT_rpt_ctx_data_prepare_v1_0
(
Q_INT32    IOT_rpt_ctx
);
//<2016/09/02-QCM9XTCZ00005-P01-Chao.Zhou

//<2016/09/02-QCM9XTCZ00004-P01-Chao.Zhou, <[CTCC] Segment 1==>CTCC IOT 4.0 support.>
Q_INT32 quectel_IOT_rpt_ctx_data_prepare_v4_0
(
Q_INT32    IOT_rpt_ctx
);

Q_INT32 quectel_IOT_rpt_mode(void);
//<2016/09/02-QCM9XTCZ00004-P01-Chao.Zhou

Q_INT32 quectel_IOT_rpt_send_rpt
(
Q_INT32    IOT_rpt_ctx,
Q_CHAR    *svr_addr,
Q_UINT16   svr_port,
IOT_rpt_rsp_info_cb  rsp_cb
);

Q_INT32 quectel_IOT_rpt_ctx_deinit
(
Q_INT32    IOT_rpt_ctx,
IOT_rpt_ctx_deinit_cb  nfy_cb
);

void quectel_IOT_rpt_app_start(void);

#endif/*QUECTEL_IOT_RPT_SUPPORT*/
#endif/*QUECTEL_IOT_REPORT_APP_H*/
