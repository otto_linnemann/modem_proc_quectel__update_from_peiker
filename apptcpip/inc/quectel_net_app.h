#ifndef QUECTEL_NET_APP_H
#define  QUECTEL_NET_APP_H

#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_netmgr.h"

#if defined(QUECTEL_APPTCPIP_SUPPORT)

typedef enum
{
	DSAT_TCPIP_CLIENT = 0,
	DNSGIP_APP_CLENT,
	SOC_APP_CLIENT,
	PING_APP_CLIENT,
	NTP_APP_CLIENT,
#ifdef QUECTEL_FTP_APP_SUPPORT
	FTP_APP_CLIENT,
	#endif
#if defined(QUECTEL_HTTP_APP_SUPPORT)
	HTTP_APP_CLIENT,
#endif
    #ifdef QUECTEL_SMTP_SUPPORT
	SMTP_APP_CLIENT,
	#endif
    #ifdef QUECTEL_MMS_SUPPORT
    MMS_APP_CLIENT,
    #endif
#if defined(QUECTEL_IOT_RPT_SUPPORT)
    IOT_RPT_APP_CLIENT,
#endif

// add by herry.Geng on 20190408 for supporting lwm2m
#if defined(QUECTEL_LWM2M_SUPPORT)
		LWM2M_APP_CLIENT,
#endif


	MAX_NET_CLIENT
}quectel_net_client_id_type;

typedef  struct
{
    ds3g_siolib_port_e_type    cmd_port;
	 Q_INT32                           client_id;
	Q_INT32                           pdp_cid;
}quectel_net_active_cmd_params;

typedef  struct
{
    ds3g_siolib_port_e_type    cmd_port;
	Q_INT32                           client_id;
	Q_INT32                           pdp_cid;
}quectel_net_deactive_cmd_params;


typedef struct
{
	ds3g_siolib_port_e_type                  cmd_port;
	Q_INT32                                        pdp_cid;
	Q_CHAR                                        host_name[255];
}quectel_net_qidnsgip_cmd_params;


void  quectel_net_app_init
(
void
);

Q_INT32   quectel_net_set_pdp_profile
(
Q_INT32  pdp_id,
apptcpip_pdp_profile_param_struct   * pdp_profile
);

Q_INT32   quectel_net_get_pdp_profile
(
Q_INT32  pdp_id,
apptcpip_pdp_profile_param_struct   * pdp_profile
);

Q_INT32   quectel_net_active
(
quectel_net_active_cmd_params  * cmd_params
);

Q_INT32   quectel_net_deactive
(
quectel_net_deactive_cmd_params  * cmd_params
);

Q_INT32   quectel_net_getipaddrbyhostname
(
quectel_net_qidnsgip_cmd_params   *cmd_params
);

Q_INT32 quectel_net_get_dns_address
(
Q_INT32  pdp_id,
apptcpip_ip_addr_struct  *pri_dns,
apptcpip_ip_addr_struct  *sec_dns
);

Q_INT32 quectel_net_set_dns_address
(
Q_INT32  pdp_id,
apptcpip_ip_addr_struct  *pri_dns,
apptcpip_ip_addr_struct  *sec_dns
);

Q_INT32  quectel_net_get_local_address
(
Q_INT32  pdp_id,
apptcpip_ip_addr_struct *local_ip
);

Q_INT32  quectel_net_get_gate_address
(
Q_INT32  pdp_id,
apptcpip_ip_addr_struct *gate_ip
);

#endif/*QUECTEL_APPTCPIP_SUPPORT */
#endif/*QUECTEL_NET_APP_H*/
