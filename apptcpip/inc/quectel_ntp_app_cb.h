#ifndef  QUECTEL_NTP_APP_CB_H
#define   QUECTEL_NTP_APP_CB_H

#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"

#if defined(QUECTEL_NTP_APP_SUPPORT)

typedef  struct
{
	ds3g_siolib_port_e_type      cmd_port;
	Q_INT32                            result;
	Q_INT32                            pdp_cid;
	Q_UINT32                          sync_time;
	Q_BOOL                             auto_sync_local_time;
}quectel_ntp_sync_result_ind_params;

typedef  struct
{
	ds3g_siolib_port_e_type         cmd_port;
	Q_INT32                               result;
	Q_UINT16                             ntp_server_port; 
	apptcpip_ip_addr_struct          ntp_server_ipaddr;
	Q_UINT8                               ntp_server_hostname[255];
	apptcpip_host_address_type   ntp_server_addr_type;
}quectel_ntp_read_server_info_rsp_params;

void quectel_ntp_sync_result_ind(quectel_ntp_sync_result_ind_params *ind_param);

void quectel_ntp_read_server_info_rsp(quectel_ntp_read_server_info_rsp_params *rsp_param);

typedef struct quectel_ntp_cb_tbl
{
void (*quectel_ntp_sync_result_ind)(quectel_ntp_sync_result_ind_params *ind_param);

void (*quectel_ntp_read_server_info_rsp)(quectel_ntp_read_server_info_rsp_params *rsp_param);

}quectel_ntp_cb_tbl;



#endif/*QUECTEL_NTP_APP_SUPPORT*/
#endif/*QUECTEL_NTP_APP_CB_H*/

