#ifndef  QUECTEL_PING_APP_H
#define   QUECTEL_PING_APP_H

#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"

#if defined(QUECTEL_PING_APP_SUPPORT)
#define QUECTEL_PING_DEFAULT_PING_SIZE                             32

#define   QUECTEL_PING_MAX_RTT                                              0xFFFFFFFFU
#define   QUECTEL_PING_DEFAULT_TTL                                       255
#define   QUECTEL_PING_MAX_PING_DATA_BYTES                      256
#define   QUECTEL_ICMP_ECHO_MSG_TOTAL_HDR_SIZE              8

#define   QUECTEL_ICMP_ECHO_MSG_SEQ_HDR_SIZE                  2
#define   QUECTEL_ICMP_ECHO_MSG_ID_HDR_SIZE                     2
#define   QUECTEL_ICMP_ECHO_MSG_TOTAL_HDR_SIZE               8
#define   QUECTEL_ICMP_ECHO_MSG_ID_HDR_OFFSET                 4
#define   QUECTEL_ICMP_ECHO_MSG_SEQ_HDR_OFFSET              6
#define   QUECTEL_ICMP_ECHO_MSG_PAYLOAD_OFFSET              8

#define   QUECTEL_PING_ICMP_IDENTIFIER                                 0x200




typedef enum 
{
  QUECTEL_PING_STATE_IDLE = 0,
  QUECTEL_PING_STATE_PDP_ACTTVING,
  QUECTEL_PING_STATE_DNSGIP,
  QUECTEL_PING_STATE_SEND_REQ,
  QUECTEL_PING_STATE_WAIT_RSP,
  QUECTEL_PING_STATE_FINISH = QUECTEL_PING_STATE_IDLE,
  QUECTEL_PING_STATE_NUMS
}quectel_ping_state_e_type;


typedef struct
{
	 q_link_type                                    link;
	 ds3g_siolib_port_e_type                  cmd_port;
	 Q_INT32                                         pdp_cid;
	 Q_INT16                                         sock_fd;
    quectel_ping_state_e_type               ping_state;
	 apptcpip_host_address_type            address_type;
     Q_UINT8                                        dst_hostname[256];
    apptcpip_ip_addr_struct                   dst_ip_addr;
    
    Q_UINT32                                       ping_cnt;
    Q_UINT32                                       ping_retransmit_tm;
    Q_UINT16                                       ping_pkt_size; //ping_pkt_size: only data size(exclude icmp header)
    Q_INT32                                         ttl;

    Q_UINT32                                      ping_retransmit_timer_id;

    Q_INT32                                        icmp_echo_msg_id;
    Q_UINT16                                      icmp_echo_req_seq_num;
    Q_UINT16                                      icmp_echo_rsp_seq_num;
    Q_UINT32                                      cur_rsp_rtt;

    
    Q_UINT32                                     max_rsp_rtt;
    Q_UINT32                                     min_rsp_rtt;
    Q_UINT32                                     avg_rsp_rtt;
    Q_UINT8                                      num_pkt_send;
    Q_UINT8                                      num_pkt_recv;
    Q_UINT8                                      num_pkt_lost;
    
}quectel_ping_session;

typedef struct
{
	 ds3g_siolib_port_e_type                  cmd_port;
	 Q_INT32                                         pdp_cid;
	 apptcpip_host_address_type            address_type;
    Q_UINT8                                        dst_hostname[256];
    apptcpip_ip_addr_struct                   dst_ip_addr;
	 Q_UINT32                                      ping_retransmit_cnt;
	 Q_UINT32                                      ping_retransmit_tm;
}quectel_ping_params;

void  quectel_ping_app_init
(
void
);


Q_INT32 quectel_ping
(
quectel_ping_params  *cmd_params
);

#endif/*QUECTEL_PING_APP_SUPPORT*/
#endif/*QUECTEL_PING_APP_H*/
