#ifndef QUECTEL_NTP_APP_H
#define  QUECTEL_NTP_APP_H

#include "ds3gsiolib.h"

#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"

#if defined(QUECTEL_NTP_APP_SUPPORT)

typedef enum
{
	NTP_CLIENT_IDLE = 0,
	NTP_CLIENT_PDP_ACTIVING,
	NTP_CLIENT_DNSGIP,
	NTP_CLIENT_SENDING_REQ,
	NTP_CLIENT_WAITING_RSP,
	NTP_CLIENT_SYNC_FINISHING,
	NTP_CLIENT_SYNC_FINISHED = NTP_CLIENT_IDLE
}quectel_ntp_client_status;



typedef struct
{
	ds3g_siolib_port_e_type         cmd_port;
	Q_INT32                               pdp_cid;
	Q_INT16                               sock_fd;
	Q_UINT8                               retry_count;
	Q_UINT32                             orig_time;
	Q_UINT32                             sync_time;
	apptcpip_timer_id                  retry_timer_id;
	quectel_ntp_client_status        ntp_client_status;
	Q_BOOL                                auto_sync_local_time;
	Q_UINT16                             ntp_server_port;
	apptcpip_ip_addr_struct          ntp_server_ipaddr;
	Q_UINT8                               ntp_server_hostname[255];
	apptcpip_host_address_type   ntp_server_addr_type;
	Q_INT32                               sync_result;
}quectel_ntp_client_struct;

typedef struct
{
	ds3g_siolib_port_e_type       cmd_port;
	Q_INT32                              pdp_cid;
	Q_UINT32                            auto_sync_local_time;
	apptcpip_host_address_type  address_type;
	apptcpip_ip_addr_struct         ip_address;
	Q_UINT8                               host_name[255];
	Q_UINT32                             host_port;
}quectel_ntp_sync_param;

void  quectel_ntp_client_init
(
void
);

Q_BOOL  quectel_ntp_client_is_running
(
void
);

Q_INT32  quectel_ntp_client_sync_time
(
quectel_ntp_sync_param  *sync_params
);

Q_INT32  quectel_ntp_client_read_server_info
(
ds3g_siolib_port_e_type  cmd_port
);

#endif/*QUECTEL_NTP_APP_SUPPORT*/

#endif/*QUECTEL_NTP_APP_H*/
