#ifndef QUECTEL_NET_APP_CB_H
#define  QUECTEL_NET_APP_CB_H

#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_netmgr.h"
#include "quectel_net_app.h"

#if defined(QUECTEL_APPTCPIP_SUPPORT)
typedef  struct
{
    ds3g_siolib_port_e_type    cmd_port;
    Q_INT32  pdp_cid;
    Q_INT32  result;
}quectel_net_active_rsp_param;


typedef  struct
{
    ds3g_siolib_port_e_type    cmd_port;
    Q_INT32  pdp_cid;
    Q_INT32  result;
}quectel_net_deactive_rsp_param;


typedef  struct
{
    ds3g_siolib_port_e_type    cmd_port;
    Q_INT32  pdp_cid;
    Q_INT32  result;
}quectel_net_auto_deactive_ind_param;


typedef struct
{
   ds3g_siolib_port_e_type    cmd_port;
	Q_INT32    pdp_cid;
   Q_INT32    result;
   Q_INT32    entry_num;
   Q_UINT32  ttl_val;
   apptcpip_ip_addr_struct  entity[APPTCPIP_DNSGIP_MAX_A_ENTRY];
}quectel_net_dnsgip_result_ind_param;

void quectel_net_active_cmd_rsp(quectel_net_active_rsp_param *rsp_param);

void quectel_net_deactive_cmd_rsp(quectel_net_active_rsp_param *rsp_param);

void  quectel_net_auto_deact_ind(quectel_net_auto_deactive_ind_param *rsp_param);

void  quectel_net_dnsgip_result_ind(quectel_net_dnsgip_result_ind_param  *ind_param);


typedef struct quectel_net_cb_tbl
{
void (*quectel_net_active_cmd_rsp)(quectel_net_active_rsp_param *rsp_param);

void (*quectel_net_deactive_cmd_rsp)(quectel_net_deactive_rsp_param *rsp_param);

void (*quectel_net_auto_deact_ind)(quectel_net_auto_deactive_ind_param *rsp_param);

void (*quectel_net_dnsgip_result_ind)(quectel_net_dnsgip_result_ind_param  *ind_param);
}quectel_net_cb_tbl;

/*
typedef void (*quectel_net_active_cmd_rsp)(quectel_net_active_rsp_param *rsp_param);

typedef void (*quectel_net_deactive_cmd_rsp)(quectel_net_deactive_rsp_param *rsp_param);

typedef void (*quectel_net_auto_deact_ind)(quectel_net_auto_deactive_ind_param *rsp_param);

typedef void (*quectel_net_dnsgip_result_ind)(quectel_net_dnsgip_result_ind_param  *ind_param);

typedef struct quectel_net_cb_tbl
{
	quectel_net_active_cmd_rsp   			net_active_cmd_rsp;
   quectel_net_deactive_cmd_rsp			net_deactive_cmd_rsp;
	quectel_net_auto_deact_ind            net_auto_deactive_ind;
   quectel_net_dnsgip_result_ind         net_dnsgip_result_ind;
}quectel_net_cb_tbl;
*/
#endif/*QUECTEL_APPTCPIP_SUPPORT*/
#endif/*QUECTEL_NET_APP_CB_H*/
