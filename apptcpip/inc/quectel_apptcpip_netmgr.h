#ifndef  QUECTEL_APPTCPIP_NETMGR_H
#define   QUECTEL_APPTCPIP_NETMGR_H


#include "dserrno.h"
#include "dssdns.h"
#include "dssocket_defs.h"
#include "dss_netpolicy.h"
#include "ps_iface_defs.h"
#include "dss_iface_ioctl.h"
#include "ds3gsiolib.h"

#include "quectel_apptcpip_util.h"

// add by herry.Geng on 20190408 for supporting lwm2m
#include "quectel_apptcpip_socmgr.h"


#if defined(QUECTEL_APPTCPIP_SUPPORT)

typedef  enum
{
    APPTCPIP_NETIFACE_IDLE = 0, 
    APPTCPIP_NETIFACE_INIT,  
    APPTCPIP_NETIFACE_UPING,
    APPTCPIP_NETIFACE_UP,
    APPTCPIP_NETIFACE_DOWNING,
    APPTCPIP_NETIFACE_DOWN = APPTCPIP_NETIFACE_IDLE
}apptcpip_netiface_state;



typedef enum
{
    APPTCPIP_NET_ESTABLISHED = 0,
    APPTCPIP_NET_MANUAL_DISCONNECT,
    APPTCPIP_NET_AUTO_DISCONNECT
}apptcpip_net_event_enum;

typedef enum
{
	APPTCPIP_NET_ACTIVE_WITH_APN,
	APPTCPIP_NET_ACTIVE_WITH_NETIF_ID
}apptcpip_net_policy_type;

#define DEFAULT_PROFILE                 0x00000001

#define LBS_APP_TYPE                    0x00000020 /* This value is specified in 
                                                     RUIM spec 80-W1429-1 */
                                                     
typedef  void (*apptcpip_net_event_cb_fcn)
(
Q_UINT8     pdp_cid,
Q_UINT32   event_mask,
void *     net_user_data,
Q_INT32  result
);

typedef void(*apptcpip_dns_result_cb_fcn)
(
Q_UINT8    pdp_cid,
Q_UINT16  num_records,
void *     dns_user_data,
Q_INT32   result
);



typedef struct
{
    Q_UINT8      pdp_addr_type;
    Q_UINT8      apn[APPTCPIP_MAX_APN_LEN+1];
    Q_UINT8      username[APPTCPIP_MAX_USERNAME_LEN+1];
    Q_UINT8      password[APPTCPIP_MAX_USERNAME_LEN+1];
    Q_UINT8      auth_type;
}apptcpip_pdp_profile_param_struct;

typedef struct
{
    q_link_type                                link;
    Q_INT32                                    netif_pdp_cid;
	Q_INT32                                    netif_ip_version;
	//params define for dss_socket.h
	Q_INT32                                    netif_dss_net_handle;
	Q_INT32                                    netif_dns_session_handle;
	Q_INT32                                    netif_dns_query_handle;
	Q_INT32                                    netif_close_cause;
    apptcpip_netiface_state              netif_state;
	//params define for quectel
	apptcpip_net_event_cb_fcn         netif_event_cb;
	void*                                        netif_event_user_data;
	apptcpip_dns_result_cb_fcn         netif_async_rsp_cb;
	void*                                        netif_dns_user_data;
}apptcpip_netiface_type;



void  apptcpip_netif_mgr_init
(
void
);

Q_INT32 apptcpip_netif_set_pdp_profile
(
Q_INT32 pdp_id,
apptcpip_pdp_profile_param_struct  *pdp_profile
);

Q_INT32 apptcpip_netif_get_pdp_profile
(
Q_INT32 pdp_id,
apptcpip_pdp_profile_param_struct  *pdp_profile
);

Q_INT32  apptcpip_netif_get_actived_cnt
(
void
);

Q_INT32  apptcpip_netif_open
(
Q_INT32  pdp_cid,
apptcpip_net_event_cb_fcn  event_cb_fcn,
void*      user_data
);


Q_INT32  apptcpip_netif_close
(
Q_INT32  pdp_cid
);

Q_INT32  apptcpip_netif_get_state
(
Q_INT32  pdp_cid,
apptcpip_netiface_state  *state 
);

Q_INT32  apptcpip_netif_getipaddrbyhostname_async
(
Q_INT32  pdp_cid,
Q_CHAR  *host_name,
apptcpip_dns_result_cb_fcn  dns_rsp_cb,
void*       user_data
);

Q_INT32  apptcpip_netif_read_ipaddr_info
(
Q_INT32  pdp_cid,
Q_UINT32 total_records,
Q_UINT32  *ttl,
apptcpip_ip_addr_struct *ipaddr
);

void  apptcpip_netif_get_net_policy_info
(
Q_INT32  pdp_cid,
dss_net_policy_info_type*  p_policy_info
);

Q_INT32 apptcpip_netif_get_net_handle_by_pdp_cid
(
Q_INT32  pdp_cid,
Q_INT16 *dss_net_handle
);

Q_INT32   apptcpip_netif_get_dns_session_handle_by_pdp_cid
(
Q_INT32  pdp_cid,
Q_INT32 *dns_session_handle
);

Q_INT32  apptcpip_netif_get_local_ip_address
(
Q_INT32  pdp_cid,
apptcpip_ip_addr_struct  *local_ip
);

// add by herry.Geng on 20190408 for supporting lwm2m
Q_INT32  apptcpip_netif_get_local_ip_address_extend
(
    Q_INT32  pdp_cid,
    apptcpip_ip_addr_struct  *local_ip,
    apptcpip_protocol_family_type dst_ip_family
);

Q_INT32 apptcpip_netif_get_gateway_ip_address
(
Q_INT32  pdp_cid,
apptcpip_ip_addr_struct  *gate_ip
);

Q_INT32  apptcpip_netif_get_dns_server_address
(
Q_INT32  pdp_cid,
apptcpip_ip_addr_struct  *pri_addr,
apptcpip_ip_addr_struct  *sec_addr
);

Q_INT32  apptcpip_netif_set_dns_server_address
(
Q_INT32  pdp_cid,
apptcpip_ip_addr_struct  *pri_addr,
apptcpip_ip_addr_struct  *sec_addr
);

Q_INT32 apptcpip_get_gateway_ip_address
(
Q_INT32  pdp_cid,
apptcpip_ip_addr_struct  *local_ip
);

Q_UINT32  apptcpip_get_dns_cache_cfg_val();

void  apptcpip_set_dns_cache_cfg_val
(
Q_UINT32 dns_cache
);
#endif/*QUECTEL_APPTCPIP_SUPPORT*/

#endif/*QUECTEL_APPTCPIP_NETMGR_H*/
