#ifndef QUECTEL_SOC_APP_H
#define  QUECTEL_SOC_APP_H


#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"
#if defined(QUECTEL_SOC_APP_SUPPORT)


typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_UINT32                                       pdp_cid;
#if defined(QUECTEL_CYASSL_SUPPORT)
	Q_UINT32                                       ssl_ctx_id;
#endif
	Q_UINT32                                       socket_id;
	Q_UINT8                                        service_type;
	apptcpip_host_address_type            address_type;
   Q_UINT8                                        hostname[256];
   apptcpip_ip_addr_struct                   ip_addr;
	Q_UINT32                                      remote_port;
	Q_UINT32                                      local_port;
	Q_UINT32                                      access_mode;
}quectel_socket_open_param;

typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_UINT32                                       socket_id;
	Q_UINT32                                       close_tm;
}quectel_socket_close_param;


typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_UINT32                                         socket_id;
}quectel_socket_write_param;

typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_UINT32                                         socket_id;
	Q_UINT32                                         read_len;
}quectel_socket_read_param;

typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_UINT32                                       socket_id;
	Q_UINT32                                       send_len;
	Q_UINT8                                         data_format;
	Q_INT32                                         service_type;
	Q_UINT32                                       access_mode;
	Q_UINT16                                       data_to_port;
	apptcpip_ip_addr_struct                   data_to_address;
	dsm_item_type                               *data_item;                                     
}quectel_socket_write_data_param;



typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_INT32                                         socket_id;
	Q_INT32                                         read_len;
	Q_UINT8                                         data_format;
	Q_INT32                                         service_type;
	Q_INT32                                         access_mode;
	Q_UINT16                                       data_from_port;
	apptcpip_ip_addr_struct                   data_from_address;
	dsm_item_type                               *data_item;
}quectel_socket_read_data_param;

typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_INT32                                         socket_id;
	Q_INT32                                         access_mode;
}quectel_socket_access_mode_switch_param;


typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_INT32                                         socket_id;
	Q_INT32                                         info_type;
}quectel_socket_rdwr_info_get_param;


typedef struct
{
	ds3g_siolib_port_e_type                   cmd_port;
	Q_INT32                                         read_mode;
	Q_INT32                                         socket_id;
	Q_INT32                                         pdp_cid;
}quectel_socket_state_read_param;

typedef struct
{
	ds3g_siolib_port_e_type                  service_port;
	Q_INT32                                        pdp_cid;
	Q_INT32                                        socket_id;
	Q_UINT8                                        service_type;
	Q_INT8                                          socket_family_type;
	apptcpip_host_address_type            address_type;
   Q_UINT8                                        hostname[256];
   apptcpip_ip_addr_struct                   ip_addr;
	Q_UINT32                                      remote_port;
	Q_UINT32                                      local_port;
	Q_UINT32                                      access_mode;
	Q_INT32                                        server_id;
	Q_INT32                                        service_state;
#if defined(QUECTEL_CYASSL_SUPPORT)
	Q_INT32                                        ssl_ctx_id;
#endif
}quectel_socket_state_type;

typedef struct
{
	Q_UINT32                 socket_id;
	Q_UINT32                 total_write_cnt;
	Q_UINT32                 acked_write_cnt;
	Q_UINT32                 unacked_write_cnt;
	Q_UINT32                 rest_write_cnt;
}quectel_socket_write_info_type;

typedef struct
{
	Q_UINT32               total_recv_cnt;
	Q_UINT32               readed_cnt;
	Q_UINT32               unread_cnt;
}quectel_socket_receive_info_type;

void  quectel_sockets_init
(
void
);


Q_INT32  quectel_socket_open
(
quectel_socket_open_param  *cmd_param
);


Q_INT32  quectel_socket_state_read
(
Q_INT32  socket_id,
quectel_socket_state_type *socket_state
);


Q_INT32  quectel_socket_write_data
(
quectel_socket_write_data_param *cmd_param
);

Q_INT32  quectel_socket_read_data
(
quectel_socket_read_data_param *cmd_param
);

Q_INT32  quectel_socket_wrtie
(
quectel_socket_write_param *cmd_param
);

Q_INT32  quectel_socket_read
(
quectel_socket_read_param *cmd_param
);

Q_INT32  quectel_socket_state_async_read
(
quectel_socket_state_read_param *cmd_param
);

Q_INT32  quectel_socket_close
(
quectel_socket_close_param *cmd_param
);

Q_INT32 quectel_socket_set_access_mode
(
quectel_socket_access_mode_switch_param *cmd_param
);

Q_INT32 quectel_socket_rdwr_info_read
(
quectel_socket_rdwr_info_get_param  *cmd_param
);

void quectel_socket_set_data_mode_param
(
Q_INT32 socket_id,
Q_INT32 online_pkt_size,
Q_INT32 online_wait_tm
);

#endif/*QUECTEL_SOC_APP_SUPPORT*/


#endif/*QUECTEL_SOC_APP_H*/
