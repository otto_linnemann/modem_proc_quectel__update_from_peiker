#ifndef QUECTEL_SOC_APP_DEFINE_H
#define  QUECTEL_SOC_APP_DEFINE_H

#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"

#if defined(QUECTEL_SOC_APP_SUPPORT)

typedef enum
{
	QUECTEL_TCP_CLIENT = 0,
	QUECTEL_UDP_CLIENT,
	QUECTEL_TCP_LISTENER,
	QUECTEL_UDP_SERVICE,
#if defined(QUECTEL_UDP_LISTENER_SUPPORT)
	QUECTEL_UDP_LISTENER,
#endif
	QUECTEL_TCP_INCOME,
#if defined(QUECTEL_UDP_LISTENER_SUPPORT)
	QUECTEL_UDP_INCOME,
#endif
	QUECTEL_SSL_CLIENT,
	QUECTEL_MAX_SOCKET_SERVICE_TYPE
}quectel_socket_service_type;


typedef enum
{
	QUECTEL_BUFFER_MODE = 0,
	QUECTEL_DIRECT_PUSH_MODE,
	QUECTEL_DATA_MODE
}quectel_socket_access_mode_type;

typedef enum
{
	QUECTEL_SOCKET_SERVICE_IDLE = 0,
	QUECTEL_SOCKET_SERVICE_PDP_ACTIVING,// 1
	QUECTEL_SOCKET_SERVICE_DNSGIP, // 2
	QUECTEL_SOCKET_SERVICE_INIT, // 3
	QUECTEL_SOCKET_SERVICE_LISTEN, // 4
	QUECTEL_SOCKET_SERVICE_CONNECTING, // 5
	QUECTEL_SOCKET_SERVICE_SSL_HANDSHARK, // 6
	QUECTEL_SOCKET_SERVICE_CONNECTED, // 7
	QUECTEL_SOCKET_SERVICE_WAIT_CLOSE, // 8
	QUECTEL_SOCKET_SERVICE_CLOSING, // 9
	QUECTEL_SOCKET_SERVICE_CLOSED = QUECTEL_SOCKET_SERVICE_IDLE
}quectek_socket_service_state_type;


typedef enum
{
	QUECTEL_SOCKET_STATE_INFO = 0,
	QUECTEL_SOCKET_RD_INFO,
	QUECTEL_SOCKET_WR_INFO
}quectel_socket_info_type;

#if 0 //fix bug18481
typedef enum
{
	QUECTEL_SOCKET_SENDQ_HIGH_LEVEL = 0,
	QUECTEL_SOCKET_SENDQ_LOW_LEVEL,
	QUECTEL_SOCKET_SENDQ_ENQUEUE_DATA,
	QUECTEL_SOCKET_SENDQ_DEQUEUE_DATA
}quectel_socket_sendq_monitor_type;
#endif

typedef struct
{
	apptcpip_ip_addr_struct      data_from_addr;
	Q_UINT16                          data_from_port;
}quectel_socket_udp_data_header;


typedef struct
{
	Q_UINT8                            server_id;

	Q_UINT32                          total_send_cnt;
	Q_UINT32                          acked_send_cnt;

	Q_UINT32                          total_recv_cnt;

	Q_BOOL                             rw_wm_ready;
	Q_BOOL                             send_wm_high;
	dsm_item_type                   *current_send_data_ptr;
	dsm_watermark_type          send_wm_ptr;
	q_type                                send_q;

	Q_BOOL                              recv_wm_high;
	dsm_item_type                    *current_recv_data_ptr;
	dsm_watermark_type          recv_wm_ptr;
	q_type                                recv_q;
	Q_UINT32                           udp_recv_pkt_cnt;
	Q_UINT32                           udp_readed_pkt_cnt;
	Q_UINT32                           udp_send_pkt_cnt;
	Q_UINT32                           udp_sent_pkt_cnt;
	
	
	Q_UINT32                           tcp_total_ack_cnt; //  store total ack cnt before firset call dss_soc_close(), because after first call dss_soc_close(), the ack cnt store in tcp layer has been reset to 0
	apptcpip_timer_id                online_send_timer_id;
	Q_UINT32                           online_send_tm;
	Q_UINT32                           online_send_pkt_size;
}quectel_socket_transfer_session;

typedef struct
{
	Q_UINT8  client_ref_num;
	Q_UINT8  listen_num;
}quectel_socket_tcp_server;


typedef struct
{
	q_link_type                                         link;
   ds3g_siolib_port_e_type                       serivce_port;
   ds3g_siolib_port_e_type                       last_data_port;
	Q_INT32                                             pdp_cid;
#if defined(QUECTEL_CYASSL_SUPPORT)
   Q_INT32                                             ssl_ctx_id;
   Q_INT32                                             ssl_session_handle;
	Q_BOOL                                             ssl_abort;
#endif
	Q_INT32                                             socket_index;
	Q_INT16                                             sockfd;
	Q_INT8                                               sock_type;
	Q_INT8                                               sock_family_type;
	Q_INT8                                               service_state;
	Q_INT8                                               service_type;
	Q_INT8                                               access_mode;
	Q_UINT8                                             open_addr_type;
	Q_UINT8                                             open_hostname[255];
	apptcpip_ip_addr_struct                       local_ip_addr;
	apptcpip_ip_addr_struct                       remote_ip_addr;
	Q_UINT16                                           local_port;
	Q_UINT16                                           remote_port;
	Q_BOOL                                             close_called;
	Q_BOOL                                             open_completed;
	union
	{
		quectel_socket_transfer_session         connection;
	    quectel_socket_tcp_server                 tcp_server;
	}u;
	
#define quectel_connection                   u.connection
#define quectel_tcp_server                   u.tcp_server
}quectel_socket_service;

#define QUECTEL_SOCKETS_SERVICE_NUM                  12
#define QUECTEL_SOCKET_SENDQ_HIGH_LEVEL          10*1024
#define QUECTEL_SOCKET_RECVQ_HIGH_LEVEL          10*1024
#define QUECTEL_SOCKET_ONLINE_SEND_PKT_SIZE_DEFAULT   1024
#define QUECTEL_SOCKET_ONLINE_SEND_WAIT_TM_DEFAULT   2

#endif/*QUECTEL_SOC_APP_SUPPORT*/
#endif/*QUECTEL_SOC_APP_DEFINE_H*/
