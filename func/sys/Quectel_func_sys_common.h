
/**  
  @file
  Quectel_func_sys_common.h

  @brief
  This file defines declaration for FUNC SYS-COMMON configuration
  
  @see
  Create this file by QCM9X00113C011-P01
**/
 
#ifndef __QUECTEL_FUNC_SYS_COMMON_H__
#define __QUECTEL_FUNC_SYS_COMMON_H__

#include "quectel_func_sys_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
//<2018/09/07-QCM9X00113C011-P03-Vicent.Gao, <[FUNC] Segment 3==> Add code for SYS COMMON functions.>
#define FUNCSYS_IMEI_MIN_LEN      (15 + 1)
#define FUNCSYS_SVN_MIN_LEN       (2 + 1)
#define FUNCSYS_IMEI_SVN_MIN_LEN  (16 + 1)
//>2018/09/07-QCM9X00113C011-P03-Vicent.Gao

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean cm_rpm_check_reset_allowed(void);
extern void sys_m_initiate_shutdown(void);

extern boolean FUNC_SYS_DeviceResetCfun(void);
extern boolean FUNC_SYS_DeviceResetAssert(void);

//<2018/08/22-QCM9X00113C011-P02-Vicent.Gao, <[FUNC] Segment 2==> Add code for SYS COMMON functions.>
/******************************************************************************************
Vicent.Gao-2018/08/23: Add API for switch cfun 0 to 1
Refer to [Req-Depot].[RQ0000167][Submitter:2018-08-23,Date:2018-08-23]
<MDM9x07>
******************************************************************************************/
extern boolean FUNC_SYS_CardPowerDown(void);
extern boolean FUNC_SYS_CardPowerUp(void);
extern boolean FUNC_SYS_SetCfun0(void);
extern boolean FUNC_SYS_SetCfun1(void);
extern boolean FUNC_SYS_SwitchCfun0To1(void);
//>2018/08/22-QCM9X00113C011-P02-Vicent.Gao

//<2018/09/07-QCM9X00113C011-P03-Vicent.Gao, <[FUNC] Segment 3==> Add code for SYS COMMON functions.>
extern boolean nvim_op_get_presence(nv_items_enum_type item);
extern uint16 nvim_op_get_array_size (nv_items_enum_type item);
extern uint32 nvim_op_get_size(nv_items_enum_type item_code);

extern boolean FUNC_SYS_WriteNvItemByMcfg(nv_items_enum_type eItem,byte *pData,uint16 *pDataLen,uint16 uDataMaxLen);
extern boolean FUNC_SYS_GetImei(char *pImei,uint8 uImeiMaxLen);
extern boolean FUNC_SYS_GetImeiMcfg(char *pImei,uint8 uImeiMaxLen);
extern boolean FUNC_SYS_GetSvn(char *pSvn,uint8 uSvnMaxLen);
extern boolean FUNC_SYS_GetSvnMcfg(char *pSvn,uint8 uSvnMaxLen);
extern boolean FUNC_SYS_GetImeiSvn(char *pImeiSvn,uint8 uImeiSvnMaxLen);
extern boolean FUNC_SYS_GetImeiSvnMcfg(char *pImeiSvn,uint8 uImeiSvnMaxLen);
//>2018/09/07-QCM9X00113C011-P03-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_FUNC_SYS_COMMON_H__
