
/*==========================================================================
 |              QUECTEL - Build a smarter world.
 |
 |              Copyright(c) 2018 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for FUNC PHB-COMMON configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2018/03/10       Vicent GAO        Create this file by QCM9X00113C009-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_FUNC_PHB_COMMON_H__
#define __QUECTEL_FUNC_PHB_COMMON_H__

#include "quectel_func_phb_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern boolean FUNC_PHB_ConvAsciiNumToBcdCdma(char *pAsciiNum,byte *pTonNpi,byte *pBcdNum,uint8 *pBcdNumLen,uint8 uBcdNumMaxLen);

#endif  // #ifndef __QUECTEL_FUNC_PHB_COMMON_H__
