/*!  
  @file
  Quectel_func_net_atc.h

  @brief
  This file defines interfaces for FUNC-NET ATC use

  @see
  Create this file by QCM9X00113C012-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/27/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_FUNC_NET_ATC_H__
#define __QUECTEL_FUNC_NET_ATC_H__

#include "quectel_func_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean QL_QFUNCNETOPER_SendResponseS3S4(void);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern dsat_result_enum_type quectel_exec_qfuncnetoper_cmd
(
    dsat_mode_enum_type eMode,             /*  AT command mode:            */
    const dsati_cmd_type *pParseTab,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *pTok,    /*  Command tokens from parser  */
    dsm_item_type *pResBuf           /*  Place to put response       */
);

#endif  // #ifndef __QUECTEL_FUNC_NET_ATC_H__
