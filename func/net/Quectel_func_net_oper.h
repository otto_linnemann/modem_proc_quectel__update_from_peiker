
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for FUNC NET-OPER configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/11/02       Vicent GAO        Create this file by QCM9X00113C001-P01
 |  2018/06/28       Vicent GAO        Add new korea operator: KT/SKT
 |  2018/07/22       Vicent GAO        Add new operator: Telefonica(Germany)
 |  2018/09/05       Vicent GAO        Add new operator: Telefonica(Spain)
 |  2018/11/25       Vicent GAO        Add function to get macro string
 |  2018/11/27       Vicent GAO        Add function to get operator count and operator name
 |  2018/11/27       Vicent GAO        Add function to get plmn list of operator
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_FUNC_NET_OPER_H__
#define __QUECTEL_FUNC_NET_OPER_H__

#include "Quectel_func_net_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    FUNC_NET_OPER_CMCC = 0,
    FUNC_NET_OPER_CUCC = 1,
    FUNC_NET_OPER_CTCC = 2,
    FUNC_NET_OPER_TELSTRA = 3,
    //<2017/11/09-QCM9X00113C003-P01-Vicent.Gao, <[FUNC] Segment 1==> Add code for NET OPER functions.>
    FUNC_NET_OPER_LGU = 4,
    //>2017/11/09-QCM9X00113C003-P01-Vicent.Gao
    //<2018/03/29-QCM9X00113C003-P02-Vicent.Gao, <[FUNC] Segment 2==> Add code for NET OPER functions.>
    FUNC_NET_OPER_DOCOMO = 5,
    FUNC_NET_OPER_SOFTBANK = 6,
    FUNC_NET_OPER_KDDI = 7,
    //>2018/03/29-QCM9X00113C003-P02-Vicent.Gao
    //<2018/06/11-QCM9X00113C003-P03-Vicent.Gao, <[FUNC] Segment 3==> Add code for NET OPER functions.>
    /******************************************************************************************
    Vicent.Gao-2018/06/11: NET OPER function maintain
    Refer to [Req-Depot].[RQ0000107][Submitter:2018-06-11,Date:2018-06-11]
    <MDM9x07>
    ******************************************************************************************/
    FUNC_NET_OPER_VERIZON = 8,
    FUNC_NET_OPER_ATT = 9,
    FUNC_NET_OPER_TMO = 10,
    //>2018/06/11-QCM9X00113C003-P03-Vicent.Gao
    //<2018/06/27-QCM9X00113C003-P04-Vicent.Gao, <[FUNC] Segment 4==> Add code for NET OPER functions.>
    /******************************************************************************************
    Vicent.Gao-2018/06/11: NET OPER function maintain
    Refer to [Req-Depot].[RQ0000107][Submitter:2018-06-11,Date:2018-06-11]
    <MDM9x07>
    ******************************************************************************************/
    FUNC_NET_OPER_KT = 11,
    FUNC_NET_OPER_SKT = 12,
    //>2018/06/27-QCM9X00113C003-P04-Vicent.Gao
    //<2018/07/19-QCM9X00113C003-P05-Vicent.Gao, <[FUNC] Segment 5==> Add code for NET OPER functions.>
    FUNC_NET_OPER_TELEFONICA_GERMANY = 13,
    //>2018/07/19-QCM9X00113C003-P05-Vicent.Gao
    //<2018/09/05-QCM9X00113C003-P06-Vicent.Gao, <[FUNC] Segment 6==> Add code for NET OPER functions.>
    FUNC_NET_OPER_TELEFONICA_SPAIN = 14,
    //>2018/09/05-QCM9X00113C003-P06-Vicent.Gao
    //<2018/09/19-QCM9X00113C003-P07-Vicent.Gao, <[FUNC] Segment 7==> Add code for NET OPER functions.>
    FUNC_NET_OPER_DT = 15, //Germany - Deutsch Telekom
    //>2018/09/19-QCM9X00113C003-P07-Vicent.Gao
    //<2018/09/25-QCM9X00113C003-P08-Vicent.Gao, <[FUNC] Segment 8==> Add code for NET OPER functions.>
    FUNC_NET_OPER_ATT_FIRSTNET = 16,
    FUNC_NET_OPER_ATT_NDO = 17,
    //>2018/09/25-QCM9X00113C003-P08-Vicent.Gao

    //Warning!==> Please add new FUNC NET OPER upper this line.
    FUNC_NET_OPER_MAX = -1
} FUNCNET_OperEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern int FUNC_NET_GetOper(void);
//<2017/11/09-QCM9X00113C003-P01-Vicent.Gao, <[FUNC] Segment 1==> Add code for NET OPER functions.>
extern int FUNC_NET_GetOperByPlmn(char *pPlmnStr);
//>2017/11/09-QCM9X00113C003-P01-Vicent.Gao
//<2018/03/29-QCM9X00113C003-P02-Vicent.Gao, <[FUNC] Segment 2==> Add code for NET OPER functions.>
extern int FUNC_NET_GetOperS2(void);
extern boolean FUNC_SIM_GetImsiGsmS2(uint8 *pImsi,uint16 uImsiMaxLen);
extern int32 FUNC_SIM_GetPlmnByImsiS2(char *pImsi,uint8 *pPlmn,uint16 uPlmnMaxLen);
//>2018/03/29-QCM9X00113C003-P02-Vicent.Gao

//<2018/09/05-QCM9X00113C003-P06-Vicent.Gao, <[FUNC] Segment 6==> Add code for NET OPER functions.>
extern int FUNC_NET_GetOperS3(void);
extern boolean FUNC_MCFG_GetPlmnFromActiveMbn(char *pPlmn,uint8 uPlmnMaxLen);
//>2018/09/05-QCM9X00113C003-P06-Vicent.Gao

//<2018/11/24-QCM9X00113C003-P09-Vicent.Gao, <[FUNC] Segment 9==> Add code for NET OPER functions.>
extern char* FUNC_NET_GetMacroName(uint32 uMacro);
//>2018/11/24-QCM9X00113C003-P09-Vicent.Gao

//<2018/11/27-QCM9X00113C003-P10-Vicent.Gao, <[FUNC] Segment 10==> Add code for NET OPER functions.>
extern int FUNC_NET_GetOperCount(void);
extern boolean FUNC_NET_GetOperName(int iOper,char *pOperName,uint8 uOperNameMaxLen);
//>2018/11/27-QCM9X00113C003-P10-Vicent.Gao

//<2018/11/27-QCM9X00113C003-P11-Vicent.Gao, <[FUNC] Segment 11==> Add code for NET OPER functions.>
extern int FUNC_NET_GetOperByOperName(char *pOperName);
extern char** FUNC_NET_GetOperPlmnList(int iOper,int *pPlmnCnt);
//>2018/11/27-QCM9X00113C003-P11-Vicent.Gao

#endif  // #ifndef __QUECTEL_FUNC_NET_OPER_H__
