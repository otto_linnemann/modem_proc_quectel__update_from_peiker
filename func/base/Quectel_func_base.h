
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for FUNC BASE configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/11/02       Vicent GAO        Create this file by QCM9X00113C001-P01
 |  2018/11/25       Vicent GAO        Add function to get macro string
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_FUNC_BASE_H__
#define __QUECTEL_FUNC_BASE_H__

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define  FUNC_ARR_SIZE( a )  ( sizeof( (a) ) / sizeof( (a[0]) ) )

//<2018/11/24-QCM9X00113C003-P09-Vicent.Gao, <[FUNC] Segment 9==> Add code for NET OPER functions.>
#define CASE_FUNC_GET_MACRO_NAME(Macro,Name)   case ((uint32)(Macro)): (Name) = (char*)#Macro; break
//>2018/11/24-QCM9X00113C003-P09-Vicent.Gao

#endif  // #ifndef __QUECTEL_FUNC_BASE_H__

