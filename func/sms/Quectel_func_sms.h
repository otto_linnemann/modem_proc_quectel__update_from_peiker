
/*!  
  @file
  Quectel_func_sms.h

  @brief
  This file defines declarations for FUNC SMS use

  @see
  Create this file by QCM9X00113C013-P01
*/

/*===============================================================================
  Copyright (c) 2019 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
01/18/2019   vicent        Init
=================================================================================*/
 
#ifndef __QUECTEL_FUNC_SMS_H__
#define __QUECTEL_FUNC_SMS_H__

#include "quectel_func_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern dsat_result_enum_type FUNC_SMS_ChangeStToRead(sms_msg_status_info_s_type *pMsg);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define FUNC_SMS_TRACE_0(f)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),SMS_" f,__FUNCTION__,__LINE__)
#define FUNC_SMS_TRACE_1(f,g1)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),SMS_" f,__FUNCTION__,__LINE__,g1)
#define FUNC_SMS_TRACE_2(f,g1,g2)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),SMS_" f,__FUNCTION__,__LINE__,g1,g2)
#define FUNC_SMS_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_5(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),SMS_" f,__FUNCTION__,__LINE__,g1,g2,g3)
#define FUNC_SMS_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_6(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),SMS_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4)
#define FUNC_SMS_TRACE_5(f,g1,g2,g3,g4,g5)  MSG_SPRINTF_7(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),SMS_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5)
#define FUNC_SMS_TRACE_6(f,g1,g2,g3,g4,g5,g6)  MSG_SPRINTF_8(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),SMS_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5,g6)

#endif  // #ifndef __QUECTEL_FUNC_SMS_H__
