
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for FUNC SIM-COMMON configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/11/11       Vicent GAO        Create this file by QCM9X00113C006-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_FUNC_SIM_COMMON_H__
#define __QUECTEL_FUNC_SIM_COMMON_H__

#include "quectel_func_sim_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern int FUNC_SIM_GetEfImsiName(void);
extern boolean FUNC_SIM_GetImsiGsmS2(uint8 *pImsi,uint16 uImsiMaxLen);
extern int32 FUNC_SIM_GetPlmnByImsiS2(char *pImsi,uint8 *pPlmn,uint16 uPlmnMaxLen);

// add by herry.Geng on 20190408 for supporting lwm2m
extern boolean FUNC_SIM_GetIccid(uint8 *pICCID,uint16 uIccidMaxLen);

//<2018/04/09-QCM9X00113C006-P02-Vicent.Gao, <[FUNC] Segment 2==> Add code for SIM COMMON functions.>
/******************************************************************************************
Vicent.Gao-2018/04/10: Add code for SIM COMMON functions
Refer to [Req-Depot].[RQ0000035][Submitter:2018-04-10,Date:2018-04-10]
<>
******************************************************************************************/
extern int FUNC_SIM_GetEfMsisdnName(void);
extern boolean FUNC_SIM_GetMsisdnGsm(uint8 *pMsisdn,uint16 *pMsisdnLen,uint16 uMsisdnMaxLen);
extern boolean FUNC_SIM_GetMsisdnFromCardGsm(uint8 *pMsisdn,uint16 *pMsisdnLen,uint16 uMsisdnMaxLen);
extern boolean FUNC_SIM_ReadRecordFromCardSessIdSync
(
    mmgsdi_session_id_type uSessId,
    mmgsdi_app_enum_type eAppType,
    mmgsdi_file_enum_type eFile,
    uint8 uRecordNum,
    uint8 *pData,
    uint16 *pDataLen,
    uint16 uDataMaxLen
);
//>2018/04/09-QCM9X00113C006-P02-Vicent.Gao

//<2019/05/23-QCM9X00113C006-P04-Vicent.Gao, <[FUNC] Segment 4==> Add code for SIM COMMON functions.>
extern boolean pbm_bcd_to_ascii(
  PACKED const byte *bcd_number,
  uint8              bcd_len,
  uint8              ton_npi,
  byte              *ascii_number
);

extern boolean FUNC_SIM_GetMsisdnGsmNum(char *pNum,uint16 uNumMaxLen);
//>2019/05/23-QCM9X00113C006-P04-Vicent.Gao

#endif  // #ifndef __QUECTEL_FUNC_SIM_COMMON_H__
