
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for FUNC PROFILE-COMMON configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/11/09       Vicent GAO        Create this file by QCM9X00113C004-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_FUNC_PROFILE_COMMON_H__
#define __QUECTEL_FUNC_PROFILE_COMMON_H__

#include "quectel_func_profile_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean FUNC_PROFILE_IsProfileIdValid(uint16 uProfileId);
extern boolean FUNC_PROFILE_GetPdpContextApn(uint16 uProfileId,char *pApn,uint8 uApnMaxLen);
extern boolean FUNC_PROFILE_SetPdpContextApn(uint16 uProfileId,char *pApn);
extern boolean FUNC_PROFILE_GetPdpContextPdpType(uint16 uProfileId,uint8 *pPdpType);
extern boolean FUNC_PROFILE_SetPdpContextPdpType(uint16 uProfileId,uint8 uPdpType);
extern boolean FUNC_PROFILE_GetPcscfReqFlag(uint16 uProfileId,uint8 *pPcscfReqFlag);
extern boolean FUNC_PROFILE_SetPcscfReqFlag(uint16 uProfileId,uint8 uPcscfReqFlag);
//<2018/11/22-QCM9X00113C004-P03-Vicent.Gao, <[FUNC] Segment 3==> Add code for PROFILE COMMON functions.>
extern boolean FUNC_PROFILE_SetOverrideHomePdpType(uint16 uProfileId,uint8 uOverrideHomePdpType);
//>2018/11/22-QCM9X00113C004-P03-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_FUNC_PROFILE_COMMON_H__
