
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for FUNC CALL-COMMON configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/11/23       Vicent GAO        Create this file by QCM9X00113C007-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_FUNC_CALL_COMMON_H__
#define __QUECTEL_FUNC_CALL_COMMON_H__

#include "quectel_func_call_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern boolean FUNC_CALL_EncodeBcdNum(byte *pBcdNum,byte uBcdNumLen,byte *pEnc,byte uEncMaxLen);
extern boolean FUNC_CALL_GetBcdNumSiPi(byte *pBcdNum,byte uBcdNumLen,byte *pPi,byte *pSi);
extern boolean FUNC_CALL_GetCmNumByCallingPartyBcdNo(cm_calling_party_bcd_no_s_type *pPartyBcdNo,cm_num_s_type *pCmNum);
extern boolean FUNC_CALL_GetCmSubAddrByCallingPartySubAddr(cm_calling_party_subaddress_s_type *pPartySubAddr,cm_subaddress_s_type *pCmSubAddr);

#endif  // #ifndef __QUECTEL_FUNC_CALL_COMMON_H__
