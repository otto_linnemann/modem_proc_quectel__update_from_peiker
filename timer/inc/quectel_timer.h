#ifndef QUECTEL_TIMER_H
#define QUECTEL_TIMER_H

#ifdef QUECTEL_TIMER_GROUP
#define QUECTEL_TIMER_MAX    10


int32 quectel_timer_register(uint32 timer_id);

int32 quectel_timer_start(uint32 timer_id, uint32 interval, boolean auto_repeat);

int32 quectel_timer_stop(uint32 timer_id);

int32 quectel_timer_unregister(uint32 timer_id);

#endif//QUECTEL_TIMER_GROUP

#endif//QUECTEL_TIMER_H
