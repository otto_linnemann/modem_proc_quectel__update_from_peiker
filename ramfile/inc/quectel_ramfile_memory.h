#ifndef __QUECTEL_RAMFILE_MEMORY__
#define __QUECTEL_RAMFILE_MEMORY__
/*===========================================================================

                  quectel_ramfile_memory.h
                   

        DESCRIPTION
   

        when         who    what, where, why
        --------    ---    ----------------------------------------------------------
        04/06/13    Gibson     Created module

===========================================================================*/

#include "memheap.h"
#include "msg_diag_service.h"
#include "QuecOEM_feature.h"
#include "dsm_pool.h"

#define QUEC_MEM_SIZE (50*1024)

/*===========================================================================
FUNCTION quec_init_heap()

DESCRIPTION
  This function will init the heap of the modem

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/

void quec_init_heap(void);

/*===========================================================================
FUNCTION void *quec_mem_malloc(uint32 size);

DESCRIPTION
  This function will allocate the space of the size to the user

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/

void *quec_mem_malloc(uint32 size);



/*===========================================================================
FUNCTION void *quec_mem_malloc(uint32 size);

DESCRIPTION
  This function will allocate the space of the size and initial the space.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/

void *quec_mem_calloc(uint32 size);

/*===========================================================================
FUNCTION void quec_mem_free(void *ptr);

DESCRIPTION
  This function will free the the space which the ptr points

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void quec_mem_free(void *ptr);


/*===========================================================================
FUNCTION void *quec_dsmem_malloc(int32 size, uint32 *pBlockCount);

DESCRIPTION
  This function will allot the the space which the ptr points

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void *quec_dsmem_malloc(int32 size, uint32 *pBlockCount);




/*===========================================================================
FUNCTION void quec_dsm_free(void **ptr);

DESCRIPTION
    For dsm buffer, we use this function to free.
  
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void quec_dsm_free(void **ptr);


/*===========================================================================
FUNCTION uint16 quec_getdsmitemsize(dsm_mempool_id_type pool_id);

DESCRIPTION
    Get the the item size of the pool
  
DEPENDENCIES
  None.

RETURN VALUE
  item size

SIDE EFFECTS
  None.
===========================================================================*/
uint16 quec_getdsmitemsize(dsm_mempool_id_type pool_id);


/*===========================================================================
FUNCTION uint32 quec_getFreeSize(dsm_mempool_id_type pool_id);

DESCRIPTION
    Get the the free space of the pool id
  
DEPENDENCIES
  None.

RETURN VALUE
  free size

SIDE EFFECTS
  None.
===========================================================================*/
uint32 quec_getFreeSize(void);


/*===========================================================================
FUNCTION uint32 quec_getTotalSize(dsm_mempool_id_type pool_id);

DESCRIPTION
    Get the the total space of the pool id
  
DEPENDENCIES
  None.

RETURN VALUE
  total size

SIDE EFFECTS
  None.
===========================================================================*/
uint32 quec_getTotalSize(void);

/*===========================================================================
FUNCTION void quec_dsmem_enqueue(void **head, void *node);

DESCRIPTION
    enqueue node to head
  
DEPENDENCIES
  None.

RETURN VALUE
  void

SIDE EFFECTS
  None.
===========================================================================*/
void quec_dsmem_enqueue(void **head, void **node);

/*===========================================================================
FUNCTION uint16 quec_getDsmItemSize();

DESCRIPTION
    Get the item size of the RAM FILE
  
DEPENDENCIES
  None.

RETURN VALUE
  void

SIDE EFFECTS
  None.
===========================================================================*/
uint16 quec_getDsmItemSize(void);



/*===========================================================================
FUNCTION uint32 quec_getDsmLength(dsm_item_type *pkt_ptr);

DESCRIPTION
    Get the size of the packet ptr
  
DEPENDENCIES
  None.

RETURN VALUE
  void

SIDE EFFECTS
  None.
===========================================================================*/
uint32 quec_getDsmLength(dsm_item_type *pkt_ptr);

#endif
