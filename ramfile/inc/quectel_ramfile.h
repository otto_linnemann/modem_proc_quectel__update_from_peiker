#ifndef __QUECTEL_RAMFILE_HEAD_FILE__
#define __QUECTEL_RAMFILE_HEAD_FILE__
/*===========================================================================

                  quectel_ramfile.h
                   

        DESCRIPTION
   

        when         who    what, where, why
        --------    ---    ----------------------------------------------------------
        04/06/13    Gibson     Created module

===========================================================================*/

#include "quectel_ramfile_memory.h"
#include "quectel_file_common.h"
#include "rex.h"



#ifdef QUECTEL_RAM_FILE

#define RAMFILE_MODULE "RamFile"

#define RAMFILE_LockType        rex_crit_sect_type
#define QRamFile_Lock(p)        rex_enter_crit_sect(p)
#define QRamFile_Unlock(p)     rex_leave_crit_sect(p)
#define QRamFile_Initlock(p)    rex_init_crit_sect(p)



#define GIBSON_RF_DEBUG

#define GIBSON_RF_MSG_1(xx_fmt, xx_arg1)  \
 //tommy   MSG_SPRINTF_1(MSG_SSID_DS_ATCOP, MSG_LEGACY_HIGH, xx_fmt, xx_arg1)

#define GIBSON_RF_ERRORMSG_1(xx_fmt, xx_arg1)  \
  //tommy    MSG_SPRINTF_1(MSG_SSID_DS_ATCOP, MSG_LEGACY_ERROR, xx_fmt, xx_arg1)

#define GIBSON_RF_ERRORMSG_2(xx_fmt, xx_arg1, xx_arg2)  \
  //tommy    MSG_SPRINTF_2(MSG_SSID_DS_ATCOP, MSG_LEGACY_ERROR, xx_fmt, xx_arg1, xx_arg2)

#define GIBSON_RF_ERRORMSG_3(xx_fmt, xx_arg1, xx_arg2, xx_arg3)  \
   //tommy   MSG_SPRINTF_3(MSG_SSID_DS_ATCOP, MSG_LEGACY_ERROR, xx_fmt, xx_arg1, xx_arg2, xx_arg3)


typedef Q_INT32 QRAM_FILE_HANDLE;

#define MAX_OPEN_FILE_COUNT (15)
#define QRAM_FILE_START_HANDLE      (3000)    
#define QRAM_FILE_HANDLE_INVALID       (-1)
#define QRAM_MAX_FILE_NUM           (MAX_OPEN_FILE_COUNT)
#define QRAM_DEFAULT_SIZE               (102400)
#define QRAM_FAIL                          (-1)
#define QRAM_SUCCESS                      (0)



#define RAMFILE_MaxFileNameLen  (QFILE_MAX_PATH_LENGTH+4)
#define QRAM_ContentPtr  dsm_item_type


typedef struct tagQDRamNode
{
    struct tagQDRamNode *pNext;
    Q_INT8    asciifilename[RAMFILE_MaxFileNameLen];
    QRAM_ContentPtr    *rampointer;
    Q_UINT32  validbytes;
    //Q_BOOL allocram_inner;
    Q_UINT32  blockCount;
    Q_UINT16  ref_count;
}QRamDNode;



typedef struct tagQRamFileHandle
{
    QRAM_FILE_HANDLE    handle;
    QRamDNode               *pNode;
    Q_UINT32                 ramcurrentpos;
    Q_UINT8                   mode;
}QRamFileHandle;


/*===========================================================================
FUNCTION QRAM_Init()

DESCRIPTION
  This function will init the module of the Quectel Ram

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
void QRAM_Init(void);


/*===========================================================================
FUNCTION Q_BOOL QRAM_CreateFile(Q_UINT8 *asciifilename, Q_UINT8 flag, Q_UINT32 filesize);

DESCRIPTION
  It creates the file node.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
QRamDNode *QRAM_CreateFile(Q_INT8 *asciifilename, Q_UINT8 flag);


/*===========================================================================
FUNCTION QRAM_FILE_HANDLE QRAM_OpenFile(Q_INT8 filename, Q_UINT8 flag);

DESCRIPTION
  Open the ramfile

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
QRAM_FILE_HANDLE QRAM_OpenFile(Q_INT8 *filename, Q_INT32 flag);




/*===========================================================================
FUNCTION Q_UINT32 QRAM_ReadFile(QRAM_FILE_HANDLE fileHandle, Q_UINT8 buf, Q_UINT32 buf_size);

DESCRIPTION
  read buf_size data from the ramfile

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/

Q_INT32 QRAM_ReadFile(QRAM_FILE_HANDLE fileHandle, Q_INT8 *buf, Q_UINT32 buf_size);


/*===========================================================================
FUNCTION Q_INT32 QRAM_WriteFile(QRAM_FILE_HANDLE fileHandle, Q_INT8 *buf, Q_UINT32 buf_size);

DESCRIPTION
  write buf_size data from the ramfile

DEPENDENCIES
  None.

RETURN VALUE
  The actual the size to be written.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_WriteFile(QRAM_FILE_HANDLE fileHandle, Q_INT8 *buf, Q_UINT32 buf_size);


/*===========================================================================
FUNCTION Q_INT32 QRAM_CloseFile(QRAM_FILE_HANDLE fileHandle);

DESCRIPTION
  close the file.

DEPENDENCIES
  None.

RETURN VALUE
  SUCCESS or NOT.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_CloseFile(QRAM_FILE_HANDLE fileHandle);


/*===========================================================================
FUNCTION Q_INT32 QRAM_DeleteFile(Q_INT8 *filename);

DESCRIPTION
  delete the file.

DEPENDENCIES
  None.

RETURN VALUE
  Success or not.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_DeleteFile(Q_INT8 *filename);

/*===========================================================================
FUNCTION Q_INT32 QRAM_SeekFile(QRAM_FILE_HANDLE fileHandle, Q_INT32 offset, Q_INT32 whence);

DESCRIPTION
  seek the pointer of the file.

DEPENDENCIES
  None.

RETURN VALUE
  The offset value.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_SeekFile(QRAM_FILE_HANDLE fileHandle, Q_INT32 offset, Q_INT32 whence);



/*===========================================================================
FUNCTION Q_INT32 QRAM_IsFileExist(Q_INT8 *filename);

DESCRIPTION
   check the ram exist

DEPENDENCIES
  None.

RETURN VALUE
  The offset value.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_IsFileExist(Q_INT8 *filename);


/*===========================================================================
FUNCTION Q_UINT32 QRAM_GetFreeSize();

DESCRIPTION
   Get the free space of the Ram

DEPENDENCIES
  None.

RETURN VALUE
  The offset value.

SIDE EFFECTS
  None.
===========================================================================*/
Q_UINT64 QRAM_GetFreeSize(void);


/*===========================================================================
FUNCTION Q_UINT32 QRAM_GetFileSizeByHandler(QRAM_FILE_HANDLE fileHandler);

DESCRIPTION
   Get the size of the file by handler

DEPENDENCIES
  None.

RETURN VALUE
  The size

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_GetFileSizeByHandler(QRAM_FILE_HANDLE fileHandler);

/*===========================================================================
FUNCTION Q_INT32 QRAM_GetFileSizeByName(Q_INT8 *fileName);

DESCRIPTION
   Get the size of the file by Name

DEPENDENCIES
  None.

RETURN VALUE
  The size

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_GetFileSizeByName(Q_INT8 *fileName);

/*===========================================================================
FUNCTION Q_INT32 QRAM_GetStorageInfo(STORAGE_INFO *storage_info);

DESCRIPTION
   Get the info of the RAM space 

DEPENDENCIES
  the structure of the storage

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_GetStorageInfo(STORAGE_INFO *storage_info);

/*===========================================================================
FUNCTION Q_INT8 QRAM_GetAllFileInfoByDefault(QF_LISTFILEINFO *fileInfoStruct);

DESCRIPTION
   Only one task uses this interface

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT8 QRAM_GetAllFileInfoByDefault(QF_LISTFILEINFO *fileInfoStruct);

/*===========================================================================
FUNCTION Q_INT32 QRAM_GetFileRamSizeByName(Q_INT8 *fileName);

DESCRIPTION
   Get the ram size of the file

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_GetFileRamSizeByName(Q_INT8 *fileName);

/*===========================================================================
FUNCTION Q_INT32 QRAM_TruncateFile(QRAM_FILE_HANDLE fileHandle, Q_INT32 length);

DESCRIPTION
   Truncate the File

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_INT32 QRAM_TruncateFile(QRAM_FILE_HANDLE fileHandle, Q_INT32 length);


/*===========================================================================
FUNCTION Q_INT32 QRAM_TruncateFile(QRAM_FILE_HANDLE fileHandle, Q_INT32 length);

DESCRIPTION
   Truncate the File

DEPENDENCIES

RETURN VALUE
  the result.

SIDE EFFECTS
  None.
===========================================================================*/
Q_BOOL QRAM_IsRamFileByHandle(QRAM_FILE_HANDLE fileHandle);

#endif
#endif
