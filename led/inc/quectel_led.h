#ifndef QUECTEL_LED_H
#define QUECTEL_LED_H

#include "com_dtypes.h"



#define QUECTEL_PACKET_TRANSFER_START_IND 0X100
#define QUECTEL_PACKET_TRANSFER_END_IND 0X101

#ifdef QUECTEL_FATAL_ERR_ACTION
//#define QUECTEL_FATAL_ERR_DLOAD_IND          0X106 //enter DLOAD mode
//#define QUECTEL_FATAL_ERR_RESET_IND          0X107 //enter RESET mode
extern void quectel_register_fatal_inc_timer(void);
#endif

#ifdef QUECTEL_FLOWCTRL
//#define QUECTEL_FLOWCTRL_DISABLE_IND        0X108
//#define QUECTEL_FLOWCTRL_ENABLE_IND         0X109
#endif

extern void quectel_send_ind(uint16 msg_id);

typedef struct gpio_status_msg_data
{
	unsigned int gpio;
	unsigned int status;
	unsigned int on_time;	//0~60000 ms
	unsigned int of_time;	//0~60000 ms
} gpio_status_msg_data_type;

extern gpio_status_msg_data_type* quec_get_gpio_status(void);

#if 0
extern void quectel_register_ps_transfer_timer(void);
extern void quectel_start_ps_transfer_timer(void);
extern boolean  quectel_ps_transfer_timer_is_run(void);
#endif
//extern void set_ps_transfer_mode(boolean bFlag);

extern void  quectel_switch_ps_data_transfer(boolean on);

extern void quectel_netlight_set_net_mode_gpio(boolean on);

extern void quectel_netlight_net_status_control(void);

extern uint8 quectel_get_led_mode_value(void);
extern  void quectel_set_led_mode_value(uint8 mode);
extern void quectel_netlight_nv_init(void);

#ifdef QUECTEL_LED_MINIPCIE_MODE
#ifdef QUECTEL_NET_LED_LINUX //lory 2016/09/27
extern void quectel_qmi_nas_set_led_mode_ind(uint32 on_time,uint32 off_time);
extern void quectel_set_led_mode_time(uint32 on_time,uint32 off_time);

#endif
#endif
#endif //end QUECTEL_LED_H

