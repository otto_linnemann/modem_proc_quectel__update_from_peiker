#ifndef QUECTEL_MMS_DEFINE_H
#define QUECTEL_MMS_DEFINE_H

#include "comdef.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"


#if defined(QUECTEL_MMS_SUPPORT)


#define QUECTEL_MMS_TRANSCATIONID_MAX_LENGTH 50
#define QUECTEL_MMS_FROM_MAX_LENGTH                   50
#define QUECTEL_MMS_ADDRESS_MAX_LENGTH             50
#define QUECTEL_MMS_ADDRESS_MAX_NUMBER             6
#define QUECTEL_MMS_SUBJECT_MAX_LENGTH              202
//使用内存分配的方式
#define QUECTEL_MMS_SENDFILE_MAX_NUMBER             13               /*发送MMS最大的文件附件数*/
#define QUECTEL_MMS_SENDFILE_MAX_NAMELENGTH     90  //8.3格式  原来是13
#define QUECTEL_MMS_SENDFILE_MAX_FULLPATHLENGTH     (QUECTEL_MMS_SENDFILE_MAX_NAMELENGTH+10)  //全路径，包括SD: , RAM:
#define QUECTEL_MMS_MESSAGEID_MAX_LENGTH        30
#define QUECTEL_MMS_RESPONSETEXT_MAX_LENGTH   40
#define QUECTEL_MMS_CONTENTLOCATION_MAX_LENGTH 100
#define QUECTEL_MMS_CONTENTTYPETEXT_MAX_LENGTH  30
#define QUECTEL_MMS_MULTIPARTRELATEDSTART_MAX_LENGTH    30                                                            
#define QUECTEL_MMS_MULTIPARTRELATEDTYPE_MAX_LENGTH  30


#define QUECTEL_MMS_PROXY_PORT 80

#define CHARSET_ASCII_VALUE     0x03
#define CHARSET_UTF8_VALUE      0x6A
#define CHARSET_UCS2_VALUE      0x03E8
#define CHARSET_BIG5_VALUE       0x07EA
#define CHARSET_GBK_VALUE         0x71


#define MMS_FILE_TEXT_TYPE            1
#define MMS_FILE_IMAGE_TYPE          2
#define MMS_FILE_AUDIO_TYPE          3
#define MMS_FILE_VIDEO_TYPE           4
#define MMS_FILE_SMIL_TYPE             5


/******************************************************************************
* Types
******************************************************************************/
/* mms of heads */ 
#define 	MMS_MessageType 				0x8C 
#define 	MMS_Transaction_ID				0x98 
#define 	MMS_Version 					0x8D 
#define 	MMS_Date						0x85 
#define 	MMS_From						0x89 
#define 	MMS_To							0x97	 
#define 	MMS_Cc							0x82 
#define 	MMS_Bcc 						0x81 
#define 	MMS_Subject 					0x96 
#define 	MMS_Message_Class				0x8A 
#define 	MMS_Expiry					       0x88 
#define 	MMS_Delivery_Time			       0x87 
#define 	MMS_Priority				       0x8F 
#define 	MMS_Sender_Visibility		       0x94 
#define 	MMS_Delivery_Report 		       0x86 
#define 	MMS_Store					       0xA2 
#define 	MMS_State					       0xA3 
#define 	MMS_Stored					       0xA7 
#define 	Mms_Response_Status 		       0x92 
#define 	Mms_Response_Text			       0x93 
#define 	MMS_Message_ID				0x8B 
#define 	Mms_Content_Location		       0x83 
#define 	Mms_Store_Status			       0xA5 
#define 	Mms_Store_Status_Text		       0xA6 
#define 	Mms_Message_Size			       0x8E 
#define 	Mms_Distribution_Indicator	       0xB1 
#define 	Mms_Element_Descriptor		       0xB2 
#define 	Mms_Report_Allowed			       0x91 
#define 	Mms_Previously_Sent_By		       0xA0 
#define 	Mms_Previously_Sent_Date	       0xA1 
#define 	Mms_Read_Report 			       0x90 
#define 	Mms_Retrieve_Status 		       0x99 
#define 	Mms_Retrieve_Text			       0x9A 
#define 	MMS_Read_Status 			       0x9B 
#define 	MMS_Content_Type				0x84 
#define 	MMS_Status_value                          0x95


/* value of mms messagetype */ 
#define 	m_send_req					0x80 
#define 	m_send_conf 				0x81 
#define 	m_notification_ind			0x82 
#define 	m_notifyresp_ind			0x83 
#define 	m_retrieve_conf 			0x84 
#define 	m_acknowledge_ind			0x85 
#define 	m_delivery_ind				0x86 
#define 	m_read_rec_ind				0x87 
#define 	m_read_orig_ind 			0x88 
 
/* value of http content type ,the 'any' is mean of '*'	 */ 
#define 	content_type_any 										0x80 
#define 	content_type_text_any									0x81 
#define 	content_type_text_html									0x82 
#define 	content_type_text_plain									0x83 
#define 	content_type_text_x_hdml 								0x84 
#define 	content_type_text_x_ttml 								0x85 
#define 	content_type_text_x_vCalendar							0x86 
#define 	content_type_text_x_vCard								0x87 
#define 	content_type_text_vnd_wap_wml							0x88 
#define 	content_type_text_vnd_wap_wmlscript					0x89 
#define 	content_type_application_vnd_wap_catc					0x8A 
#define 	content_type_Multipart_any								0x8B 
#define 	content_type_Multipart_mixed 							0x8C 
#define 	content_type_Multipart_form_data 						0x8D 
#define 	content_type_Multipart_byteranges						0x8E 
#define 	content_type_multipart_alternative						0x8F 
#define 	content_type_application_any 							0x90 
#define 	content_type_application_java_vm 						0x91 
#define 	content_type_application_x_www_form_urlencoded		0x92 
#define 	content_type_application_x_hdmlc 						0x93 
#define 	content_type_application_vnd_wap_wmlc					0x94 
#define 	content_type_application_vnd_wap_wmlscriptc			0x95 
#define 	content_type_application_vnd_wap_wsic					0x96 
#define 	content_type_application_vnd_wap_uaprof				0x97 
#define 	content_type_application_vnd_wap_wtls_ca_certificate 	0x98 
#define 	content_type_application_vnd_wap_wtls_user_certificate	0x99 
#define 	content_type_application_x_x509_ca_cert					0x9A 
#define 	content_type_application_x_x509_user_cert				0x9B 
#define 	content_type_image_any									0x9C 
#define 	content_type_image_gif									0x9D 
#define 	content_type_image_jpeg								0x9E 
#define 	content_type_image_tiff									0x9F 
#define 	content_type_image_png									0xA0 
#define 	content_type_image_vnd_wap_wbmp						0xA1 
#define 	content_type_application_vnd_wap_multipart_any			0xA2 
#define 	content_type_application_vnd_wap_multipart_mixed 		0xA3 
#define 	content_type_application_vnd_wap_multipart_form_data 	0xA4 
#define 	content_type_application_vnd_wap_multipart_byteranges	0xA5 
#define 	content_type_application_vnd_wap_multipart_alternative	0xA6 
#define 	content_type_application_xml 							0xA7 
#define 	content_type_text_xml									0xA8 
#define 	content_type_application_vnd_wap_wbxml				0xA9 
#define 	content_type_application_vnd_wap_multipart_related 		0xB3 

/* value of http header name */ 
#define 	http_header_Accept										0x80 
#define 	http_header_Accept_Charset								0x81 
#define 	http_header_Accept_Encoding 							0x82 
#define 	http_header_Accept_Language 							0x83 
#define 	http_header_Accept_Ranges								0x84 
#define 	http_header_Age 										0x85 
#define 	http_header_Allow										0x86 
#define 	http_header_Authorization								0x87 
#define 	http_header_Cache_Control								0x88 
#define 	http_header_Connection									0x89 
#define 	http_header_Content_Base								0x8A 
#define 	http_header_Content_Encoding							0x8B 
#define 	http_header_Content_Language							0x8C 
#define 	http_header_Content_Length								0x8D 
#define 	http_header_Content_Location							0x8E 
#define 	http_header_Content_MD5 								0x8F 
#define 	http_header_Content_Range								0x90 
#define 	http_header_Content_Type_Single 						0x91 
#define 	http_header_Date_Single 								0x92 
#define 	http_header_Etag										0x93 
#define 	http_header_Expires 									0x94 
#define 	http_header_From_Single 								0x95 
#define 	http_header_Host										0x96 
#define 	http_header_If_Modified_Since							0x97 
#define 	http_header_If_Match									0x98 
#define 	http_header_If_None_Match								0x99 
#define 	http_header_If_Range									0x9A 
#define 	http_header_If_Unmodified_Since 						0x9B 
#define 	http_header_Location									0x9C 
#define 	http_header_Last_Modified								0x9D 
#define 	http_header_Max_Forwards								0x9E 
#define 	http_header_Pragma										0x9F 
#define 	http_header_Proxy_Authenticate							0xA0 
#define 	http_header_Proxy_Authorization 						0xA1 
#define 	http_header_Range										0xA3 
#define 	http_header_Referer 									0xA4 
#define 	http_header_Retry_After 								0xA5 
#define 	http_header_Server										0xA6 
#define 	http_header_Transfer_Encoding							0xA7 
#define 	http_header_Upgrade 									0xA8 
#define 	http_header_User_Agent									0xA9 
#define 	http_header_Vary										0xAA 
#define 	http_header_Via 										0xAB 
#define 	http_header_Warning 									0xAC 
#define 	http_header_WWW_Authenticate							0xAD 
#define 	http_header_Content_Disposition 							0xAE 
#define 	http_header_X_Wap_Application_Id						0xAF 
#define 	http_header_X_Wap_Content_URI							0xB0 
#define 	http_header_X_Wap_Initiator_URI 						0xB1 
#define 	http_header_Accept_Application							0xB2 
#define 	http_header_Bearer_Indication							0xB3 
#define 	http_header_Push_Flag									0xB4 
#define 	http_header_Profile 										0xB5 
#define 	http_header_Profile_Diff									0xB6 
#define 	http_header_Profile_Warning 							0xB7 
#define 	http_header_Content_ID									0xC0 

 
typedef enum
{
	APP_MMS_RESULT_OK =0,
	APP_MMS_RESULT_UNKNOW_ERROR = -1,
	APP_MMS_RESULT_ERROR_WOULDBLOCK = -2,
	APP_MMS_RESULT_ERROR_URL_LENGTHERROR = -3,
       APP_MMS_RESULT_ERROR_URL_ERROR = -4,       /*failed to parse domain name*/
       APP_MMS_RESULT_ERROR_PROXYTYPE_NOSUPPORT = -5,   /*failed to establish socket or the network is deactivated*/
       APP_MMS_RESULT_ERROR_PROXYADDR_ERROR = -6,
       APP_MMS_RESULT_ERROR_PARAM_ERROR = -7,
       APP_MMS_RESULT_ERROR_PARAM_TOADDR_FULL = -8,  
       APP_MMS_RESULT_ERROR_PARAM_CCADDR_FULL = -9,
       APP_MMS_RESULT_ERROR_PARAM_BCCADDR_FULL = -10,
       APP_MMS_RESULT_ERROR_PARAM_BODYFILE_FULL = -11,
       APP_MMS_RESULT_ERROR_FILE = -12,
       APP_MMS_RESULT_ERROR_PARAM_EMTPY_TOADDR = -13,
       APP_MMS_RESULT_ERROR_FILE_NOT_FOUND = -14,
       APP_MMS_RESULT_ERROR_MMS_BUSY = -15,
       APP_MMS_RESULT_ERROR_HTTP_RESPONSE_FAILED = -16,
       APP_MMS_RESULT_ERROR_MMS_POST_RESPONS_INVALIDMSG = -17,
       APP_MMS_RESULT_ERROR_MMS_POST_RESPONSE_REPORTERROR =  -18,
       APP_MMS_RESULT_ERROR_NETWORK_OPENFAILED = -19,
       APP_MMS_RESULT_ERROR_NETWORK_ERROR = -20,
       APP_MMS_RESULT_ERROR_SOC_CREATEFAILED = -21,
       APP_MMS_RESULT_ERROR_SOC_CONNECTFAILED = -22,
       APP_MMS_RESULT_ERROR_SOC_READFAILED = -23,
       APP_MMS_RESULT_ERROR_SOC_WRITEFAILED = -24,
       APP_MMS_RESULT_ERROR_SOC_CLOSE = -25,
       APP_MMS_RESULT_ERROR_TIMEOUT = -26,   
       APP_MMS_RESULT_ERROR_ENCODE_DATAFAILED = -27,  
       APP_MMS_RESULT_ERROR_HTTP_DECODEERROR = -28,
       /*
       APP_MMS_RESULT_ERROR_DECODE_DATAFAILED = -28, 
       APP_MMS_RESULT_ERROR_PDU_INPUTALIGNERROR = -29,
       APP_MMS_RESULT_ERROR_PDU_INPUTCHARERROR = -30, 
       APP_MMS_RESULT_ERROR_MMS_NOTEXIST = -31,
       APP_MMS_RESULT_ERROR_INVALIDADDRESS = -32,
       APP_MMS_RESULT_ERROR_VOICEBUSY = -33,
       APP_MMS_RESULT_ERROR_ALLOC_MEMORY= -34,
       APP_MMS_RESULT_ERROR_ENCODE_GETMMSLENTHFAILED = -35,
       APP_MMS_RESULT_ERROR_SMS_DECODEERROR = -36,
       APP_MMS_RESULT_ERROR_MMS_RECEIVEFULL = -37,
       */
}mms_error_code;


typedef enum
{
 	MMS_STATUS_IDLE = 0,
 	MMS_STATUS_PDP_ACTIVING,
 	MMS_STATUS_DNS_PARSE,
	MMS_STATUS_CONNECTING_TO_PROXY,
	MMS_STATUS_CONNECTED,
	
 	MMS_STATUS_CLOSING,
 	MMS_STATUS_CLOSED = MMS_STATUS_IDLE
}mms_service_state;


typedef enum 
{
    MMS_PARAM_VALID_1HOUR=0,
    MMS_PARAM_VALID_12HOURS,
    MMS_PARAM_VALID_24HOURS,
    MMS_PARAM_VALID_2DAYS,
    MMS_PARAM_VALID_1WEEK,
    MMS_PARAM_VALID_MAXIMUM,
    MMS_PARAM_VALID_DEFAULT
}mms_valid_type;

typedef enum 
{
    MMS_PARAM_PRIORITY_LOWEST=0,
    MMS_PARAM_PRIORITY_MORMAL,
    MMS_PARAM_PRIORITY_HIGHEST,
    MMS_PARAM_PRIORITY_DEFAULT
}mms_priority_type;

typedef enum 
{
    MMS_PARAM_SENDERADDR_HIDE=0,
    MMS_PARAM_SENDERADDR_SHOW,
    MMS_PARAM_SENDERADDR_DEFAULT
}mms_visible_type;

typedef enum
{
    MMS_PARAM_CLASS_PERSONAL=0,
    MMS_PARAM_CLASS_ADVERTISEMENT,
    MMS_PARAM_CLASS_INFORMATIONAL,
    MMS_PARAM_CLASS_AUTO,
    MMS_PARAM_CLASS_DEFAULT
}mms_class_type;

typedef enum 
{
    MMS_CODERESULT_OK                     =0,  
    MMS_CODERESULT_END                    =1, /*结束*/
    MMS_CODERESULT_BUFFERFULL     = 2, /*buffer满了*/
    MMS_CODERESULT_DATAABSENT    = 3, /*数据不够*/
    MMS_CODERESULT_ERROR              = -1, 
    MMS_CODERESULT_ERROR_FILEACCESS   =  -2, /*文件访问错误*/
    MMS_CODERESULT_ERROR_MMSFILED  =   -3, /*MMS 字段有问题*/
    NUM_OF_MMS_CODERESULT
}mms_code_result_type;


typedef enum 
{
    ENCODE_END = -1,
    ENCODE_MessageType = 0,/*send req step*/
    ENCODE_Transaction_ID,
    ENCODE_Version,
    ENCODE_Date,
    ENCODE_From,
    ENCODE_To,
    ENCODE_Cc,
    ENCODE_Bcc, 
    ENCODE_Subject, 
    ENCODE_Message_Class, 
    ENCODE_MMS_Expiry, 
    ENCODE_MMS_Delivery_Time, 
    ENCODE_MMS_Priority, 
    ENCODE_MMS_Sender_Visibility, 
    ENCODE_MMS_Delivery_Report,
    ENCODE_Mms_Read_Report,
    ENCODE_Content_Type,
    ENCODE_BODY_Context_Type,/*body step*/
    ENCODE_BODY_Context_Location,
    ENCODE_BODY_Context_ID,
    ENCODE_BODY_Data,
    ENCODE_Status_value,
    NUM_OF_ENCODE
}mms_header_id_type;

typedef struct 
{
    const char  *charsetname;
    Q_UINT16    charsetvalue;
    Q_BOOL      tail00;
}mms_charset_type;

typedef struct 
{
    const char  *file_ext;
    const char  *mime_string;
    Q_UINT8   filetype;
    Q_UINT8  well_known_media;
}mms_file_mime_t;


typedef enum 
{
  MMS_RESPONSE_OK               = 0x80,
  MMS_RESPOSNE_ERR_UNSPECIFIED  = 0x81,
  MMS_RESPONSE_ERR_DENIED       = 0x82, 
  MMS_RESPONSE_ERR_MMS_FORMAT   = 0x83,
  MMS_RESPONSE_ERR_MMS_ADDRESS  = 0x84,
  MMS_RESPONSE_ERR_NOT_FOUND    = 0x85,
  MMS_RESPONSE_ERR_NETWORK      = 0x86,
  MMS_RESPONSE_ERR_CONTENT      = 0x87,
  MMS_RESPONSE_ERR_UNSUPPORTED  = 0x88
}mms_response_status_type;

typedef enum 
{
    MMS_ADDRESS_TYPE_INVALID = 0,
    MMS_ADDRESS_TYPE_NUMBER= 1,
    MMS_ADDRESS_TYPE_EMAIL,
    MMS_ADDRESS_TYPE_OTHER
}mms_address_type;

typedef struct 
{
    Q_UINT8                       address[QUECTEL_MMS_ADDRESS_MAX_LENGTH+1];        /*使用内存分配的方式*/
    Q_UINT16                     addresslength;/*这个字段可以不用*/
    mms_address_type       addresstype;
}mms_address;


typedef struct
{
	Q_UINT8                     		    subject[QUECTEL_MMS_SUBJECT_MAX_LENGTH+1]; /*主题*//*解码可以使用*/
       Q_UINT16                               subject_charset;
       Q_UINT32                               subject_length;/*注意subject 和subject_length得内容和长度都要包含\0 或\0\0(USC2)*/
}mms_subject;

typedef struct
{
	Q_UINT8                       exits_flag;   
	Q_UINT32                     data_length;    
	Q_UINT8                       context_type;
    	Q_UINT8                       context_type_Text[QUECTEL_MMS_CONTENTTYPETEXT_MAX_LENGTH+1]; /*假如是SMIL, 用MMS_HEADER_SMIL_TYPE[] = "application/smil\0";*/
       Q_UINT8                       sendfilenamefullpath[QUECTEL_MMS_SENDFILE_MAX_FULLPATHLENGTH+1]; //包括SD:, RAM: 等盘符信息
       Q_UINT8                       sendfilename[QUECTEL_MMS_SENDFILE_MAX_NAMELENGTH+1];
       Q_UINT16                     charset;     
       Q_UINT8                       content_location[QUECTEL_MMS_CONTENTLOCATION_MAX_LENGTH+1];
       Q_UINT8                       content_id[QUECTEL_MMS_CONTENTLOCATION_MAX_LENGTH+1];
}mms_body;

typedef struct   
{ 
	Q_INT32 year; 
	Q_INT32 month; 
	Q_INT32 day; 
	Q_INT32 hour; 
	Q_INT32 minute; 
	Q_INT32 second; 
}mms_date; 

typedef struct
{
	Q_UINT8                                 mms_url[256];
	Q_UINT8                                 mms_proxy[256];
	Q_UINT16                               mms_proxy_port;
	Q_UINT16                               mms_charset;
	Q_UINT8                                 validity_period;
	Q_UINT8                                 priority;
	Q_UINT8                                 delivery_rpt;
	Q_UINT8                                 read_rpt;
	Q_UINT8                                 class;
	Q_UINT8                                 visible;
}mms_common_cfg;

typedef struct
{   
       mms_response_status_type      response_status;/*解码时使用*/
	Q_UINT8                     		    messagetype;/*加0x80的值*//*解码可以使用*/
    	Q_UINT8                     		    version;/*编码时强制0x90*//*解码可以使用*/
    	Q_UINT8                     		    transcationid[QUECTEL_MMS_TRANSCATIONID_MAX_LENGTH+1];/*解码可以使用*/
   	Q_UINT8                    		    context_type; /*编码时需要初始化*//*解码时使用application_vnd_wap_multipart_mixed, application_vnd_wap_multipart_related*/
    	Q_UINT8                    		    context_type_text[QUECTEL_MMS_CONTENTTYPETEXT_MAX_LENGTH+1];/*解码时使用,context_type的text方式*/
    	Q_UINT8                                 multipart_related_start[QUECTEL_MMS_MULTIPARTRELATEDSTART_MAX_LENGTH+1] ;  /*multipart_related 编解码时使用*/                                                                               
    	Q_UINT8                                 multipart_related_type[QUECTEL_MMS_MULTIPARTRELATEDTYPE_MAX_LENGTH+1] ;   /*multipart_related 编解码时使用*/                                                                                
       Q_UINT8                                 mms_body_index;//current body index
       Q_UINT8                                 message_id[QUECTEL_MMS_MESSAGEID_MAX_LENGTH+1];
	Q_UINT8                                 response_text[QUECTEL_MMS_RESPONSETEXT_MAX_LENGTH+1];
	Q_UINT8                                 content_location[QUECTEL_MMS_CONTENTLOCATION_MAX_LENGTH+1];/*解码SMS PUSH 时得到下载中心的URL*/
	Q_UINT8                                 from[QUECTEL_MMS_FROM_MAX_LENGTH+1];/*解码可以使用*/
       mms_date                               date;
       Q_INT32                                 message_size;
	mms_address				    to_addr[QUECTEL_MMS_ADDRESS_MAX_NUMBER];
	Q_UINT32                                to_addr_num;
	mms_address                          cc_addr[QUECTEL_MMS_ADDRESS_MAX_NUMBER];
	Q_UINT32                                cc_addr_num;
	mms_address				    bcc_addr[QUECTEL_MMS_ADDRESS_MAX_NUMBER];
	Q_UINT32                                bcc_addr_num;
       Q_UINT16                               common_charset;     /*其它信息的charset,譬如from , Response_Text, 等*/
	mms_subject                           subject;
	Q_UINT8                                 status_value;/*发送m_notifyresp_ind时用*/
	Q_UINT8                                 validity_period;
	Q_UINT8                                 priority;
	Q_UINT8                                 delivery_rpt;
	Q_UINT8                                 read_rpt;
	Q_UINT8                                 class;
	Q_UINT8                                 visible;
	mms_body                              body[QUECTEL_MMS_SENDFILE_MAX_NUMBER];
	Q_UINT32                     	    body_number;/*编码或解码时设置，获得*/
}mms_message_cfg;


typedef struct
{
    Q_UINT16     encodestep;
    Q_INT32       *encode_arrange;
    Q_INT32       param1;
    Q_INT32       param2;
    Q_BOOL        encode_end;/*表示encode结束*/    
    Q_UINT32     currentmmsbody;
}mms_encode_info;


typedef struct 
{
    Q_UINT8    	   messagetype;
    Q_UINT16 	   decodestep;/*解码时mmsDecodeMmsFile 使用*/
    Q_UINT32     decodedatalen;
    Q_UINT8       http_chunked; /*香港的彩信下载使用TRANSFER-ENCODING: CHUNKED ，所以下载这类彩信时要chunked解码*/
    Q_UINT32      chunked_size;
    Q_UINT32      mms_total_length;/*下载时作为mms head + body 大小，由http header分析得来*/
                                                                    /*接收时作为下载数据量*/
    Q_UINT32      decodebodynumber;/*解码body时解码body个数*/
    Q_UINT32      offsetinfile;/*解码时保存mms中偏移位置*/
    Q_UINT32      param1;
    Q_UINT32      param2;
    int                 mmsfilehandle;/*下载时作为mms 文件句柄*/
}mms_decode_info;



typedef struct
{
 	mms_service_state                  state;
 	Q_UINT8                                 pdp_id;
  	Q_INT32                                 sockfd;
	mms_common_cfg                  common_cfg;
	mms_message_cfg                  message_cfg;
	apptcpip_timer_id                    message_send_timer;
	Q_UINT32                               message_send_timeout;
	http_header_struct                   http_header;
	http_encode_info                     http_encoder; 
	http_decode_info                     http_decoder; 
	mms_encode_info                    mms_encoder;
	mms_decode_info                    mms_decoder;
	Q_UINT8                                 mms_send_partion; //0: send_http_header; 1: send_mms_header; 2:send_mms_body
	Q_UINT8                                 mms_recv_partion; //0: recv_http_header; 1: recv_mms_header
	Q_BOOL                                  soc_write_block;
	dsm_item_type*                      message_send_item;
	dsm_item_type*                      message_rsp_item;
	quectel_mms_event_cb            event_cb;
#ifdef QUECTEL_MMS_ADD_X_UP_CALLING_ID_FIELD
    int                             x_up_calling_id;
#endif
}mms_service_context;
#endif//QUECTEL_MMS_SUPPORT

#endif//QUECTEL_MMS_DEFINE_H
