#ifndef QUECTEL_MMS_UTIL_H
#define  QUECTEL_MMS_UTIL_H

#if defined(QUECTEL_MMS_SUPPORT)

Q_INT32 quectel_mms_is_multipart_mixed
(
void
); 

Q_BOOL quectel_mms_msg_addr_is_email
(
Q_UINT8 *msg_addr
);

Q_BOOL quectel_mms_is_phone_number
(
Q_UINT8 *number
);

Q_BOOL quectel_mms_is_usc2endstring
(
Q_UINT16 charset
);

Q_UINT32 quectel_mms_get_file_content_type
(
Q_UINT8 *filename
);

Q_BOOL  quectel_mms_is_txt_file
(
Q_UINT8 *filename
);

Q_INT32  quectel_mms_get_charset_val
(
Q_UINT8  *charset_name,
Q_UINT16  *charset_val
);

Q_INT32 quectel_mms_get_charset_name
(
Q_UINT16 charset,
Q_UINT8 **charset_name
);

Q_INT32  quectel_mms_convert_hex_str_to_value
(
Q_CHAR *hex_str, 
Q_CHAR *hex_value, 
Q_UINT32 max_output_len
);

Q_INT32  quectel_mms_convert_hex_value_to_str
(
byte *hex_value,
byte *hex_str,  
uint32 max_output_len
);

Q_UINT8 quectel_mms_digits_number
(
Q_UINT32 val
);


Q_UINT8  quectel_mms_get_next_mms_body
(
Q_UINT8 index, 
mms_message_cfg *mms_msg
);

Q_UINT8  quectel_mms_get_next_free_mms_body
(
Q_UINT8 index, 
mms_message_cfg *mms_msg
);

Q_UINT32 quectel_mms_getgiletype
(
Q_UINT8 *filename
);

#endif

#endif//QUECTEL_MMS_UTIL_H

