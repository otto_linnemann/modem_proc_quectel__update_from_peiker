#ifndef QUECTEL_MMS_APP_H
#define  QUECTEL_MMS_APP_H

#if defined(QUECTEL_MMS_SUPPORT)


typedef enum
{
	QUECTEL_MMS_PDP_ID_CFG = 0,
	QUECTEL_MMS_MMSC_CFG,
	QUECTEL_MMS_PROXY_CFG,
	QUECTEL_MMS_CHARSET_CFG,
	QUECTEL_MMS_VALID_PERIOD_CFG,
	QUECTEL_MMS_PRIORITY_CFG,
	QUECTEL_MMS_CLASS_CFG,
	QUECTEL_MMS_VISIBLE_CFG,
	QUECTEL_MMS_SEND_RPT_CFG,
	QUECTEL_MMS_READ_RPT_CFG
#ifdef QUECTEL_MMS_ADD_X_UP_CALLING_ID_FIELD
    ,QUECTEL_MMS_SUPPORT_FIELD_CFG
#endif
}quectel_mms_common_cfg_type;


typedef enum
{
	QUECTEL_MMS_TO_ADDR = 1,
	QUECTEL_MMS_CC_ADDR = 2,
	QUECTEL_MMS_BCC_ADDR = 3
}quectel_mms_msg_addr_type;


typedef enum
{
	QUECTEL_MMS_ADDR_READ,
	QUECTEL_MMS_TITLE_READ,
	QUECTEL_MMS_ATTACHMENT_READ,
	QUECTEL_MMS_SEND_RSP
}quectel_mms_event_enum;

typedef struct
{
	Q_INT32            result;
	Q_UINT8            addr_type;
	Q_BOOL             exit_flag;
	Q_BOOL             begin;
	Q_BOOL             end;
	Q_UINT8            addr[128];
}quectel_mms_addr_info;


typedef struct
{
	Q_INT32            result;
	Q_BOOL             exit_flag;
	Q_UINT8            charset_name[40];
	Q_UINT8            title[256];
}quectel_mms_title_info;



typedef struct
{
	Q_INT32            result;
	Q_BOOL             exit_flag;
	Q_BOOL             begin;
	Q_BOOL             end;
	Q_UINT8            attachment[256];
}quectel_mms_attachment_info;

typedef struct
{
	Q_INT32          result;
	Q_INT32          protocol_result;
	Q_UINT8          mms_error_msg[256];
}quectel_mms_send_rsp_info;


typedef struct
{
	Q_UINT8   *proxy_addr;
	Q_UINT16   proxy_port;
}quectel_mms_proxy;

typedef void (*quectel_mms_event_cb)(Q_INT32 mms_ctx, quectel_mms_event_enum event, void *event_ptr);


Q_INT32  quectel_mms_ctx_get
(
quectel_mms_event_cb   cb
);


Q_INT32    quectel_mms_common_configuration_set
(
Q_INT32     mms_ctx,
Q_UINT8     opt_name,
void           *opt_val
);

Q_INT32   quectel_mms_common_configuration_get
(
Q_INT32    mms_ctx,
Q_UINT8    opt_name,
void          *opt_val
);


Q_INT32  quectel_mms_add_msg_addr
(
Q_INT32    mms_ctx,
Q_UINT8   addr_type,
Q_UINT8  *msg_addr
);

Q_INT32  quectel_mms_delete_msg_addr
(
Q_INT32    mms_ctx,
Q_UINT8   addr_type
);

Q_INT32   quectel_mms_read_msg_addr
(
Q_INT32    mms_ctx,
Q_UINT8   addr_type
);

Q_INT32  quectel_mms_add_msg_title
(
Q_INT32    mms_ctx,
Q_UINT8  *msg_title
);

Q_INT32  quectel_mms_delete_msg_title
(
Q_INT32    mms_ctx
);

Q_INT32   quectel_mms_read_msg_title
(
Q_INT32    mms_ctx
);

Q_INT32  quectel_mms_add_msg_attachment
(
Q_INT32    mms_ctx,
Q_UINT8  *msg_attachment
);

Q_INT32  quectel_mms_delete_msg_attachment
(
Q_INT32    mms_ctx
);

Q_INT32   quectel_mms_read_msg_attachment
(
Q_INT32    mms_ctx
);

Q_INT32 quectel_mms_send_message
(
Q_INT32   mms_ctx,
Q_UINT32 send_timeout
);

#endif//QUECTEL_MMS_SUPPORT


#endif//QUECTEL_MMS_APP_H

