#ifndef QUECTEL_MMS_DECODE_H
#define  QUECTEL_MMS_DECODE_H

#if defined(QUECTEL_MMS_SUPPORT)

Q_INT32  quectel_mms_decode_header
(
mms_message_cfg     *mms_message,
mms_decode_info      *mms_decoder,
Q_UINT8                   *decode_buffer,
Q_INT32                     decode_buffer_length, 
Q_INT32                    *decode_length,
Q_UINT8                   *current_type
);


#endif//QUECTEL_MMS_SUPPORT

#endif//QUECTEL_MMS_DECODE_H
