#ifndef   QUECTEL_MMS_ENCODE_H
#define    QUECTEL_MMS_ENCODE_H


#if defined(QUECTEL_MMS_SUPPORT)


Q_INT32  quectel_mms_encode_header
(
mms_message_cfg     *mms_message,
mms_encode_info      *mms_encoder,
Q_UINT8                   *encode_buffer,
Q_INT32                     encode_buffer_length, 
Q_INT32                    *encode_length
);

Q_INT32  quectel_mms_encode_body
(
mms_message_cfg     *mms_message,
mms_encode_info     *mms_encoder,
Q_UINT8                  *encode_buffer,
Q_INT32                    encode_buffer_length, 
Q_INT32                   *encode_length
);

#endif//QUECTEL_MMS_SUPPORT

#endif//QUECTEL_MMS_ENCODE_H

