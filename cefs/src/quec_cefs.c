/*!	@file
	quec_cefs.c
	@brief
	This file provides apis to backup cefs, and restore cefs.
*/
/*===========================================================================
	Copyright(c) 2018 Quectel Wierless Solution,Co.,Ltd. All Rights Reserved.
	Quectel Wireless Solution Proprietary and Confidential.
============================================================================*/
/*===========================================================================
							EDIT HISTORY FOR MODULE
This section contains comments describing changees made to the module.
Notice that changes are listed in reverse chronological order.
WHEN		 WHO		WHAT,WHERE,WHY
----------	-----		----------------------------------------------------
04/17/2018	Ramos		fix the cefs can't backup when the beackup info Area first block is a bad block
01/05/2019	Ramos		optimize that  if backup data is invaild , set cefs flag funciton return, not in reboot system
01/10/2019	Ramos		add verion number for QPRTPARA,  v1.0 version must include efs imei, nv10, nv6828 /lte_bandpref check .
01/12/2019	Ramos		add cefs  directory completeness check function, if one of directory lost (directory in EFS_file_check_list[] list), restore cefs.
============================================================================*/

#include "quec_cefs.h"
#include "../../core/storage/efs/src/fs_global_lock.h"
#include "fs_sys_types.h"
#include "../../core/storage/efs/inc/fs_util.h"
#include "fs_public.h"
#include "../../core/storage/efs/inc/fs_priv_funcs.h"
#include "nv_items.h"
#include "dsati.h"
#include "../../core/storage/flash/src/dal/flash_dal.h"
#include "../../core/storage/efs/src/fs_flash_dal_shim.h"
#include "dog.h"
#ifdef QUECTEL_CEFS_COMPLETENESS_CHECK
#include "fs_sys_types.h"
#include "mcfg_fs.h"
#endif

#ifdef QUECTEL_BACKUP_CEFS

#define CEFS_FILE_HANDLE        (333)
#define DATA_CACHE_LEN          (4200) //一个page 4096个字节
#define CEFS_FILE_MAGIC1        (0x51D24368)
#define CEFS_FILE_MAGIC2        (0x4378AC6E)

#define QUEC_BACKUP_MAGIC1        (0x78E5D4C2)
#define QUEC_BACKUP_MAGIC2        (0x54F7D60E)
#define QUEC_BACKUP_INFO_BLOCK_NUMS (3)

extern void quec_disable_dog(void);
extern uint32 crc_32_calc
(
  /* Pointer to data over which to compute CRC */
  uint8  *buf_ptr,

  /* Number of bits over which to compute CRC */
  uint16  len,

  /* Seed for CRC computation */
  uint32  seed
);

static int s_backup_info_start_block_id;
static int s_backup_info_end_block_id;

static int s_data_start_block_id;
static int s_data_end_block_id;

static char s_buff_temp[DATA_CACHE_LEN];

static quec_cefs_file_context s_cefs_context = {0};

#ifdef QUECTEL_CEFS_COMPLETENESS_CHECK
/* 注意:   EFS_file_check_list  这个table 表一旦固定后不能在list 里增加任何文件
不然已经量产出货的版本，升级到修改后版本模块会不停的还原
*/
char *EFS_file_check_list[]=
{
"/acdb/Q",
"/apn_throttle/Q",
"/cert/Q",
"/cgps/nv/item_files/ulp/tle/Q",
"/CGPS_ME/Q",
"/client-cert/Q",
"/client-key/Q",
"/data/Q",
"/ds/atcop/strings/Q",
"/gba/Q",
"/hdr/Q",
"/ims/Q",
"/mmode/cmcall/Q",
"/mmode/cmmm/Q",
"/mmode/cmph/Q",
"/mmode/cmss/Q",
"/nv/item_files/conf/Q",
"/nv/item_files/CoreCpu/CoreAll/Startup/Q",
"/nv/item_files/CoreCpu/Default/Fixed/Q",
"/nv/item_files/CoreCpu/Default/qdsp_classic/Q",
"/nv/item_files/CoreCpu/Default/Startup/Q",
"/nv/item_files/data/3gpp/Q",
"/nv/item_files/data/3gpp2/Q",
"/nv/item_files/data/common/Q",
"/nv/item_files/gps/cgps/sm/Q",
"/nv/item_files/gps/cgps/ulp/tle/Q",
"/nv/item_files/ims/Q",
"/nv/item_files/mcfg/Q",
"/nv/item_files/mcs/cfcm/Q",
"/nv/item_files/mcs/cxm/Q",
"/nv/item_files/mcs/lmtsmgr/battery/Q",
"/nv/item_files/mcs/lmtsmgr/coex/desense/Q",
"/nv/item_files/mcs/lmtsmgr/sar/Q",
"/nv/item_files/mcs/lmtsmgr/sem/Q",
"/nv/item_files/mcs/tcxomgr/Q",
"/nv/item_files/mcs/trm/Q",
"/nv/item_files/modem/data/3gpp/lteps/Q",
"/nv/item_files/modem/data/3gpp/ps/Q",
"/nv/item_files/modem/data/protocols/Q",
"/nv/item_files/modem/ecall/Q",
"/nv/item_files/modem/geran/grr/Q",
"/nv/item_files/modem/lte/common/Q",
"/nv/item_files/modem/lte/ML1/Q",
"/nv/item_files/modem/lte/rrc/cap/Q",
"/nv/item_files/modem/lte/rrc/cep/Q",
"/nv/item_files/modem/lte/rrc/irat/Q",
"/nv/item_files/modem/mmode/sd/Q",
"/nv/item_files/modem/nas/Q",
"/nv/item_files/modem/qmi/cat/Q",
"/nv/item_files/modem/qmi/uim/Q",
"/nv/item_files/modem/sms/Q",
"/nv/item_files/modem/tdscdma/l1/Q",
"/nv/item_files/modem/tdscdma/rrc/Q",
"/nv/item_files/modem/uim/gstk/Q",
"/nv/item_files/modem/uim/mmgsdi/Q",
"/nv/item_files/modem/uim/uimdrv/Q",
"/nv/item_files/modem/utils/cfm/Q",
"/nv/item_files/pbm/Q",
"/nv/item_files/quectel/Q",
"/nv/item_files/rfnv/Q",
"/nv/item_files/Thin_UI/Q",
"/nv/item_files/wcdma/cme/Q",
"/nv/item_files/wcdma/fet/Q",
"/nv/item_files/wcdma/idle/Q",
"/nv/item_files/wcdma/irat/Q",
"/nv/item_files/wcdma/l1/dl/Q",
"/nv/item_files/wcdma/l1/srch/Q",
"/nv/item_files/wcdma/l1/ul/Q",
"/nv/item_files/wcdma/l1utils/Q",
"/nv/item_files/wcdma/rach/Q",
"/nv/item_files/wcdma/rrc/Q",
"/nv/item_files/wcdma/rxd/Q",
"/nv/item_store/rfnv/Q",
"/nv/reg_files/modem/lte/rrc/csp/Q",
"/nvruim/Q",
"/nvm/num/Q",
"/pdp_profiles/Q",
"/policyman/Q",
"/quecsfs/data/Q",
"/rfc/rffe_scan/Q",
"/rpm/Q",
"/scrub/logs/err/Q",
"/scrub/logs/msg/Q",
"/sd/Q",
"/sms/Q",
"/SUPL/Q",
"/Q",
NULL
};
#endif

extern int efs_stat (const char *path, struct fs_stat *buf);

extern int quec_erase_block(flash_handle_t handle_flash, int block_id);
extern int quec_flash_write_page (flash_handle_t handle, page_id page, void *data);
extern int quec_flash_read_page (flash_handle_t handle, page_id page, void *data);

static quec_cefs_file_context_t get_cefs_file_context(quec_cefs_file_handle_type handle)
{
  if (CEFS_FILE_HANDLE == handle)
    return &s_cefs_context;

  return NULL;
}

uint32 get_page_size(quec_flash_context_t flash_context)
{
    return flash_context->flash_client_ctxt->client_data.page_size_bytes;
}

uint32 get_block_count (quec_flash_context_t flash_context)
{
  return flash_context->flash_client_ctxt->client_data.block_count;
}

uint32 get_pages_per_block (quec_flash_context_t flash_context)
{
  return flash_context->flash_client_ctxt->client_data.pages_per_block;
}

boolean bad_block_check(quec_flash_context_t flash_context, int block_id)
{  
  enum flash_block_state block_state = FLASH_BLOCK_BAD;

  flash_block_get_state(flash_context, block_id, &block_state);

  return (block_state == FLASH_BLOCK_OK);  
}

static quec_flash_context_t get_cefs_flash_context(quec_cefs_file_handle_type handle)
{
  if (CEFS_FILE_HANDLE == handle)
    return s_cefs_context.flash_context;

  return NULL;
}

static int find_next_good_block_backward(quec_flash_context_t flash_context, int block_id, int end_block_id)
{
  while (!bad_block_check(flash_context, block_id))
  {          
    block_id--;
    if (block_id < end_block_id)
    {
      return -1;
    }
  }
  
  return block_id;
}

static int get_and_erase_block_backward(quec_flash_context_t flash_context, int block_id, int end_block_id)
{
  while(block_id >= end_block_id)
  {      
    block_id = find_next_good_block_backward(flash_context, block_id, end_block_id);
    if (block_id < end_block_id)
    {
      return FS_DEVICE_FAIL;
    }
  
    if (quec_erase_block(flash_context, block_id) == FS_DEVICE_OK)
    {
      return block_id;
    }
    
    flash_block_set_state(flash_context, block_id, FLASH_BLOCK_BAD);
    block_id--;
  }
  return FS_DEVICE_FAIL;
}

static int find_next_good_block_forward(quec_flash_context_t flash_context, int block_id)
{
  while (!bad_block_check(flash_context, block_id))
  {          
    block_id++;
    if (block_id > s_data_end_block_id)
    {
      return -1;
    }
  }
  
  return block_id;
}

static int get_and_erase_block_forward(quec_flash_context_t flash_context, int block_id, int end_block_id)
{
  while(block_id <= end_block_id)
  {      
    block_id = find_next_good_block_forward(flash_context, block_id);
    if (block_id > end_block_id)
    {
      return FS_DEVICE_FAIL;
    }
  
    if (quec_erase_block(flash_context, block_id) == FS_DEVICE_OK)
    {
      return block_id;
    }
    
    flash_block_set_state(flash_context, block_id, FLASH_BLOCK_BAD);
    block_id++;
  }
  return FS_DEVICE_FAIL;
}

static int find_backupInfoArea_next_good_block_forward(quec_flash_context_t flash_context, int block_id)
{
  while (!bad_block_check(flash_context, block_id))
  {          
    block_id++;
    if (block_id > s_backup_info_end_block_id)
    {
      return -1;
    }
  }
  
  return block_id;
}

static int get_and_backupInfoArea_erase_block_forward(quec_flash_context_t flash_context, int block_id, int end_block_id)
{
  while(block_id <= end_block_id)
  {      
    block_id = find_backupInfoArea_next_good_block_forward(flash_context, block_id);
    if (block_id > end_block_id)
    {
      return FS_DEVICE_FAIL;
    }
  
    if (quec_erase_block(flash_context, block_id) == FS_DEVICE_OK)
    {
      return block_id;
    }
    
    flash_block_set_state(flash_context, block_id, FLASH_BLOCK_BAD);
    block_id++;
  }
  return FS_DEVICE_FAIL;
}


static boolean init_cefs_backup_info_context(quec_cefs_file_handle_type handle)
{
  quec_cefs_file_context_t cefs_context = get_cefs_file_context(handle);
  quec_flash_context_t flash_context = get_cefs_flash_context(handle);
  int block_id = 0;

   s_backup_info_end_block_id= get_block_count(flash_context) - 1;
   s_backup_info_start_block_id= s_backup_info_end_block_id - QUEC_BACKUP_INFO_BLOCK_NUMS + 1;


	/******************************************************************************************
	Ramos.zhang-2018/04/17: Resolve  cefs bacukup info Area first block is bad block, case cefs backup fail.
	Refer to [Issue-Depot].[IS0000123][Submitter:ramos.zhang,Date:2018-04-17]
	<cefs备份信息区域good block查找问题导致产线个别模块无法做cefs备份>
	******************************************************************************************/
  block_id = find_backupInfoArea_next_good_block_forward(flash_context, s_backup_info_start_block_id);//resolve find backupInfo Area good block issue
  if (block_id > s_backup_info_end_block_id)
  {
    return FALSE;
  }
  cefs_context->backup_info_page_id = (block_id * get_pages_per_block(flash_context));

 DS_AT_MSG3_HIGH("@Ramos  init_cefs_backup_info_context block_id=%d,  s_backup_info_start_block_id=%d, s_backup_info_end_block_id=%d\r\n",block_id,s_backup_info_start_block_id,s_backup_info_end_block_id);
 DS_AT_MSG1_HIGH("@Ramos  init_cefs_backup_info_context  offset addr=%x \r\n", (cefs_context->backup_info_page_id)*4096);
  memset(s_buff_temp, 0xFF, sizeof(s_buff_temp));
  quec_flash_read_page(flash_context, cefs_context->backup_info_page_id, s_buff_temp);
  memcpy(&cefs_context->backup_info, s_buff_temp, sizeof(cefs_context->backup_info));
  
  if (cefs_context->backup_info.magic1 != QUEC_BACKUP_MAGIC1 || cefs_context->backup_info.magic2 != QUEC_BACKUP_MAGIC2)
  {
    if (cefs_context->op_type == QUEC_FILE_READ_BACKUP_INFO)
      return FALSE;

    memset(&cefs_context->backup_info, 0, sizeof(cefs_context->backup_info));
    cefs_context->backup_info.magic1 = QUEC_BACKUP_MAGIC1;
    cefs_context->backup_info.magic2 = QUEC_BACKUP_MAGIC2;
  }
  
  return TRUE;
}

static boolean init_cefs_file_context(quec_cefs_file_handle_type handle)
{
  quec_cefs_file_context_t cefs_context = get_cefs_file_context(handle);
  quec_flash_context_t flash_context = get_cefs_flash_context(handle);
  int block_id = 0;


  s_data_start_block_id =  0;
  s_data_end_block_id = get_block_count(flash_context) - 1 - QUEC_BACKUP_INFO_BLOCK_NUMS;
  
 DS_AT_MSG1_HIGH("@Ramos  init_cefs_file_context  1111 s_data_start_block_id=%d \r\n", s_data_start_block_id);
  if (cefs_context->op_type == QUEC_FILE_WRITE)
  {
    memset(&cefs_context->file_header, 0, sizeof(cefs_context->file_header));
    cefs_context->file_header.magic1 = CEFS_FILE_MAGIC1;
    cefs_context->file_header.magic2 = CEFS_FILE_MAGIC2;

    block_id = get_and_erase_block_forward(flash_context, s_data_start_block_id, s_data_end_block_id);
  }
  else
  {    
    block_id = find_next_good_block_forward(flash_context, s_data_start_block_id);
  }
  
  if (block_id > s_data_end_block_id)
  {
  	return FALSE;
  }

  cefs_context->header_page_id = (block_id * get_pages_per_block(flash_context));
  cefs_context->current_page_id = cefs_context->header_page_id + 1;

 DS_AT_MSG3_HIGH("@Ramos  init_cefs_file_context block_id=%d,  s_backup_info_start_block_id=%d, s_backup_info_end_block_id=%d\r\n",block_id,s_backup_info_start_block_id,s_backup_info_end_block_id);
 DS_AT_MSG1_HIGH("@Ramos  init_cefs_file_context write data  offset addr=%x \r\n", ( cefs_context->current_page_id )*4096);
 
  if (cefs_context->op_type == QUEC_FILE_READ)
  {
    int i = 0;
    uint32 crc = 0;
  
    memset(s_buff_temp, 0xFF, sizeof(s_buff_temp));
    quec_flash_read_page(flash_context, cefs_context->header_page_id, s_buff_temp);
    memcpy(&cefs_context->file_header, s_buff_temp, sizeof(cefs_context->file_header));
    
    if (cefs_context->file_header.magic1 != CEFS_FILE_MAGIC1 || cefs_context->file_header.magic2 != CEFS_FILE_MAGIC2)
    {
      return FALSE;
    }

    for (i=0; i<cefs_context->file_header.page_count; i++)
    {      
      memset(s_buff_temp, 0xFF, sizeof(s_buff_temp));
      quec_cefs_read_page(cefs_context->handle, s_buff_temp);
      
      crc = crc_32_calc((uint8*)s_buff_temp, 2048*8, crc);
    }
    
    if (crc != cefs_context->file_header.data_crc)
    {
      return FALSE;
    }
    
    cefs_context->current_page_id = cefs_context->header_page_id + 1;
  }

  return TRUE;
}

quec_cefs_file_handle_type quec_cefs_file_open(quec_file_op_type op_type)
{
  boolean init_ret = FALSE;
  
  if (s_cefs_context.handle != NULL)
  {
  	DS_AT_MSG1_HIGH("@Ramos  quec_cefs_file_open s_cefs_context.handle  =%x \r\n", s_cefs_context.handle );
    	return FS_DEVICE_FAIL;
  }

  memset(&s_cefs_context, 0, sizeof(s_cefs_context));

  if(DAL_SUCCESS != flash_device_attach(DALDEVICEID_FLASH_DEVICE_1, &s_cefs_context.flash_context))
  {  
    DS_AT_MSG1_HIGH("@Ramos  quec_cefs_file_open 1111=%d \r\n", 1);
    return FS_DEVICE_FAIL;
  }

    DS_AT_MSG1_HIGH("@Ramos  quec_cefs_file_open 2222 misc misc!!1=%d \r\n", 2);
  if( DAL_SUCCESS != flash_open_partition(s_cefs_context.flash_context, (const unsigned char *)"0:sys_rev"))
  {  
      DS_AT_MSG1_HIGH("@Ramos  quec_cefs_file_open 33333=%d \r\n", 3);
    flash_device_detach((DalDeviceHandle*)s_cefs_context.flash_context);
    return FS_DEVICE_FAIL;
  }

  s_cefs_context.handle = CEFS_FILE_HANDLE;
  s_cefs_context.op_type = op_type;

  if (op_type == QUEC_FILE_READ || op_type == QUEC_FILE_WRITE)
  {

    init_ret = init_cefs_file_context(s_cefs_context.handle);
  }
  else if (op_type == QUEC_FILE_READ_BACKUP_INFO|| op_type == QUEC_FILE_WRITE_BACKUP_INFO)
  {
    init_ret = init_cefs_backup_info_context(s_cefs_context.handle);
  }

      DS_AT_MSG1_HIGH("@Ramos  quec_cefs_file_open 444 init_ret=%d \r\n", init_ret);
  if (!init_ret)
  {
    flash_device_close((DalDeviceHandle*)s_cefs_context.flash_context);
    flash_device_detach((DalDeviceHandle*)s_cefs_context.flash_context);
    s_cefs_context.handle = NULL;
    return FS_DEVICE_FAIL;
  }

  return s_cefs_context.handle;
}

int quec_update_cefs_backup_info(quec_cefs_file_handle_type handle)
{
  quec_cefs_file_context_t cefs_context = get_cefs_file_context(handle);
  int pages_per_block = get_pages_per_block(cefs_context->flash_context);
  int block_id = 0;

  memset(s_buff_temp, 0xFF, sizeof(s_buff_temp));
  memcpy(s_buff_temp, &cefs_context->backup_info, sizeof(cefs_context->backup_info));

  while (TRUE)
  {
  	/******************************************************************************************
	Ramos.zhang-2018/04/17: Resolve  cefs bacukup info Area first block is bad block, case cefs backup fail.
	Refer to [Issue-Depot].[IS0000123][Submitter:ramos.zhang,Date:2018-04-17]
	<cefs备份信息区域good block查找问题导致产线个别模块无法做cefs备份>
	******************************************************************************************/
    block_id = get_and_backupInfoArea_erase_block_forward(cefs_context->flash_context, s_backup_info_start_block_id, s_backup_info_end_block_id); //resolve find backupInfo Area good block issue
    if (block_id > s_backup_info_end_block_id)
    {
      break;
    }
    cefs_context->backup_info_page_id = block_id * pages_per_block;
    if (quec_flash_write_page(cefs_context->flash_context, cefs_context->backup_info_page_id, s_buff_temp) == FS_DEVICE_OK)
    {
      return FS_DEVICE_OK;
    }
    else
    {
      quec_erase_block(cefs_context->flash_context, block_id);      
      flash_block_set_state(cefs_context->flash_context, block_id, FLASH_BLOCK_BAD);
    }
  }
  
  return FS_DEVICE_FAIL;
}

int quec_cefs_file_close(quec_cefs_file_handle_type handle)
{
  quec_cefs_file_context_t cefs_context = get_cefs_file_context(handle);  
  int pages_per_block = get_pages_per_block(cefs_context->flash_context);
  int ret = FS_DEVICE_FAIL;
  
  if (cefs_context == NULL)
  {
    return FS_DEVICE_FAIL;
  }

  if (cefs_context->update_header)
  {    
    memset(s_buff_temp, 0xFF, sizeof(s_buff_temp));
    if (cefs_context->op_type == QUEC_FILE_READ || cefs_context->op_type == QUEC_FILE_WRITE)
    {
      memcpy(s_buff_temp, &cefs_context->file_header, sizeof(cefs_context->file_header));
      ret = quec_flash_write_page(cefs_context->flash_context, cefs_context->header_page_id, s_buff_temp);
    }
/******************************************************************************************
francis-2019/08/19:Description....
Refer to [Issue-Depot].[IS0000858][Submitter:francis.huan,Date:2019-08-19]
<由于每次查询cefs还原次数都写入flash导致在查询cefs还原次数时候导致modem低概率assert的现象>
******************************************************************************************/
//    else if (cefs_context->op_type == QUEC_FILE_READ_BACKUP_INFO || cefs_context->op_type == QUEC_FILE_WRITE_BACKUP_INFO)
    else if (cefs_context->op_type == QUEC_FILE_WRITE_BACKUP_INFO)
    {
      ret = quec_update_cefs_backup_info(handle);
    }
  }
  
  flash_device_close((DalDeviceHandle*)cefs_context->flash_context);
  flash_device_detach((DalDeviceHandle*)cefs_context->flash_context);
  cefs_context->handle = NULL;

  return ret;
}

int quec_cefs_write_page(quec_cefs_file_handle_type handle, void *data)
{
  quec_cefs_file_context_t cefs_context = get_cefs_file_context(handle);
  int pages_per_block = get_pages_per_block(cefs_context->flash_context);
  int ret;
  
  if ((cefs_context->current_page_id % pages_per_block) == 0)
  {
    int block_id = cefs_context->current_page_id / pages_per_block; 

    block_id = get_and_erase_block_forward(cefs_context->flash_context, block_id, s_data_end_block_id);
    
    if (block_id > s_data_end_block_id)
    {
      return FS_DEVICE_FAIL;
    }
    
    cefs_context->current_page_id = pages_per_block * block_id;
  }

  cefs_context->file_header.page_count++;
  ret = quec_flash_write_page (cefs_context->flash_context, cefs_context->current_page_id++, data);

/*
  if ((cefs_context->current_page_id % pages_per_block) == 0)
  {
    cefs_context->current_page_id -= (pages_per_block * 2); 
  }
 */
  return ret;
}

int quec_cefs_read_page(quec_cefs_file_handle_type handle, void *data)
{
  quec_cefs_file_context_t cefs_context = get_cefs_file_context(handle);
  int pages_per_block = get_pages_per_block(cefs_context->flash_context);
  int ret;
  
  if ((cefs_context->current_page_id % pages_per_block) == 0)
  {
    int block_id = cefs_context->current_page_id / pages_per_block;
    block_id = find_next_good_block_forward(cefs_context->flash_context, block_id);

    if (block_id > s_data_end_block_id)
    {
      return FS_DEVICE_FAIL;
    }
    
    cefs_context->current_page_id = pages_per_block * block_id;
  }

  ret = quec_flash_read_page (cefs_context->flash_context, cefs_context->current_page_id++, data);  

 #if 0 
  if ((cefs_context->current_page_id % pages_per_block) == 0)
  {
   cefs_context->current_page_id -= (pages_per_block * 2); 
  }
#endif

  return ret;
}

boolean quec_is_imei_valid()
{
  static byte default_imei[NV_UE_IMEI_SIZE] = {8,138,22,21,6,0,64,5,66};
  static nv_item_type cefs_nv_item;
  int i;
  
  memset(&cefs_nv_item, 0, sizeof(cefs_nv_item));  
  if (dsatutil_get_nv_item (NV_UE_IMEI_I, &cefs_nv_item) != NV_DONE_S)
  {
    return FALSE;
  }

  for (i=0; i<NV_UE_IMEI_SIZE; i++)
  {
    if (default_imei[i] != cefs_nv_item.ue_imei.ue_imei[i])
      return TRUE;
  }

  return FALSE;
}

boolean quec_is_cefs_backup_valid()
{
  quec_cefs_file_handle_type handle = quec_cefs_file_open(QUEC_FILE_READ);

  if (handle <= 0)
  {
    return FALSE;
  }

  quec_cefs_file_close(handle);
  
  return TRUE;
}


int quec_get_cefs_partition_block_count(int *blockCount, int *badBlockCount)
{
	quec_flash_context_t cefs_partition_context = {0};
	int block_count =0 ;
	int block_id;
	int bad_block_count=0;

	if(DAL_SUCCESS != flash_device_attach(DALDEVICEID_FLASH_DEVICE_1, &cefs_partition_context))
	{
		DS_AT_MSG0_HIGH("@Ramos  quec_get_cefs_bad_block_count attach  failed!!\r\n");
		return FALSE;
	}

	if (DAL_SUCCESS != flash_open_partition(cefs_partition_context,  (const unsigned char *)"0:EFS2"))
	{
		DS_AT_MSG0_HIGH("@Ramos get cefs bad block  flash_open_partition  failed !!!\r\n");
		flash_device_detach((DalDeviceHandle*)cefs_partition_context);
		return FALSE;
	}

	block_count = get_block_count(cefs_partition_context);
	DS_AT_MSG1_HIGH("@Ramos  quec get cefs bad block count  block_count=%d \r\n", block_count);
	for(block_id =0 ; block_id < block_count; block_id++)
	{
		if(!bad_block_check(cefs_partition_context, block_id))
		{
			bad_block_count++;
		 	DS_AT_MSG1_HIGH("@Ramos  quec get cefs bad block count  bad block_id=%d \r\n", block_id);
		}
	}
	*blockCount = block_count;
	*badBlockCount =  bad_block_count;
	DS_AT_MSG2_HIGH("@Ramos  quec get cefs  block count=%d,  bad block count =%d \r\n",block_count,  bad_block_count);	
	flash_device_close((DalDeviceHandle*)cefs_partition_context);
	flash_device_detach((DalDeviceHandle*)cefs_partition_context);

  return block_count;
}

boolean quec_get_cefs_backup_info(int *backup, int *restore, int In_where[10], int *page_count, int *efs2BadBlockCount)
{
  quec_cefs_file_handle_type handle = quec_cefs_file_open(QUEC_FILE_READ_BACKUP_INFO);
  quec_cefs_file_context_t cefs_context;
   int efs2BlockCount =0;

  *efs2BadBlockCount = 0;
   quec_get_cefs_partition_block_count(&efs2BlockCount, efs2BadBlockCount);

  *backup = 0;
  *restore = 0;
  *page_count = 0;
  DS_AT_MSG1_HIGH("@Ramos  quec_get_cefs_backup_info 1111 handle=%d \r\n", handle);
  if (handle > 0)
  {
    int i= 0;
    cefs_context = get_cefs_file_context(handle);
    *backup = cefs_context->backup_info.cefs_backup_times;
    *restore = cefs_context->backup_info.cefs_restore_times;
	for(i =0; i<10; i++)
	{
		In_where[i] = cefs_context->backup_info.cefs_crash[i];
	}
     DS_AT_MSG2_HIGH("@Ramos  quec_get_cefs_backup_info  backup_times=%d restore times=%d\r\n", cefs_context->backup_info.cefs_backup_times, cefs_context->backup_info.cefs_restore_times);
    quec_cefs_file_close(handle);
  }

  handle = quec_cefs_file_open(QUEC_FILE_READ);
  if (handle <= 0)
  {
    return FALSE;
  }
  
  cefs_context = get_cefs_file_context(handle);
  *page_count = cefs_context->file_header.page_count;
   DS_AT_MSG1_HIGH("@Ramos  quec_get_cefs_backup_info  page_count=%d\r\n", cefs_context->file_header.page_count);

  quec_cefs_file_close(handle);

  return TRUE;
}

boolean quec_backup_cefs()
{
  struct fs_factimage_read_info read_info = {0};  
  int32 status = 1;
  qword now;
  qword start_time;
  qword elapsed;
  static byte efs_one_page[4200];  // 一个page  4096个字节 
  quec_cefs_file_handle_type cefs_handle;
  quec_flash_context_t flash_context;
  uint32 page_size;
  uint32 crc = 0;

  if (quec_is_imei_valid() == FALSE)
    return FALSE;

#ifdef QUECTEL_CEFS_COMPLETENESS_CHECK
/******************************************************************************************
who-2019/01/12:Description....
Refer to [Issue-Depot].[IS0000431][Submitter:ramos.zhang,Date:2019-01-12]
<针对efs文件系统目录出现随机丢失导入对cefs文件系统打桩完整性检查还原方案>
******************************************************************************************/

    if( 0 == quec_IsEfsCompletenessCheckFunc_disable())
   {
 	if(FALSE == quec_check_flag_files())
  	{
  		return FALSE;
  	}
   }
#endif

  cefs_handle = quec_cefs_file_open(QUEC_FILE_WRITE);
  if (cefs_handle <= 0)
  {
    DS_AT_MSG1_HIGH("@Ramos  222222222 cefs_handle=%d\r\n", cefs_handle);
    return FALSE;
  }
  
  FS_GLOBAL_LOCK();
  fs_time_ms (start_time);
  while(status != 0)
  {
    status = efs_image_prepare ("/");
     DS_AT_MSG1_HIGH("@Ramos  3333 status=%d\r\n", status);
    if(status == 0)
    {
        break;
    }
    
    fs_time_ms (now);
    qw_sub (elapsed, now, start_time);
    if(qw_hi (elapsed) != 0 || qw_lo (elapsed) > 400)
    {
      quec_cefs_file_close(cefs_handle);
      cefs_handle = NULL;
      
      FS_GLOBAL_UNLOCK();
      return FALSE;
    }
  }
  
  quec_disable_dog(); //Ramos 20160203
 
  flash_context =get_cefs_flash_context(cefs_handle);
  page_size = get_page_size(flash_context);

  do
  {    
    int i = 0;
    memset(efs_one_page, 0xFF, sizeof(efs_one_page));
    for (i=0; i<(page_size/512); i++) // 根据page 大小要调整。
    {
      if (0 != efs_get_fs_data ("/", &read_info, &efs_one_page[i*512]))
      {
       	DS_AT_MSG1_HIGH("@Ramos  4444 efs_get_fs_data  failed !!!  i=%d stop  \r\n", i);
        quec_cefs_file_close(cefs_handle);
        FS_GLOBAL_UNLOCK();
        return FALSE;
      }
      
      if (read_info.stream_state == 0)
        break;      
    }
    
    if (i > 0)
    {
      crc = crc_32_calc((uint8*)efs_one_page, 2048*8, crc);
      
      if (quec_cefs_write_page(cefs_handle, efs_one_page) != FS_DEVICE_OK)
      {
        quec_cefs_file_close(cefs_handle);
        FS_GLOBAL_UNLOCK();
        return FALSE;
      }
    }
    
  } while (read_info.stream_state != 0);

  get_cefs_file_context(cefs_handle)->file_header.data_crc = crc;
  get_cefs_file_context(cefs_handle)->update_header = TRUE;

  if (quec_cefs_file_close(cefs_handle) != 0)
  {
    FS_GLOBAL_UNLOCK();
    return FALSE;
  }
  
  FS_GLOBAL_UNLOCK();

  cefs_handle = quec_cefs_file_open(QUEC_FILE_WRITE_BACKUP_INFO);
  if (cefs_handle > 0)
  {
    get_cefs_file_context(cefs_handle)->backup_info.cefs_backup_times++;
    get_cefs_file_context(cefs_handle)->update_header = TRUE;
   DS_AT_MSG1_HIGH("@Ramos  backup successed !! backup_info.backup_times=%d \r\n",  get_cefs_file_context(cefs_handle)->backup_info.cefs_backup_times);

    quec_cefs_file_close(cefs_handle);
  }

  DS_AT_MSG1_HIGH("@Ramos  55555 quectel cefs backup successed !! backup_info.backup_times=%d \r\n",  get_cefs_file_context(cefs_handle)->backup_info.cefs_backup_times);
  return TRUE;
}

boolean quec_set_restore(quec_restore_cefs_enum cefs_restore, int In_where)
{
  quec_cefs_file_handle_type handle;
  quec_cefs_file_context_t cefs_context;

  if (!quec_is_cefs_backup_valid())
  {
    return FALSE;
  }
  
  handle = quec_cefs_file_open(QUEC_FILE_WRITE_BACKUP_INFO);
  if (handle <= 0)
  {
    return FALSE;
  }
  cefs_context = get_cefs_file_context(handle);
  

  if (RESTORE_CEFS_NORMAL == cefs_context->backup_info.cefs_restore_flag)
  {
	return TRUE; // 如果cefs还原flag已经设置，就不再去重复设置这个还原flag，主要是modem反复重启，不去重启linux，而还原过程再aboot里做的
  }
  
  cefs_context->backup_info.cefs_restore_flag = cefs_restore;
  if(In_where < 10) // 防止溢出
  cefs_context->backup_info.cefs_crash[In_where] += 1;
  cefs_context->update_header = TRUE;
  DS_AT_MSG1_ERROR("@Ramos  quec_set_restore!! restore_flag=%d \r\n", cefs_context->backup_info.cefs_restore_flag);
  return (quec_cefs_file_close(handle) == FS_DEVICE_OK);  
}


boolean quec_Set_LinuxrestoreFlag(boolean linuxRestoreFlag)
{
  quec_cefs_file_handle_type handle;
  quec_cefs_file_context_t cefs_context;

  if (!quec_is_cefs_backup_valid())
  {
    return FALSE;
  }
  
  handle = quec_cefs_file_open(QUEC_FILE_WRITE_BACKUP_INFO);
  if (handle <= 0)
  {
    return FALSE;
  }
  cefs_context = get_cefs_file_context(handle);
  if(0 ==linuxRestoreFlag)
  {
  	cefs_context->backup_info.linuxfs_backup_times = 0xAA55;
  }
  else
  {
  	cefs_context->backup_info.linuxfs_backup_times = linuxRestoreFlag;
  }
  cefs_context->update_header = TRUE;
  DS_AT_MSG1_HIGH("@Ramos  quec_set_restore!! linuxfs_backup_times=%d \r\n", cefs_context->backup_info.linuxfs_backup_times);
  return (quec_cefs_file_close(handle) == FS_DEVICE_OK);  
}

char temp_data[4096];
int quec_get_cefs_block_count()
{
  quec_cefs_file_handle_type handle;
  quec_cefs_file_context_t cefs_context;  
  quec_flash_context_t flash_context;
  int pages_per_block;
  int block_count = 0;
  
  handle = quec_cefs_file_open(QUEC_FILE_READ);
  if (handle <= 0)
  {
    return 0;
  }
  cefs_context = get_cefs_file_context(handle);
  flash_context = cefs_context->flash_context;
  
  pages_per_block = get_pages_per_block(flash_context);
  block_count = ((cefs_context->file_header.page_count/pages_per_block) + 1) + QUEC_BACKUP_INFO_BLOCK_NUMS;

  quec_cefs_file_close(handle);
  
  return block_count;  
}

boolean quec_write_efs2_blockpage(int block_id, int page_id, int wdata)
{
	int block_count = 0;
	int ret = TRUE;
        int pages_per_block ;
	quec_flash_context_t cefs_partition_context = {0};
	
	if(block_id <= 0)
	{
		return FALSE;
	}
	if(DAL_SUCCESS != flash_device_attach(DALDEVICEID_FLASH_DEVICE_1, &cefs_partition_context))
	{
		return FALSE;
	}

	if (DAL_SUCCESS != flash_open_partition(cefs_partition_context,  (const unsigned char *)"0:EFS2"))
	{
		flash_device_detach((DalDeviceHandle*)cefs_partition_context);
		return FALSE;
	}

	block_count = get_block_count(cefs_partition_context);
        block_id = block_id -1; // 底层擦写block是从0开始的， 这个AT命令第一时是从1开始的，所以要减去1.
	if(block_id > block_count)
		return FALSE;

	if (bad_block_check(cefs_partition_context, block_id))
	{
	        memset(temp_data, wdata, 4096);
                pages_per_block = get_pages_per_block(cefs_partition_context);
		if (quec_flash_write_page(cefs_partition_context, (block_id*pages_per_block +page_id),temp_data) != FS_DEVICE_OK)
		{        
			ret= FALSE;
		}
	}
	else
	{
		 ret= FALSE;
	}
	flash_device_close((DalDeviceHandle*)cefs_partition_context);
	flash_device_detach((DalDeviceHandle*)cefs_partition_context);
  return ret;
}

boolean quec_erase_cefs_block(int block_id)
{
	int block_count = 0;
	int ret = TRUE;
	quec_flash_context_t cefs_partition_context = {0};

	if(block_id <= 0)
	{
		return FALSE;
	}
	if(DAL_SUCCESS != flash_device_attach(DALDEVICEID_FLASH_DEVICE_1, &cefs_partition_context))
	{
		return FALSE;
	}

	if (DAL_SUCCESS != flash_open_partition(cefs_partition_context,  (const unsigned char *)"0:EFS2"))
	{
		flash_device_detach((DalDeviceHandle*)cefs_partition_context);
		return FALSE;
	}

	block_count = get_block_count(cefs_partition_context);
        block_id = block_id -1; // 底层擦写block是从0开始的， 这个AT命令第一时是从1开始的，所以要减去1.
        if(block_id > block_count)
        {
            return FALSE;
        }       
	if (bad_block_check(cefs_partition_context, block_id))
	{
		if (quec_erase_block(cefs_partition_context, block_id) != FS_DEVICE_OK)
		{        
			ret= FALSE;
		}
	}
	else
	{
		 ret= FALSE;
	}

  flash_device_close((DalDeviceHandle*)cefs_partition_context);
  flash_device_detach((DalDeviceHandle*)cefs_partition_context);
  return ret;
}

/*
extern void dog_hb_report(dog_report_type handle);
extern uint32  ds_dog_rpt_id ;
*/

boolean quec_efs2_BlockEraseWrite_StrTest(int block_id, int times, int wdata)
{
	int block_count = 0;
	int ret = TRUE;
        uint32 i, pageId, start_pageId;
	quec_flash_context_t cefs_partition_context = {0};
        int pages_per_block ;
	
	if(block_id <= 0)
	{
		return FALSE;
	}
	if(DAL_SUCCESS != flash_device_attach(DALDEVICEID_FLASH_DEVICE_1, &cefs_partition_context))
	{
                //DS_AT_MSG0_HIGH("@Ramos BlockEraseWrite_StrTest  1111111\r\n");	
		return FALSE;
	}

	if (DAL_SUCCESS != flash_open_partition(cefs_partition_context,  (const unsigned char *)"0:EFS2"))
	{
		flash_device_detach((DalDeviceHandle*)cefs_partition_context);
                //DS_AT_MSG0_HIGH("@Ramos BlockEraseWrite_StrTest  22222\r\n");	
		return FALSE;
	}
        block_count = get_block_count(cefs_partition_context);
        block_id = block_id -1; // 底层擦写block是从0开始的， 这个AT命令第一时是从1开始的，所以要减去1.
        if(block_id > block_count)
        {
            //DS_AT_MSG0_HIGH("@Ramos BlockEraseWrite_StrTest  3333333\r\n");	
            return FALSE;
        }        

        pages_per_block = get_pages_per_block(cefs_partition_context);
        memset(temp_data, wdata, 4096);
        for(i=0;i<times;i++)
        {

            if (bad_block_check(cefs_partition_context, block_id))
            {
                //DS_AT_MSG1_HIGH("@Ramos erase block id=%d !!!\r\n",block_id);
            	if (quec_erase_block(cefs_partition_context, block_id) != FS_DEVICE_OK)
            	{        
                    //DS_AT_MSG0_HIGH("@Ramos BlockEraseWrite_StrTest  55555\r\n");
            	    ret= FALSE;
                    break;
            	}
            }
            else
            {
                //DS_AT_MSG0_HIGH("@Ramos BlockEraseWrite_StrTest  66666\r\n");
            	 ret= FALSE;
                 break;
            }
            if (bad_block_check(cefs_partition_context, block_id))
            {
                start_pageId = block_id*pages_per_block;
                for(pageId =0 ; pageId<64 ; pageId++)
                {
            	    if (quec_flash_write_page(cefs_partition_context, start_pageId+pageId,temp_data) != FS_DEVICE_OK)
            	    {        
                        //DS_AT_MSG0_HIGH("@Ramos BlockEraseWrite_StrTest  7777777\r\n");
            		ret= FALSE;
                        break;
            	    }
                }   
            }
            else
            {
                //DS_AT_MSG0_HIGH("@Ramos BlockEraseWrite_StrTest  88888 \r\n");
            	 ret= FALSE;
                 break;
            }
    /*
        这行代码不要，在循环擦除block次数太多时会死机，因为DS task 无法喂狗，不想去修改dstask.c里的 ds_dog_rpt_id static类型
        这里就不添加了,flash压力测试时，不会对EFS2 进行压力测试。
      dog_hb_report( ds_dog_rpt_id);
    */    
            rex_sleep(20);
        }
	flash_device_close((DalDeviceHandle*)cefs_partition_context);
	flash_device_detach((DalDeviceHandle*)cefs_partition_context);
  return ret;
}


int test_debug=0;
extern void sys_m_initiate_shutdown(void);
extern boolean cm_rpm_check_reset_allowed( void);
boolean quec_set_cefs_restore_flag_and_reboot(int In_where)
{
  	quec_disable_dog(); //Ramos 20160203
	if( TRUE == quec_set_restore(RESTORE_CEFS_NORMAL,In_where))// Ramos.zhang-20190104 备份区数据有效，且设置还原flag，模块才重启，否则忽略
	{
		while(1)
		{
			rex_sleep(2000);
			DS_AT_MSG1_HIGH("@Ramos restore_flag  In_where=%d \r\n",In_where);	
			if(cm_rpm_check_reset_allowed())
			{
				test_debug ++;
				sys_m_initiate_shutdown();
				return TRUE;
			}
			 rex_sleep(100);
		}
	}
	return TRUE;
}

boolean quec_restore_cefs()
{
  quec_cefs_file_handle_type handle;
  quec_cefs_file_context_t cefs_context;
  int cefs_partition_page_id = 0, cefs_partition_block_id = 0;
  int pages_per_block;
  quec_flash_context_t cefs_partition_context = {0};
  int cefs_blk_count = 0;
  int i = 0;

     DS_AT_MSG0_HIGH("@Ramos: quec_restore_cefs entry!!!\r\n");
  handle = quec_cefs_file_open(QUEC_FILE_READ_BACKUP_INFO);
  if (handle <= 0)
  {
    return  FALSE;
  }
  
  cefs_context = get_cefs_file_context(handle);
    DS_AT_MSG1_HIGH("@Ramos: restore cefs get flag=%d", cefs_context->backup_info.cefs_restore_flag);
  if (RESTORE_CEFS_NORMAL != cefs_context->backup_info.cefs_restore_flag)
  {
    quec_cefs_file_close(handle);
    return FALSE;
  }
  quec_cefs_file_close(handle);  
  
  handle = quec_cefs_file_open(QUEC_FILE_READ);
    DS_AT_MSG1_HIGH("@Ramos: restore cefs cefs open=%d", handle);
  if (handle <= 0)
  {
    return  FALSE;
  }
  cefs_context = get_cefs_file_context(handle);
  
  if(DAL_SUCCESS != flash_device_attach(DALDEVICEID_FLASH_DEVICE_1, &cefs_partition_context))
  {
    quec_cefs_file_close(handle);  
    return FALSE;
  }
  
  if (DAL_SUCCESS != flash_open_partition(cefs_partition_context, (const unsigned char *)"0:EFS2"))
  {
    quec_cefs_file_close(handle);  
    flash_device_detach((DalDeviceHandle*)cefs_partition_context);
    return FALSE;
  }

  cefs_blk_count = get_block_count(cefs_partition_context);
  for (i=0; i<cefs_blk_count; i++)
  {
    if (!bad_block_check(cefs_partition_context, i))
      continue;
    
    if (quec_erase_block(cefs_partition_context, i) != FS_DEVICE_OK)
    {        
      flash_block_set_state(cefs_partition_context, i, FLASH_BLOCK_BAD);
    }
  }

  pages_per_block = get_pages_per_block(cefs_context->flash_context);
  DS_AT_MSG1_HIGH("@Ramos: restore cefs pages_per_block=%d",pages_per_block);
  for (i=0; i<cefs_context->file_header.page_count; i++)
  {      
    memset(s_buff_temp, 0xFF, sizeof(s_buff_temp));
    if (quec_cefs_read_page(handle, s_buff_temp) != FS_DEVICE_OK)
    {
    DS_AT_MSG0_HIGH("@Ramos: restore cefs failed 1111 \r\n");
      quec_cefs_file_close(handle);
      return  FALSE;
    }
    
    if ((cefs_partition_page_id % pages_per_block) == 0)
    {
      cefs_partition_block_id = (cefs_partition_page_id / pages_per_block);
      cefs_partition_block_id = find_next_good_block_forward(cefs_partition_context, cefs_partition_block_id);
      cefs_partition_page_id = cefs_partition_block_id * pages_per_block;
    }
    
    if (quec_flash_write_page(cefs_partition_context, cefs_partition_page_id, s_buff_temp) != FS_DEVICE_OK)
    {
      int move_back_pages = (cefs_partition_page_id % pages_per_block) + 1;
      
      quec_erase_block(cefs_partition_context, cefs_partition_block_id);      
      flash_block_set_state(cefs_partition_context, cefs_partition_block_id, FLASH_BLOCK_BAD);
      
      cefs_partition_block_id = find_next_good_block_forward(cefs_partition_context, cefs_partition_block_id+1);
      cefs_partition_page_id = cefs_partition_block_id * pages_per_block;
      
      while(move_back_pages-- > 0)
      {
        cefs_context->current_page_id--;
        if ((cefs_context->current_page_id % pages_per_block) == 0)
        {
         cefs_context->current_page_id += (pages_per_block * 2); 
        }
        i--;
      }
    }
    else
    {
      cefs_partition_page_id++;
    }
  }

  flash_device_close((DalDeviceHandle*)cefs_partition_context);
  flash_device_detach((DalDeviceHandle*)cefs_partition_context);
  quec_cefs_file_close(handle);
  
  handle = quec_cefs_file_open(QUEC_FILE_WRITE_BACKUP_INFO);
  if (handle <= 0)
  {
    return  FALSE;
  }
  cefs_context = get_cefs_file_context(handle);
  
  cefs_context->backup_info.cefs_restore_flag = RESTORE_CEFS_NONE;
  cefs_context->backup_info.cefs_restore_times++;
  cefs_context->update_header = TRUE;

  quec_cefs_file_close(handle);
DS_AT_MSG0_HIGH("@Ramos: restore cefs successfully !!!!! \r\n");
  return TRUE;
}

#define MIBIB_DL_FLAG_BLOCK  (7)
#define MIBIB_BLOCK_MAX      (10)
#define MIBIB_DL_FLAG_MAGIC1 (0x1D2E3F86)
#define MIBIB_DL_FLAG_MAGIC2 (0x53DE1379)
#define USB_ID_MAGIC         (0x3D2E2157)

typedef struct 
{
  uint32 magic1;
  uint32 magic2;
  uint32 version;
  uint32 dl_flag;
  uint32 port;
  uint32 baudrate;
  uint32 fota_flag;
  uint32 reserve2;
  uint32 reserve3;
  uint32 reserve4;
  uint16 vid;
  uint16 pid;
  char manufacturer[64];
  char product[64];
  char serial_no[64];
} quec_mi_dl_info_block_type;

static char s_buff_usb_id[4200];

int quec_set_download_usb_id(uint16 vid, uint16 pid)
{
  flash_handle_t handle_flash;
  int block_id = MIBIB_DL_FLAG_BLOCK;
  int ret = 0;
  static quec_mi_dl_info_block_type dl_info;

  if(DAL_SUCCESS != flash_device_attach(DALDEVICEID_FLASH_DEVICE_1, &handle_flash))
    return -1;

  if(DAL_SUCCESS != flash_open_partition(handle_flash, (const unsigned char*)"0:MIBIB"))
  {
    flash_device_detach((DalDeviceHandle*)handle_flash);
    return -1;
  }

  while (!bad_block_check(handle_flash, block_id))
  {          
    block_id++;
    if (block_id >= MIBIB_BLOCK_MAX)
    {
      flash_device_close((DalDeviceHandle*)handle_flash);
      flash_device_detach((DalDeviceHandle*)handle_flash);
      return -1;
    }
  }
  
  memset(s_buff_usb_id, 0, sizeof(s_buff_usb_id));
  quec_flash_read_page(handle_flash, block_id*handle_flash->flash_client_ctxt->client_data.pages_per_block, s_buff_usb_id);
  memcpy(&dl_info, s_buff_usb_id, sizeof(dl_info));
  
  if (dl_info.magic1 != MIBIB_DL_FLAG_MAGIC1 || dl_info.magic2 != MIBIB_DL_FLAG_MAGIC2)
  {
    memset(&dl_info, 0, sizeof(dl_info));
    
    dl_info.magic1 = MIBIB_DL_FLAG_MAGIC1;
    dl_info.magic2 = MIBIB_DL_FLAG_MAGIC2;
    dl_info.version = 1;
  }
  else if (dl_info.reserve4 == USB_ID_MAGIC && dl_info.pid == pid && dl_info.vid == vid)
  {
    flash_device_close((DalDeviceHandle*)handle_flash);
    flash_device_detach((DalDeviceHandle*)handle_flash);

    return ret;
  }

  dl_info.reserve4 = USB_ID_MAGIC;
  dl_info.pid = pid;
  dl_info.vid = vid;
  
  memset(s_buff_usb_id, 0xFF, sizeof(s_buff_usb_id));
  memcpy(s_buff_usb_id, &dl_info, sizeof(dl_info));

  while (quec_erase_block(handle_flash, block_id) != FS_DEVICE_OK)
  {      
    flash_block_set_state(handle_flash, block_id, FLASH_BLOCK_BAD);
    block_id++;
    if (block_id >= MIBIB_BLOCK_MAX)
    {
      flash_device_close((DalDeviceHandle*)handle_flash);
      flash_device_detach((DalDeviceHandle*)handle_flash);
      return FLASH_DEVICE_NO_MEM;
    }
  }
      
  ret = quec_flash_write_page(handle_flash, block_id*handle_flash->flash_client_ctxt->client_data.pages_per_block, s_buff_usb_id);
  
  flash_device_close((DalDeviceHandle*)handle_flash);
  flash_device_detach((DalDeviceHandle*)handle_flash);

  return ret;
}

#ifdef QUECTEL_CEFS_COMPLETENESS_CHECK

/******************************************************************************************
who-2019/01/12:Description....
Refer to [Issue-Depot].[IS0000431][Submitter:ramos.zhang,Date:2019-01-12]
<针对efs文件系统目录出现随机丢失导入对cefs文件系统打桩完整性检查还原方案>
******************************************************************************************/
boolean quec_check_flag_files(void)
{
	struct fs_stat file_stat;
	int check_index=0;

	for(check_index=0;NULL != EFS_file_check_list[check_index];check_index++)
	{
		if(-1 == efs_stat((char *)EFS_file_check_list[check_index], &file_stat))
		{
			DS_AT_MSG1_HIGH("quec_check_flag_files index(%d), file flag  check not found !!!!!!!",check_index);
			
			if(-1 == efs_stat((char *)EFS_file_check_list[check_index], &file_stat))// try again
			{
				DS_AT_MSG1_HIGH("quec_check_flag_files index(%d), file flag check two times not found !!",check_index);
				return FALSE;
			}
		}
	}

	return TRUE;
}

boolean quec_add_flag_files(void)
{
	int check_index=0;
	char data='1';
	mcfg_fs_status_e_type status; 

	for(check_index=0;NULL != EFS_file_check_list[check_index];check_index++)
	{
		//DS_AT_MSG1_HIGH("quec_add_flag_files  write %d, flag!!!!!",check_index);
		  status = mcfg_fs_write ( EFS_file_check_list[check_index], 
                       (void*)&data, 
                       1,
                       MCFG_FS_O_RDWR|MCFG_FS_O_CREAT|MCFG_FS_O_TRUNC|MCFG_FS_O_SYNC|MCFG_FS_O_AUTODIR, 
                       MCFG_FS_ALLPERMS, 
                       MCFG_FS_TYPE_EFS, 
                       MCFG_FS_SUBID_0 );
		if(MCFG_FS_STATUS_OK != status)
		{
			DS_AT_MSG1_HIGH("quec_add_flag_files write  index(%d), flag failed!!!!!!",check_index);
			return FALSE;
		}
	}

	return TRUE;
}

void Quectel_check_efs_completeness(void)
{
	struct fs_stat file_stat;

	if(-1 == efs_stat((char *)"/Q", &file_stat) ) //  "/Q" file flag not found, 
	{
		if((0 == efs_stat((char *)"/nv/item_files/modem/mmode/lte_bandpref", &file_stat))  \
		&& (0 == efs_stat((char *)"/nvm/num/10", &file_stat)) \
		&& (0 == efs_stat((char *)"/nvm/num/6828", &file_stat)) )// nv 10   nv LTE_BCCONFIG(6228)  Lte_bandpref  must be exist
		{
			DS_AT_MSG0_HIGH("Quectel_check_efs_completenesswrite files flag  start !!!!");
			if(FALSE == quec_add_flag_files())
			{
				return;
			}
			DS_AT_MSG0_HIGH("Quectel_check_efs_completeness  write all files flag  end !!!!");
		}
	}
	else
	{
		DS_AT_MSG0_HIGH("Quectel_check_efs_completeness (\"/Q\") file  flag  check  other flag  !!!!!");
		if(FALSE == quec_check_flag_files())
		{
			quec_set_cefs_restore_flag_and_reboot(5); // 文件标记检失败，
			return;
		}
		DS_AT_MSG0_HIGH("Quectel_check_efs_completeness  all  flag  check  completely  !!!!");
	}
}


int quec_IsEfsCompletenessCheckFunc_disable(void)
{
   unsigned int  func_disable = 0;
   QUECEFS_ErrEnum eErr = QUEC_EFS_ERR_SUCC;

   eErr = QL_EFS_Read(QUEC_EFS_CHECK_EFS_COMPLETENESS_CTL, &func_disable,sizeof(func_disable ));
   if(eErr != QUEC_EFS_ERR_SUCC)
   {
	   DS_AT_MSG3_HIGH("Enter %s(%d),[Quectel0125]QL_EFS_read(w_disable) FAIL! eErr:%d", __FUNCTION__,__LINE__,eErr);
	   func_disable = 0;
   }
   return (int)func_disable;
}
   
#endif

#endif
