#ifndef __QUEC_CEFS_H__
#define __QUEC_CEFS_H__

#include "comdef.h"
#include "customer.h"
#include "err.h"
#include "sys.h"
#include "cm.h"
#include "flash.h"
#include "DALSysTypes.h"
#include "DALStdDef.h"

#include "quec_flash.h"

#ifndef QUECTEL_CEFS_COMPLETENESS_CHECK
#define Quectel_QPRTPARA_VERSION    "V1.0"
#else
#define Quectel_QPRTPARA_VERSION    "V1.1"
#endif

typedef int quec_cefs_file_handle_type;

typedef enum 
{
  RESTORE_CEFS_NONE,
  RESTORE_CEFS_NORMAL,
}
quec_restore_cefs_enum;

#define  BACKUP_MTD_NUM 10
typedef struct 
{
	char ql_mtd_name[12];
	char ql_mtd_restore_name[12];
	uint64_t total_size;
	uint32_t ql_mtd_nub;
	uint32_t restore_flag;
	uint32_t restore_times;
	uint32_t backup_times;
	uint32_t crash[12];
} Ql_Mtd_Info;

typedef struct
{
	uint32_t magic1;  
	uint32_t magic2;

//add by [francis],20180524,match for modem ,make sure cefs is ok
	uint32_t cefs_restore_flag;
	uint32_t cefs_restore_times;
	uint32_t cefs_backup_times;
	uint32_t cefs_crash[10];  

	uint32_t linuxfs_restore_flag;
	uint32_t linuxfs_restore_times;
	uint32_t linuxfs_backup_times;
	uint32_t linuxfs_crash[10];  

	// modem backup restore flag
	uint32_t modem_restore_flag;
	uint32_t modem_restore_times;
	uint32_t modem_backup_times;
	uint32_t modem_crash[10];
	
    // other image restore flag
	uint32_t image_restoring_flag;
	uint32_t reserved1;
	uint32_t reserved2[100];

	uint32_t data_magic1;
	uint32_t data_magic2;

    Ql_Mtd_Info ql_mtd_info[BACKUP_MTD_NUM];
  //add by [Francis.huan] ,20180416 ,partition info that who  need to restore or erase  
} quec_backup_info_type;


typedef struct 
{
  uint32 magic1;  
  uint32 magic2;
  uint32 page_count;
  uint32 data_crc;
  
  uint32 reserve1;  
  uint32 reserve2;
  uint32 reserve3;
  uint32 reserve4;
} quec_cefs_file_header_type;

typedef flash_handle_t quec_flash_context_t;

typedef struct
{
  quec_flash_context_t flash_context;
  int current_page_id;
  int header_page_id;  
  int backup_info_page_id;
  quec_file_op_type op_type;
  boolean update_header;
  quec_cefs_file_handle_type handle;
  quec_cefs_file_header_type file_header;
  quec_backup_info_type backup_info;
} quec_cefs_file_context;

typedef quec_cefs_file_context *quec_cefs_file_context_t;

quec_cefs_file_handle_type quec_cefs_file_open(quec_file_op_type op_type);
int quec_cefs_file_close(quec_cefs_file_handle_type handle);
int quec_cefs_write_page(quec_cefs_file_handle_type handle, void *data);
int quec_cefs_read_page(quec_cefs_file_handle_type handle, void *data);
boolean quec_backup_cefs(void);
boolean quec_restore_cefs(void);
boolean quec_set_restore(quec_restore_cefs_enum cefs_restore, int In_where);
boolean quec_Set_LinuxrestoreFlag(boolean linuxRestoreFlag);
int quec_get_cefs_block_count(void);
boolean quec_is_imei_valid(void);
boolean quec_is_cefs_backup_valid(void);
boolean quec_get_cefs_backup_info(int *backup, int *restore, int In_where[10],int *page_count, int *efs2BadBlockCount);
boolean quec_write_efs2_blockpage(int block_id, int page_id, int wdata);
boolean quec_erase_cefs_block(int block_id);

boolean quec_efs2_BlockEraseWrite_StrTest(int block_id, int times, int wdata);

#ifdef QUECTEL_CEFS_COMPLETENESS_CHECK
boolean quec_check_flag_files(void);
boolean quec_add_flag_files(void);
void Quectel_check_efs_completeness(void);
int quec_IsEfsCompletenessCheckFunc_disable(void);
#endif

#endif
