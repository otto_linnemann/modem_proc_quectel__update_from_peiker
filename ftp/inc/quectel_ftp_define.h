#ifndef QUECTEL_FTP_DEFINE_H
#define QUECTEL_FTP_DEFINE_H

#include "comdef.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"
#include "quectel_ftp_app.h"
#if defined(QUECTEL_FTP_APP_SUPPORT)
#define MAX_FTP_USER_NAME_LEN      255
#define MAX_FTP_PASSWORD_LEN       255
#define MAX_FTP_PATH_NAME_LEN      255
#define MAX_FTP_COMMAND_LEN         512

#define MAX_FTP_UL_ACK_QUERY_CNT   10

#define FTP_KEEP_ALIVE_INTERVAL    120 //second
#define FTP_KEEP_ALIVE_TIMES          10  


typedef enum
{
  QUECTEL_FTP_IDLE = 0,
  QUECTEL_FTP_PDP_ACTIVING,
  QUECTEL_FTP_DNS_QUERY,
  QUECTEL_FTP_CONNECTING,
  QUECTEL_FTP_SSL_HAND_SHAKE,
  QUECTEL_FTP_CONNECTED,
  QUECTEL_FTP_LOGINED,
  QUECTEL_FTP_CLOSING,
  QUECTEL_FTP_CLOSED = QUECTEL_FTP_IDLE,
}quectel_ftp_state;

typedef enum
{
   QUECTEL_FTP_DATA_SESSION_IDLE = 0,
   QUECTEL_FTP_DATA_SESSION_LISTEN,
   QUECTEL_FTP_DATA_SESSION_CONNECTING,
   QUECTEL_FTP_DATA_SESSION_SSL_HANDSHAKE,
   QUECTEL_FTP_DATA_SESSION_CONNECTED,
   QUECTEL_FTP_DATA_SESSION_CLOSING,
   QUECTEL_FTP_DATA_SESSION_CLOSED = QUECTEL_FTP_DATA_SESSION_IDLE
}quectel_ftp_data_session_state;

typedef enum
{   
  FTP_COMMAND_NONE,
  FTP_COMMAND_AUTH,
  FTP_COMMAND_USER,
  FTP_COMMAND_PASS,
  FTP_COMMAND_PBSZ,
  FTP_COMMAND_PROT,
  FTP_COMMAND_SYST,
  FTP_COMMAND_TYPE,//4    // 4
  FTP_COMMAND_REST,
  FTP_COMMAND_PASV,
  FTP_COMMAND_PORT,
  FTP_COMMAND_RETR,
  FTP_COMMAND_STOR,
  FTP_COMMAND_APPE,
  FTP_COMMAND_STOU,
  FTP_COMMAND_QUIT,
  FTP_COMMAND_ABOR,
  FTP_COMMAND_NOOP,
  FTP_COMMAND_DELE, 
  FTP_COMMAND_MKD,
  FTP_COMMAND_RMD,
  FTP_COMMAND_SIZE,
  FTP_COMMAND_RNFR,
  FTP_COMMAND_RNTO,
  FTP_COMMAND_LIST,
  FTP_COMMAND_NLST,
  FTP_COMMAND_CWD,
  FTP_COMMAND_PWD,
  FTP_COMMAND_MLSD,
  FTP_COMMAND_MDTM,
  FTP_COMMAND_AUTH_TLS,
  FTP_COMMAND_PROT_P,
  FTP_COMMAND_MAX
}ftp_command_enum;

typedef enum 
{
	FTP_OPER_NULL,//无
	FTP_OPER_RETR ,//取
	FTP_OPER_STOR,//传，覆盖原文件
	FTP_OPER_APPE,//传，续写原文件
	FTP_OPER_STOU,//传，建立新文件
	FTP_OPER_LIST,
    FTP_OPER_NLST,
    FTP_OPER_MLSD,
}ftp_oper_enum;


typedef enum
{
	FTP_NONE_SSL = 0,
	FTP_IMPLICIT_SSL,
	FTP_EXPLICIT_SSL
}ftp_ssl_type_enum;

#define FTP_RESPONSE_RESTART          110
#define FTP_RESPONSE_READY_LATER      120
#define FTP_RESPONSE_OPENED           125
#define FTP_RESPONSE_OPEN_DATA        150
#define FTP_RESPONSE_CMD_OK           200
#define FTP_RESPONSE_CMD_FAILED       202
#define FTP_RESPONSE_SYS_STATE        211
#define FTP_RESPONSE_DIR_STATE        212
#define FTP_RESPONSE_FILE_STATE       213
#define FTP_RESPONSE_HELP_INFO        214
#define FTP_RESPONSE_NAME_TYPE        215
#define FTP_RESPONSE_READY_NEW        220
#define FTP_RESPONSE_BYE              221
#define FTP_RESPONSE_DAT_OPENED       225
#define FTP_RESPONSE_DAT_SUCCESS      226
#define FTP_RESPONSE_ENTER_PASV       227
#define FTP_RESPONSE_USER_LOGIN       230
#define FTP_RESPONSE_TLS_OK           234
#define FTP_RESPONSE_DAT_FINISH       250
#define FTP_RESPONSE_PATHNAME         257
#define FTP_RESPONSE_NEED_PWD         331
#define FTP_RESPONSE_NEED_ACC         332
#define FTP_RESPONSE_NEXT_CMD         350
#define FTP_RESPONSE_SERVICE_END      421
#define FTP_RESPONSE_DAT_FAILED       425
#define FTP_RESPONSE_CLOSE_CONN       426
#define FTP_RESPONSE_FILE_UNOPER      450
#define FTP_RESPONSE_REQUEST_ABORT    451
#define FTP_RESPONSE_ERROR_MEMORY     452
#define FTP_RESPONSE_WRONG_FORMAT     500
#define FTP_RESPONSE_INVALID_PARA     501
#define FTP_RESPONSE_CMD_UNREALIZED   502
#define FTP_RESPONSE_WRONG_SEQUENCE   503
#define FTP_RESPONSE_FUNCTION_FAILED  504
#define FTP_RESPONSE_NOT_LOGIN        530
#define FTP_RESPONSE_NO_ACCOUNT       532
#define FTP_RESPONSE_REQUEST_FAILED   550
#define FTP_RESPONSE_REQUEST_STOP     551
#define FTP_RESPONSE_FILE_REQ_STOP    552
#define FTP_RESPONSE_INVALID_FILE_N   553

typedef enum
{
	APP_FTP_OK = 0,
	APP_FTP_ERR_FAIL = -1,
   APP_FTP_WOULDBLOCK      = -2,
   APP_FTP_BUSY            = -3,
   APP_FTP_ERROR_DNS       = -4,   /*failed to parse domain name*/
   APP_FTP_ERROR_NETWORK   = -5,   /*failed to establish socket or the network is deactivated*/
   APP_FTP_CTRL_CLOSE      = -6,   /*the ftp session has been closed by the ftp server*/
   APP_FTP_DATA_CLOSE      = -7,   /*the data connection was closed by the ftp server*/
   APP_FTP_BEARER_FAIL     = -8,
   APP_FTP_ERROR_TIMEOUT   = -9,
   APP_FTP_INVALID_PARAM   = -10,
   APP_FTP_FILE_NOT_FOUND  = -11,
   APP_FTP_FILE_POINT_ERR  = -12,
   APP_FTP_FS_FILE_ERROR   = -13,
   APP_FTP_SERVICE_END     = -14,
   APP_FTP_DATA_FAILED     = -15,
   APP_FTP_CLOSE_CONN      = -16,
   APP_FTP_FILE_UNOPER     = -17,
   APP_FTP_REQUEST_ABORT   = -18,
   APP_FTP_OVER_MEMORY     = -19,
   APP_FTP_COMMAND_ERROR   = -20,
   APP_FTP_PARAM_ERROR     = -21,
   APP_FTP_COMMAND_FAILED  = -22,
   APP_FTP_COMMAND_BAD_SEQUENCE = -23,
   APP_FTP_COMMAND_NOT_IMPLENT = -24,
   APP_FTP_ERROR_UNLOGIN   = -25,
   APP_FTP_NO_ACCOUNT      = -26,
   APP_FTP_REQUEST_FAILED  = -27,
   APP_FTP_REQUEST_STOP    = -28,
   APP_FTP_FILEREQ_STOP    = -29,
   APP_FTP_FILENAME_ERROR  = -30,
   APP_FTP_SSL_AUTH_FAIL   = -31,
}quectel_ftp_error_code;

typedef struct
{
  Q_INT32                      sockfd;
#if defined(QUECTEL_CYASSL_SUPPORT)
  Q_INT32                      ssl_session_handle;
#endif
  dsm_item_type            *cmd_data_ptr; // EC25--->server
  dsm_item_type            *ctrl_data_ptr; //server-->EC25
  Q_UINT8                      login_user[MAX_FTP_USER_NAME_LEN+1];
  Q_UINT8                      login_pwd[MAX_FTP_PASSWORD_LEN+1];
  Q_UINT8                      transfer_mode; // 0: port mode 1:pasvi mode
  Q_UINT8                      transfer_type; // 0 :TYPE_I 1:TYPE_A
  Q_BOOL                       ftp_syst;  // 1 :query ftp server os system; 0:not query
  Q_INT32                      ftp_reply_code;
  Q_UINT32                    ftp_reply_file_size;
  Q_UINT8                      ftp_remote_path[255];
  Q_UINT8                      ftp_remote_old_name[255];
  Q_UINT8                      ftp_remote_new_name[256];
  Q_UINT8                      ftp_file_modtime[128];
  Q_UINT32                    restart_pos;
  Q_UINT32                    ftp_noop_cnt;
  Q_UINT32                    read_data_len;
  Q_UINT32                    write_data_len;
  Q_BOOL                       last_ul_packet;
  Q_UINT8                      last_cmd;
}quectel_ftp_control_session;

typedef struct
{
	Q_UINT32                   start_pos;
	dsm_item_type           *cache_data_ptr;
}quectel_ftp_dl_file_cache_struct;

typedef struct
{  
  Q_INT32                               sockfd;
#if defined(QUECTEL_CYASSL_SUPPORT)
  Q_INT32                               ssl_session_handle;
#endif	
  Q_INT32                               listen_sockfd;
  apptcpip_ip_addr_struct         remote_addr;
  Q_UINT16                             listen_port;
  Q_UINT16                             remote_port;
  Q_UINT8                               data_session_state;

  Q_UINT8                               last_dl_file_name[255];
  Q_UINT8                               last_ul_file_name[255];

  apptcpip_timer_id                  ul_ack_timer;
  Q_UINT32                             ul_ack_query_cnt;
  Q_UINT32                             ul_base_ack_len;
  Q_BOOL                               ul_complete;

  //UL data queue
  Q_UINT32                             current_ul_pos;
  dsm_item_type                      *current_ul_data_ptr;
  Q_BOOL                                ul_wm_high;
  dsm_watermark_type             ul_wm_ptr;
  q_type                                   ul_wm_q;
  apptcpip_timer_id                   ul_wait_timer;

   Q_BOOL                               dl_complete;
 //DL  data queue
   Q_UINT32                            current_dl_pos;
   dsm_item_type                     *current_dl_data_ptr;
	Q_BOOL                              dl_wm_high;
	dsm_watermark_type           dl_wm_ptr;
	q_type                                dl_wm_q;
	apptcpip_timer_id                dl_wait_timer;

}quectel_ftp_data_session;

typedef struct
{
  q_link_type                          link;
  quectel_ftp_state                  ftp_state;
  Q_UINT16                            pdp_cid;
  Q_UINT32                            ftp_timeout;
  apptcpip_timer_id                 ftp_ctrl_timer;// cmd response timer
  apptcpip_timer_id                 ftp_keepalive_timer;
  ftp_oper_enum                     ftp_data_oper;
  Q_CHAR                              server_addr_str[255];
  apptcpip_ip_addr_struct        server_addr;
  Q_UINT16                            server_port;
  ftp_ssl_type_enum                ftp_ssl_type; 
#if defined(QUECTEL_CYASSL_SUPPORT)
  Q_INT32                              ssl_ctx_id;
#endif	 
  Q_BOOL                               ftp_logined;
  Q_BOOL                               ftp_close_call;
  quectel_ftp_control_session   ftp_control_session;
  quectel_ftp_data_session      ftp_data_session;
  quectel_ftp_event_cb            ftp_event_cb;
}quectel_ftp_context;

#endif/*QUECTEL_FTP_APP_SUPPORT*/
#endif/*QUECTEL_FTP_DEFINE_H*/
