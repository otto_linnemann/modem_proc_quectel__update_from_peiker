#ifndef QUECTEL_FTP_APP_H
#define QUECTEL_FTP_APP_H

#if defined(QUECTEL_FTP_APP_SUPPORT)
typedef enum
{
 QUECTEL_FTP_OPEN_EVENT = 0,
 QUECTEL_FTP_CLOSE_EVENT,  // 1
 QUECTEL_FTP_SIZE_EVENT, // 2
 QUECTEL_FTP_DATA_TRANSFER_START, // 3
 QUECTEL_FTP_DL_DATA_IND, // 4
 QUECTEL_FTP_UL_DATA_IND, // 5
 QUECTEL_FTP_DATA_TRANSFER_END, // 6
 QUECTEL_FTP_GET_EVENT, // 7
 QUECTEL_FTP_DELETE_EVENT,
 QUECTEL_FTP_RENAME_EVENT,
 QUECTEL_FTP_PUT_EVENT,
 QUECTEL_FTP_MKDIR_EVENT,
 QUECTEL_FTP_RMDIR_EVENT,
 QUECTEL_FTP_CWD_EVENT,
 QUECTEL_FTP_PWD_EVENT,
 QUECTEL_FTP_LIST_EVENT,
 QUECTEL_FTP_NLST_EVENT,
 QUECTEL_FTP_MODTIME_EVENT,
 QUECTEL_FTP_MLSD_EVENT,
 QUECTEL_FTP_TIMEOUT_EVENT,
}quectel_ftp_event_enum;

typedef enum
{
   QUECTEL_FTP_LOGIN_USER = 0,
   QUECTEL_FTP_LOGIN_PASSWORD,
   QUECTEL_FTP_TRANSFER_TYPE,
   QUECTEL_FTP_TRANSFER_MODE,
   QUECTEL_FTP_TIMEOUT,
   QUECTEL_FTP_SSL_TYPE,
#if defined(QUECTEL_CYASSL_SUPPORT)
   QUECTEL_FTP_SSL_CTX_ID
#endif
}quectel_ftp_option_enum;



typedef struct
{
   Q_INT32 result_code;
   void    *info_ptr;
}quectel_ftp_event_info_type;


typedef void (*quectel_ftp_event_cb)(Q_INT32 ftp_ctx, quectel_ftp_event_enum event, quectel_ftp_event_info_type *event_ptr);

Q_INT32  quectel_ftp_ctx_init
(
quectel_ftp_event_cb  ftp_event_cb
);

Q_INT32  quectel_ftp_set_option
(
Q_INT32 ftp_ctx,
Q_UINT8 option_type,
void    *option_value
);
    
Q_INT32  quectel_ftp_open
(
Q_INT32  ftp_ctx,
Q_UINT16  pdp_cid,
Q_CHAR   *server_addr,
Q_UINT16 server_port
);

Q_INT32  quectel_ftp_close
(
Q_INT32  ftp_ctx
);


Q_INT32  quectel_ftp_get_file_size
(
Q_INT32  ftp_ctx,
Q_CHAR   *file_name
);

//if read from start_pos to file end, pls input 0xFFFFFFFF as read_len 
Q_INT32  quectel_ftp_get_file
(
Q_INT32  ftp_ctx,
Q_CHAR   *file_name,
Q_UINT32  start_pos,
Q_UINT32  read_len
);

Q_INT32  quectel_ftp_delete_file
(
Q_INT32   ftp_ctx,
Q_CHAR    *file_name
);

Q_INT32  quectel_ftp_mkdir
(
Q_INT32  ftp_ctx,
Q_CHAR   *dir_name
);

Q_INT32  quectel_ftp_goto_dir
(
Q_INT32  ftp_ctx,
Q_CHAR    *dir_name
);

Q_INT32  quectel_ftp_get_current_dir
(
Q_INT32  ftp_ctx
);

Q_INT32  quectel_ftp_rmdir
(
Q_INT32  ftp_ctx,
Q_CHAR   *dir_name
);

Q_INT32  quectel_ftp_put_file
(
Q_INT32   ftp_ctx,
Q_CHAR    *file_name,
Q_UINT32  start_pos,
Q_UINT32  write_len,
Q_BOOL    eof
);

Q_INT32  quectel_ftp_rename
(
Q_INT32  ftp_ctx,
Q_CHAR  *old_name,
Q_CHAR  *new_name
);

Q_INT32  quectel_ftp_get_last_modify_time
(
Q_INT32   ftp_ctx,
Q_CHAR    *file_name
);

Q_INT32  quectel_ftp_list
(
Q_INT32   ftp_ctx,
Q_CHAR    *dir_name
);

Q_INT32  quectel_ftp_nlst
(
Q_INT32   ftp_ctx,
Q_CHAR    *dir_name
);

Q_INT32  quectel_ftp_mlsd
(
Q_INT32   ftp_ctx,
Q_CHAR    *dir_name
);


Q_INT32 quectel_ftp_ctx_deinit
(
Q_INT32  ftp_ctx
);

Q_INT32 quectel_ftp_get_dl_data
(
Q_INT32 ftp_ctx,
dsm_item_type **dl_data_ptr,
Q_INT32  data_len
);

Q_INT32 quectel_ftp_put_ul_data
(
Q_INT32  ftp_ctx,
dsm_item_type *ul_data_ptr
);

Q_INT32 quectel_ftp_cancel_data_transfer
(
Q_INT32  ftp_ctx
);

#endif/*QUECTEL_FTP_APP_SUPPORT*/
#endif/*QUECTEL_FTP_APP_H*/
