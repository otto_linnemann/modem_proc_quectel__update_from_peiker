
/*===========================================================================
	Copyright(c) 2018 Quectel Wierless Solution,Co.,Ltd. All Rights Reserved.
	Quectel Wireless Solution Proprietary and Confidential.
============================================================================*/
/*===========================================================================
							EDIT HISTORY FOR MODULE
This section contains comments describing changees made to the module.
Notice that changes are listed in reverse chronological order.
WHEN		 WHO		WHAT,WHERE,WHY
----------	-----		----------------------------------------------------
07/11/2018	Chavis		Add cmd AT+QSSLCFG="closetimemode",<ssl_ctx_id>[,<closetime_mode>]
						to configure the unit of ssl close linger time.
08/14/2018	Chavis		add cmd to ignore multiple cert chains verify and invalid cert signature.
09/12/2018	Chavis		Add a parameter of AT+QSSLCFG="clientkey" to support private key with pwd.
============================================================================*/


#ifndef QUEC_SSL_INTERFACE_H
#define QUEC_SSL_INTERFACE_H

#include "quectel_apptcpip_util.h"


#if defined(QUECTEL_MBEDTLS_SUPPORT)

#define  QUEC_MAX_SSL_CTX_NUM     6
#define  QUEC_MAX_SSL_CS_LIST_MAX_SIZE  25
#define QUEC_SSL_MAX_RECORD_SIZE   16384
typedef enum
{
  QUEC_SSL_VER_30 = 0,         /**< SSL protocol ver. 3.0. */
  QUEC_SSL_VER_TLS10 = 1,      /**< TLS protocol ver. 1.0 (SSL 3.1). */
  QUEC_SSL_VER_TLS11 = 2,       /**< TLS protocol ver. 1.1 (SSL 3.2). */
  QUEC_SSL_VER_TLS12 = 3,
  QUEC_SSL_VER_ALL = 4,
  QUEC_SSL_VER_TLS13 = 5,
} quectel_ssl_version_enum_type;

typedef enum
{
   QUEC_SSL_VERIFY_NULL = 0x0000,
   QUEC_SSL_VERIFY_CLIENT = 0x0001,
   QUEC_SSL_VERIFY_CLIENT_SERVER = 0x0002
} quectel_ssl_verify_enum_type;


typedef  Q_UINT32    quectel_ssl_ciphersuite_id;

typedef  Q_CHAR      quectel_ssl_ses_path;

typedef  Q_CHAR      quectel_ssl_pwd;

typedef Q_BOOL  quectel_ssl_ignore_expiration_data_type;

typedef  Q_UINT32  quectel_ssl_negotiate_timeout;

typedef  Q_UINT16   quectel_ssl_sni;


typedef struct
{
  quectel_ssl_ciphersuite_id  list[QUEC_MAX_SSL_CS_LIST_MAX_SIZE];
  Q_UINT16                    size;
} quectel_ssl_cs_list_type;


typedef  struct
{
    quectel_ssl_version_enum_type          ssl_version;
    quectel_ssl_cs_list_type                    ssl_cs_list;
    quectel_ssl_verify_enum_type            ssl_verify_mode;
    quectel_ssl_ignore_expiration_data_type ssl_ignore_expiration;
    quectel_ssl_ses_path                        ssl_ca_cert_path[256];
    quectel_ssl_ses_path                        ssl_client_cert_path[256];
    quectel_ssl_ses_path                        ssl_client_key_path[256]; 
	quectel_ssl_pwd                             ssl_client_key_pwd[256];    
    quectel_ssl_negotiate_timeout            ssl_negotiate_timeout;
    quectel_ssl_sni                                  ssl_sni_ext;
\
}quectel_ssl_ctx_opt;


typedef enum
{
   QUEC_SSL_VERSION = 0,
   QUEC_SSL_CS_LIST,
   QUEC_SSL_VERIFY_MODE,
   QUEC_SSL_CA_CERT,
   QUEC_SSL_CLIENT_CERT,
   QUEC_SSL_CLIENT_KEY,
   QUEC_SSL_CLIENT_KEY_PWD,
   QUEC_SSL_IGNORE_EXPIRATION,
   QUEC_SSL_NEGOTIATE_TIMEOUT,
   QUEC_SSL_SNI,
   QUEC_SSL_MAX_OPT
}quectel_ssl_ctx_opt_type;

typedef union
{
    quectel_ssl_version_enum_type          ssl_version_pdata;
    quectel_ssl_cs_list_type                    ssl_cs_list_pdata;
    quectel_ssl_verify_enum_type            ssl_verify_mode_pdata;
    quectel_ssl_ses_path                        *ssl_ca_cert_path_pdata;
    quectel_ssl_ses_path                        *ssl_client_cert_path_pdata;
    quectel_ssl_ses_path                        *ssl_client_key_path_pdata;
    quectel_ssl_pwd                             *ssl_client_key_pwd_pdata;
    quectel_ssl_ignore_expiration_data_type ssl_ignore_expiration_pdata;
    quectel_ssl_negotiate_timeout               ssl_negotiate_timeout_pdata;
    quectel_ssl_sni                                     ssl_sni_ext;
}quectel_ssl_ctx_param_pdata_union_type;

typedef Q_INT32  quectel_ssl_ctx_handle;

typedef  Q_INT32  quectel_ssl_session_handle;

extern  void        quectel_ssl_init();

extern  Q_INT32  quectel_ssl_ctx_init(quectel_ssl_ctx_handle   ssl_handle);

extern  Q_INT32   quectel_ssl_ctx_deinit(quectel_ssl_ctx_handle  ssl_handle);

extern  Q_INT32   quectel_ssl_ctx_set_opt(quectel_ssl_ctx_handle  ssl_handle,
                                                                   quectel_ssl_ctx_opt_type  opt_type,
                                                                   quectel_ssl_ctx_param_pdata_union_type  *pdata_ptr);

extern  Q_INT32    quectel_ssl_ctx_get_opt(quectel_ssl_ctx_handle  ssl_handle,
                                                                     quectel_ssl_ctx_opt_type  opt_type,
                                                                     quectel_ssl_ctx_param_pdata_union_type  *pdata_ptr);

extern   quectel_ssl_session_handle   quectel_ssl_open_session(quectel_ssl_ctx_handle  ssl_handle, 
                                                                        Q_INT32 *err_code);

extern   Q_INT32 quectel_ssl_close_session(quectel_ssl_session_handle  ssl_session);

extern   Q_INT32  quectel_ssl_free_session(quectel_ssl_session_handle  ssl_session);

extern   Q_INT32  quectel_ssl_reuse_session(quectel_ssl_session_handle  old_ssl_session, quectel_ssl_session_handle  new_ssl_session);

extern   Q_INT32 quectel_ssl_set_fd(quectel_ssl_session_handle  ssl_session, Q_INT32 sockfd);

extern   Q_INT32  quectel_ssl_get_fd(quectel_ssl_session_handle ssl_session, Q_INT32 *sockfd);

extern   Q_INT32  quectel_ssl_use_sni(quectel_ssl_ctx_handle  ssl_handle,quectel_ssl_session_handle  ssl_session, Q_CHAR *server_name);

extern   Q_INT32  quectel_ssl_connect(quectel_ssl_session_handle  ssl_session);

extern   Q_INT32  quectel_ssl_write(quectel_ssl_session_handle ssl_session,
                                                       Q_UINT8  *write_buf_ptr,
                                                       Q_UINT16  write_len);
extern   Q_INT32  quectel_ssl_peek(quectel_ssl_session_handle ssl_session);
extern   Q_INT32  quectel_ssl_read(quectel_ssl_session_handle  ssl_session,
                                                     Q_UINT8   *read_buf_ptr,
                                                     Q_UINT16  read_len);

extern   Q_UINT32 quectel_ssl_get_pending_length(quectel_ssl_session_handle  ssl_session);

extern   Q_BOOL quectel_ssl_is_handshake_finished(quectel_ssl_session_handle  ssl_session);

extern   Q_INT32 quectel_ssl_get_last_error(quectel_ssl_session_handle ssl_session);
 

#endif

#endif
