
/**  
  @file
  Quectel_qvoltedbg_ltenas.h

  @brief
  This file defines declaration for QVOLTEDBG LTENAS configuration
  
  @see
  Create this file by QCM9X00178C001-P02
*/
/*============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
04/27/2018   vicent        Initial revision
=================================================================================*/
 
#ifndef __QUECTEL_QVOLTEDBG_LTENAS_H__
#define __QUECTEL_QVOLTEDBG_LTENAS_H__

#include "quectel_qvoltedbg_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
extern byte nas_emm_index;
extern byte nas_esm_index;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_QVOLTEDBG_LTENAS_H__
