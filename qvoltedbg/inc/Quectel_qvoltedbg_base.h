
/**  
  @file
  Quectel_qvoltedbg_base.h

  @brief
  This file defines declaration for QVOLTEDBG BASE configuration
  
  @see
  Create this file by QCM9X00178C001-P01
**/
/*============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
04/28/2018   Vicent        Add code for QVOLTEDBG/LTENAS
=================================================================================*/
 
#ifndef __QUECTEL_QVOLTEDBG_BASE_H__
#define __QUECTEL_QVOLTEDBG_BASE_H__

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef dsat_result_enum_type (*QVOLTEDBG_ProcFunc)( dsat_mode_enum_type,const struct dsati_cmd_struct*,const tokens_struct_type*,dsm_item_type* );

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean QVOLTEDBG_BASE_SendResponseS3S4(void);
extern boolean QVOLTEDBG_BASE_GetCmdParamL2
(
    char *pCmdName,
    dsati_cmd_type *pParseTab,
    QVOLTEDBG_ProcFunc *pProcFun,
    const dsati_cmd_type *pTab,
    int iCnt,
    const dsati_cmd_ex_type *pTabEx
);
extern boolean QVOLTEDBG_BASE_GetCmdParamL1
(
    char *pCmdName,
    dsati_cmd_type *pParseTab,
    QVOLTEDBG_ProcFunc *pProcFun,
    dsati_at_cmd_table_entry_type *pCmdTab,
    dsati_at_cmd_table_ex_entry_type *pCmdTabEx
);
extern boolean QVOLTEDBG_BASE_GetCmdParam(char *pCmdName,dsati_cmd_type *pParseTab,QVOLTEDBG_ProcFunc *pProcFun);

//<2018/04/27-QCM9X00178C001-P02-Vicent.Gao, <[QVOLTEDBG] Segment 2==>Base code submit.>
/******************************************************************************************
Vicent.Gao-2018/04/28: Add QVOLTEDBG sub-cmd LTENAS
Refer to [Req-Depot].[RQ0000059][Submitter:2018-04-28,Date:2018-04-28]
<MDM9x07>
******************************************************************************************/
extern boolean FUNC_DBG_SendStrToDS(char *pStr);
extern uint8 QL_QVOLTEDBG_GetLteNasInd(void);

extern boolean QVOLTEDBG_BASE_GetApnString(byte *pApn,uint16 uApnLen,char *pApnStr,uint16 uApnStrMaxLen);
//>2018/04/27-QCM9X00178C001-P02-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_QVOLTEDBG_BASE_H__
