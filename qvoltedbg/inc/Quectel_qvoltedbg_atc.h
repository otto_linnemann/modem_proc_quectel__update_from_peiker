
/**  
  @file
  Quectel_qvoltedbg_atc.h

  @brief
  This file defines declaration for QVOLTEDBG ATC configuration
  
  @see
  Create this file by QCM9X00178C001-P01
**/
/*============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
04/28/2018   Vicent        Add code for QVOLTEDBG/LTENAS
=================================================================================*/
 
#ifndef __QUECTEL_QVOLTEDBG_ATC_H__
#define __QUECTEL_QVOLTEDBG_ATC_H__

#include "quectel_qvoltedbg_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef struct
{
	 char *name;
     uint8 op;
     uint16 args_found;
     char *arg[10]; //Refer to MAX_ARG
} QVOLTEDBG_CmdTabStruct;

//<2018/04/27-QCM9X00178C001-P02-Vicent.Gao, <[QVOLTEDBG] Segment 2==>Base code submit.>
/******************************************************************************************
Vicent.Gao-2018/04/28: Add QVOLTEDBG sub-cmd LTENAS
Refer to [Req-Depot].[RQ0000059][Submitter:2018-04-28,Date:2018-04-28]
<MDM9x07>
******************************************************************************************/
typedef struct
{
    uint8 uLteNasInd;

    uint8 aRev[30];
} QVOLTEDBG_CntxStruct;
//>2018/04/27-QCM9X00178C001-P02-Vicent.Gao

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern dsat_result_enum_type quectel_exec_qvoltedbg_cmd
(
  dsat_mode_enum_type eMode,        
  const dsati_cmd_type *pParseTab, 
  const tokens_struct_type *pTok, 
  dsm_item_type *pResBuf  
);

extern dsat_result_enum_type quectel_exec_qnwinfo_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type quectel_exec_qcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type quectel_exec_cversion_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsatetsime_exec_cimi_cmd
(
  dsat_mode_enum_type  mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,      /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,      /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr             /*  Place to put response       */
);

extern dsat_result_enum_type dsatetsicall_exec_cops_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsatetsime_exec_csq_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_QVOLTEDBG_ATC_H__
