
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      quectel raw data item
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   will.shao
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2018/05/04        will.shao      Initialization
 |  2019/01/18        matthew.ma      Add for pin test
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef _QUEC_RAW_DATA_ITEM_H_
#define _QUEC_RAW_DATA_ITEM_H_

#include "com_dtypes.h"

#define QUEC_PACKED __packed
#define QUEC_PACKED_POST

// RAW DATA ID
typedef enum
{
  RAW_DATA_USB_CFG,
  RAW_DATA_DOWNLOAD_INFO_ID,  
  RAW_DATA_RECOVERY_INFO_ID,
  RAW_DATA_RECOVERY_HISTORY_ID,
  RAW_DATA_RECOVERY_HISTORY_EXT1_ID,
  RAW_DATA_FOTA_INFO_ID,
  RAW_DATA_SYSTEM_CFG_ID,
  RAW_DATA_MAX_ID
} quec_raw_data_id;

#define QUEC_RAW_USB_MANUF_DATA_LEN       32
#define QUEC_RAW_USB_PRODUCT_DATA_LEN     32
#define QUEC_RAW_USB_MAGIC                0x20180703

typedef enum
{
  USB_FUNC_DIAG,
  USB_FUNC_NMEA,
  USB_FUNC_AT,
  USB_FUNC_MODEM,
  USB_FUNC_RMNET,
  USB_FUNC_ADB,
  USB_FUNC_UAC,
  USB_FUNC_MAX
} ql_usb_func_enum;

typedef enum
{
  USB_NET_RMNET,
  USB_NET_ECM,
  USB_NET_MBIM,
  USB_NET_RNDIS,
  USB_NET_MAX,
} ql_usb_net_enum;

// RAW DATA TYPE
typedef QUEC_PACKED struct
{
  int  usb_magic;
  int  vid;
  int  pid;
  char usb_manuf_info[QUEC_RAW_USB_MANUF_DATA_LEN];
  char usb_product_info[QUEC_RAW_USB_PRODUCT_DATA_LEN];
  int  func;
  uint32 net;
  uint8 reserved1[44];
} QUEC_PACKED_POST ql_usb_cfg_type;

typedef enum
{
  BOOT_MODE_NORMAL,
  BOOT_MODE_DOWNLOAD,
  BOOT_MODE_BACKUP_SBL,
  BOOT_MODE_FOTA,
} boot_mode_type;

typedef enum /*not used*/
{
  FOTA_MODE_NORMAL,
  FOTA_MODE_RECOVERY,
} fota_mode_type;

typedef QUEC_PACKED struct 
{
  uint32 boot_mode;
  uint32 port;
  uint32 baudrate;
  uint32 fota;  
  uint8  reserved1[48]; //max defined 64 bytes, reserved 48 bytes
} QUEC_PACKED_POST quec_download_info_type;

#define PARTITION_NAME_LEN_MAX              (40)
#define PARTITION_RECOVERY_MAX              (4)
#define PARTITION_RECOVERY_WHERE_MAX        (15)
#define PARTITION_RECOVERY_HISTORY_MAX      (2)

typedef QUEC_PACKED struct
{
  char partition[PARTITION_NAME_LEN_MAX];
  int where;
  int recovery_retry;
} QUEC_PACKED_POST quec_recovery_info_type;

typedef QUEC_PACKED struct
{
  QUEC_PACKED struct
  {
    uint16 backup_times;
    uint16 restore_times[PARTITION_RECOVERY_WHERE_MAX];
  } QUEC_PACKED_POST partition[PARTITION_RECOVERY_MAX];
  
} QUEC_PACKED_POST quec_recovery_history_type;

/*RAW_DATA_FOTA_INFO_ID*/
#define QUEC_MAX_DELTA_NUM 7

typedef QUEC_PACKED struct {
  unsigned int read_addr;
  unsigned int write_addr;
  unsigned int package_size;
} quec_package_info;

typedef QUEC_PACKED struct {
  unsigned int cur_status;
  int err_code;
  unsigned char update_file;
  unsigned char passed_files;
  unsigned char  foting_flag;
  unsigned long int  total_upgrade_time;
  quec_package_info package_info[QUEC_MAX_DELTA_NUM];
} QUEC_PACKED_POST quec_fota_info_type;


typedef enum 
{
  CRASH_MODE_DISABLE,
  CRASH_MODE_NORMAL,
} crash_mode_type;

/******************************************************************************************
matthew-2019/01/18:Add for pin test
Refer to [Req-Depot].[RQ0000245][Submitter:matthew.ma,Date:2019-01-18]
<sbl������pin test����>
******************************************************************************************/
typedef enum 
{
  PINTEST_TABLE_ECXX=10,
  PINTEST_TABLE_EGXX,
} pintest_table_type_enum;

typedef QUEC_PACKED struct {
  uint8 crash_mode;
  uint8 pintest_table_type;
  uint8 reserved[126];
}QUEC_PACKED_POST quec_system_cfg_type;
//end matthew-2019/01/18

#endif //_QUEC_RAW_DATA_ITEM_H_
