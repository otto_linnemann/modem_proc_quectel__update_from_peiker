/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      quectel raw data 
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   will.shao
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2018/05/04        will.shao      Initialization
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef _QUEC_RAW_FLASH_DATA_H
#define _QUEC_RAW_FLASH_DATA_H

#include "comdef.h"
#include "quec_raw_data_item.h"

#define RAW_DATA_ITEM_MAX_LEN   (128)
#define RAW_DATA_ITEM_NUMS      (30)

boolean quec_raw_flash_data_open(void);
boolean quec_raw_flash_data_close(void);

boolean quec_raw_flash_data_write_item(quec_raw_data_id data_id, void* data);
boolean quec_raw_flash_data_read_item(quec_raw_data_id data_id, void* data);
boolean quec_raw_flash_data_sync(void);

boolean quec_rawdata_get_item(quec_raw_data_id item, void *item_buf_ptr);
boolean quec_rawdata_set_item(quec_raw_data_id item, void *item_buf_ptr);

#endif //_QUEC_RAW_FLASH_DATA_H

