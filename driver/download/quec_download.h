/**  
  @file
  quec_download.c

  @brief
  add quectel download opt.
  
*/
/*============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

						EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


WHEN		WHO 	  WHAT, WHERE, WHY
----------	--------  -------------------------------------------------------
18/12/17    asa   	add quectel flash driver

=============================================================================*/

#ifndef __QUEC_DOWNLOAD_H__
#define __QUEC_DOWNLOAD_H__

#include "quec_raw_data_item.h"


boot_mode_type quec_download_get_boot_mode(void);
boolean quec_download_set_boot_mode(boot_mode_type boot_mode);
fota_mode_type quec_download_get_fota_mode(void);
boolean quec_download_set_fota_mode(fota_mode_type fota_mode);


#endif //#ifndef __QUEC_DOWNLOAD_H__

