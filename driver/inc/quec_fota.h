#ifndef QUEC_FOTA_H
#define QUEC_FOTA_H

#include "comdef.h"
#include "customer.h"
#include "err.h"
#include "sys.h"
#include "cm.h"
#include "flash.h"
#include "DALSysTypes.h"
#include "DALStdDef.h"

#include "quec_flash.h"

#ifdef QUECTEL_VRM_FOTA

typedef int quec_fota_file_handle_type;

extern quec_fota_file_handle_type quec_fota_delta_file_open(quec_file_op_type op_type);
extern int quec_fota_delta_file_close(quec_fota_file_handle_type handle);
extern int quec_fota_delta_file_write(quec_fota_file_handle_type handle, char* buffer, int len);
extern int quec_fota_delta_file_read(quec_fota_file_handle_type handle, char* buffer, int len);
extern void quec_fota_set_flag();

extern void quec_fota_reboot();


#endif

#endif
