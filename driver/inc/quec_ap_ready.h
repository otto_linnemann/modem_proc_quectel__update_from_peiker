#ifndef _QUEC_AP_READY_H_
#define _QUEC_AP_READY_H_
#include "comdef.h"
#include "customer.h"
#include "err.h"
#include "sys.h"
#include "cm.h"
#include "QuecOEM_feature.h"
#include "ds3gsiolib.h"
#include "quec_power.h"

#ifdef QUECTEL_DYNAMIC_AP_READY

#ifndef QUECTEL_NANWANG_NETLIGET
#define QUECTEL_AP_READY_GPIO       (10)//ap ready gpio 16
#else
#define QUECTEL_AP_READY_GPIO       (79)//(10) Ramos 20170107 准捷南网要用到 gpio_10
#endif
#define QUECTEL_AP_READY_CFG        DAL_GPIO_CFG(QUECTEL_AP_READY_GPIO, 0, DAL_GPIO_INPUT, DAL_GPIO_NO_PULL,DAL_GPIO_2MA)//


/************************************************************
                    Function List
************************************************************/

/*
  Follow is AP_READY function code.
  ---------------- Begin -------------------
*/

typedef enum
{
    COMMAND_AP_READY_PARAM_ENABLE = 1,               //indicate ap readyenum
    COMMAND_AP_READY_PARAM_ACTIVE_LEVEL,             //check circle
    COMMAND_AP_READY_PARAM_INTERVAL,                 //Enable or Disable
}CFG_COMMAND_AP_READY_PARAM;


typedef struct _TAP_READY_INFO
{
    gpio_value_type Active_Level;   //indicate ap ready
    uint32          uiInterval;     //check circle
    boolean         bEnable;        //Enable or Disable
}TAP_READY_INFO;

#define Q_APREADY_DEFAULT_INTERVAL      500
#define Q_APREADY_DEFAULT_ACTIVE_LEVEL  GPIO_LOW_VALUE
#define Q_APREADY_MIN_INTERVAL          100
#define Q_APREADY_MAX_INTERVAL          3000


/*
  RESET AP ready cfg
*/
#define Quectel_ResetAPReady_CFG(x)\
do \
{ \
    x.Active_Level = Q_APREADY_DEFAULT_ACTIVE_LEVEL; \
    x.uiInterval = Q_APREADY_DEFAULT_INTERVAL; \
    x.bEnable = FALSE; \
}while(0)

/*when moudle power on*/
extern void quectel_ap_ready_nv_init( void );

/*
 Description: init app ready function
*/
extern boolean quectel_ap_ready_init( void );

/*
 Description: update app ready configuration in memory and nvram
*/
extern boolean quectel_set_ap_ready_info
(
  TAP_READY_INFO *pInfo
);

/*
 Description: get ap ready configuration im memroy
*/
extern TAP_READY_INFO *quectel_get_ap_ready_info( void );

/*
 Descripton: check if ap is ready
*/
extern boolean quectel_check_ap_state( void );

/*
 Descripton: check if ap ready is init OK
*/
extern boolean quectel_get_ap_ready_status( void );

/*
 Descripton: set ap ready init status
*/
extern void quectel_set_ap_ready_status
(
  boolean ap_status
);

extern dsat_result_enum_type quectel_qcfg_get_ap_ready
(
  dsm_item_type *res_buff_ptr
);

extern dsat_result_enum_type quectel_qcfg_set_ap_ready
(
  const tokens_struct_type *tok_ptr
);


/*
  ---------------- End -------------------
*/

#endif/*QUECTEL_DYNAMIC_AP_READY*/

#endif /*_QUEC_AP_READY_H_*/

