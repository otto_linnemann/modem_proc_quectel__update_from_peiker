#ifndef _QUEC_SIM_H
#define _QUEC_SIM_H
#include "comdef.h"
#include "customer.h"

#ifdef QUECTEL_SIM_HOTSWAP_DETECT

void quectel_simdet_sim_active_hotswap
(
  boolean enable, 
  boolean sim_inserted_level
);

#endif//QUECTEL_SIM_HOTSWAP_DETECT

#endif//_QUEC_SIM_H

