#ifndef _QUEC_POWER_H_
#define _QUEC_POWER_H_

#include "DDITlmm.h"
#include "DDIGPIOInt.h"

//#include "tramp.h"

#ifdef QUECTEL_FLY_MODE
#include "../../../core/systemdrivers/hal/gpioint/inc/HALgpioint.h"
#endif

#define QUECTEL_FLY_MODE_GPIO_TRIGGER   TRAMP_TRIGGER_RISING//TRAMP_TRIGGER_HIGH,TRAMP_TRIGGER_RISING
//#define QUECTEL_FLY_MODE_GPIO           (74)//Ramos.zhang 20151204 add for net_mode pin

#ifndef QUECTEL_NANWANG_NETLIGET
#define QUECTEL_FLY_MODE_GPIO           (11) //lory 20160918  test airplanecontrol feature
#else
#define QUECTEL_FLY_MODE_GPIO           (79) //(11) Ramos 20170107 准捷南网要用到gpio_11
#endif

#if 0 //test
#define QUECTEL_FLY_MODE_CFG_OUT_GPIO   DAL_GPIO_CFG(QUECTEL_FLY_MODE_GPIO, 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL,DAL_GPIO_2MA)
#endif
#define QUECTEL_FLY_MODE_CFG_GPIO       DAL_GPIO_CFG(QUECTEL_FLY_MODE_GPIO, 0, DAL_GPIO_INPUT, DAL_GPIO_PULL_UP,DAL_GPIO_2MA)//
#define QUECTEL_FLY_MODE_CFG_GPIO_DOWN       DAL_GPIO_CFG(QUECTEL_FLY_MODE_GPIO, 0, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN,DAL_GPIO_2MA)//

#ifdef QUECTEL_WAKEUP_IN
#define QUEC_WAKEUPIN_GPIO 25
#define QUECTEL_WAKUPIN_CFG_GPIO_UP       DAL_GPIO_CFG(QUEC_WAKEUPIN_GPIO, 0, DAL_GPIO_INPUT, DAL_GPIO_PULL_UP,DAL_GPIO_2MA)//
#define QUECTEL_WAKUPIN_CFG_GPIO_DOWN       DAL_GPIO_CFG(QUEC_WAKEUPIN_GPIO, 0, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN,DAL_GPIO_2MA)//
#endif

#ifdef QUECTEL_SLEEP_IND
#ifndef QUECTEL_NANWANG_NETLIGET
#define QUEC_SLEEPIND_GPIO 42
#else
#define QUEC_SLEEPIND_GPIO  79   //42 Ramos 20170107 准捷南网要用到gpio_42

#endif
#define QUECTEL_SLEEPIND_CFG_GPIO_OUT       DAL_GPIO_CFG(QUEC_SLEEPIND_GPIO, 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL,DAL_GPIO_2MA)//
#endif


typedef enum
{
  GPIO_INVALID_VALUE = -1,
  GPIO_LOW_VALUE  = 0,
  GPIO_HIGH_VALUE = 1
} gpio_value_type;

typedef enum
{
  FLY_MODE_IN = GPIO_LOW_VALUE,
  FLY_MODE_OUT = GPIO_HIGH_VALUE,
} fly_mode_type;

typedef enum
{
  FLY_MODE_IN_TRIGGER = 1, //TRAMP_TRIGGER_LOW
  FLY_MODE_OUT_TRIGGER = 0, //TRAMP_TRIGGER_HIGH
} fly_mode_trigger_type;


typedef enum
{
  SLEEP_URC_STATE_IDLE = 0,
  SLEEP_URC_STATE_ING,
  SLEEP_URC_STATE_END,
} sleep_urc_state_e;

//extern void quectel_w_disable_read();

//lory 20160912 for airplanecontrol
#ifdef QUECTEL_FLY_MODE
typedef struct
{
  DALSYSSyncHandle  handle;
  DALSYSSyncObj     obj;
} fly_mutex_type;

typedef void (*GPIOINTISR_VIOD) (void);
#endif

extern gpio_value_type quectel_get_w_disable_pin_value( void );

extern boolean quectel_dal_gpio_init( void );
#if defined(QUECTEL_GPIO)
extern gpio_value_type quectel_gpio_in(DALGpioSignalType gpio_signal);

extern void quectel_config_gpio(DALGpioSignalType gpio_signal);

extern void quectel_gpio_out(DALGpioSignalType gpio_signal, uint8 out_value);
#endif

#ifdef QUECTEL_DRIVER_ATC_QSCLK
extern void quec_sclk_okts(boolean enable);
extern boolean quec_is_sclk_enable( void );
#endif//QUECTEL_DRIVER_ATC_QSCLK

#ifdef QUECTEL_POWER_ATC
extern void quec_qpower_value(boolean enable);
extern boolean quec_is_qpower_enable( void );
#endif//QUECTEL_POWER_ATC

#ifdef QUECTEL_CANCEL_CFUN_RESET_URC
extern void set_cfun_reset(boolean flag);
extern boolean get_cfun_reset( void );

#endif//QUECTEL_CANCEL_CFUN_RESET_URC

#ifdef QUECTEL_UART_SLEEP_CONTROL

void dsat_uart_sleep_state_send( void );

#endif

#ifdef QUECTEL_FLY_MODE
boolean quectel_gpio_int_set_handler(uint32 gpio,gpio_value_type polarity,GPIOINTISR isr);
boolean quectel_gpio_register_isr(uint32 gpio,GPIOIntTriggerType trigger,GPIOINTISR isr,GPIOINTISRCtx param);
boolean quectel_gpio_set_trigger(uint32  gpio,GPIOIntTriggerType trigger);
void quectel_gpio_deregister(uint32  gpio,GPIOIntTriggerType trigger,GPIOINTISR isr);
void quectel_gpio_unmask_and_reverse(uint32 gpio,gpio_value_type polarity,GPIOINTISR isr);

#endif

#ifdef QUECTEL_SLEEP_IND
extern boolean quectel_get_sleepind_state(void);
extern void quectel_set_sleepind_state(boolean state);
extern void quectel_sleepind_nv_init(void);
extern void quectel_config_sleepind_gpio(void);

#endif


#ifdef QUECTEL_WAKEUP_IN
extern boolean quectel_get_wakeupin_state(void);
extern void quectel_set_wakeupin_state(boolean state);
extern void quectel_wakeupin_nv_init(void);
extern  void quectel_config_wakeupin_gpio(void);
#endif
#endif//_QUEC_POWER_H_
