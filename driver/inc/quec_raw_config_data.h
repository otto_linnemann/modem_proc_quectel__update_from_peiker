#ifndef _QUEC_RAW_CONFIG_DATA_H
#define _QUEC_RAW_CONFIG_DATA_H


typedef enum
{
  RAW_DATA_USB_ID,
  RAW_DATA_MAX_ID
} quec_raw_data_id;

typedef struct
{
  int vid;
  int pid;
} quec_usb_id_type;

boolean quec_raw_flash_data_open(void);
boolean quec_raw_flash_data_close(void);

boolean quec_raw_flash_data_write_item(quec_raw_data_id data_id, void* data);
boolean quec_raw_flash_data_read_item(quec_raw_data_id data_id, void* data);
boolean quec_raw_flash_data_sync(void);

#endif //_QUEC_RAW_CONFIG_DATA_H

