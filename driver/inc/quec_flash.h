#ifndef _QUEC_FLASH_H_
#define _QUEC_FLASH_H_
#include "comdef.h"
#include "err.h"
#include "sys.h"
#include "cm.h"
#include "fs_flash_dal_shim.h"

#define QUEC_FLASH_PAGE_SIZE          (2112/2)

#ifdef QUECTEL_FM64D1G12A_MCP
#define QUEC_UUID_PAGE_START          (7)
#elif (defined QUECTEL_MST9D16D16FMHC_MCP)
#define QUEC_UUID_PAGE_START          (7)
#endif

#define QUEC_OTP_UUID_OFFSET          (QUEC_UUID_PAGE_START*QUEC_FLASH_PAGE_SIZE)


typedef enum
{
  QUEC_FILE_READ,
  QUEC_FILE_WRITE,   
  QUEC_FILE_READ_BACKUP_INFO,
  QUEC_FILE_WRITE_BACKUP_INFO,
} quec_file_op_type;

boolean quec_flash_init(void);
boolean quec_flash_get_uid(uint8* uid_buf, uint32 uid_buf_len, uint32* uid_len);
uint32 quec_flash_read_otp(uint32 otp_offset, uint8* otp_buf, uint32 read_len);
uint32 quec_flash_write_otp(uint32 otp_offset, uint8* otp_buf, uint32 write_len);
uint32 quec_flash_lock_otp(uint32 otp_offset);
uint32 quec_flash_set_uid(uint8* uid_buf, uint32 uid_len);


flash_handle_t quec_flash_open(unsigned char* partition);
int quec_flash_close(flash_handle_t handle);

int quec_flash_read_page (flash_handle_t handle, page_id page, void *data);
int quec_flash_write_page (flash_handle_t handle, page_id page, void *data);
int quec_flash_read_page_with_type (flash_handle_t handle, page_id page, void *data, uint32 read_type);
int quec_flash_write_page_with_type (flash_handle_t handle, page_id page, void *data, uint32 write_type);

int quec_erase_block(flash_handle_t handle, int block_id);
boolean quec_flash_is_block_good(flash_handle_t handle, int block_id);
int quec_flash_set_block_state(flash_handle_t handle, int block_id, enum flash_block_state block_state);

uint32 quec_flash_pages_per_block (flash_handle_t handle);
uint32 quec_flash_get_block_count(flash_handle_t handle);
uint32 quec_flash_get_block_start(flash_handle_t handle);
page_id quec_flash_get_page_id(flash_handle_t handle,int block_id, int page_index_in_block);

enum flash_ecc_state quec_flash_get_ecc_state(flash_handle_t handle);
uint32 quec_flash_page_size (flash_handle_t handle);


#endif //_QUEC_FLASH_H_

