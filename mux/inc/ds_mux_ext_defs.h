#ifndef _DS_MUX_EXT_DEFS_H
#define _DS_MUX_EXT_DEFS_H

/*===========================================================================

                    D A T A   S E R V I C E S    M U X   

                                  EXT DEFS 

DESCRIPTION 
  This module holds all the utility services for mux. 

Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.NI.6.0.1.c5.2/Main/modem_proc/datamodem/interface/mux/inc/ds_mux_ext_defs.h#1 $ 
  $DateTime: 2013/12/04 08:06:07 $ 
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/14/10   kr      Initial revision with CMUX definitions
===========================================================================*/

/*===========================================================================
                      INCLUDE FILES FOR MODULE
===========================================================================*/

#ifdef FEATURE_DATA_MUX

/*===========================================================================
                      DEFINITIONS  FOR THE MODULE
===========================================================================*/

/** Enum to specify the operation performed on the physical port
*/
typedef enum
{
  DS_MUX_PHY_PORT_OPERATION_MIN         =0,
  DS_MUX_PHY_PORT_OPERATION_CLOSE     =DS_MUX_PHY_PORT_OPERATION_MIN,
  DS_MUX_PHY_PORT_OPERATION_OPEN       =1,
}ds_mux_phy_port_state;

/** Enum to specify the status of physical port operations
*/
typedef enum
{
  DS_MUX_PHY_PORT_OPERATION_STATUS_INVALID     =-1,
  DS_MUX_PHY_PORT_OPERATION_STATUS_SUCCESS     =0,
  DS_MUX_PHY_PORT_OPERATION_STATUS_FAILED      =1,
}ds_mux_phy_port_operation_status;

/** Mode definitions based on 3GPP 27.010
*/

typedef enum
{
  DS_MUX_MODE_MIN = 0,
  DS_MUX_MODE_BASIC = DS_MUX_MODE_MIN,
  DS_MUX_MODE_ADVANCED = 1,
}ds_mux_mode_enum_type;

/** Frame type definitions based on 3GPP 27.010
*/

typedef enum
{
  DS_MUX_SUBSET_MIN = 0,
  DS_MUX_SUBSET_UIH = DS_MUX_SUBSET_MIN,
  DS_MUX_SUBSET_UI = 1,
  DS_MUX_SUBSET_I = 2,
}ds_mux_subset_enum_type;

/** Port speed definitions based on 3GPP 27.010
*/

typedef enum
{
  DS_MUX_PHY_PORT_SPEED_INVALID = 0,
  DS_MUX_PHY_PORT_SPEED_1 = 1,  /* 9,600 bit/s */
  DS_MUX_PHY_PORT_SPEED_2 = 2,  /* 19,200 bit/s */
  DS_MUX_PHY_PORT_SPEED_3 = 3,  /* 38,400 bit/s */
  DS_MUX_PHY_PORT_SPEED_4 = 4,  /* 57,600 bit/s */
  DS_MUX_PHY_PORT_SPEED_5 = 5,  /* 115,200 bit/s */
  DS_MUX_PHY_PORT_SPEED_6 = 6,  /* 230,400 bit/s */
//2015-01-29 add by scott.hu
#ifdef QUECTEL_CMUX_BASE
  DS_MUX_PHY_PORT_SPEED_7 = 7,  /* 460,800 bit/s */
#endif  
//end scott.hu
}ds_mux_port_speed_enum_type;

/** Window size definitions based on 3GPP 27.010, applicable only for
    Advanced mode. */

typedef enum
{
  DS_MUX_WINDOW_SIZE_INVALID = 0,
  DS_MUX_WINDOW_SIZE_1 = 1,
  DS_MUX_WINDOW_SIZE_2 = 2,
  DS_MUX_WINDOW_SIZE_3 = 3,
  DS_MUX_WINDOW_SIZE_4 = 4,
  DS_MUX_WINDOW_SIZE_5 = 5,
  DS_MUX_WINDOW_SIZE_6 = 6,
  DS_MUX_WINDOW_SIZE_7 = 7,
}ds_mux_window_size_enum_type;

/** Max size in bytes in Information field as per 3GPP 27.010 */
#define DS_MUX_CMUX_MAX_FRAME_N1                      32767
/** Default size in bytes in Information field per 3GPP 27.010, basic mode */
//2015-01-29 add by scott.hu
#ifdef QUECTEL_CMUX_BASE
#define DS_MUX_CMUX_BASIC_MODE_DEFAULT_FRAME_N1       127
#else
#define DS_MUX_CMUX_BASIC_MODE_DEFAULT_FRAME_N1       31
#endif
//end scott.hu
/** Default size in bytes in Information field per 3GPP 27.010, advanced mode */
#define DS_MUX_CMUX_ADVANCED_MODE_DEFAULT_FRAME_N1    63
/** Max Number of re-tranmission as per 3GPP 27.010 */
#define DS_MUX_CMUX_DEFAULT_MAX_TX_N2                 3
/** Default value of acknowledgement timer in ms as per 3GPP 27.010 */
#define DS_MUX_CMUX_DEFAULT_CMD_TIMER_T1              100
/** Default value of DLCI0 response timer in ms as per 3GPP 27.010 */
#define DS_MUX_CMUX_DEFAULT_DLCI0_TIMER_T2            300
/** Default value of response timer as per 3GPP 27.010 */
#define DS_MUX_CMUX_DEFAULT_TIMER_T3                  10
/** Max number of re-transmissions as per 3GPP 27.010*/
#define DS_MUX_CMUX_MAX_TX_N2                         255

/** The structure ds_mux_cmux_info_type holds the parameters passed +CMUX
    and is as per 3GPP 27.010 spec
*/    
typedef struct 
{
  uint8   operating_mode;               
  /**< Mode 0 - basic, 1- advanced.        */
  uint8 subset;                         
  /**< 0-UIH, 1-UI, 2-I frames.            */
  uint8   max_cmd_num_tx_times_N2;      
  /**< Max re-tries N2 (0-255).            */
  uint8   window_size_k;                
  /**< Window size default 2, range (1-7). */
  uint16 port_speed;                    
  /**< port speed valid values: 1-6.       */
  uint16  max_frame_size_N1;            
  /**< Max frame size (1-32768).           */
  uint32  response_timer_T1;            
  /**<  Time UE waits for an acknowledgement before
    resorting to other action (e.g. transmitting a frame)
    default 100ms min value 10 ms.
  */
  uint32  response_timer_T2;            
  /**< Time mux control channel waits before 
  re-transmitting a command default 300ms and
  min value 20 ms.
  */
  uint32  response_timer_T3;            
  /**<  Time UE waits for response for a 
  power wake-up command default 10ms and min
  value 1 ms.
  */
} ds_mux_cmux_info_type;

/** @ds_mux_ext_cli_init
  External function to initialize MUX and setup the logical channels

  @dataypes
  #ds_mux_cmux_info_type
  #int
  #void *
  
  @param[in] cmux_param       CMUX parameters
  @param[in] port_id          PHY port id
  @param[out] notify_func_ptr callback func. to notify the port status
   
  @return
  0  -- no error
  -1 -- Error

  @dependencies
  None.
*/
int ds_mux_ext_cli_init
(
  ds_mux_cmux_info_type cmux_param,
  int port_id,
  void (*notify_func_ptr)(int, int)
);

#endif /* FEATURE_DATA_MUX */

#endif  /* _DS_MUX_EXT_DEFS_H */

