/**
*@file    quectel_remotefs_oma.h
*@date    2016-09-13
*@author  running.qian
*@brief
*/
#ifndef __QUECTEL_REMOTEFS_OMA_H__
#define __QUECTEL_REMOTEFS_OMA_H__

#include "quectel_remotefs_task.h"

typedef struct __rFs_oma_packet_hdr_type {
    uint8 type;     /* RFSI_APP_SMS */
    uint8 mode;     /* req, resp */
    uint8 cmd;      /* sms cmd */
    int8  err;      /* err code */
} rFs_oma_packet_hdr_type;

#define RFSI_OMA_PACKET_HDR_LEN        (sizeof(rFs_oma_packet_hdr_type))
#define RFSI_OMA_PACKET_DATA_MAX_SIZE  (RFSI_PACKET_MAX_SIZE - RFSI_OMA_PACKET_HDR_LEN)

void rFsi_oma_client_process(const rFsi_packet_type *packet);

#endif/*__QUECTEL_REMOTEFS_OMA_H__*/
