/**
*@file    quectel_remotefs_task.h
*@date    2016-07-02
*@author  running.qian
*@brief
*/

#ifndef __QUECTEL_REMOTEFS_TASK_H__
#define __QUECTEL_REMOTEFS_TASK_H__

#include "QUECoem_feature.h"
#include "task.h"//"rcinit_rex.h"//"rex.h"
#include "queue.h"
#include "msg.h"
#include "dsm.h"
#include "dog_hb_rex.h"
#include "modem_mem.h"

#define MSG_SSID_QRFS  (MSG_SSID_DS)//In future it should be move to msgcfg.h
    #define MSG_SSID_QRFS_TASK  (MSG_SSID_QRFS)
    #define MSG_SSID_QRFS_SIO   (MSG_SSID_QRFS + 1)
    #define MSG_SSID_QRFS_UI    (MSG_SSID_QRFS + 2)
    #define MSG_SSID_QRFS_FILE  (MSG_SSID_QRFS + 3)

#ifndef MSG_BUILD_MASK_MSG_SSID_QRFS
#define MSG_BUILD_MASK_MSG_SSID_QRFS            (MSG_LVL_MED)
#endif

#ifndef MSG_BUILD_MASK_MSG_SSID_QRFS_TASK
#define MSG_BUILD_MASK_MSG_SSID_QRFS_TASK       (MSG_LVL_MED)
#endif

#ifndef MSG_BUILD_MASK_MSG_SSID_QRFS_SIO
#define MSG_BUILD_MASK_MSG_SSID_QRFS_SIO        (MSG_LVL_MED)
#endif

#ifndef MSG_BUILD_MASK_MSG_SSID_QRFS_UI
#define MSG_BUILD_MASK_MSG_SSID_QRFS_UI         (MSG_LVL_MED)
#endif

#ifndef MSG_BUILD_MASK_MSG_SSID_QRFS_FILE
#define MSG_BUILD_MASK_MSG_SSID_QRFS_FILE       (MSG_LVL_MED)
#endif


typedef rex_crit_sect_type rFsi_mutex_type;
#define RFSI_MUTEX_INIT(mutex)      rex_init_crit_sect(mutex)
#define RFSI_MUTEX_LOCK(mutex)      rex_enter_crit_sect(mutex)
#define RFSI_MUTEX_UNLOCK(mutex)    rex_leave_crit_sect(mutex)


/* remoteFs Task signals */
#define QRFS_CMD_Q_SIG           0x0001
#define QRFS_DOG_HB_REPORT_SIG   0x0002
#define QRFS_SIO_RX_SIG          0x0004
#define QRFS_TIMER_SIG           0x0008
#define QRFS_TASK_OFFLINE_SIG    TASK_OFFLINE_SIG    /* 0x2000 */
#define QRFS_TASK_STOP_SIG       TASK_STOP_SIG       /* 0x4000 */
#define QRFS_TASK_START_SIG      TASK_START_SIG      /* 0x8000 */


/* Config Frame size */
#define RFSI_FRAME_MAX_SIZE             1500//可以修改，必须跟service端大小一样
#define RFSI_FRAME_HEAD_TAIL_SIZE       6
#define RFSI_FRAME_DATA_MAX_SIZE        (RFSI_FRAME_MAX_SIZE - RFSI_FRAME_HEAD_TAIL_SIZE)

/*Config Packet size */
#define RFSI_PACKET_MAX_SIZE            RFSI_FRAME_DATA_MAX_SIZE

#define RFSI_SEND_FRAME_RESP    1 //if 1, sending frame need to wait resp every time

typedef enum {
    QRFS_MIN_CMD = -1,
    QRFS_RDM_OPEN_CMD,
    QRFS_RDM_CLOSE_CMD,
    QRFS_FILE_ASEND_CMD,
    QRFS_MAX_CMD
} rFsi_cmd_enum_type;


typedef enum __rFsi_app_type {
    RFSI_APP_MIN,
    RFSI_APP_UNKNOWN = RFSI_APP_MIN,
    RFSI_APP_FILE,
    RFSI_APP_OMA,
    RFSI_APP_MAX
} rFsi_app_type;


typedef struct {
    q_link_type       link;         /* Queue link type */
    rFsi_cmd_enum_type  cmd_id; /* Identifies the command */

#ifdef FEATURE_REMOTEFS_CMD_TIMESTAMP_DEBUG
    time_type         timestamp;    /* Timestamp of the command measured in ms after phone is powered on */
#endif
} rFsi_cmd_hdr_type;

typedef struct {
    rFsi_cmd_hdr_type   hdr;         /* Command header */
    void             *cmd_payload_ptr;   /* Command Payload */
} rFsi_cmd_type;

typedef struct __rFsi_packet_type {
    uint8   data[RFSI_PACKET_MAX_SIZE + 2];//include frame's cs and tail,but will not used in packet
    uint16  len;
} rFsi_packet_type;

typedef void (*time_out_cb_func)(int);


void rFsi_put_cmd(rFsi_cmd_type *cmd_ptr);
rFsi_cmd_type* rFsi_allocate_cmd_buf(size_t payload_size);
void rFsi_release_cmd_buf(rFsi_cmd_type **cmd_pptr);
rex_tcb_type* rFsi_get_selfTcb(void);

boolean rFsi_send_packet(const uint8 *data, uint16 len);

uint32 rFsi_timer_get_val(void);
int8 rFs_timer_reg_func(time_out_cb_func func, int arg);
void rFsi_timer_start(int8 reg_id, int time_val);
void rFsi_timer_stop(int8 reg_id);

void qremotefs_task(uint32 dummy);
#endif /* __QUECTEL_REMOTEFS_TASK_H__ */

