/**
*@file    quectel_remotefs_ui.h
*@date    2016-07-02
*@author  running.qian
*@brief
*/
#ifndef __QUECTEL_REMOTEFS_UI_H__
#define __QUECTEL_REMOTEFS_UI_H__

#include "quectel_remotefs_task.h"
#include "quectel_remotefs_file.h"
#ifdef QUECTEL_QRFS_SUPPORT

/*****************************************************************/
/****************************** FILE *****************************/
/*****************************************************************/
extern int rFs_aopen(const char *pathname, int flags, mode_t mode, rFs_file_resp_cb resp_func, int *open_ret);
extern int rFs_aclose(int fd, rFs_file_resp_cb resp_func, int *close_ret);
extern int rFs_awrite(int fd, const void *buf, size_t count, rFs_file_resp_cb resp_func, int *write_ret);
extern off_t rFs_alseek(int fd, off_t offset, int whence, rFs_file_resp_cb resp_func, int *lseek_ret);
extern int rFs_asize(int fd, rFs_file_resp_cb resp_func, int *size_ret);
extern int rFs_aftruncate(int fd, off_t length, rFs_file_resp_cb resp_func, int *trunc_ret);
extern int rFs_aflush(int fd, rFs_file_resp_cb resp_func, int *size_ret);

extern int rFs_afsize(const char *pathname, rFs_file_resp_cb resp_func, int *fsize_ret);
extern int rFs_astatfs(const char *path, struct rFs_statfs *buf, rFs_file_resp_cb resp_func, int *statfs_ret);
extern int rFs_astat(const char *file_name, struct rFs_stat *buf, rFs_file_resp_cb resp_func, int *stat_ret);
extern int rFs_afind(const char *pathname, rFs_file_resp_cb resp_func, int *find_ret);
extern int rFs_aremove(const char *pathname, rFs_file_resp_cb resp_func, int *remove_ret);
extern int rFs_amove(const char *oldpath, const char *newpath, rFs_file_resp_cb resp_func, int *move_ret);
extern int rFs_alist(const char *path, struct rFs_file_info *buf, rFs_file_resp_cb resp_func, int *list_ret);
extern int rFs_achdir(const char *pathdir, char* curpathbuf, uint16_t bufsize, rFs_file_resp_cb resp_func, int *chdir_ret);
extern int rFs_amkdir(const char *pathdir, rFs_file_resp_cb resp_func, int *mkdir_ret);


extern int rFs_open(const char *pathname, int flags);
extern int rFs_close(int fd);
extern int rFs_write(int fd, const void *buf, size_t count);
extern int rFs_read(int fd, void *buf, size_t count);
extern int rFs_size(int fd);
extern off_t rFs_lseek(int fd, off_t offset,int whence);
extern int rFs_ftruncate(int fd, off_t length);
extern int rFs_flush(int fd);

extern int rFs_fsize(const char *pathname);
extern int rFs_statfs(const char *path, struct rFs_statfs *buf);
extern int rFs_stat(const char *file_name, struct rFs_stat *buf);
extern int rFs_find(const char *pathname);
extern int rFs_remove(const char *pathname);
extern int rFs_move(const char *oldpath, const char *newpath);
extern int rFs_list(const char *path, struct rFs_file_info *buf);
extern int rFs_chdir(const char *pathname, char* curpathbuf, uint16_t bufsize);
extern int rFs_mkdir(const char *pathname);


#include "quectel_file_common.h"
extern int rFs_ufs_info(STORAGE_INFO *storage_info);
extern int8 rFs_ufs_fileInfo(QF_LISTFILEINFO *finf);
extern int64 rFs_ufs_freeSpace(void);
extern int32 rFs_ufs_allfileinfo(UFS_FILE_INFO *files_info);

extern int rFs_sd_info(STORAGE_INFO *storage_info);
extern int8 rFs_sd_fileInfo(QF_LISTFILEINFO *finf);
extern int64 rFs_sd_freeSpace(void);
extern int32 rFs_sd_allfileinfo(UFS_FILE_INFO *files_info);


/*****************************************************************/
/****************************** OMA ******************************/
/*****************************************************************/
extern int rFs_oma_send(const uint8 *data, uint16 len);
#endif
#endif/*__QUECTEL_REMOTEFS_UI_H__*/
