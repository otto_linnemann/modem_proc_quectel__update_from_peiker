/**
 *@file     quectel_remotefs_sio.h
 *@date     2016-07-02
 *@author   running.qian
 *@brief
 */
#ifndef __QUECTEL_REMOTEFS_SIO_H__
#define __QUECTEL_REMOTEFS_SIO_H__

#include "quectel_remotefs_task.h"

typedef enum __rFsi_sio_tx_mode_type {
    RFSI_SIO_TX_FRAME,
    RFSI_SIO_TX_RAWDATA,
} rFsi_sio_tx_mode_type;


boolean rFsi_sio_init(void);
void rFsi_sio_rdm_open_handle(rFsi_cmd_type *cmd_ptr);
void rFsi_sio_rdm_close_handle(rFsi_cmd_type *cmd_ptr);
dsm_watermark_type *rFsi_sio_get_wm_rx(void);
void rFsi_sio_transmit(dsm_item_type *dsm_item_ptr);

#endif /* __QUECTEL_REMOTEFS_SIO_H__ */