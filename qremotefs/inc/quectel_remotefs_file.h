/**
*@file    quectel_remotefs_file.h
*@date    2016-07-02
*@author  running.qian
*@brief
*/
#ifndef __QUECTEL_REMOTEFS_FILE_H__
#define __QUECTEL_REMOTEFS_FILE_H__

#include "quectel_remotefs_task.h"


typedef struct __rFs_file_packet_hdr_type {
    uint8 type;     /* RFSI_APP_FILE */
    uint8 mode;     /* req, resp */
    uint8 cmd;      /* file cmd */
    int8  err;      /* err code */
    int32 fd;
} rFs_file_packet_hdr_type;


#define RFSI_FILE_PACKTE_MAX_SIZE       RFSI_PACKET_MAX_SIZE

#define RFSI_FILE_PACKET_HDR_LEN        (sizeof(rFs_file_packet_hdr_type))
#define RFSI_FILE_PACKET_DATA_MAX_SIZE  (RFSI_PACKET_MAX_SIZE - RFSI_FILE_PACKET_HDR_LEN)

#define RFSI_FILE_PATHNAME_MAX_LEN      128//这个值要小于(RFSI_FILE_PACKET_DATA_MAX_SIZE/2 - 2),且与service端必须一样


struct rFs_statfs {
    long f_type;    /* 文件系统类型 */
    long f_bsize;   /* 经过优化的传输块大小 */
    long f_blocks;  /* 文件系统数据块总数 */
    long f_bfree;   /* 可用块数 */
    long f_bavail;  /* 非超级用户可获取的块数 */
    long f_files;   /* 文件结点总数 */
    long f_ffree;   /* 可用文件结点数 */
//  fsid_t f_fsid;  /* 文件系统标识 */
    long f_namelen; /* 文件名的最大长度 */
};

struct rFs_stat {
    uint16  dev;       /**< Device ID.                   */
    uint32  ino;       /**< Inode number.                */
    uint16  mode;      /**< File mode.                   */
    uint8   nlink;     /**< Number of links.             */
    uint32  size;      /**< File size in bytes.          */
    uint32  blksize;
    uint32  blocks;
    uint32  atime;     /**< Time of last access.         */
    uint32  mtime;     /**< Time of last modification.   */
    uint32  ctime;     /**< Time of last status change.  */
    uint32  rdev;      /**< Major & Minor device number. */
    uint16  uid;       /**< Owner ID.                    */
    uint16  gid;       /**< Group ID.                    */
};

struct rFs_file_info {
    uint8  name[RFSI_FILE_PATHNAME_MAX_LEN];
    uint32 size;
    uint32 mtime;     /**< Time of last modification.   */
};

typedef void (*rFs_file_resp_cb)(void);


void rFsi_file_zombiefd_process(void);
void rFsi_file_client_process(const rFsi_packet_type *packet);
void rFsi_file_asend_handle(void);
int  rFsi_file_init(void);

#endif/*__QUECTEL_REMOTEFS_FILE_H__*/
