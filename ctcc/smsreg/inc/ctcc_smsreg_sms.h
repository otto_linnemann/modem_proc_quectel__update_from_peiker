
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CTCC_SMS_REG_SMS_H__
#define __CTCC_SMS_REG_SMS_H__

#include "ctcc_smsreg_base.h"
#if defined(QUECTEL_CTCC_SMS_REG)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define CTCC_SMS_REG_SMS_MO_TELESERVICE_ID   4098
#define CTCC_SMS_REG_SMS_MT_TELESERVICE_ID   65005

#define CTCC_SMS_REG_SMS_SERVICE_NUM   "10659401"

#define CTCC_SMS_REG_SMS_VP_REL   167
#define CTCC_SMS_REG_SMS_DDT_REL   167

#define CTCC_SMS_REG_SMS_DATA_MAX_LEN   160

#define CTCC_SMS_REG_SMS_SEND_MAX_CNT   3

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef struct
{
    uint8 uSendCnt;
    boolean bIsSending;
} CTCC_SmsRegSmsStruct;

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
extern wms_client_id_type dsatsmsi_client_id;
extern dsat_sms_cmd_s_type  dsatsmsi_pres_cmd;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_SMSREG_SmsSendMsg(void);
extern boolean CTCC_SMSREG_SmsInit(void);
extern CTCC_SmsRegSmsStruct *CTCC_SMSREG_SmsGetCntx(void);

extern boolean CTCC_SMSREG_SmsIsRegisterData(uint8 *pData,uint16 uDataLen);

//<2016/06/13-QCM9XTVG00042C003-P01-Vicent.Gao, <[CTCC] Segment 1==> SMS-REG base code submit.>
extern boolean CTCC_SMSREG_IsEnvReadyCdma(void);
//>2016/06/13-QCM9XTVG00042C003-P01-Vicent.Gao
#endif
#endif  // #ifndef __CTCC_SMS_REG_SMS_H__
