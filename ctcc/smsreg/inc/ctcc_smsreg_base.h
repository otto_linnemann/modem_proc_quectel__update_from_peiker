
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_SMS_REG_BASE_H__
#define __CTCC_SMS_REG_BASE_H__

#include "ctcc_base.h"
#include "dsatsmsi.h"
#include "dsat707sms.h"
#if defined(QUECTEL_CTCC_COMMON)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define CTCC_SMS_REG_DATA_MAX_LEN   (128)
#define CTCC_SMS_REG_CRC_FIX_LEN   (8)

#define CTCC_SMS_REG_PDU_MIN_LEN (1 + 1 + 1 + 1 + CTCC_SMS_REG_CRC_FIX_LEN)
#define CTCC_SMS_REG_PDU_MAX_LEN (CTCC_SMS_REG_PDU_MIN_LEN + CTCC_SMS_REG_DATA_MAX_LEN)

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{

    //Warning!==>Please add new CTCC SMS-REG PROT TYPE upper this line.
    CTCC_SMS_REG_PROT_TYPE_INVALID = 0xFF
} CTCC_SmsRegProtTypeEnum;

typedef enum
{
    CTCC_SMS_REG_CMD_TYPE_AUTO_REG = 0x03,
    CTCC_SMS_REG_CMD_TYPE_REG_SUCC = 0x04,

    //Warning!==>Please add new CTCC SMS-REG CMD TYPE upper this line.
    CTCC_SMS_REG_CMD_TYPE_INVALID = 0xFF
} CTCC_SmsRegCmdTypeEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define CTCC_SMSREG_TRACE_2(f,g1,g2)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2)
#define CTCC_SMSREG_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2,g3)
#define CTCC_SMSREG_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2,g3,g4)
#endif
#endif  // #ifndef __CTCC_SMS_REG_BASE_H__

