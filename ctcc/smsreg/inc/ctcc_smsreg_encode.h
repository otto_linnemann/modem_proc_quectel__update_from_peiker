/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG common use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_SMS_REG_ENCODE_H__
#define __CTCC_SMS_REG_ENCODE_H__

#include "ctcc_smsreg_base.h"
#if defined(QUECTEL_CTCC_SMS_REG)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef struct
{
    uint8 aModInfo[CTCC_BASE_MOD_INFO_MAX_LEN + 1]; //String type
    uint8 aMeid[CTCC_BASE_MEID_MAX_LEN + 1]; //String type
    uint8 aImsi[CTCC_BASE_IMSI_MAX_LEN + 1]; //String type
    uint8 aSwVer[CTCC_BASE_SW_VERSION_MAX_LEN + 1];
} CTCC_SmsRegEncodeDataStruct;

typedef struct
{
    uint8 uProtVer;
    uint8 uCmdType;
    
    CTCC_SmsRegEncodeDataStruct sData;
} CTCC_SmsRegEncodeStruct;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_SMSREG_EncodeCmm(CTCC_SmsRegEncodeStruct *pEncode,uint8 *pData,uint16 uDataMaxLen,uint16 *pDataLen);
extern boolean CTCC_SMSREG_Encode(uint8 *pData,uint16 uDataMaxLen,uint16 *pDataLen);

extern boolean CTCC_SMSREG_WmsTsPackAscii
(
  const char *pAscii,       /* IN */
  uint8 uAsciiLen, /* IN */
  uint8 *pData,            /* OUT */
  uint8 *pDataLen,    /* OUT */
  uint8 *pPaddingBits /* OUT */
);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITION
/////////////////////////////////////////////////////////////

#endif
#endif  // #ifndef __CTCC_SMS_REG_ENCODE_H__


