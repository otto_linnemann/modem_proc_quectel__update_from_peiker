
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CTCC_SMS_REG_PROC_H__
#define __CTCC_SMS_REG_PROC_H__

#include "ctcc_smsreg_base.h"
#if defined(QUECTEL_CTCC_SMS_REG)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
//<2016/06/13-QCM9XTVG00042C003-P01-Vicent.Gao,<[CTCC] Segment 1==> SMS-REG base code submit.>
typedef enum
{
    CTCC_SMS_REG_PROC_STAT_IDLE = 0,
    CTCC_SMS_REG_PROC_STAT_WAIT_REGISTER = 1,

    //Warning!==>Please add new CTCC SMS-REG/PROC STAT upper this line
    CTCC_SMS_REG_PROC_STAT_MAX
} CTCC_SmsRegProcStatEnum;
//>2016/06/13-QCM9XTVG00042C003-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
//<2016/06/13-QCM9XTVG00042C003-P01-Vicent.Gao,<[CTCC] Segment 1==> SMS-REG base code submit.>
typedef struct
{
    uint8 uStat; //It's value is as 'CTCC_SmsRegProcStatEnum'
} CTCC_SmsRegProcStruct;
//>2016/06/13-QCM9XTVG00042C003-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_SMSREG_ProcCheckSendMsg(void);
extern boolean CTCC_SMSREG_ProcMain(void);
extern boolean CTCC_SMSREG_ProcUpdateWaitRegister(void);
extern boolean CTCC_SMSREG_ProcUpdateRegister(void);
#endif
#endif  // #ifndef __CTCC_SMS_REG_PROC_H__
