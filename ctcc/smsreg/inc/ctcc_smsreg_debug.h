
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CTCC_SMS_REG_DEBUG_H__
#define __CTCC_SMS_REG_DEBUG_H__

#include "ctcc_smsreg_base.h"
#if defined(QUECTEL_CTCC_SMS_REG)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_SMSREGDBG_EncodeFull(void);
extern boolean CTCC_SMSREGDBG_EncodeCmm(void);
extern boolean CTCC_SMSREGDBG_EncodeInfo(void);

extern boolean CTCC_SMSREGDBG_SmsSendMsg(void);
#endif
#endif  // #ifndef __CTCC_SMS_REG_DEBUG_H__
