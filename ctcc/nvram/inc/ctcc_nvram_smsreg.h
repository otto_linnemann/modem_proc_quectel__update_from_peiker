
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CTCC_NVRAM_SMS_REG_H__
#define __CTCC_NVRAM_SMS_REG_H__

#include "ctcc_base.h"
#if defined(QUECTEL_CTCC_COMMON)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    CTCC_NVRAM_SMS_REG_REG_STAT_NOT_READY = 0,
    CTCC_NVRAM_SMS_REG_REG_STAT_READY = 1,

    //Warning==>Please add new CTCC NVRAM SMS-REG REG STAT upper this line
    CTCC_NVRAM_SMS_REG_REG_STAT_INVALID = 0xFFFF
} CTCC_NvramSmsRegRegStatEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef struct
{
    uint8 uRegStat; //'CTCC_NvramSmsRegRegStatEnum' value
    char aImsi[CTCC_BASE_IMSI_MAX_LEN + 1];
} CTCC_NvramSmsRegStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_NVRAM_SmsRegInit(void);
extern boolean CTCC_NVRAM_SmsRegSave(void);
extern CTCC_NvramSmsRegStruct *CTCC_NVRAM_SmsRegGetCntx(void);
#endif
#endif  // #ifndef __CTCC_NVRAM_SMS_REG_H__
