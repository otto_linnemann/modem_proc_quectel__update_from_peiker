
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CTCC_NVRAM_DS_REG_H__
#define __CTCC_NVRAM_DS_REG_H__

#include "ctcc_base.h"
#if defined(QUECTEL_CTCC_COMMON)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    CTCC_NVRAM_DS_REG_REG_STAT_NOT_READY = 0,
    CTCC_NVRAM_DS_REG_REG_STAT_READY = 1,

    //Warning==>Please add new CTCC NVRAM DS-REG REG STAT upper this line
    CTCC_NVRAM_DS_REG_REG_STAT_INVALID = 0xFFFF
} CTCC_NvramDSRegRegStatEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef struct
{
//<2016/06/22-QCM9XTCZ00015-P03-Chao.Zhou,<[comment: add new at command for get ueiccid and control enrypt].>
    boolean b_encrypt;
    char    aSvrIP[128];
    uint16  SvrPort;
//<2016/06/22-QCM9XTCZ00015-P03-Chao.Zhou
    char aUEIccid[CTCC_BASE_ICCID_MAX_LEN + 1];
} CTCC_NvramDSRegStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_NVRAM_DSRegInit(void);
extern boolean CTCC_NVRAM_DSRegSave(void);
extern CTCC_NvramDSRegStruct *CTCC_NVRAM_DSRegGetCntx(void);
#endif
#endif  // #ifndef __CTCC_NVRAM_DS_REG_H__
