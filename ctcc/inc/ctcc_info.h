
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_INFO_H__
#define __CTCC_INFO_H__

#include "ctcc_info_module.h"
#include "ctcc_info_card.h"
#include "ctcc_info_device.h"
#include "ctcc_info_network.h"
/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

typedef struct
{
    CTCC_InfoModuleStruct *pModule;
    CTCC_InfoCardStruct *pCard;
    CTCC_InfoDeviceStruct *pDevice;
    CTCC_InfoNetworkStruct *pNetwork;
} CTCC_InfoStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_INFO_Init(void);
extern CTCC_InfoStruct *CTCC_INFO_GetCntx(void);
extern boolean CTCC_INFO_Dsp(void);
#endif
#endif  // #ifndef __CTCC_INFO_H__

