
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_NVRAM_H__
#define __CTCC_NVRAM_H__

#include "ctcc_nvram_smsreg.h"
#include "ctcc_nvram_dsreg.h"

#if defined(QUECTEL_CTCC_COMMON)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef struct
{
    CTCC_NvramSmsRegStruct *pSmsReg;
    CTCC_NvramDSRegStruct	*pDSReg;

} CTCC_NvramStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_NVRAM_Init(void);
extern boolean CTCC_NVRAM_Save(void);
extern CTCC_NvramStruct *CTCC_NVRAM_GetCntx(void);
#endif
#endif  // #ifndef __CTCC_NVRAM_H__

