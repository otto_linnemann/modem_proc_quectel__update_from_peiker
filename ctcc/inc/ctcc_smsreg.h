
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CTCC_SMS_REG_H__
#define __CTCC_SMS_REG_H__

#include "ctcc_smsreg_encode.h"
#include "ctcc_smsreg_sms.h"
#include "ctcc_smsreg_proc.h"
#include "ctcc_smsreg_debug.h"
#if defined(QUECTEL_CTCC_COMMON)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_SMSREG_Init(void);
#endif

#endif  // #ifndef __CTCC_SMS_REG_H__
