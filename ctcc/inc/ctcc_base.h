
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_BASE_H__
#define __CTCC_BASE_H__

#include "ctcc_base_common.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

#define CTCC_BASE_IMSI_MAX_LEN  (15)
#define CTCC_BASE_ICCID_MAX_LEN  (20)

#define CTCC_BASE_MANU_CODE_MAX_LEN  (3)
#define CTCC_BASE_MOD_NAME_MAX_LEN  (16)
#define CTCC_BASE_MOD_INFO_MAX_LEN  (CTCC_BASE_MANU_CODE_MAX_LEN + CTCC_BASE_MOD_NAME_MAX_LEN + 1)

#define CTCC_BASE_IMEI_MAX_LEN  (15)
#define CTCC_BASE_MEID_MAX_LEN  (14)
#define CTCC_BASE_SW_VERSION_MAX_LEN  (60)

#define CTCC_BASE_BASEID_MAX_LEN (16)
#define CTCC_BASE_CDMA_SID_MAX_LEN (5)
#define CTCC_BASE_CDMA_NID_MAX_LEN (5)
/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define CTCC_BASE_HEX_OCT_TO_CHAR(Oct)  (((Oct) < 0x0A) ? ((Oct) + '0') : ((Oct) - 0x0A + 'A'))
#endif
#endif  // #ifndef __CTCC_BASE_H__


