
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_INFO_MODULE_H__
#define __CTCC_INFO_MODULE_H__

#include "ctcc_info_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

typedef struct
{
    uint8 aModInfo[CTCC_BASE_MOD_INFO_MAX_LEN + 1]; //String type  
    uint8 aImei[CTCC_BASE_IMEI_MAX_LEN + 1]; //String type 
    uint8 aMeid[CTCC_BASE_MEID_MAX_LEN + 1]; //String type    
    uint8 aSwVer[CTCC_BASE_SW_VERSION_MAX_LEN + 1];
} CTCC_InfoModuleStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_INFO_ModuleInit(void);
extern CTCC_InfoModuleStruct *CTCC_INFO_ModuleGetCntx(void);
extern boolean CTCC_INFO_ModuleDsp(void);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#endif

#endif  // #ifndef __CTCC_INFO_MODULE_H__

