
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_INFO_NETWORK_H__
#define __CTCC_INFO_NETWORK_H__

#include "ctcc_info_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

typedef struct
{
    uint8 aBaseID[CTCC_BASE_BASEID_MAX_LEN + 1]; //Manufacturer code,string type
    uint8 aCDMASID[CTCC_BASE_CDMA_SID_MAX_LEN + 1]; //Module name,String type
    uint8 aCDMANID[CTCC_BASE_CDMA_NID_MAX_LEN + 1]; //String type
} CTCC_InfoNetworkStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

//<2016/09/19-QCM9XTCZ00003-P03-Chao.Zhou, <[CTCC] Segment 3==>Fix CTCC IOT common issue.>
extern boolean CTCC_INFO_NetworkCdmaInit();
extern quectel_nwinfo_type *quectel_get_network_info(uint8 stack_id);
//<2016/09/19-QCM9XTCZ00003-P03-Chao.Zhou

extern boolean CTCC_INFO_NetworkInit(void);
extern CTCC_InfoNetworkStruct *CTCC_INFO_NetworkGetCntx(void);
extern boolean CTCC_INFO_NetworkDsp(void);
/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif
#endif  // #ifndef __CTCC_INFO_NETWORK_H__

