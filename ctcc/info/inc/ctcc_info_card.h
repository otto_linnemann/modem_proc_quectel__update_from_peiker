
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_INFO_CARD_H__
#define __CTCC_INFO_CARD_H__

#include "ctcc_info_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

typedef enum
{
    CTCC_INFO_CARD_TYPE_ICC = 1,
    CTCC_INFO_CARD_TYPE_UICC = 2,

    //Warning!==>Please add CTCC INFO CARD TYPE upper this line.
    CTCC_INFO_CARD_TYPE_INVALID = 0xFF
} CTCC_InfoCardTypeEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef struct
{
    uint8 uType; //Its value is as 'CTCC_InfoCardTypeEnum'
    uint8 aImsi[CTCC_BASE_IMSI_MAX_LEN + 1]; //String type
    uint8 aCdmaImsi[CTCC_BASE_IMSI_MAX_LEN + 1]; //String type
    uint8 aLteImsi[CTCC_BASE_IMSI_MAX_LEN + 1]; //String type
    uint8 aIccid[CTCC_BASE_ICCID_MAX_LEN + 1]; //String type
    uint8 aMeid[CTCC_BASE_MEID_MAX_LEN + 1]; //String type 
} CTCC_InfoCardStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_INFO_CardInit(void);
extern CTCC_InfoCardStruct *CTCC_INFO_CardGetCntx(void);
extern boolean CTCC_INFO_CardSaveMeid(void);
extern boolean CTCC_INFO_CardDsp(void);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#endif

#endif  // #ifndef __CTCC_INFO_CARD_H__

