
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_INFO_DEVICE_H__
#define __CTCC_INFO_DEVICE_H__

#include "ctcc_info_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

typedef enum
{
    CTCC_INFO_DEVICE_TYPE_PHONE = 0x01,
    CTCC_INFO_DEVICE_TYPE_PAD = 0x02,
    CTCC_INFO_DEVICE_TYPE_DATA_CARD = 0x03,
    CTCC_INFO_DEVICE_TYPE_CPE = 0x04,
    CTCC_INFO_DEVICE_TYPE_MIFI = 0x05,
    CTCC_INFO_DEVICE_TYPE_OTHER = 0x06,
    CTCC_INFO_DEVICE_TYPE_IOT_MODULE = 0x07,
    CTCC_INFO_DEVICE_TYPE_IOT_TERMINAL = 0x08,

    //Warning!==>Please add new CTCC INFO DEVICE TYPE upper this line.
    CTCC_INFO_DEVICE_TYPE_INVALID = 0xFF
} CTCC_InfoDeviceTypeEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef struct
{
    uint8 uType; //Its value is as 'CTCC_InfoDeviceTypeEnum'
} CTCC_InfoDeviceStruct;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CTCC_INFO_DeviceInit(void);
extern CTCC_InfoDeviceStruct *CTCC_INFO_DeviceGetCntx(void);
extern boolean CTCC_INFO_DeviceDsp(void);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#endif
#endif  // #ifndef __CTCC_INFO_DEVICE_H__

