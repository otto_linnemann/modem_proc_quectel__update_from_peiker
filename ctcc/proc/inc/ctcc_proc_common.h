/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC PROC common use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/06/13       Vicent GAO        Create this file by QCM9XTVG00042C001-P02
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_PROC_COMMON_H__
#define __CTCC_PROC_COMMON_H__

#include "ctcc_base.h"
#if defined(QUECTEL_CTCC_COMMON)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define CTCC_PROC_MODE_CS   (1 << 0)
#define CTCC_PROC_MODE_PS   (1 << 1)

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITION
/////////////////////////////////////////////////////////////
extern boolean CTCC_PROC_IsLtePlmn(sys_plmn_id_s_type sPlmnId);
extern boolean CTCC_PROC_Main(void);

extern quectel_nwinfo_type *quectel_get_network_info(uint8 stack_id);
extern void quectel_IOT_rpt_app_start(void);;
#endif
#endif  // #ifndef __CTCC_PROC_COMMON_H__


