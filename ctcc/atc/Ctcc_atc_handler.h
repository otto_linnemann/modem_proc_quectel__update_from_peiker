/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC ATC common use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Frederic ZHANG
 |  Coded    by     :   Frederic ZHANG
 |  Tested   by     :   Frederic ZHANG
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/07/25       Frederic ZHANG        Create this file by QCM9X07FZ00001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_ATC_HANDLER_H__
#define __CTCC_ATC_HANDLER_H__

#include "ctcc_atc_base.h"
#if defined(QUECTEL_CTCC_COMMON)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern dsat_result_enum_type quectel_exec_hwver_cmd
(
  dsat_mode_enum_type mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,     /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,     /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr            /*  Place to put response       */
);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITION
/////////////////////////////////////////////////////////////
#endif

#endif  // #ifndef __CTCC_ATC_HANDLER_H__
