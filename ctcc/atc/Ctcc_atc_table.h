/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC ATC common use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Frederic ZHANG
 |  Coded    by     :   Frederic ZHANG
 |  Tested   by     :   Frederic ZHANG
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/07/25       Frederic ZHANG        Create this file by QCM9X07FZ00001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CTCC_ATC_TABLE_H__
#define __CTCC_ATC_TABLE_H__

#include "ctcc_atc_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

extern const dsati_cmd_type ctcc_atc_table[];
extern const dsati_cmd_ex_type ctcc_atc_table_ex[];
extern const unsigned int ctcc_atc_table_size;

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITION
/////////////////////////////////////////////////////////////

#endif
#endif  // #ifndef __CTCC_ATC_COMMON_H__
