
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CTCC SMS-REG configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/05/24       Vicent GAO        Create this file by QCM9XTVG00042C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CTCC_BASE_COMMON_H__
#define __CTCC_BASE_COMMON_H__

#include "comdef.h"
#include "dsati.h"
#include "msg.h"
#include "nv.h"
#include "../../../datamodem/interface/atcop/src/dsatme.h"
//<2016/06/13-QCM9XTVG00042C001-P02-Vicent.Gao, <[CTCC] Segment 2==> base/smsreg base code submit.>
#include "sys.h"
#include "quectel_nw_atc.h"
//>2016/06/13-QCM9XTVG00042C001-P02-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
#if defined(QUECTEL_CTCC_COMMON)

extern boolean CTCC_DBG_DspResBuf(uint8 *pBuf,uint16 uLen);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define CTCC_TRACE_2(f,g1,g2)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2)
#define CTCC_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2,g3)
#define CTCC_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2,g3,g4)
#endif

#endif  // #ifndef __CTCC_BASE_COMMON_H__
