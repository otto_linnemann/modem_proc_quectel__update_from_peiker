
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CDMA SMS-PDU AT common use functions
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/06/27       Vicent GAO        Create this file by QCM9XTVG00043C004-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CDMA_SMS_PDU_AT_COMMON_H__
#define __CDMA_SMS_PDU_AT_COMMON_H__

#include "cdmasmspdu_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define QCFG_LTE_CTCC_SMS_STORAGE_CDMA  (0)
#define QCFG_LTE_CTCC_SMS_STORAGE_GSM   (1)

//<2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao, <[CDMASMS] Add cmd: qcfg-cdmasms/cmtformat.>
#define QCFG_CDMA_SMS_CMT_FORMAT_CDMA     (0)
#define QCFG_CDMA_SMS_CMT_FORMAT_GSM      (1)
#define QCFG_CDMA_SMS_CMT_FORMAT_DEFAULT  (QCFG_CDMA_SMS_CMT_FORMAT_CDMA)
//>2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern uint8 QCFG_LTECTCC_GetSmsStorage(void);

extern dsat_result_enum_type dsat_exec_qcfg_ltectcc_smsstorage_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

//<2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao, <[CDMASMS] Add cmd: qcfg-cdmasms/cmtformat.>
extern uint8 QCFG_CDSMCF_GetVal(void);
extern boolean QCFG_CDSMCF_SaveNv(uint8 uCmtFormat);
extern boolean QCFG_CDSMCF_ReadNv(void);

extern dsat_result_enum_type dsat_exec_qcfg_cdmasms_cmtformat_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
//>2016/10/08-QCM9XTVG00043C006-P01-Vicent.Gao

#endif  // #ifndef __CDMA_SMS_PDU_AT_COMMON_H__

