#===============================================================================
#
# Modem wrapper script
#
# GENERAL DESCRIPTION
#    build script to load modem data software units
#
# Copyright (c) 2014 by QUECTEL, Incorporated.
# All Rights Reserved.
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 05/27/16   Vicent.Gao     Initial revision
#
#===============================================================================

Import('env')

env = env.Clone()

from glob import glob
from os.path import join, basename
#turn off debug if requested
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG = "")


#turn on debug if requested
if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG = "-g --dwarf2")
    env.Replace(HEXAGON_DBG = "-g")
    env.Replace(GCC_DBG = "-g")

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_DS",
])

#-------------------------------------------------------------------------------
# Necessary Core Public API's
#-------------------------------------------------------------------------------
CORE_PUBLIC_APIS = [
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'WIREDCONNECTIVITY',
    'STORAGE',
    'SECUREMSM',
    'BUSES',
    'IODEVICES',
    'DAL',

    # needs to be last also contains wrong comdef.h
    'KERNEL',
    ]
env.RequirePublicApi(CORE_PUBLIC_APIS, area='core')
env.RequirePublicApi(['IMSVOLTEPUBLIC'], area='IMS_VOLTE')
env.RequirePublicApi(['NAS','AUTH','CSVT','DMS','WDA','WDS','OTDT','IMSDCM', 'IMSRTP'], area='QMIMSGS')
env.RequirePublicApi(['IMSPUBLIC'], area='IMS')
#-------------------------------------------------------------------------------
# Necessary Modem Public API's
#-------------------------------------------------------------------------------
MODEM_PUBLIC_APIS = [
    'MMODE',
    'DATAMODEM',
    'UIM',
    'MCS',
    'ONEX',
    'DATA',
    'HDR',
    'WMS',
    'PBM',
    'NAS',
    'GERAN',
    'WCDMA',
    'UTILS',
    'RFA',
    'GPS',
    'ECALL',
    ]

env.RequirePublicApi(MODEM_PUBLIC_APIS)

segment_load_public_api_list = [
       ('MCFG',                'MCFG'),
       ('GPS',                 'GPS'),
       ('RFA',                 'CDMA'),
       ('RFA',                 'MEAS'),
       ('FW',                  'GERAN'),
       ('RFA',                 'LM'),
       ('RFA',                 'GSM'),
       ('RFA',                 'GNSS'),
       ('RFA',                 'LTE'),
       ('FW',                  'RF'),
       ('FW',                  'COMMON'),
       ]

for api_area,api_name in segment_load_public_api_list:
    env.RequirePublicApi([api_name], area=api_area)
#-------------------------------------------------------------------------------
# Necessary Modem Restricted API's
#-------------------------------------------------------------------------------
MODEM_RESTRICTED_APIS = [
    'MODEM_DATA',
    'MODEM_DATACOMMON',
    'DATAMODEM',
    'MMCP',
    'MCS',
    'ONEX',
    'NAS',
    'HDR',
    'MMODE',
    'RFA',
    'GERAN',
    'UIM',
    'WCDMA',
    'UTILS',
    'MDSP',
    'GPS',
    'FW',
    'LTE',
    ]

env.RequireRestrictedApi(MODEM_RESTRICTED_APIS)
#-------------------------------------------------------------------------------
# Necessary Multimedia Public API's
#-------------------------------------------------------------------------------
MM_PUBLIC_APIS = [
    'AUDIO',
    'MVS',
    ]

env.RequirePublicApi(MM_PUBLIC_APIS, area='multimedia')

#-------------------------------------------------------------------------------
# Necessary Modem Restricted API's
#-------------------------------------------------------------------------------
MODEM_PROTECTED_APIS = [
    'DATA_ATCOP',
    ]
env.RequireProtectedApi(MODEM_PROTECTED_APIS)

#-------------------------------------------------------------------------------
# Required external APIs not built with SCons (if any)
# e.g. ['BREW',]
#-------------------------------------------------------------------------------
REQUIRED_NON_SCONS_APIS = [
    'WCONNECT', 
    'BREW',
    ]

if REQUIRED_NON_SCONS_APIS != []:
  env.RequireExternalApi(REQUIRED_NON_SCONS_APIS)

env.RequirePublicApi(['COMPRESSED_HEAP', ], area='PERF')
env.RequirePublicApi(['QUECTEL_ACT'],area='quectel')
env.RequirePublicApi(['QUECTEL_NV_OPERATOR'],area='quectel')
env.RequirePublicApi(['DATACOMMON'], area='datamodem')
env.RequirePublicApi(['QUECTEL_LED'],area='quectel')
env.RequirePublicApi(['DATAMODEM'], area='datamodem')
env.RequirePublicApi(['UIM'], area='uim')

#-------------------------------------------------------------------------------
# Necessary Modem Restricted API's
#-------------------------------------------------------------------------------
MODEM_PROTECTED_APIS = [
    'DATA_ATCOP',
    ]
env.RequireProtectedApi(MODEM_PROTECTED_APIS)
#-------------------------------------------------------------------------------
# Required external APIs not built with SCons (if any)
# e.g. ['BREW',]
#-------------------------------------------------------------------------------
REQUIRED_NON_SCONS_APIS = [
    'WCONNECT',
    'BREW',
    ]
if REQUIRED_NON_SCONS_APIS != []:
  env.RequireExternalApi(REQUIRED_NON_SCONS_APIS)
#-------------------------------------------------------------------------------
# Non-compliant Private Header Include Paths (Must be removed for CRM builds)
#-------------------------------------------------------------------------------
#if ARGUMENTS.get('SCONS_VIOLATIONS_LEVEL',0) > 99:
print "SCONS VIOLATIONS enabled"

env.PublishPrivateApi('VIOLATIONS',[
    '${INC_ROOT}/data/1x/707/src',
    '${INC_ROOT}/data/1x/bcmcs/src',
    '${INC_ROOT}/core/systemdrivers/clk/inc',
    '${INC_ROOT}/rfa/rf/common/rf/nv/src',
    '${INC_ROOT}/rfa/rf/common/rf/core/src',
    '${INC_ROOT}/rfa/rf/common/rf/rfc/src',
    '${INC_ROOT}/rfa/rf/device/rtr8600_1x/inc',
    '${INC_ROOT}/rfa/rf/device/rfdev_intf/inc',
    '${INC_ROOT}/rfa/rf/hal/p2_1x/inc',
    '${INC_ROOT}/rfa/rf/hal/common/inc',
    '${INC_ROOT}/rfa/rf/wcdma/rf/mc/inc',
    '${INC_ROOT}/rfa/rf/wcdma/rf/nv/inc',
    '${INC_ROOT}/rfa/rf/gsm/rf/core/src',
    '${INC_ROOT}/rfa/variation/inc',
    '${INC_ROOT}/rfa/rf/common/ftm/inc',
    '${INC_ROOT}/utils/oss/oss_asn1_rvds21/include',
    '${INC_ROOT}/gps/gnss/inc',
    '${INC_ROOT}/lte/api/',
    '${INC_ROOT}/core/cust/inc',
    '${INC_ROOT}/uim/api/',
    '${INC_ROOT}/uim/common/inc/',
    '${INC_ROOT}/uim/gstk/src',
    '${INC_ROOT}/uim/pbm/src',
    '${INC_ROOT}/uim/pbm/inc',
    '${INC_ROOT}/uim/variation/inc',
    '${INC_ROOT}/uim/cust/inc/',
    '${INC_ROOT}/mmcp/nas/mm/src/',
    '$(INC_ROOT}/mmcp/variation/inc',
    '$(INC_ROOT)/mmcp/mmode/sd/inc',
    '$(INC_ROOT)/mmcp/mmode/sd/src',
    '${INC_ROOT}/datamodem/protocols/api',
    '${INC_ROOT}/datamodem/interface/atcop/inc',
    '${INC_ROOT}/datamodem/interface/atcop/src',
    '${INC_ROOT}/datamodem/interface/sysapi/inc',
    '${INC_ROOT}/datamodem/interface/dsprofiledb/inc',
    '${INC_ROOT}/datamodem/interface/rmifacectls/inc',
    '${INC_ROOT}/datamodem/interface/qmicore/inc',
    '${INC_ROOT}/datamodem/interface/qmidata/inc',
    '${INC_ROOT}/datamodem/interface/utils/inc',
    '${INC_ROOT}/datamodem/interface/netiface/inc',
    '${INC_ROOT}/datamodem/interface/tasks/inc',
    '${INC_ROOT}/datamodem/3gpp/rmsm/inc',
    '${INC_ROOT}/datamodem/3gpp/ps/inc',
    '${INC_ROOT}/datamodem/3gpp/dsprofile/inc',
    '${INC_ROOT}/datamodem/variation/inc',
    '${INC_ROOT}/datamodem/cust/inc',
    '${INC_ROOT}/datamodem/interface/api',
    '${INC_ROOT}/datamodem/3gpp2/dsmgr/inc',
    '${INC_ROOT}/datamodem/protocols/linklayer/inc',
    '$(INC_ROOT)/perf/compressed_heap/api',
    '$(INC_ROOT)/core/services/nv/src',
    '$(INC_ROOT)/core/memory/dsm/src',
    '$(INC_ROOT)/core/systemdrivers/tlmm/src',
    '$(INC_ROOT)/core/storage/flash/src/dal',
   ])

env.RequirePrivateApi('VIOLATIONS')

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = ".."

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)
#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------
#code shipped as source
MODEM_QUECTEL_CDMASMSPDU_SOURCES = []
CDMASMSPDU_COMMON_SOURCES = [
                            '${BUILDPATH}/base/src/cdmasmspdu_base_base.c',
                            '${BUILDPATH}/base/src/cdmasmspdu_base_gsm.c',
                            '${BUILDPATH}/base/src/cdmasmspdu_base_cdma.c',
                            '${BUILDPATH}/conv/src/cdmasmspdu_conv_gsm.c',
                            '${BUILDPATH}/conv/src/cdmasmspdu_conv_cdma.c',
                            '${BUILDPATH}/conv/src/cdmasmspdu_conv_debug.c',
                            '${BUILDPATH}/at/cdmasmspdu_at_common.c',
                         ]
CDMASMSPDU_LIB_FILES = [
                        '${BUILDPATH}/base/src/cdmasmspdu_base_base.c',
                        '${BUILDPATH}/base/src/cdmasmspdu_base_gsm.c',
                        '${BUILDPATH}/base/src/cdmasmspdu_base_cdma.c',
                        '${BUILDPATH}/conv/src/cdmasmspdu_conv_gsm.c',
                        '${BUILDPATH}/conv/src/cdmasmspdu_conv_cdma.c',
                        '${BUILDPATH}/conv/src/cdmasmspdu_conv_debug.c',
                        '${BUILDPATH}/cmm/src/cdmasmspdu_cmm_retry.c',
                        '${BUILDPATH}/at/cdmasmspdu_at_common.c',
                    ]
                 
MODEM_QUECTEL_CDMASMSPDU_SOURCES += CDMASMSPDU_COMMON_SOURCES

env.AddBinaryLibrary(['MODEM_MODEM', ], '${BUILDPATH}/cdmasmspdu', [MODEM_QUECTEL_CDMASMSPDU_SOURCES,])

#env.LoadSoftwareUnits()
