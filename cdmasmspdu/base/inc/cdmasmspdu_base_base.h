
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CDMA BASE configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/06/14       Vicent GAO        Create this file by QCM9XTVG00043C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CDMA_SMS_PDU_BASE_BASE_H__
#define __CDMA_SMS_PDU_BASE_BASE_H__

#include "comdef.h"
#include "dsati.h"
#include "msg.h"
#include "nv.h"
#include "../../../datamodem/interface/atcop/src/dsatme.h"
#include "wms.h"
#include "../../../datamodem/interface/atcop/src/Dsat707sms.h"
//<2016/06/24-QCM9XTVG00043C003-P01-Vicent.Gao, <[CDMASMS] Segment 1==> sms retry send.>
#include "../../../datamodem/interface/atcop/src/dsatsmsi.h"
//>2016/06/24-QCM9XTVG00043C003-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define CDMA_SMS_DBG_BUF_MAX_LEN  (600)

#define CDMA_SMS_PDU_GSM_SCA_MAX_BYTE_LEN   (1 + 10)
#define CDMA_SMS_PDU_GSM_SCA_FIELD_MAX_BYTE_LEN  (1 + CDMA_SMS_PDU_GSM_SCA_MAX_BYTE_LEN)

#define CDMA_SMS_PDU_GSM_TPDU_MAX_LEN   (180)
#define CDMA_SMS_PDU_GSM_PDU_MAX_LEN  (CDMA_SMS_PDU_GSM_SCA_FIELD_MAX_BYTE_LEN + CDMA_SMS_PDU_GSM_TPDU_MAX_LEN)

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
extern uint8 g_aCDMASMSDbgBuf[CDMA_SMS_DBG_BUF_MAX_LEN + 1];

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean CDMASMS_DBG_DspResBuf(uint8 *pBuf,uint16 uLen);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define CDMASMSPDU_TRACE_2(f,g1,g2)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2)
#define CDMASMSPDU_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2,g3)
#define CDMASMSPDU_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,f,g1,g2,g3,g4)

#endif  // #ifndef __CDMA_SMS_PDU_BASE_BASE_H__

