
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CDMA BASE configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/06/14       Vicent GAO        Create this file by QCM9XTVG00043C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/


#ifndef __CDMA_SMS_PDU_BASE_CDMA_H__
#define __CDMA_SMS_PDU_BASE_CDMA_H__

#include "cdmasmspdu_base_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

extern boolean CDMASMSPDU_BASE_CdmaConvAddrToStr(wms_address_s_type *pAddr, byte *pAddrStr);

extern boolean CDMASMSPDU_BASE_CdmaConvStrToAddr(wms_address_s_type *addr, byte *da);

#endif  // #ifndef __CDMA_SMS_PDU_BASE_CDMA_H__

