
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CDMA SMS-PDU configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/06/14       Vicent GAO        Create this file by QCM9XTVG00043C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CDMA_SMS_PDU_CONV_CDMA_H__
#define __CDMA_SMS_PDU_CONV_CDMA_H__

#include "cdmasmspdu_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define CDMA_SMS_PDU_CONV_CDMA_DEFAULT_MESSAGE_ID   100

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern uint8* CDMASMSPDU_CONV_Cdma(uint8 *pCdmaPdu,uint16 uCdmaPduLen,uint16 *pLen);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __CDMA_SMS_PDU_CONV_CDMA_H__

