
/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2016 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for CDMA SMS-PDU configuration
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2016/06/14       Vicent GAO        Create this file by QCM9XTVG00043C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __CDMA_SMS_PDU_CONV_DEBUG_H__
#define __CDMA_SMS_PDU_CONV_DEBUG_H__

#include "cdmasmspdu_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    CDMA_SMS_DBG_GSM_PDU_TYPE_SUBMIT_GSM = 0,
    CDMA_SMS_DBG_GSM_PDU_TYPE_SUBMIT_HEX = 1,
    CDMA_SMS_DBG_GSM_PDU_TYPE_SUBMIT_UCS2 = 2,

    CDMA_SMS_DBG_GSM_PDU_TYPE_DELIVER_GSM = 3,
    CDMA_SMS_DBG_GSM_PDU_TYPE_DELIVER_HEX = 4,
    CDMA_SMS_DBG_GSM_PDU_TYPE_DELIVER_UCS2 = 5,

    //Warning!==>Please add new CDMA-SMS DBG GSM PDU TYPE upper this line.
    CDMA_SMS_DBG_GSM_PDU_TYPE_INVALID = 0xFFFF
} CDMASMS_DbgGsmPduType;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern int32 QL_STKRAW_HexArrToUint8(char *pSrc,uint16 uSrcLen, uint8 *pDest, uint16 uDestLen);
extern boolean CDMASMSPDU_CONV_DbgGsm(void);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __CDMA_SMS_PDU_CONV_DEBUG_H__

