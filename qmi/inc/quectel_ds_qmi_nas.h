
/*!
  @file
  Quectel_ds_qmi_nas.h

  @brief
  This file defines declaration for QUECTEL DS QMI NAS use

  @see
  Create this file by QCM9X00151C012-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
08/27/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_DS_QMI_NAS_H__
#define __QUECTEL_DS_QMI_NAS_H__

#include "qmi_client.h"
#include "network_access_service_v01.h"

#if defined(QUECTEL_DSQMI_NAS)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean DSQMI_NAS_SetSystemSelectionPreferenceReqSetEmergencyMode(nas_set_system_selection_preference_req_msg_v01 *pReq,uint8 uEmergencyMode);
extern boolean DSQMI_NAS_SetSystemSelectionPreferenceReqSync(nas_set_system_selection_preference_req_msg_v01 *pReq,nas_set_system_selection_preference_resp_msg_v01 *pCnf);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif // #if defined(QUECTEL_DSQMI_NAS)

#endif // #ifndef __QUECTEL_DS_QMI_NAS_H__
