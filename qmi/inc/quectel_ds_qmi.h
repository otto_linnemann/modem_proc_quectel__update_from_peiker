/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL DS QMI use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/07/03       Vicent GAO        Create this file by QCM9X00151C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_DS_QMI_H__
#define __QUECTEL_DS_QMI_H__

#include "qmi_client.h"

#if defined(QUECTEL_DSQMI_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define DS_QMI_MSG_TIMEOUT_MS  (3000)

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef enum
{
    DS_QMI_CLIENT_TYPE_UIM = 0,
    DS_QMI_CLIENT_TYPE_VOICE = 1,
    //<2017/08/08-QCM9X00151C004-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add DSQMI/IMSS common function definition.>
    DS_QMI_CLIENT_TYPE_IMSS = 2,
    //>2017/08/08-QCM9X00151C004-P01-Vicent.Gao
    #if 0
    //<2017/11/01-QCM9X00151C008-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add QUEC-COMMON common function definition.>
    DS_QMI_CLIENT_TYPE_QUECMM = 3,
    //>2017/11/01-QCM9X00151C008-P01-Vicent.Gao
	#endif
//<2017/12/04-QCM9X00151C009-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add UIM REMOTE common function definition.>
#if defined(QUECTEL_DSQMI_UIM_REMOTE)
    DS_QMI_CLIENT_TYPE_UIM_REMOTE,
#endif
//>2017/12/04-QCM9X00151C009-P01-Vicent.Gao
//<2018/08/27-QCM9X00151C012-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add NAS common function definition.>
#if defined(QUECTEL_DSQMI_NAS)
    DS_QMI_CLIENT_TYPE_NAS,
#endif // #if defined(QUECTEL_DSQMI_NAS)
//>2018/08/27-QCM9X00151C012-P01-Vicent.Gao

    //Warning!==> Please add new DS QMI CLIENT TYPE upper this line!
    DS_QMI_CLIENT_TYPE_CNT
} DSQMI_ClientTypeEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef void (*DSQMI_EvtRegFunc)(void);

typedef struct
{
    qmi_client_type hHdlr;
    qmi_client_os_params sParam;
} DSQMI_NotifierStruct;

typedef struct
{
    qmi_client_type hHdlr;
    qmi_client_os_params sParam;
    int iCbData;

    qmi_client_ind_cb fIndCb;
    qmi_client_error_cb fErrCb;
    DSQMI_EvtRegFunc fEvtReg;
} DSQMI_ClientStruct;

typedef struct
{
    DSQMI_ClientTypeEnum eType;
    qmi_idl_service_object_type pSrvObj;    
    DSQMI_NotifierStruct sNotifier;
    DSQMI_ClientStruct sClient;    
    //<2018/01/03-QCM9X00151C001-P05-Vicent.Gao, <[QMI] Segment 5==> DS QMI implementation.>
    boolean bRegSucc;
    //>2018/01/03-QCM9X00151C001-P05-Vicent.Gao
} DSQMI_ElemStruct;

typedef struct
{
    uint16 uInitIdx;
    DSQMI_ElemStruct asElem[DS_QMI_CLIENT_TYPE_CNT];
} DSQMI_CntxStruct;

//<2017/08/10-QCM9X00151C001-P02-Vicent.Gao, <[QMI] Segment 2==> DS QMI implementation.>
typedef struct
{
    DSQMI_ClientTypeEnum eClient;

    qmi_client_type hUserHdlr;
    unsigned int uMsgId;
    void *pVoidResp;
    unsigned int uRespLen;
    void *pVoidRespCbData;
    qmi_client_error_type iErr;
} DSQMI_AsyncRespStruct;
//>2017/08/10-QCM9X00151C001-P02-Vicent.Gao

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

extern boolean DSQMI_InitStart(void);
extern boolean DSQMI_NotifySvcSigHdlr(void);

extern DSQMI_ClientStruct* DSQMI_GetClient(DSQMI_ClientTypeEnum eType);
extern boolean DSQMI_InitCntxUim(DSQMI_ElemStruct *pElem);
extern boolean DSQMI_InitCntxVoice(DSQMI_ElemStruct *pElem);
//<2017/08/08-QCM9X00151C004-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add DSQMI/IMSS common function definition.>
extern boolean DSQMI_InitCntxImss(DSQMI_ElemStruct *pElem);
extern dsat_result_enum_type DSQMI_IMSS_AsyncRespDSHdlr(DSQMI_AsyncRespStruct *pResp);
//>2017/08/08-QCM9X00151C004-P01-Vicent.Gao

//<2017/08/10-QCM9X00151C001-P02-Vicent.Gao, <[QMI] Segment 2==> DS QMI implementation.>
extern boolean DSQMI_AsyncRespForwardToDS
(
    DSQMI_ClientTypeEnum eClient,
    qmi_client_type hUserHdlr,
    unsigned int uMsgId,
    void *pVoidResp,
    unsigned int uRespLen,
    void *pVoidRespCbData,
    qmi_client_error_type iErr
);
//>2017/08/10-QCM9X00151C001-P02-Vicent.Gao

//<2017/12/08-QCM9X00151C001-P04-Vicent.Gao, <[QMI] Segment 4==> DS QMI implementation.>
extern boolean DSQMI_IndForwardToDS
(
    DSQMI_ClientTypeEnum eClient,
    qmi_client_type hUserHdlr,
    unsigned int uMsgId,
    void *pVoidResp,
    unsigned int uRespLen,
    void *pVoidRespCbData,
    qmi_client_error_type iErr
);
//>2017/12/08-QCM9X00151C001-P04-Vicent.Gao

//<2017/10/26-QCM9X00151C003-P03-Vicent.Gao, <[QMI] Segment 3==> Add DS QMI VOICE common function definition.>
extern dsat_result_enum_type DSQMI_VOICE_AsyncRespDSHdlr(DSQMI_AsyncRespStruct *pResp);
//>2017/10/26-QCM9X00151C003-P03-Vicent.Gao

#if 0
//<2017/11/01-QCM9X00151C008-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add QUEC-COMMON common function definition.>
extern boolean DSQMI_InitCntxQuecmm(DSQMI_ElemStruct *pElem);
extern dsat_result_enum_type DSQMI_QUECMM_AsyncRespDSHdlr(DSQMI_AsyncRespStruct *pResp);
//>2017/11/01-QCM9X00151C008-P01-Vicent.Gao
#endif

//<2017/12/04-QCM9X00151C009-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add UIM REMOTE common function definition.>
#if defined(QUECTEL_DSQMI_UIM_REMOTE)
extern boolean DSQMI_InitCntxUimRemote(DSQMI_ElemStruct *pElem);
extern dsat_result_enum_type DSQMI_UIMREMOTE_AsyncRespDSHdlr(DSQMI_AsyncRespStruct *pResp);
extern dsat_result_enum_type DSQMI_UIMREMOTE_IndDSHdlr(DSQMI_AsyncRespStruct *pResp);
#endif //#if defined(QUECTEL_DSQMI_UIM_REMOTE)
//>2017/12/04-QCM9X00151C009-P01-Vicent.Gao

//<2018/08/27-QCM9X00151C012-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add NAS common function definition.>
#if defined(QUECTEL_DSQMI_NAS)
extern boolean DSQMI_InitCntxNas(DSQMI_ElemStruct *pElem);
extern dsat_result_enum_type DSQMI_NAS_AsyncRespDSHdlr(DSQMI_AsyncRespStruct *pResp);
extern dsat_result_enum_type DSQMI_NAS_IndDSHdlr(DSQMI_AsyncRespStruct *pResp);
#endif // #if defined(QUECTEL_DSQMI_NAS)
//>2018/08/27-QCM9X00151C012-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////


#endif // #if defined(QUECTEL_DSQMI_FEATURE)

#endif //#ifndef __QUECTEL_DS_QMI_H__

