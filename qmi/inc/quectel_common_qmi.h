/*===========================================================================

                           QUECTEL_COMMON_QMI . H

DESCRIPTION
  eCall URC event process function  

REFERENCES
  3GPP TS 26.267

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
09/11/2018  Laurence add modeminfo ind.
09/19/2018  Laurence add quec_comm_qmi_send_ind_register_client declaration.
===========================================================================*/

#ifndef QUECTEL_COMMON_QMI_H
#define QUECTEL_COMMON_QMI_H
#include "quec_common_qmi_v01.h"

#include "com_dtypes.h"
#include "qmi_csi.h"

qmi_csi_error quec_common_qmi_send_ind(qmi_client_handle client_handle, unsigned int msg_id, void* ind, unsigned int ind_len);
void quec_comm_qmi_send_ind_all_client(unsigned int msg_id, void* ind, unsigned int ind_len);
void quec_comm_qmi_send_usb_early_enable_ind(uint8_t enable);
void quectel_send_gpio_msg_about_nw_status_light_ind(void); //lory 2017/03/27
void quec_comm_qmi_send_sclk_value_ind(unsigned int value);//lory 2017/04/18
//20180911 by Laurence for quec comm qmi register
void quec_comm_qmi_send_ind_register_client(    unsigned int msg_id,     void* ind,     unsigned int ind_len);
#endif //end QUECTEL_COMMON_QMI_H

