/*==========================================================================
 |              QUECTEL - Build a smarter world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL DS QMI ATC use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/10/26       Vicent GAO        Create this file by QCM9X00151C005-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_DS_QMI_ATC_H__
#define __QUECTEL_DS_QMI_ATC_H__

#include "qmi_client.h"

#if defined(QUECTEL_DSQMI_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
//<2017/10/28-QCM9X00151C007-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add new cmd: AT+QIMSACT.>
#define QIMSACT_STATE_DEACTIVATED   0
#define QIMSACT_STATE_ACTIVATED     1
//>2017/10/28-QCM9X00151C007-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern dsat_result_enum_type quectel_exec_qdial_cmd(dsat_mode_enum_type eMode,const dsati_cmd_type *pParseTab,const tokens_struct_type *pTok,dsm_item_type *pResBuf);

//<2017/10/28-QCM9X00151C006-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add new cmd: AT+ QCHLDIPMPTY.>
extern dsat_result_enum_type quectel_exec_qchldipmpty_cmd
(
    dsat_mode_enum_type eMode,             /*  AT command mode:            */
    const dsati_cmd_type *pParseTab,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *pTok,    /*  Command tokens from parser  */
    dsm_item_type *pResBuf           /*  Place to put response       */
);
//>2017/10/28-QCM9X00151C006-P01-Vicent.Gao

//<2017/10/28-QCM9X00151C007-P01-Vicent.Gao, <[DSQMI] Segment 1==> Add new cmd: AT+QIMSACT.>
extern dsat_result_enum_type dsat_exec_qimsact_cmd
(
    dsat_mode_enum_type eMode, 
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok, 
    dsm_item_type *pResBuf
);

extern uint8 QCFG_IMS_IsVoLTEReady(void);
//>2017/10/28-QCM9X00151C007-P01-Vicent.Gao

//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang, <[QMI] Segment 1==> Add new AT:AT+QIMSCFG="speech_codec".>
extern boolean quectel_is_exist_voice_call(void);
extern cm_call_id_type FUNC_QMICALL_GetCmCallIdFromConnectIndex(uint8 uConIdx);
extern uint8 FUNC_QMICALL_GetConnectIndexFromCmCallId(cm_call_id_type uCmCallId);
//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////


#endif // #if defined(QUECTEL_DSQMI_FEATURE)

#endif //#ifndef __QUECTEL_DS_QMI_ATC_H__

