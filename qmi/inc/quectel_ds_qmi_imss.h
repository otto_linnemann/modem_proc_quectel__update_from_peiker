/*==========================================================================
 |              QUECTEL - Build a smarter world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL DS QMI IMSS use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/08/08       Vicent GAO        Create this file by QCM9X00151C004-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_DS_QMI_IMSS_H__
#define __QUECTEL_DS_QMI_IMSS_H__

#include "qmi_client.h"
#include "Ip_multimedia_subsystem_settings_v01.h"

#if defined(QUECTEL_DSQMI_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

//<2017/08/08-QCM9X00151C004-P02-Vicent.Gao, <[DSQMI] Segment 2==> Add DSQMI/IMSS common function definition.>
extern boolean DSQMI_IMSS_SetRegMgrConfigReqSet(ims_settings_set_reg_mgr_config_req_msg_v01 *pReq,boolean bImsAct);
extern boolean DSQMI_IMSS_SetRegMgrConfigReqSync(ims_settings_set_reg_mgr_config_req_msg_v01 *pReq, ims_settings_set_reg_mgr_config_rsp_msg_v01 *pCnf);
extern boolean DSQMI_IMSS_SetRegMgrConfigReqAsync(ims_settings_set_reg_mgr_config_req_msg_v01 *pReq);
extern boolean DSQMI_IMSS_GetRegMgrConfigReqSync(ims_settings_get_reg_mgr_config_req_msg_v01 *pReq, ims_settings_get_reg_mgr_config_rsp_msg_v01 *pCnf);
//>2017/08/08-QCM9X00151C004-P02-Vicent.Gao

#endif // #if defined(QUECTEL_DSQMI_FEATURE)

#endif //#ifndef __QUECTEL_DS_QMI_IMSS_H__

