/*==========================================================================
 |              QUECTEL - Build a smarter world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL DS QMI VOICE use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/07/03       Vicent GAO        Create this file by QCM9X00151C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_DS_QMI_VOICE_H__
#define __QUECTEL_DS_QMI_VOICE_H__

#include "qmi_client.h"
#include "Voice_service_v02.h"
//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang, <[QMI] Segment 1==> Add new AT:AT+QIMSCFG="speech_codec".>
#include "quectel_ims_atc.h"
//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang

#if defined(QUECTEL_DSQMI_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean DSQMI_VOICE_StartContDtmfReqSet(voice_start_cont_dtmf_req_msg_v02 *pReq,uint8 uCallId,char uDtmf);
extern boolean DSQMI_VOICE_StartContDtmfReqSync(voice_start_cont_dtmf_req_msg_v02 *pReq, voice_start_cont_dtmf_resp_msg_v02 *pCnf);
extern boolean DSQMI_VOICE_StopContDtmfReqSync(voice_stop_cont_dtmf_req_msg_v02 *pReq,voice_stop_cont_dtmf_resp_msg_v02 *pCnf);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

//<2017/07/11-QCM9X00151C003-P01-Vicent.Gao, <[QMI] Segment 1==> Add DS QMI VOICE common function definition.>
extern int8 DSQMI_VOICE_GetAlertingIdxFrCallInfo2(voice_call_info2_type_v02 *pInfos,uint32 uInfoLen);

extern boolean DSQMI_VOICE_GetCallTypeFrRemotePartyNumberExtWithId(voice_remote_party_number_ext_with_id_type_v02 *pRPNumExtWtId,uint8 *pType);
extern boolean DSQMI_VOICE_GetCallTypeFrRemotePartyNumber2(voice_remote_party_number2_type_v02 *pRPNum2,uint8 *pType);
extern boolean DSQMI_VOICE_GetCallTypeFrAllCallStatusInd(int8 iIdx,voice_all_call_status_ind_msg_v02 *pInd,uint8 *pType);

extern boolean DSQMI_VOICE_GetCallNumFrRemotePartyNumberExtWithId(voice_remote_party_number_ext_with_id_type_v02 *pRPNumExtWtId,char *pNum,uint8 uNumMaxLen);
extern boolean DSQMI_VOICE_GetCallNumFrRemotePartyNumber2(voice_remote_party_number2_type_v02 *pRPNum2,char *pNum,uint8 uNumMaxLen);
extern boolean DSQMI_VOICE_GetCallNumFrAllCallStatusInd(int8 iIdx,voice_all_call_status_ind_msg_v02 *pInd,char *pNum,uint8 uNumMaxLen);
//>2017/07/11-QCM9X00151C003-P01-Vicent.Gao

//<2017/07/15-QCM9X00151C003-P02-Vicent.Gao, <[QMI] Segment 2==> Add DS QMI VOICE common function definition.>
extern boolean DSQMI_VOICE_GetAllCallInfoSync(voice_get_all_call_info_resp_msg_v02 *pCnf);
extern boolean DSQMI_VOICE_GetCallNumFrGetAllCallInfoResp(int8 iIdx,voice_get_all_call_info_resp_msg_v02 *pCnf,char *pNum,uint8 uNumMaxLen);
extern boolean DSQMI_VOICE_GetCallTypeFrGetAllCallInfoResp(int8 iIdx,voice_get_all_call_info_resp_msg_v02 *pCnf,uint8 *pType);

extern boolean DSQMI_VOICE_ManageIpCallsReqSetReleaseSpecifiedCallFromConference(voice_manage_ip_calls_req_msg_v02 *pReq,char *pNum);
extern boolean DSQMI_VOICE_ManageIpCallsReqSync(voice_manage_ip_calls_req_msg_v02 *pReq, voice_manage_ip_calls_resp_msg_v02 *pCnf);
//>2017/07/15-QCM9X00151C003-P02-Vicent.Gao

//<2017/10/26-QCM9X00151C003-P03-Vicent.Gao, <[QMI] Segment 3==> Add DS QMI VOICE common function definition.>
extern boolean DSQMI_VOICE_DialCallReqSetPi(voice_dial_call_req_msg_v02 *pReq,char *pNum,uint8 uPi);
extern boolean DSQMI_VOICE_DialCallReqAsync(voice_dial_call_req_msg_v02 *pReq);
//>2017/10/26-QCM9X00151C003-P03-Vicent.Gao

//<2017/08/25-QCM9X00135-P01-Vicent.Gao, <[DSCI] Segment 1==> Fix DSCI VoLTE Alerting issues.>
#if defined(QUECTEL_CALL_FEATURE)
//extern boolean QL_DSCI_QmiReport(voice_all_call_status_ind_msg_v02 *pInd);
#endif // #if defined(QUECTEL_CALL_FEATURE)
//>2017/08/25-QCM9X00135-P01-Vicent.Gao

//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang, <[QMI] Segment 1==> Add new AT:AT+QIMSCFG="speech_codec".>
extern boolean DSQMI_VOICE_ManageIpCallsReqSetSpeechCodec(voice_manage_ip_calls_req_msg_v02 *pReq, uint32 uSpeechCodec, uint8 uCallId);
extern cm_call_id_type FUNC_QMICALL_GetCmCallIdFromConnectIndex(uint8 uConIdx);
extern uint8 FUNC_QMICALL_GetConnectIndexFromCmCallId(cm_call_id_type uCmCallId);
//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang

#endif // #if defined(QUECTEL_DSQMI_FEATURE)

#endif //#ifndef __QUECTEL_DS_QMI_VOICE_H__

