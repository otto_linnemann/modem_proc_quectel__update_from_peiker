/*==========================================================================
 |              QUECTEL - Build a Smarter World.
 |
 |              Copyright(c) 2018 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL DS QMI TIMER use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2018/01/02       Vicent GAO        Create this file by QCM9X00151C011-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_DS_QMI_TIMER_H__
#define __QUECTEL_DS_QMI_TIMER_H__

#include "qmi_client.h"

#if defined(QUECTEL_DSQMI_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define DSQMI_TIMER_NOTIFY_INIT_MS   (1000)

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    DSQMI_TIMER_TYPE_NOTIFY_INIT,

    //Warning!==>Please add new DSQMI TIMER TYPE upper this line.
    DSQMI_TIMER_TYPE_INVALID = 0xFF
} DSQMI_TimerTypeEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern void DSQMI_StartTimer(uint32 uMs);
extern void DSQMI_StartTimerNotifyInit(void);
extern void DSQMI_StopTimer(void);
extern void DSQMI_TimeoutHdlr(void);
extern void DSQMI_TimeoutHdlrNotifyInit(void);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif // #if defined(QUECTEL_DSQMI_FEATURE)

#endif //#ifndef __QUECTEL_DS_QMI_TIMER_H__

