/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL DS QMI UIM use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Vicent GAO
 |  Coded    by     :   Vicent GAO
 |  Tested   by     :   Vicent GAO
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/07/03       Vicent GAO        Create this file by QCM9X00151C001-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_DS_QMI_UIM_H__
#define __QUECTEL_DS_QMI_UIM_H__

#include "qmi_client.h"
#include "user_identity_module_v01.h"

#if defined(QUECTEL_DSQMI_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean DSQMI_UIM_OpenLogicalChannelReqSet(uim_open_logical_channel_req_msg_v01 *pReq,byte *pAid,uint16 uAidLen);
extern boolean DSQMI_UIM_OpenLogicalChannelReqSync(uim_open_logical_channel_req_msg_v01 *pReq,uim_open_logical_channel_resp_msg_v01 *pCnf);
//<2017/07/07-QCM9X00151C002-P01-Aaron.Li, <[QMI] Segment 1==> Add QMI common function definition.>
extern boolean DSQMI_UIM_LogicalChannelReqSetClose(uim_logical_channel_req_msg_v01 *pReq,uint8 uSessionId);
extern boolean DSQMI_UIM_LogicalChannelReqSync(uim_logical_channel_req_msg_v01 *pReq,uim_logical_channel_resp_msg_v01 *pCnf);
//>2017/07/07-QCM9X00151C002-P01-Aaron.Li
//<2017/07/10-QCM9X00151C002-P02-Aaron.Li, <[QMI] Segment 2==> Add QMI common function definition.>
extern boolean DSQMI_UIM_SendApduReqSet(uim_send_apdu_req_msg_v01 *pReq,byte *pApdu,uint16 uApduLen,uint8 uSessionId);
extern boolean DSQMI_UIM_SendApduReqSync(uim_send_apdu_req_msg_v01 *pReq,uim_send_apdu_resp_msg_v01 *pCnf);
//>2017/07/10-QCM9X00151C002-P02-Aaron.Li

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////


#endif // #if defined(QUECTEL_DSQMI_FEATURE)

#endif // #ifndef __QUECTEL_DS_QMI_UIM_H__

