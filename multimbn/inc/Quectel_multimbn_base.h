
/*!  
  @file
  Quectel_multimbn_base.h

  @brief
  This file defines declarations for MUTLI-MBN BASE use

  @see
  Create this file by QCM96B00054C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
12/17/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MULTIMBN_BASE_H__
#define __QUECTEL_MULTIMBN_BASE_H__

#include "mcfg_common.h"
#include "mcfg_proc.h"
#include "mcfg_load.h"
#include "mcfg_trl.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
//#define QMULTIMBN_CONFIG_MAX_COUNT   (25)

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef PACKED struct
{
   uint32	magic1;
   uint16	mcfg_format_ver_num;
   uint16	mcfg_type;
   uint32	mcfg_num_items;
   uint16	mcfg_muxd_carrier_index_info;
   uint16	spare_crc;
} QMULTIMBN_McfgHeaderStruct;

typedef struct
{
  uint32 multi_mbn_size;
  QMULTIMBN_McfgHeaderStruct header;
  uint16 type;
  uint16 length;
  uint32 version;
} QMULTIMBN_HeaderStruct;

typedef struct
{
    mcfg_config_type_e_type eType;
    mcfg_config_s_type sConfig;
} QMULTIMBN_ConfigStruct;

typedef struct
{
    char aCarrierName[MCFG_TRL_CARRIER_NAME_MAX_LEN + 1];
    char aVersion[10 + 1];
    char aRelDate[9 + 1];
} QMULTIMBN_ElemStruct;

typedef struct
{
    QMULTIMBN_ElemStruct *pElem;
    uint16 uElemCount;
} QMULTIMBN_DisplayStruct;

typedef struct
{
    uint32 uMultiMbnVersion;
    QMULTIMBN_ConfigStruct *pConfig;
    uint16 uConfigCount;
} QMULTIMBN_DecodeStruct;

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
extern unsigned char mcfg_multi_MBN[];

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean FUNC_MCFG_GetConfigVerStr(mcfg_config_info_s_type *pInfo,char *pStr,uint16 uStrMaxLen);
extern boolean FUNC_MCFG_GetRelDateStr(mcfg_config_info_s_type *pInfo,char *pStr,uint16 uStrMaxLen);
extern boolean FUNC_MCFG_GetCarrierNameFromMbnS2(mcfg_config_info_s_type *pInfo, char *pCarrierName, uint16 uCarrierNameMaxLen);
extern boolean mcfg_utils_parse_config_info
(
  mcfg_config_s_type      *config,
  mcfg_config_info_s_type *info
);

//<2019/03/02-QCM96B00054C004-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> 9X07/LE2.2 code compatible.>
extern boolean mcfg_utils_check_and_update_config
(
    mcfg_config_s_type *config,
    uint32 uncompressed_buffer_size
);
extern boolean mcfg_utils_free_comp_config_buffer
(
    mcfg_config_s_type *config
);
//>2019/03/02-QCM96B00054C004-P01-Vicent.Gao

extern boolean QL_MULTIMBN_GetConfig(uint8 *pData,QMULTIMBN_ConfigStruct *pMultiMbnConfig);
extern boolean QL_MULTIMBN_GetConfigList(QMULTIMBN_DecodeStruct *pInfo, uint8 *pData, uint32 uMbnNum);
extern boolean QL_MULTIMBN_GetMbnNum(QMULTIMBN_HeaderStruct *pHeader,uint16 *pMbnNum);
extern boolean QL_MULTIMBN_Decode(uint8 *pMultiMbn,QMULTIMBN_DecodeStruct *pDecode);
extern boolean QL_MULTIMBN_ConvConfigToElem(QMULTIMBN_ConfigStruct *pConfig,QMULTIMBN_ElemStruct *pElem);
extern boolean QL_MULTIMBN_ConvDecodeToDisplay(QMULTIMBN_DecodeStruct *pDecode,QMULTIMBN_DisplayStruct *pDisplay);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define MULTIMBN_TRACE_0(f)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MULTIMBN_" f,__FUNCTION__,__LINE__)
#define MULTIMBN_TRACE_1(f,g1)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MULTIMBN_" f,__FUNCTION__,__LINE__,g1)
#define MULTIMBN_TRACE_2(f,g1,g2)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MULTIMBN_" f,__FUNCTION__,__LINE__,g1,g2)
#define MULTIMBN_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_5(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MULTIMBN_" f,__FUNCTION__,__LINE__,g1,g2,g3)
#define MULTIMBN_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_6(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MULTIMBN_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4)
#define MULTIMBN_TRACE_5(f,g1,g2,g3,g4,g5)  MSG_SPRINTF_7(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MULTIMBN_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5)
#define MULTIMBN_TRACE_6(f,g1,g2,g3,g4,g5,g6)  MSG_SPRINTF_8(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MULTIMBN_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5,g6)

#endif  // #ifndef __QUECTEL_MULTIMBN_BASE_H__
