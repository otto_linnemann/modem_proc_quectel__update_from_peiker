
/*!  
  @file
  Quectel_multimbn_common.h

  @brief
  This file defines declarations for MULTI-MBN COMMON use

  @see
  Create this file by QCM96B00054C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
12/18/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MULTIMBN_COMMON_H__
#define __QUECTEL_MULTIMBN_COMMON_H__

#include "quectel_multimbn_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
//<2019/01/11-QCM96B00054C001-P03-Vicent.Gao, <[MULTIMBN] Segment 3==> base code submit.>
#define MULTIMBN_CARRIER_NAME_VERIZON  "hVoLTE-Verizon"
//>2019/01/11-QCM96B00054C001-P03-Vicent.Gao

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean QL_MULTIMBN_Main(void);
//<2019/01/11-QCM96B00054C001-P03-Vicent.Gao, <[MULTIMBN] Segment 3==> base code submit.>
extern boolean FUNC_MCFG_GetVersionRelDateByCarrierName(char *pCarrierName, char *pVersion, char *pRelDate);
//>2019/01/11-QCM96B00054C001-P03-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_MULTIMBN_COMMON_H__
