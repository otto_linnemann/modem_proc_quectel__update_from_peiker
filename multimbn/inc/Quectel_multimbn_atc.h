
/*!  
  @file
  Quectel_multimbn_atc.h

  @brief
  This file defines declarations for MULTI-MBN ATC use

  @see
  Create this file by QCM96B00054C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
12/18/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MULTIMBN_ATC_H__
#define __QUECTEL_MULTIMBN_ATC_H__

#include "quectel_multimbn_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
//<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
#define QUEC_EFS_QMULTIMBN_CFG  "qmultimbn_cfg"
//>2018/12/21-QCM96B00054C002-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
//<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
typedef struct
{
    uint8 uEnable;
    uint8 aRev[23];
} QMULTIMBN_CfgStruct;
//>2018/12/21-QCM96B00054C002-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern QMULTIMBN_DisplayStruct* QL_MULTIMBN_GetDisplay(void);
extern dsat_result_enum_type quectel_exec_qmultimbn_cmd
(
    dsat_mode_enum_type eMode,             
    const dsati_cmd_type *pParseTab,   
    const tokens_struct_type *pTok,  
    dsm_item_type *pResBuf         
);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_MULTIMBN_ATC_H__
