
/*!  
  @file
  Quectel_multimbn_base.c

  @brief
  This file defines interfaces for MULTI-MBN BASE use

  @see
  Create this file by QCM96B00054C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
12/18/2018   vicent        Init
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"

#include "quectel_multimbn_base.h"

#if defined(QUECTEL_MULTIMBN_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Extern functions
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_MULTIMBN_GetMbnNum
=========================================================================*/
/*! @brief 
     Get MBN number in MULTI-MBN

    @param
     <pHeader>  [IN] The pointer of 'QMULTIMBN_HeaderStruct' data
     <pMbnNum>  [OUT] The pointer of MBN number

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_GetMbnNum(QMULTIMBN_HeaderStruct *pHeader,uint16 *pMbnNum)
{
    uint16 uMbnNum = 0;

    if((NULL == pHeader) || (NULL == pMbnNum))
    {
        MULTIMBN_TRACE_2("FAIL! Parameter is INVALID. pHeader:%p,pMbnNum:%p",pHeader,pMbnNum);
        return FALSE;
    }

    uMbnNum = validate_mcfg_header(MCFG_TYPE_SW,(uint8 *)&pHeader->header,MCFG_SUB_ID_FIRST,MCFG_REFRESH_TYPE_SLOT_N_SUBS);
    if(0 == uMbnNum)
    {
        MULTIMBN_TRACE_2("FAIL! validate_mcfg_header FAIL!",0,0);
        return FALSE;
    }

    if(pHeader->type != MCFG_SW_VERSION_I)
    {
        MULTIMBN_TRACE_2("FAIL! type:%d NOT MCFG_SW_VERSION_I(%d)",pHeader->type,MCFG_SW_VERSION_I);
        return FALSE;
    }

    if(pHeader->length != sizeof(pHeader->version))
    {
        MULTIMBN_TRACE_2("FAIL! length:%d(valid:%d) is INVALID.",pHeader->length,sizeof(pHeader->version));
        return FALSE;
    }

    if ((pHeader->version >> 24) != 0xEE) //MCFG_SW_VER_COMPAT_VER_BITS / MCFG_MULTI_MBN_VERSION
    {
        MULTIMBN_TRACE_2("FAIL! version:%X is INVALID.",pHeader->version,0);
        return FALSE;
    }

    *pMbnNum = uMbnNum;

    return TRUE;    
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_GetConfig
=========================================================================*/
/*! @brief 
     Get 'QMULTIMBN_ConfigStruct' data in MULTI-MBN

    @param
     <pData>  [IN] The pointer of data in MULTI-MBN
     <pMultiMbnConfig>  [OUT] The pointer of 'QMULTIMBN_ConfigStruct' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_GetConfig(uint8 *pData,QMULTIMBN_ConfigStruct *pMultiMbnConfig)
{
    mcfg_tlv sMbnItemHeader;
    mcfg_nv_item_hdr_type *pHeaderHdr = NULL;
    mcfg_tlv sMbnItemBody;
    mcfg_nv_item_hdr_type *pBodyHdr = NULL;
    mcfg_config_s_type sConfig;

    uint8 *pDataBody = NULL;
    boolean bResult = FALSE;
    QMULTIMBN_McfgHeaderStruct *pHeaderInfo = NULL;
    uint32 uMcfgAddInfo = 0;

    if((NULL == pData) || (NULL == pMultiMbnConfig))
    {
        MULTIMBN_TRACE_3("FAIL! Parameter is INVALID. pData:%p,pMultiMbnConfig:%p", pData, pMultiMbnConfig, 0);
        return FALSE;
    }

    //Initialize
    memset(&sMbnItemHeader, 0x00, sizeof(sMbnItemHeader));
    pHeaderHdr = &(sMbnItemHeader.hdr);
    memset(&sMbnItemBody, 0x00, sizeof(sMbnItemBody));
    pBodyHdr = &(sMbnItemBody.hdr);
    memset(&sConfig, 0x00, sizeof(sConfig));

    // read mbn filename data
    mcfg_read_tlv(&sMbnItemHeader, pData);
    
    // read mbn contents data
    pDataBody = (pData + pHeaderHdr->mcfg_nv_item_length + MCFG_TL_SIZE_BYTES);
    mcfg_read_tlv(&sMbnItemBody, pDataBody);

    bResult = mcfg_verify_efs_item_contents(&sMbnItemHeader, &sMbnItemBody);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! mcfg_verify_efs_item_contents FAIL!", 0, 0);
        return FALSE;
    }
    
    sConfig.config_len = pBodyHdr->mcfg_nv_item_length;
    if(sConfig.config_len < 0 || sConfig.config_len > MCFG_CONFIG_SIZE_MAX)
    {
        MULTIMBN_TRACE_2("FAIL! config_len:%d(0 - %d) is INVALID.", sConfig.config_len, MCFG_CONFIG_SIZE_MAX);
        return FALSE;
    }
      
    sConfig.config_addr = sMbnItemBody.value;
    //<2019/03/02-QCM96B00054C004-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> 9X07/LE2.2 code compatible.>
    sConfig.comp_status = MCFG_COMP_STATUS_NONE;

    bResult = mcfg_utils_check_and_update_config(&sConfig, MCFG_CONFIG_SIZE_MAX);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! mcfg_utils_check_and_update_config FAIL!", 0, 0);
        return FALSE;
    }
    //>2019/03/02-QCM96B00054C004-P01-Vicent.Gao
    
    pHeaderInfo = (QMULTIMBN_McfgHeaderStruct *)mcfg_load_seg(&sConfig, &uMcfgAddInfo);
    if(NULL == pHeaderInfo)
    {
        MULTIMBN_TRACE_2("FAIL! mcfg_load_seg FAIL!", 0, 0);
        return FALSE;
    }

    pMultiMbnConfig->eType = pHeaderInfo->mcfg_type;
    pMultiMbnConfig->sConfig = sConfig;
    
    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_GetConfigList
=========================================================================*/
/*! @brief 
     Get list of MULTI-MBN config

    @param
     <pDecode>  [OUT] The pointer of 'QMULTIMBN_DecodeStruct' data
     <pData>  [IN] The pointer of MULTI-MBN data
     <uMbnNum>  [IN] The number of MBN

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_GetConfigList(QMULTIMBN_DecodeStruct *pDecode, uint8 *pData, uint32 uConfigCount)
{
    mcfg_item_prefix_type sMcfgItemPrefix;
    uint8 uMcfgItemTypeInt = 0;
    
    uint32 i = 0;
    QMULTIMBN_ConfigStruct *pConfig = NULL;
    boolean bResult = FALSE;

    if((NULL == pDecode) || (NULL == pData) || (0 == uConfigCount))
    {
        MULTIMBN_TRACE_3("FAIL! Parameter is INVALID. pDecode:%p,pData:%p,uConfigCount:%d", pDecode, pData, uConfigCount);
        return FALSE;
    }

    //Initialize
    memset(&sMcfgItemPrefix, 0x00, sizeof(sMcfgItemPrefix));

    // Starts from 1 as the first element is MCFG_VERSION, ram_cust_data_ptr points to the start of MCFG Item after version
    for (i = 0; i < uConfigCount; i++) 
    {
        // Parse the MCFG Item Prefix for further processing
        uMcfgItemTypeInt = mcfg_parse_item_prefix(
                               MCFG_TYPE_SW, 
                               pData, 
                               &sMcfgItemPrefix, 
                               MCFG_REFRESH_TYPE_SLOT_N_SUBS, 
                               MCFG_SUB_ID_FIRST
                           );

        // Set PTR to the start of the MCFG item data after prefix
        pData += sizeof(sMcfgItemPrefix);

        if(MCFG_INT_EFS_FILE == uMcfgItemTypeInt)
        {
            pConfig = &pDecode->pConfig[i]; 
            
            bResult = QL_MULTIMBN_GetConfig(pData,pConfig);
            if(FALSE == bResult)
            {
                MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_GetConfig FAIL! i:%d", i, 0);
            }
        }
        else
        {
            MULTIMBN_TRACE_2("FAIL! [%d]->uMcfgItemTypeInt:%d NOT support.", i, uMcfgItemTypeInt);
        }

        pData += (sMcfgItemPrefix.item_length - sizeof(mcfg_item_prefix_type));
    } 

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_Decode
=========================================================================*/
/*! @brief 
     Decode for MULTI-MBN

    @param
     <pMultiMbn>  [IN] The pointer of MULTI-MBN data
     <pDecode>  [OUT] The pointer of 'QMULTIMBN_DecodeStruct' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_Decode(uint8 *pMultiMbn,QMULTIMBN_DecodeStruct *pDecode)
{
    QMULTIMBN_HeaderStruct *pMultiMbnHdr = NULL;
    uint8 *pMultiMbnData = NULL;    
    boolean bResult = FALSE;

    if((NULL == pMultiMbn) || (NULL == pDecode))
    {
        MULTIMBN_TRACE_2("FAIL! Paramter is INVALID. pMultiMbn:%p,pDecode:%p", pMultiMbn, pDecode);
        return FALSE;
    }

    //Initialize
    pMultiMbnHdr = (QMULTIMBN_HeaderStruct *)pMultiMbn;
    pMultiMbnData = (pMultiMbn + sizeof(QMULTIMBN_HeaderStruct));

    pDecode->uMultiMbnVersion = pMultiMbnHdr->version;

    bResult = QL_MULTIMBN_GetConfigList(pDecode,pMultiMbnData,pDecode->uConfigCount);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_GetConfigList FAIL!", 0, 0);
        return FALSE;
    }

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_ConvConfigToElem
=========================================================================*/
/*! @brief 
     Covert 'QMULTIMBN_ConfigStruct' data to 'QMULTIMBN_ElemStruct' data

    @param
     <pConfig>  [IN] The pointer of 'QMULTIMBN_ConfigStruct' data
     <pElem>  [OUT] The pointer of 'QMULTIMBN_ElemStruct' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_ConvConfigToElem(QMULTIMBN_ConfigStruct *pConfig,QMULTIMBN_ElemStruct *pElem)
{
    mcfg_config_info_s_type sMcfgConfigInfo;
    boolean bResult = FALSE;

    if((NULL == pConfig) || (NULL == pElem))
    {
        MULTIMBN_TRACE_2("FAIL! Paramter is INVALID. pConfig:%p,pElem:%p", pConfig, pElem);
        return FALSE;
    }

    //Initialize
    memset(&sMcfgConfigInfo, 0x00, sizeof(sMcfgConfigInfo));

    bResult = mcfg_utils_parse_config_info(&(pConfig->sConfig), &sMcfgConfigInfo);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! mcfg_utils_parse_config_info FAIL!", 0, 0);
        return FALSE;
    }

    bResult = FUNC_MCFG_GetCarrierNameFromMbnS2(&sMcfgConfigInfo,pElem->aCarrierName,sizeof(pElem->aCarrierName));
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! FUNC_MCFG_GetCarrierNameFromMbnS2 FAIL!", 0, 0);
        return FALSE;
    }

    bResult = FUNC_MCFG_GetConfigVerStr(&sMcfgConfigInfo,pElem->aVersion,sizeof(pElem->aVersion));
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! FUNC_MCFG_GetConfigVerStr FAIL!", 0, 0);
        return FALSE;
    }

    bResult = FUNC_MCFG_GetRelDateStr(&sMcfgConfigInfo,pElem->aRelDate,sizeof(pElem->aRelDate));
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! FUNC_MCFG_GetRelDateStr FAIL!", 0, 0);
        return FALSE;
    }

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_ConvDecodeToDisplay
=========================================================================*/
/*! @brief 
     Covert 'QMULTIMBN_DecodeStruct' data to 'QMULTIMBN_DisplayStruct' data

    @param
     <pDecode>  [IN] The pointer of 'QMULTIMBN_DecodeStruct' data
     <pDisplay>  [OUT] The pointer of 'QMULTIMBN_DisplayStruct' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_ConvDecodeToDisplay(QMULTIMBN_DecodeStruct *pDecode,QMULTIMBN_DisplayStruct *pDisplay)
{
    uint32 i = 0;
    boolean bResult = FALSE;
    QMULTIMBN_ElemStruct *pElem = NULL;

    if((NULL == pDecode) || (NULL == pDisplay))
    {
        MULTIMBN_TRACE_2("FAIL! Paramter is INVALID. pDecode:%p,pDisplay:%p", pDecode, pDisplay);
        return FALSE;
    }

    pElem = &(pDisplay->pElem[0]);

    strcpy(pElem->aCarrierName,"multi-mbn");
    snprintf(pElem->aVersion,sizeof(pElem->aVersion),"0x%08X",pDecode->uMultiMbnVersion);
    snprintf(pElem->aRelDate,sizeof(pElem->aRelDate),"%09lu",0);
    
    for(i = 0; i < pDecode->uConfigCount; i++)
    {
        pElem = &(pDisplay->pElem[i + 1]);
    
        bResult = QL_MULTIMBN_ConvConfigToElem(&(pDecode->pConfig[i]),pElem);
        if(FALSE == bResult)
        {
            MULTIMBN_TRACE_2("WARNING! QL_MULTIMBN_ConvConfigToElem FAIL! i:%d", i, 0);
        }
    }

    return TRUE;
}

#endif //#if defined(QUECTEL_MULTIMBN_FEATURE)

