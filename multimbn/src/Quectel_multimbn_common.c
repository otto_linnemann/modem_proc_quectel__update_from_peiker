
/*!  
  @file
  Quectel_multimbn_common.c

  @brief
  This file defines interfaces for MULTI-MBN COMMON use

  @see
  Create this file by QCM96B00054C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
12/18/2018   vicent        Init
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"

#include "quectel_multimbn_common.h"

#if defined(QUECTEL_MULTIMBN_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////
static QMULTIMBN_DecodeStruct sg_sMULTIMBNDecode;
static QMULTIMBN_DisplayStruct sg_sMULTIMBNDisplay;

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_MULTIMBN_sAllocDecode
=========================================================================*/
/*! @brief 
     Allocate memory of 'QMULTIMBN_DecodeStruct' data for MULTI-MBN

    @param
     <pDecode>  [OUT] The pointer of 'QMULTIMBN_DecodeStruct' data
     <uMbnNum>  [IN] Number of MBN

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MULTIMBN_sAllocDecode(QMULTIMBN_DecodeStruct *pDecode,uint16 uMbnNum)
{
    QMULTIMBN_ConfigStruct *pConfig = NULL;
    uint16 uConfigCount = 0;
    uint16 uConfigSize = 0;

    if((NULL == pDecode) || (uMbnNum < 2))
    {
        MULTIMBN_TRACE_2("FAIL! Parameter is INVALID. pDecode:%p,uMbnNum:%d(min:2)", pDecode, uMbnNum);
        return FALSE;
    }

    if(pDecode->pConfig != NULL)
    {
        MULTIMBN_TRACE_2("WARNING! pDecode->pConfig already alloc, free first.",0,0);
        dsatutil_free_memory((void *)pDecode->pConfig); pDecode->pConfig = NULL;
    }

    uConfigCount = uMbnNum - 1;
    uConfigSize = uConfigCount * sizeof(QMULTIMBN_ConfigStruct);

    pConfig = (QMULTIMBN_ConfigStruct *)dsat_alloc_memory(uConfigSize,FALSE);
    if(NULL == pConfig)
    {
        MULTIMBN_TRACE_2("FAIL! dsat_alloc_memory FAIL! size:%s",uConfigSize,0);
        return FALSE;
    }

    pDecode->pConfig = pConfig;
    pDecode->uConfigCount = uConfigCount;

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_sAllocDisplay
=========================================================================*/
/*! @brief 
     Allocate memory of 'QMULTIMBN_DisplayStruct' data for MULTI-MBN

    @param
     <pDisplay>  [OUT] The pointer of 'QMULTIMBN_DisplayStruct' data
     <uMbnNum>  [IN] Number of MBN

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MULTIMBN_sAllocDisplay(QMULTIMBN_DisplayStruct *pDisplay,uint16 uMbnNum)
{
    QMULTIMBN_ElemStruct *pElem = NULL;
    uint16 uElemCount = 0;
    uint16 uElemSize = 0;

    if((NULL == pDisplay) || (uMbnNum < 1))
    {
        MULTIMBN_TRACE_2("FAIL! Parameter is INVALID. pDisplay:%p,uMbnNum:%d(min:1)", pDisplay, uMbnNum);
        return FALSE;
    }

    if(pDisplay->pElem != NULL)
    {
        MULTIMBN_TRACE_2("WARNING! pDisplay->pElem already alloc, free first.",0,0);
        dsatutil_free_memory((void *)pDisplay->pElem); pDisplay->pElem = NULL;
    }

    uElemCount = uMbnNum;
    uElemSize = uElemCount * sizeof(QMULTIMBN_ElemStruct);

    pElem = (QMULTIMBN_ElemStruct *)dsat_alloc_memory(uElemSize,FALSE);
    if(NULL == pElem)
    {
        MULTIMBN_TRACE_2("FAIL! dsat_alloc_memory FAIL! size:%s",uElemSize,0);
        return FALSE;
    }

    pDisplay->pElem = pElem;
    pDisplay->uElemCount = uElemCount;

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_sFreeDecode
=========================================================================*/
/*! @brief 
     Free memory of 'QMULTIMBN_DecodeStruct' data for MULTI-MBN

    @param
     <pDecode>  [IN/OUT] The pointer of 'QMULTIMBN_DecodeStruct' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MULTIMBN_sFreeDecode(QMULTIMBN_DecodeStruct *pDecode)
{
    //<2019/03/02-QCM96B00054C004-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> 9X07/LE2.2 code compatible.>
    uint16 i = 0;
    mcfg_config_s_type *pMcfgConfig = NULL;
    boolean bResult = FALSE;
    //>2019/03/02-QCM96B00054C004-P01-Vicent.Gao

    if(NULL == pDecode)
    {
        MULTIMBN_TRACE_2("FAIL! pDecode is NULL.", 0, 0);
        return FALSE;
    }

    if(NULL == pDecode->pConfig)
    {
        MULTIMBN_TRACE_2("WARNING! pDecode->pConfig is NULL,no need free.", 0, 0);
        return TRUE;
    }

    //<2019/03/02-QCM96B00054C004-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> 9X07/LE2.2 code compatible.>
    for(i = 0; i < pDecode->uConfigCount; i++)
    {
        pMcfgConfig = &(pDecode->pConfig[i].sConfig);

        bResult = mcfg_utils_free_comp_config_buffer(pMcfgConfig);
        if(FALSE == bResult)
        {
            MULTIMBN_TRACE_2("WARNING! mcfg_utils_free_comp_config_buffer FAIL! i:%d", i, 0);
        }
    }
    //>2019/03/02-QCM96B00054C004-P01-Vicent.Gao

    dsatutil_free_memory((void *)pDecode->pConfig);

    memset(pDecode,0x00,sizeof(QMULTIMBN_DecodeStruct));

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_sAlloc
=========================================================================*/
/*! @brief 
     Allocate memory for MULTI-MBN

    @param
     <uMbnNum>  [IN] Number of MBN

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MULTIMBN_sAlloc(uint16 uMbnNum)
{    
    boolean bResult = FALSE;
    QMULTIMBN_DecodeStruct *pDecode = &sg_sMULTIMBNDecode;
    QMULTIMBN_DisplayStruct *pDisplay = &sg_sMULTIMBNDisplay;
    
    if(0 == uMbnNum)
    {
        MULTIMBN_TRACE_2("FAIL! uMbnNum is 0", 0, 0);
        return FALSE;
    }
    
    bResult = QL_MULTIMBN_sAllocDecode(pDecode,uMbnNum);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_sAllocDecode FAIL! uMbnNum:%d", uMbnNum, 0);
        return FALSE;
    }

    bResult = QL_MULTIMBN_sAllocDisplay(pDisplay,uMbnNum);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_sAllocDecode FAIL! uMbnNum:%d", uMbnNum, 0);
        return FALSE;
    }

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_sFree
=========================================================================*/
/*! @brief 
     Free memory for MULTI-MBN

    @param
     VOID

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MULTIMBN_sFree(void)
{
    QMULTIMBN_DecodeStruct *pDecode = &sg_sMULTIMBNDecode;
    boolean bResult = FALSE;

    bResult = QL_MULTIMBN_sFreeDecode(pDecode);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_sFreeDecode FAIL!", 0, 0);
        return FALSE;
    }

    return TRUE;
}

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Extern functions
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_MULTIMBN_Main
=========================================================================*/
/*! @brief 
     Main process for MULTI-MBN

    @param
     VOID

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_Main(void)
{
    boolean bResult = FALSE;
    uint8 *pMultiMbn = (uint8 *)mcfg_multi_MBN;
    QMULTIMBN_HeaderStruct *pMultiMbnHdr = (QMULTIMBN_HeaderStruct *)pMultiMbn;
    uint16 uMbnNum = 0;
    
    QMULTIMBN_DecodeStruct *pDecode = &sg_sMULTIMBNDecode;
    QMULTIMBN_DisplayStruct *pDisplay = &sg_sMULTIMBNDisplay;

    bResult = QL_MULTIMBN_GetMbnNum(pMultiMbnHdr,&uMbnNum);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_GetMbnNum FAIL!", 0, 0);
        return FALSE;
    }

    bResult = QL_MULTIMBN_sAlloc(uMbnNum);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_sAlloc FAIL! uMbnNum:%d", uMbnNum, 0);
        return FALSE;
    }

    bResult = QL_MULTIMBN_Decode(pMultiMbn,pDecode);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_Decode FAIL!", 0, 0);
        return FALSE;
    }

    bResult = QL_MULTIMBN_ConvDecodeToDisplay(pDecode,pDisplay);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_ConvDecodeToDisplay FAIL!", 0, 0);
        return FALSE;
    }

    bResult = QL_MULTIMBN_sFree();
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_sFree FAIL!", 0, 0);
        return FALSE;
    }

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_GetDisplay
=========================================================================*/
/*! @brief 
     Get the pointer of 'QMULTIMBN_DisplayStruct' data

    @param
     VOID

    @return
     The pointer of 'QMULTIMBN_DisplayStruct' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
QMULTIMBN_DisplayStruct* QL_MULTIMBN_GetDisplay(void)
{
    return &sg_sMULTIMBNDisplay;
}

//<2019/01/11-QCM96B00054C001-P03-Vicent.Gao, <[MULTIMBN] Segment 3==> base code submit.>
/*========================================================================
  FUNCTION:  QL_MULTIMBN_GetVersionRelDate
=========================================================================*/
/*! @brief 
     Get <Version>,<RelDate> by carrier name in MULTI-MBN

    @param
     <pDisplay>     [IN] The pointer of 'QMULTIMBN_DisplayStruct' data
     <pCarrierName> [IN] The pointer of carrier name
     <pVersion>     [OUT] The pointer of version string
     <pRelDate>     [OUT] The pointer of release date string

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_GetVersionRelDate
(
    QMULTIMBN_DisplayStruct *pDisplay,
    char *pCarrierName,
    char *pVersion,
    char *pRelDate
)
{
    uint16 i = 0;
    QMULTIMBN_ElemStruct *pElem = NULL;

    if(    (NULL == pDisplay) 
        || (NULL == pCarrierName)
        || (NULL == pVersion)
        || (NULL == pRelDate)
      )
    {
        MULTIMBN_TRACE_4(
            "FAIL! Parameter is INVALID. pDisplay:%p,pCarrierName:%p,pVersion:%p,pRelDate:%p", 
            pDisplay, 
            pCarrierName, 
            pVersion, 
            pRelDate
        );
        
        return FALSE;
    }

    if(NULL == pDisplay->pElem)
    {
        MULTIMBN_TRACE_2("FAIL! pDisplay->pElem is NULL.", 0, 0);
        return FALSE;
    }

    for(i = 0; i < pDisplay->uElemCount; i++)
    {
        pElem = &(pDisplay->pElem[i]);

        if(0 == dsatutil_strcmp_ig_sp_case((byte*)pCarrierName,(byte*)pElem->aCarrierName))
        {
            break;
        }
    }

    if(i < pDisplay->uElemCount)
    {
        strcpy(pVersion,pElem->aVersion);
        strcpy(pRelDate,pElem->aRelDate);
        
        return TRUE;
    }

    return FALSE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_VersionRelDateIsSameAsEfs
=========================================================================*/
/*! @brief 
     Check <Version>,<RelDate> of one MBN in MULTI-MBN and EFS

    @param
     <pCarrierName> [IN] The pointer of carrier name

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_VersionRelDateIsSameAsEfs(char *pCarrierName)
{
    boolean bResult = FALSE;
    char aMbnVersionMulti[20] = {0,};
    char aMbnRelDateMulti[20] = {0,};
    char aMbnVersionEfs[20] = {0,};
    char aMbnRelDateEfs[20] = {0,};

    if(NULL == pCarrierName)
    {
        MULTIMBN_TRACE_2("FAIL! pCarrierName is NULL.", 0, 0);
        return FALSE;
    }

    bResult = QL_MULTIMBN_GetVersionRelDate(
                  &sg_sMULTIMBNDisplay,
                  pCarrierName,
                  aMbnVersionMulti,
                  aMbnRelDateMulti
              );
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_GetVersionRelDate(%s) FAIL!", pCarrierName, 0);
        return FALSE;
    }

    bResult = FUNC_MCFG_GetVersionRelDateByCarrierName(
                  pCarrierName,
                  aMbnVersionEfs,
                  aMbnRelDateEfs
              );
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("FAIL! FUNC_MCFG_GetVersionRelDateByCarrierName(%s) FAIL!", pCarrierName, 0);
        return FALSE;
    }

    if(     (0 == dsatutil_strcmp_ig_sp_case((byte*)aMbnVersionMulti,(byte*)aMbnVersionEfs))
         && (0 == dsatutil_strcmp_ig_sp_case((byte*)aMbnRelDateMulti,(byte*)aMbnRelDateEfs))
      )
    {
        MULTIMBN_TRACE_2("NOTE: verison:%s,reldate:%s are same.", aMbnVersionMulti, aMbnRelDateMulti);
        return TRUE;
    }

    return FALSE;
}

/*========================================================================
  FUNCTION:  QL_MULTIMBN_IsNeedLoad
=========================================================================*/
/*! @brief 
     Check if need to load MULTI-MBN

    @param
     VOID

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MULTIMBN_IsNeedLoad(void)
{
    QMULTIMBN_HeaderStruct *pMultiMbnHdr = (QMULTIMBN_HeaderStruct *)mcfg_multi_MBN;
    mcfg_nv_status_e_type eMcfgStatus = MCFG_NV_STATUS_OK;
    uint32 uMultiMbnVersionEfs = 0;
    boolean bResult = FALSE;

    eMcfgStatus = mcfg_nv_read(
                      MCFG_NV_ITEM_MULTI_MBN_CONFIG_VERSION_I,
                      0, 
                      MCFG_SUB_ID_FIRST, 
                      (uint8 *)&uMultiMbnVersionEfs, 
                      sizeof(uMultiMbnVersionEfs)
                  );
    if(eMcfgStatus != MCFG_NV_STATUS_OK)
    {
        MULTIMBN_TRACE_2("NOTE: mcfg_nv_read(MULTI_MBN_CONFIG_VERSION_I) FAIL! eMcfgStatus:%d,need load.", eMcfgStatus, 0);
        return TRUE;
    }

    if(pMultiMbnHdr->version != uMultiMbnVersionEfs)
    {
        MULTIMBN_TRACE_2("NOTE: version:%X not same as uMultiMbnVersionEfs:%X,need load", pMultiMbnHdr->version, uMultiMbnVersionEfs);
        return TRUE;
    }

#if    defined (__QUECTEL_PROJECT_EC25AF_FD__)	\
    || defined (__QUECTEL_PROJECT_EC25AF__)
    bResult = QL_MULTIMBN_VersionRelDateIsSameAsEfs(MULTIMBN_CARRIER_NAME_VERIZON);
    if(FALSE == bResult)
    {
        MULTIMBN_TRACE_2("NOTE: QL_MULTIMBN_VersionRelDateIsSameAsEfs(%s) FAIL! need load", MULTIMBN_CARRIER_NAME_VERIZON, 0);
        return TRUE;
    }
#endif

    return FALSE;
}
//>2019/01/11-QCM96B00054C001-P03-Vicent.Gao

#endif //#if defined(QUECTEL_MULTIMBN_FEATURE)
