
/*!  
  @file
  Quectel_multimbn_atc.c

  @brief
  This file defines interfaces for MULTI-MBN ATC use

  @see
  Create this file by QCM96B00054C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
12/18/2018   vicent        Init
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"

#include "quectel_multimbn_atc.h"

#if defined(QUECTEL_MULTIMBN_FEATURE)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////
//<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
static QMULTIMBN_CfgStruct sg_sQMULTIMBNCfg;
//>2018/12/21-QCM96B00054C002-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////

//<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
/*========================================================================
  FUNCTION:  QL_QMULTIMBN_sSetDefault
=========================================================================*/
/*! @brief 
     Set default value of AT+QMULTIMBN

    @param
     VOID

    @return
     'boolean' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_QMULTIMBN_sSetDefault(void)
{
    QMULTIMBN_CfgStruct *pCfg = &sg_sQMULTIMBNCfg;

    memset(pCfg, 0x00, sizeof(QMULTIMBN_CfgStruct));

    pCfg->uEnable = 1;

    return TRUE;
}
//>2018/12/21-QCM96B00054C002-P01-Vicent.Gao

/*========================================================================
  FUNCTION:  QL_QMULTIMBN_sCmdHdlrTest
=========================================================================*/
/*! @brief 
     Command handler of AT+QMULTIMBN=?

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QMULTIMBN_sCmdHdlrTest
(
    dsat_mode_enum_type eMode,
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok,
    dsm_item_type *pResBuf
)
{
    pResBuf->used += sprintf((char*)(pResBuf->data_ptr + pResBuf->used),"%s: \"list\"",pTok->name);
    //<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
    pResBuf->used += sprintf((char*)(pResBuf->data_ptr + pResBuf->used),"\r\n%s: \"enable\",(0,1)",pTok->name);
    pResBuf->used += sprintf((char*)(pResBuf->data_ptr + pResBuf->used),"\r\n%s: \"reload\"",pTok->name);
    //>2018/12/21-QCM96B00054C002-P01-Vicent.Gao

    return DSAT_OK;
}

/*========================================================================
  FUNCTION:  QL_QMULTIMBN_sCmdHdlrSetList
=========================================================================*/
/*! @brief 
     Command handler of AT+QMULTIMBN="list"

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QMULTIMBN_sCmdHdlrSetList
(
    dsat_mode_enum_type eMode,
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok,
    dsm_item_type *pResBuf
)
{
    QMULTIMBN_DisplayStruct *pDisplay = NULL;
    QMULTIMBN_ElemStruct *pElem = NULL;
    uint16 i = 0;
    char aBuf[120] = {0,};
    uint16 uBufLen = 0;

    pDisplay = QL_MULTIMBN_GetDisplay();
    if(NULL == pDisplay)
    {
        MULTIMBN_TRACE_2("FAIL! QL_MULTIMBN_GetDisplay FAIL!", 0, 0);
        return DSAT_ERROR;
    }

    if(NULL == pDisplay->pElem)
    {
        MULTIMBN_TRACE_2("FAIL! pDisplay->pElem is NULL.", 0, 0);
        return DSAT_ERROR;
    }

    QL_FUNC_SendAtResponse("\r\n");

    for(i = 0; i < pDisplay->uElemCount; i++)
    {
        pElem = &(pDisplay->pElem[i]);
    
        uBufLen = snprintf(
                      aBuf,
                      sizeof(aBuf),
                      "%s: \"list\",%d,\"%s\",%s,%s\r\n",
                      pTok->name,
                      i,
                      pElem->aCarrierName,
                      pElem->aVersion,
                      pElem->aRelDate
                  );

        QL_FUNC_SendAtResponse(aBuf);
    }

    return DSAT_OK;
}

//<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
/*========================================================================
  FUNCTION:  QL_QMULTIMBN_sCmdHdlrSetEnable
=========================================================================*/
/*! @brief 
     Command handler of AT+QMULTIMBN="enable"

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QMULTIMBN_sCmdHdlrSetEnable
(
    dsat_mode_enum_type eMode,
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok,
    dsm_item_type *pResBuf
)
{
    atoi_enum_type aAtoiRst = ATOI_OK;
    dsat_num_item_type uEnable = 0;
    QMULTIMBN_CfgStruct *pCfg = &sg_sQMULTIMBNCfg;
    QUECEFS_ErrEnum eErr = QUEC_EFS_ERR_SUCC;

    if(pTok->args_found < 2) //Syntax: AT+QMULTIMBN="enable"
    {
        pResBuf->used = sprintf(
                            (char*)pResBuf->data_ptr,
                            "%s: \"enable\",%d",
                            pTok->name,
                            pCfg->uEnable
                        );
        
        return DSAT_OK;
    }

    aAtoiRst = dsatutil_atoi (&uEnable, pTok->arg[1], 10);
    if(aAtoiRst != ATOI_OK)
    {
        return DSAT_ERROR;
    }

    if((uEnable != 0) && (uEnable != 1))
    {
        MULTIMBN_TRACE_2("FAIL! uEnable:%d(valid:0,1) is INVALID.", uEnable, 0);
        return DSAT_ERROR;
    }

    if(pCfg->uEnable == uEnable)
    {
        MULTIMBN_TRACE_2("WARNING! uEnable:%d is same as current,ignore it.", uEnable, 0);
        return DSAT_OK;
    }

    pCfg->uEnable = uEnable; //Set new value

    eErr = QL_EFS_Write(QUEC_EFS_QMULTIMBN_CFG, pCfg, sizeof(QMULTIMBN_CfgStruct));
    if(eErr != QUEC_EFS_ERR_SUCC)
    {
        MULTIMBN_TRACE_2("FAIL! QL_EFS_Write(QMULTIMBN_CFG) FAIL! eErr:%d", eErr, 0);
        return DSAT_ERROR;
    }

    return DSAT_OK;
}

/*========================================================================
  FUNCTION:  QL_QMULTIMBN_sCmdHdlrSetReload
=========================================================================*/
/*! @brief 
     Command handler of AT+QMULTIMBN="reload"

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static dsat_result_enum_type QL_QMULTIMBN_sCmdHdlrSetReload
(
    dsat_mode_enum_type eMode,
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok,
    dsm_item_type *pResBuf
)
{
    mcfg_nv_status_e_type eNvStatus = MCFG_NV_STATUS_OK;

    eNvStatus = mcfg_nv_delete(MCFG_NV_ITEM_MULTI_MBN_CONFIG_VERSION_I, 0, 0);
    if(eNvStatus != MCFG_NV_STATUS_OK)
    {
        MULTIMBN_TRACE_2("FAIL! mcfg_nv_delete(MULTI_MBN_CONFIG_VERSION_I) FAIL! eNvStatus:%d", eNvStatus, 0);
        return DSAT_ERROR;
    }

    return DSAT_OK;
}
//>2018/12/21-QCM96B00054C002-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Extern functions
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  quectel_exec_qmultimbn_cmd
=========================================================================*/
/*! @brief 
     Command handler of AT+QMULTIMBN

    @param
     <eMode>    [IN]
     <pParseTab>   [IN]
     <pTok>     [IN]
     <pResBuf>  [OUT]

    @return
     'dsat_result_enum_type' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
dsat_result_enum_type quectel_exec_qmultimbn_cmd
(
    dsat_mode_enum_type eMode,             
    const dsati_cmd_type *pParseTab,   
    const tokens_struct_type *pTok,  
    dsm_item_type *pResBuf         
)
{
	char  aSubCmd[40] = {0,};
	uint32 cmd_index = 0;
    dsat_result_enum_type eResult = DSAT_OK;
    boolean bResult = FALSE;

    QL_FUNC_StrToUpper((uint8*)pTok->name);
    
	if((NA|EQ|QU) == pTok->op) //AT+QMBNUPDATE=?
	{
        eResult = QL_QMULTIMBN_sCmdHdlrTest(eMode,pParseTab,pTok,pResBuf);
	}
	else if((NA|EQ|AR) == pTok->op)
	{
        if(pTok->args_found < 1)
        {
            return DSAT_ERROR;
        }
        
        bResult = dsatutil_strip_quotes_out((byte*)pTok->arg[0], (byte *)aSubCmd,sizeof(aSubCmd));
        if(FALSE == bResult)
        {
            return DSAT_ERROR;
        }

        if(0 == dsatutil_strcmp_ig_sp_case((byte*)aSubCmd, (byte *)"list"))
        {
            eResult = QL_QMULTIMBN_sCmdHdlrSetList(eMode,pParseTab,pTok,pResBuf);
        }
        //<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
        else if(0 == dsatutil_strcmp_ig_sp_case((byte*)aSubCmd, (byte *)"enable"))
        {
            eResult = QL_QMULTIMBN_sCmdHdlrSetEnable(eMode,pParseTab,pTok,pResBuf);
        }
        else if(0 == dsatutil_strcmp_ig_sp_case((byte*)aSubCmd, (byte *)"reload"))
        {
            eResult = QL_QMULTIMBN_sCmdHdlrSetReload(eMode,pParseTab,pTok,pResBuf);
        }
        //>2018/12/21-QCM96B00054C002-P01-Vicent.Gao
        else
        {
            eResult = DSAT_ERROR;
        }
	}    
    else
    {
        eResult = DSAT_ERROR;
    }

	return eResult;
}

//<2018/12/21-QCM96B00054C002-P01-Vicent.Gao, <[MULTIMBN] Segment 1==> Add cmd: AT+QMULTIMBN.>
/*========================================================================
  FUNCTION:  QL_QMULTIMBN_Init
=========================================================================*/
/*! @brief 
     Initialize of AT+QMULTIMBN

    @param
     VOID

    @return
     'boolean' data

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_QMULTIMBN_Init(void)
{
    QMULTIMBN_CfgStruct *pCfg = &sg_sQMULTIMBNCfg;
    QUECEFS_ErrEnum eErr = QUEC_EFS_ERR_SUCC;

    eErr = QL_EFS_Read(QUEC_EFS_QMULTIMBN_CFG, pCfg, sizeof(QMULTIMBN_CfgStruct));
    if(eErr != QUEC_EFS_ERR_SUCC)
    {
        MULTIMBN_TRACE_2("FAIL! QL_EFS_Read(QMULTIMBN_CFG) FAIL! eErr:%d,use default.", eErr, 0);
        QL_QMULTIMBN_sSetDefault();
    }

    return TRUE;
}

/*========================================================================
  FUNCTION:  QL_QMULTIMBN_IsEnable
=========================================================================*/
/*! @brief 
     Get the value of AT+QMULTIMBN="enable"

    @param
     VOID

    @return
     The value of AT+QMULTIMBN="enable"

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
uint8 QL_QMULTIMBN_IsEnable(void)
{
    QMULTIMBN_CfgStruct *pCfg = &sg_sQMULTIMBNCfg;

    return pCfg->uEnable;
}
//>2018/12/21-QCM96B00054C002-P01-Vicent.Gao

#endif //#if defined(QUECTEL_MULTIMBN_FEATURE)
