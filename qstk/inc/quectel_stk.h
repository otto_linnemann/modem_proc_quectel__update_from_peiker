#ifndef QUECTEL_STK_H
#define  QUECTEL_STK_H

#if defined(QUECTEL_STK_SUPPORT)

#if defined(FEATURE_DUAL_SIM) || (defined(FEATURE_UIM_SGLTE) && defined(FEATURE_UIM_SUPPORT_DUAL_SLOTS))
  #define QSTK_MAX_CARD_COUNT                 (2)
#else
  #define QSTK_MAX_CARD_COUNT                 (1)
#endif 

#define QSTK_TERMINAL_PROFILE_SIZE 31 /* bytes */

#define  QSTK_CMD_DETAILS_LEN                      5
#define  QSTK_AUTO_RESPONSE_TIMEOUT       300 //unit: second

#define  QSTK_AUTO_MODE                               0
#define  QSTK_DEDICATE_MODE                        1
#define  QSTK_RAW_MODE                                 2

#define  QSTK_GSM_ALPHABET                        0
#define  QSTK_UCS2_ALPHABET                       1

#define  QSTK_END_PROACTIVE_CMD                253
#define  QSTK_ENVELOPE_MENU_SELECTION    253

#define  QSTK_SIM_NOT_INSERT                      254
#define  QSTK_TERMINATE_STK_SESSION        254

#define   QSTK_RESPONSE_TIMEOUT                255

#define   QSTK_PROACTIVE_CMD_ATUO_RSP_STATE                    0
#define   QSTK_PROACTIVE_CMD_TERMINAL_RSP_STATE             1
#define   QSTK_PROACTIVE_CMD_WAIT_STATE                             2

//<2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> RAW DATA develop init.>
#define STKRAW_TRACE_SWITCH     (FALSE)
#define STKRAW_TRACE_BUF        g_aDbgCmmTraceBuf  
#define STKRAW_HEX_STR_MAX_LEN  (1024)
#define STKRAW_CMD_DATA_BUF_MAX_LEN  (GSTK_MAX_DATA_SIZE + 10)
//>2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao

//<2015/10/05-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> Add STK debug info.>
typedef enum
{
    STKRAW_PCI_TYPE_HANDLED_BY_TE = 0,
    STKRAW_PCI_TYPE_HANDLED_BY_ME = 1,
    STKRAW_PCI_TYPE_NO_OTHER_CMD = 2,

    //Warning! ==> Please add new STKRAW PCI TYPE upper this line!
    STKRAW_PCI_TYPE_INVALID = 0xFFFF
} STKRAW_PCITypeEnum;
//>2015/10/05-QCM4TVG00004C02-P01-Vicent.Gao

//<2015/10/01-QCM4TVG00003-P01-Vicent.Gao,<[STK] Segment 1 ==> Add STK debug info.>
typedef enum
{
    STK_GET_MACRO_SET_QL_DS_STK_MSG_ID = 0x00, //quectel_ds_stk_msg_id
    STK_GET_MACRO_SET_GSTK_CMD_ID = 0x01,  //SEE function: qstk_convert_gstk_type_to_stk_cmd_id

    //Warning!==>Please add new STK GET MACRO SET upper this line!
    STK_GET_MACRO_SET_INVALID = 0xFFFF
} DBG_STKGetMacroSetEnum;
//>2015/10/01-QCM4TVG00003-P01-Vicent.Gao

typedef struct
{
   uint8                                                  stk_mode;
   uint8                                                  stk_alphabet_type;
   uint32                                                stk_auto_response_timeout;
   gstk_client_id_type                             stk_client_id[QSTK_MAX_CARD_COUNT];
   uint8                                                  stk_proactive_cmd_state;
   wms_client_id_type                             wms_client_id;
   qstk_call_info_type                             call_info;
   qstk_sups_info_type	                        *sups_info_ptr;
   cm_client_id_type                               cm_client_id;
   rex_timer_type                                   dtmf_timer;
   rex_timer_type                                   burst_dtmf_timer;
   uint8                                                  stk_terminal_profile[QSTK_TERMINAL_PROFILE_SIZE];
   uint16                                                stk_cmd_id;
	uint8                                                  general_result;
	uint8                                                  additional_result;
	gstk_terminal_rsp_extra_param_type   extra_param;

	 
   union
   {
	    struct
	    {
	      gstk_slot_id_enum_type                slot_id;
	      uint32                                          cmd_ref_id;
	      uint8                                           raw_hdr_tag;
	      int32                                           raw_data_len;
	      uint8                                           *raw_data_buffer;
		   int32                                           raw_main_menu_len;
			uint8                                           *raw_main_menu_ptr;
	    } raw_proactive_cmd;
	    struct
	    {
	       gstk_cmd_from_card_type               decoded_cmd;
			gstk_cmd_from_card_type               decoded_main_menu_cmd;
	    } decoded_proactive_cmd;
 	}cmd_data;
	 
}qstk_client_type;

typedef struct
{
	uint8          tlv_tag;
	uint16        tlv_length;
	uint8          tlv_value[256];
}qstk_tlv_struct;

typedef struct
{
 uint8  command_number;
 uint8  command_type;
 uint8  command_qualifer;
}qstk_command_details_struct;

typedef struct
{
   uint8     source_device;
   uint8     target_device;
}qstk_device_struct;

//<2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> RAW DATA develop init.>
typedef struct
{
    uint16 uCmdId;
    uint8 *pInd;
    int32 uLen;
} STKRAW_ProactiveCmdIndStruct;

typedef union
{
	STKRAW_ProactiveCmdIndStruct sPCI;
} STKRAW_MsgDataUnion;

typedef struct
{
    uint32 uMsgId;
    STKRAW_MsgDataUnion u;
} STKRAW_MsgInfoStruct;
//>2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao

//<2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> RAW DATA develop init.>
#define CASE_STK_GET_MACRO_NAME(Macro,Name)   case ((uint32)(Macro)): (Name) = (uint8*)#Macro; break

#if STKRAW_TRACE_SWITCH == (TRUE)

#define STKRAW_TRACE   DBG_CMM_TRACE

#else  // #if STKRAW_TRACE_SWITCH == (TRUE)

//Only for compile
#define STKRAW_TRACE(aBuf,...)

#endif // #if STKRAW_TRACE_SWITCH == (TRUE)

#define STKRAW_SEND_PROACTIVE_CMD_IND(CmdId,Ind,Len,Result) \
do  \
{   \
    STKRAW_MsgInfoStruct sInfoZ;    \
        \
    memset(&sInfoZ,0x00,sizeof(sInfoZ));    \
        \
    sInfoZ.uMsgId = QUECTEL_DS_STKRAW_SEND_PROACTIVE_CMD_IND;   \
    sInfoZ.u.sPCI.uCmdId = (CmdId);    \
    sInfoZ.u.sPCI.pInd = (Ind);  \
    sInfoZ.u.sPCI.uLen = (Len);    \
        \
    (Result) = QL_STKRAW_SendMessage(&sInfoZ); \
} while(0)
//>2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao

extern void qstk_init
(
uint8    mode,
uint8    alphabet_type,
uint16  response_timeout
);

extern void qstk_cache_terminal_profile
(
void    *profile_ptr,
uint16  profile_size 
);

extern void qstk_proactive_command_timeout
(
);

extern void qstk_process_sim_card_remove_event
(
);

extern boolean qstk_send_terminal_response
(
  gstk_general_result_enum_type  result,
  int32     additional_result
);

extern boolean qstk_send_terminal_response_ext 
(
  gstk_general_result_enum_type  result,
  int32     additional_result,
  gstk_terminal_rsp_extra_param_type         *other_info_ptr
);

extern boolean qstk_send_envelope_command
(
gstk_general_result_enum_type  result,
int32       menu_item
);


extern boolean qstk_send_select_item_command
(
gstk_general_result_enum_type  result,
int32       item_id,
int32       additional_result
);


extern boolean qstk_send_get_input_response
(
gstk_general_result_enum_type  result,
uint8  *input_content,
int32  additional_result
);


extern boolean qstk_send_get_inkey_response
(
gstk_general_result_enum_type  result,
uint8  *inkey_content,
int32  additional_result
);

extern boolean qstk_process_send_sms_req
(
 gstk_general_result_enum_type  result,
  int32     additional_result
);

extern boolean qstk_process_setup_call_req
(
 gstk_general_result_enum_type  result,
  int32     additional_result
);

extern boolean qstk_process_send_ss_req
(
 gstk_general_result_enum_type  result,
  int32     additional_result
);

extern boolean qstk_process_send_ussd_req
(
 gstk_general_result_enum_type  result,
  int32     additional_result
);

extern boolean qstk_process_send_dtmf_req
(
 gstk_general_result_enum_type  result,
  int32     additional_result
);

extern void * qstk_malloc
(
dword size
);

extern  void  qstk_free
(
void * ptr
);

extern void qstk_malloc_copy
(
  void       ** dst,
  const void  * src,
  uint32        size
);

extern void  qstk_memcpy
(
  void *dest_ptr,
  const void *src_ptr,
  size_t copy_size,
  size_t dest_max_size,
  size_t src_max_size
 );

extern uint8 qstk_find_length_of_length_value
(
  const uint8 * length_value
);

extern int32 qstk_parse_tlv
(
uint8  *raw_data_ptr,
int32   current_offset,
qstk_tlv_struct  *stk_tlv,
int32  *new_offset
);

extern int32 qstk_parse_command_details_tlv
(
qstk_tlv_struct  *stk_tlv,
qstk_command_details_struct *command_details
);

extern int32 qstk_parse_alpha_tlv
(
qstk_tlv_struct    *stk_tlv,
gstk_string_type  *alpha
);

extern int32 qstk_parse_address_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_address_type  *address
);

extern int32 qstk_parse_subaddress_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_subaddress_type  *subaddress
);

extern int32 qstk_parse_smstpdu_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_sms_tpdu_type  *tpdu
);

extern int32 qstk_parse_cap_config_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_cap_config_type  *cap_config
);

extern int32 qstk_parse_duration_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_duration_type   *duration
);

extern int32 qstk_parse_icon_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_icon_type       *icon
);

extern int32 qstk_parse_device_tlv
(
qstk_tlv_struct        *stk_tlv,
qstk_device_struct   *device
);

extern int32 qstk_parse_dtmf_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_dtmf_type   *dtmf
);

extern int32 qstk_parse_ussd_string_tlv
(
qstk_tlv_struct        *stk_tlv,
gstk_ussd_string_type   *ussd_string
);

extern int32 qstk_pack_text_tlv_with_alphset
(
int32 response_format,
uint8 *data_ptr,
uint16 data_len,
gstk_string_type *out_alpha
);

//<2015/10/01-QCM4TVG00003-P01-Vicent.Gao,<[STK] Segment 1 ==> Add STK debug info.>
extern uint8 *QL_DBG_STKGetMacroName(DBG_STKGetMacroSetEnum eSet,uint32 uMacro);
//>2015/10/01-QCM4TVG00003-P01-Vicent.Gao

//<2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> RAW DATA develop init.>
extern boolean QL_STKRAW_ShowProactiveCmdInd(uint16 uCmdId,uint8 *pData,int32 iDataLen);
extern boolean QL_STKRAW_SendMessage(STKRAW_MsgInfoStruct *pInfo);
extern uint8 quectel_ds_stk_uint8_array_to_hex_array(uint8 *in_buf,uint16 in_buf_len,char *out_buf,uint16 out_buf_len);
extern STKRAW_PCITypeEnum QL_STKRAW_GetPCIType(uint16 uCmdId);
extern int32 QL_STKRAW_HexArrToUint8(char *pSrc,uint16 uSrcLen, uint8 *pDest, uint16 uDestLen);
extern boolean dsatutil_hex_to_uint8
(
  const char *in_octet,
  uint8      *out_uint8
);
extern boolean dsatsmsi_strip_quotes
(
  const byte * in_ptr,   /* Pointer to the string to be processed  */
  byte * out_ptr         /* Pointer to the resultant string buffer */
);
extern int32 qstk_gstk_slot_id_to_slot_index
(
  gstk_slot_id_enum_type    gstk_slot_id,
  uint8                   * slot_index_ptr
);
extern void qstk_gstk_raw_env_cb
(
  gstk_cmd_from_card_type * env_rsp_ptr
);
//>2015/10/01-QCM4TVG00004C02-P01-Vicent.Gao

#endif
#endif/*QUECTEL_STK_H*/
