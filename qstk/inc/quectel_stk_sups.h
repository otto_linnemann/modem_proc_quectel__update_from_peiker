#ifndef QUECTEL_STK_SUPS_H
#define  QUECTEL_STK_SUPS_H

/*===========================================================================

  STRUCT: ESTK_CM_CALL_EVENT_ENUM_TYPE

  PARAMETERS:
    QSTK_CM_CALL_NONE_EVT    : Invalid call event
    QSTK_CM_CALL_ORIG_EVT    : Call origination event
    QSTK_CM_CALL_CONNECT_EVT : Call connected event
    QSTK_CM_CALL_END_EVT     : Call end event
    QSTK_CM_CALL_EVENT_MNG_CALLS_CONF_EVT: Event for call holds
    QSTK_CM_CALL_MOD_TO_SS_EVT : Call modified event

  PURPOSE:
    The enumeration indicates the various CM call events.
===========================================================================*/
typedef enum
{
  QSTK_CM_CALL_NONE_EVT = 0x00,
  QSTK_CM_CALL_ORIG_EVT,
  QSTK_CM_CALL_CONNECT_EVT,
  QSTK_CM_CALL_END_EVT,
  QSTK_CM_CALL_EVENT_MNG_CALLS_CONF_EVT,
  QSTK_CM_CALL_MOD_TO_SS_EVT,
  QSTK_CM_CALL_MAX_EVT = 0x7FFFFFFF
}qstk_cm_call_event_enum_type;

/*===========================================================================

  STRUCT: ESTK_CM_SUPPS_SS_OPERATION_ENUM_TYPE

  PARAMETERS:
    QSTK_NULL_SS_OPERATION : NULL
    QSTK_REGISTER_SS       : Register operation
    QSTK_ERASE_SS          : Erase operation
    QSTK_ACTIVATE_SS       : Activate operation
    QSTK_DEACTIVATE_SS     : Deactivate operation
    QSTK_INTERROGATE_SS    : Interrogate operation
    QSTK_PROCESS_USSD_DATA : Process USSD

  PURPOSE:
    The enumeration indicates the various supplementarty services operations
===========================================================================*/
typedef enum
{
  QSTK_NULL_SS_OPERATION,
  QSTK_REGISTER_SS,
  QSTK_ERASE_SS,
  QSTK_ACTIVATE_SS,
  QSTK_DEACTIVATE_SS,
  QSTK_INTERROGATE_SS,
  QSTK_PROCESS_USSD_DATA,
  QSTK_MAX_SS_OPERATION =  0x7FFFFFFF
}qstk_sups_ss_operation_enum_type;

typedef struct
{
  uint8                            req_id;
  uint32                           cmd_ref_id;
  qstk_sups_ss_operation_enum_type ss_operation;
}qstk_sups_info_type;

void qstk_cm_sups_event_cb
(
  cm_sups_event_e_type          sups_event,
  const cm_sups_info_s_type    *sups_info_ptr
);

boolean qstk_send_ss
(
gstk_send_ss_req_type     *send_ss_req_ptr,
uint32        command_ref_id
);

boolean qstk_send_ussd
(
gstk_send_ussd_req_type     *send_ussd_req_ptr,
uint32        command_ref_id
);

extern boolean qstk_parse_raw_send_ss_req
(
gstk_send_ss_req_type *send_ss_req_ptr
);

extern boolean qstk_parse_raw_send_ussd_req
(
gstk_send_ussd_req_type *send_ussd_req_ptr
);


#endif/*QUECTEL_STK_SUPS_H*/
