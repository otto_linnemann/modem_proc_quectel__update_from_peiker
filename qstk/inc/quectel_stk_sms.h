#ifndef QUECTEL_STK_SMS_H
#define  QUECTEL_STK_SMS_H


#if defined(QUECTEL_STK_SUPPORT)

extern void  qstk_sms_reg( void );

extern boolean qstk_parse_raw_send_sms_req
(
gstk_send_sms_req_type *send_sms_req_ptr
);

extern boolean qstk_send_sms
(
  gstk_send_sms_req_type   *send_sms_req_ptr,
  uint32 command_ref_id
);

#endif/*QUECTEL_STK_SUPPORT*/

#endif/*QUECTEL_STK_SMS_H*/
