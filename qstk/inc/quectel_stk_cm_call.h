#ifndef QUECTEL_STK_CM_CALL_H
#define  QUECTEL_STK_CM_CALL_H

#if defined(QUECTEL_STK_SUPPORT)

typedef struct 
{
  uint8    call_id;
  boolean  call_active;
}qstk_call_list_from_cm;


typedef struct
{
  char                                *dtmf_str_ptr;
  char                                *temp_DTMF_str_ptr;
  int32                                dtmf_len;
  int32                                curr_dtmf_pos_in_str;
  boolean                            start_dtmf;
  qstk_call_list_from_cm      call_list[CM_CALL_ID_MAX];
  uint8                                call_id;
  boolean                            hold_active_failed;
}qstk_call_info_type;



void qstk_cm_call_event_cb 
(
 cm_call_event_e_type          call_event,
  const cm_mm_call_info_s_type *call_info_ptr
);



void qstk_send_DTMF
(
uint32 dummy
);

void qstk_handle_burst_DTMF
(
uint32 dummy
);

void qstk_cm_call_info_list_cb
(
 const cm_call_state_info_list_s_type *list_ptr
);

boolean qstk_parse_raw_setup_call_req
(
gstk_setup_call_req_type   *setup_call_req_ptr
);

boolean  qstk_setup_call
(
gstk_setup_call_req_type   *setup_call_req_ptr,
uint32                  command_ref_id
);


boolean qstk_process_dtmf
(
gstk_send_dtmf_req_type   *send_dtmf_req_ptr,
uint32                  command_ref_id
);

boolean qstk_parse_raw_send_dtmf_req
(
gstk_send_dtmf_req_type   *send_dtmf_req_ptr
);

#endif/*QUECTEL_STK_SUPPORT*/

#endif/*QUECTEL_STK_CM_CALL_H*/
