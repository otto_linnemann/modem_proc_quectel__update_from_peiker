/***************************************************************************************
*
*			File Name:quectel_smem_handler.h
*
*		       Description:Smem task
*
*			Author:Tommy
*		
*			Created Time: 2015.08.12
*
***************************************************************************************/

#ifndef __QUECTEL_SMEM_HANDLER__
#define __QUECTEL_SMEM_HANDLER__



#include "comdef.h"
#include "task.h"
#include "dog.h"
#include "queue.h"
#include "quectel_smem_task.h"

#define QSMEM_DATASIZE   1000
#define OPDES_MAXLEN     64
#define ALIVE_TIME_MAX   10 /* unit:s */

/* MsgId define */
typedef enum
{
    GET_INIT_NV_MSG=1,
    GET_GPIO_STATUS_MSG=2,
    GET_USB_EARLY_ENBLE_MSG=3,
    GET_USB_FUNCTIONS_MSG=4,
    QSMEM_MSG_MAX=255
}QSMEM_MSG_ID;

/* smem buf define */
typedef struct qsmem_buf
{
    uint32        qsmem_msg_spinlock;
    unsigned char Status; /* 'A'-APPS write,'M'-Modem wirte,'H'-Handle,'C'-Clean */
    unsigned char AliveTime;
    uint32 MsgId;
    uint32       DataLen;
    unsigned char Data[QSMEM_DATASIZE];
}QSMEM_BUF;

typedef struct qsmem_cmd_list
{
        QSMEM_MSG_ID MsgId;
        boolean (*func)(QSMEM_BUF *param);
}QSMEM_CMD_LIST_T;

extern QSMEM_CMD_LIST_T qsmem_cmd[];

/* OpMode define */
typedef enum
{
    OPMODE_GET=1,
    OPMODE_PUT,
    OPMODE_EXEC,
    OPMODE_MAX
}QUECTEL_OPMODE;
/* OpResult define */
typedef enum
{
    OPRESULT_OK=1,
    OPRESULT_ERR,
    OPRESULT_MAX
}QUECTEL_OPRESULT;

/*************************GET_INIT_NV_MSG*************************/

#ifdef QUECTEL_DEFAULT_USB_ID
/* Msg data define,size must be less than QSMEM_DATASIZE byte  */
typedef struct qsmem_get_usb_id_nv_msg_data //maxcodeflag20160326
{
    unsigned char OpMode;
    unsigned int uiPID;
    unsigned int uiVID;
}QSMEM_GET_USB_ID_NV_MSG_DATA_T;

boolean quectel_get_usb_id_nv_handle(QSMEM_BUF *param);

boolean Quectel_GetPrjDefaultUSBID(unsigned int *pPID, unsigned int *pVID);
#endif

typedef struct qsmem_get_gpio_status_msg_data
{
	unsigned int gpio;
	unsigned int status;
	unsigned int on_time;	//0~60000 ms
	unsigned int of_time;	//0~60000 ms
}QSMEM_GET_GPIO_STATUS_MSG_DATA_T;

boolean quectel_get_gpio_status_handle(QSMEM_BUF *param);

//end jun.wu

/*maxcodeflag20160616*/
void Quectel_NotifyAppToInitSmem(void);

boolean quectel_get_usb_early_enable_handle(QSMEM_BUF *param);

boolean quectel_get_usb_functions_mask_handle(QSMEM_BUF *param);

#endif


