/***************************************************************************************
*
*			File Name:quectel_smem_task.h
*
*		       Description:Smem Task
*
*			Author:Tommy
*		
*			Creat Time:2015.08.12
*
***************************************************************************************/


#ifndef __QUECTEL__SMEM__TASK__
#define __QUECTEL__SMEM__TASK__

#include "Quecoem_feature.h"
#include "msgcfg.h"
#include "dsat_ext_api.h"
#include "ds3gsiolib_ex.h"
#include "fs_sys_types.h"
#include "task.h"
#include "dog.h"
//#include "tmc.h"
#ifdef FEATURE_MODEM_RCINIT
#include "rcinit.h"
#include "rcevt_rex.h"
#endif /* FEATURE_MODEM_RCINIT */
#include "dog_hb_rex.h"
#include "modem_mem.h"
#include "quectel_smem_handler.h"

#ifdef QUECTEL_SMEM_FUNC

extern void smem_spin_lock( int lock );
extern void smem_spin_unlock( int lock );
#define IS_IN_QSMEM_TASK() (q_is_in_qsmem_task())

typedef struct qsmem_struct
{
    rex_crit_sect_type qsmem_cmd_queue_crit_sect;
    rex_timer_type  qsmem_rpt_timer;
}QSMEM_TASK_PARAM;

typedef  enum
{
     QSMEM_CMD_Q_SIG               = 0x0001,
     QSMEM_RPT_TIMER_SIG           = 0x0002,
     QSMEM_NV_CMD_SIG              = 0X0004
}qft_signal_enum;
#define   QSMEM_SIGS    (QSMEM_CMD_Q_SIG |QSMEM_RPT_TIMER_SIG)


typedef enum
{
  QSMEM_TIMER_MAX             /* maximum value - used for bounds checking   */
} qsmem_timer_enum_type;

/*===========================================================================

FUNCTION QSMEM_TASK

DESCRIPTION
  This is the entry point for the Quectel Smem Task. This function contains
  the main processing loop that waits for file events (signals or commands) and
  deal with it.

DEPENDENCIES
  None

RETURN VALUE
  This function does not return.

SIDE EFFECTS
  None

===========================================================================*/
void  qsmem_task(uint32 dummy);
nv_stat_enum_type qsmem_get_nv_item
(
  nv_items_enum_type  item,           /* Which item */
  nv_item_type       *data_ptr        /* Pointer to space for item */
);

boolean q_is_in_qsmem_task
(
  void
);

#endif/*#ifdef QUECTEL_SMEM_FUNC*/
#endif
