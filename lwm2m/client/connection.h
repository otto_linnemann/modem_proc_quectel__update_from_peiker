/*******************************************************************************
 *
 * Copyright (c) 2013, 2014 Intel Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * The Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    David Navarro, Intel Corporation - initial API and implementation
 *    
 *******************************************************************************/

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include <stdio.h>
#ifndef QUECTEL_LWM2M_SUPPORT
//#include <unistd.h>
//#include <netinet/in.h>
//#include <arpa/inet.h>
//#include <netdb.h>
//#include <sys/socket.h>
//#include <sys/stat.h>
#endif
//#include <liblwm2m.h>
#include "mbedtls/config.h"
#include "mbedtls/debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/timing.h"

#include "quectel_apptcpip_util.h"

#define LWM2M_STANDARD_PORT_STR "5683"
#define LWM2M_STANDARD_PORT      5683
#define LWM2M_DTLS_PORT_STR     "5684"
#define LWM2M_DTLS_PORT          5684
#define LWM2M_BSSERVER_PORT_STR "5685"
#define LWM2M_BSSERVER_PORT      5685

typedef enum
{
	CONNECTION_STATE_SUCCESS = 0,
	CONNECTION_STATE_FAILED = -1,
	CONNECTION_STATE_PENDING = -2,
}connection_state_t;

typedef struct _dtls_session_t{
	int                          bio_fd;
	mbedtls_ssl_context          ssl_ctx;
	apptcpip_ip_addr_struct      peer_addr;
	uint16                       peer_port;
	mbedtls_ssl_config           ssl_conf;
	mbedtls_ctr_drbg_context     ctr_drbg;
	mbedtls_entropy_context      entropy;
	mbedtls_timing_delay_context timer;
	int                          cipher_suite[1];
	Q_INT32                  	 retransmit_timer;
	
}dtls_session_t;

typedef struct _connection_t
{
    struct _connection_t *      next;
    int                         sock;
    apptcpip_ip_addr_struct     addr;
	uint16                      port;
	connection_state_t          state;
	uint8                       use_dtls;
} connection_t;

connection_t * connection_find_ex(connection_t * connList, int sock);

connection_t * connection_find(connection_t * connList, apptcpip_ip_addr_struct* addr, uint16 port);

connection_t * connection_new_incoming(connection_t * connList, uint8 use_dtls,int sock, apptcpip_ip_addr_struct * addr, uint16 port);

connection_t * connection_create(connection_t * connList, int sock, apptcpip_ip_addr_struct *addr, uint16 port);

connection_t * ssl_connection_create(connection_t * connList, int ssl_session, apptcpip_ip_addr_struct *addr, uint16 port);

void connection_free(connection_t * connList);

int connection_send(connection_t *connP, uint8_t * buffer, size_t length);

#endif
