
/*===========================================================================
	Copyright(c) 2019 Quectel Wierless Solution,Co.,Ltd. All Rights Reserved.
	Quectel Wireless Solution Proprietary and Confidential.
============================================================================*/
/*===========================================================================
							EDIT HISTORY FOR MODULE
This section contains comments describing changees made to the module.
Notice that changes are listed in reverse chronological order.
WHEN		 WHO		WHAT,WHERE,WHY
----------	-----		----------------------------------------------------
10/05/2019	Herry	  	Modify file for lwm2m att apn.
============================================================================*/
#ifndef LWM2M_CLIENT_H
#define LWM2M_CLIENT_H

#if 0
#include "comdef.h"
#include "customer.h"
#include "stdio.h"
#include <stringl/stringl.h>
#endif


#include "liblwm2m.h"
//#include "connection.h"


#include "quectel_apptcpip_util.h"



#define LWM2M_CLIENT_OBJ_COUNT    11//8
#define LWMM2M_MAX_PACKET_SIZE   1024

#define LWM2M_MAX_SECURITY_INSTANCE_CNT  4

typedef enum
{
	LWM2M_CLIENT_OK = 0,
	LWM2M_CLIENT_ERR_INVALID_PARAM = -1,
	LWM2M_CLIENT_ERR_WODBLOCK = -2,
	LWM2M_CLIENT_ERR_NET_FAIL = -3,
	LWM2M_CLIENT_ERR_DNS_FAIL = -4,
	LWM2M_CLIENT_ERR_NET_ACTIVING = -5,
	LWM2M_CLIENT_ERR_OUT_MEM = -6,
	LWM2M_CLIENT_ERR_SOCK_BIND_FAIL = -7,
	LWM2M_CLIENT_ERR_SOCK_CONN_FAIL = -8,
	LWM2M_CLIENT_ERR_REG_FAIL = -9,
	LWM2M_CLIENT_ERR_UPDATE_FAIL = -10,
	LWM2M_CLIENT_ERR_DEREG_FAIL = -11,
	LWM2M_CLIENT_ERR_DTLS_FAIL = -12,
	LWM2M_CLIENT_ERR_NOT_SUPPORT = -13,
	LWM2M_CLIENT_ERR_OPEN_FAIL = -14,
	LWM2M_CLIENT_ERR_CLOSE_FAIL = -15,
	LWM2M_CLIENT_ERR_BOOTSTRAP_FAIL = -16,
}lwm2m_client_error_code;


typedef enum
{
	LWM2M_CLIENT_OPT_PDPCID = 1,
	LWM2M_CLIENT_OPT_EPNAME_MODE,
	LWM2M_CLIENT_OPT_IMSI,
	LWM2M_CLIENT_OPT_IMEI,
	LWM2M_CLIENT_OPT_MSISDN,
#if defined(CHINA_MOBILE_LWM2M_CLIENT)
	LWM2M_CLIENT_OPT_APPKEY,
	LWM2M_CLIENT_OPT_APPPWD,
#endif
	LWM2M_CLIENT_OPT_OBJLIST,
	LWM2M_CLIENT_OPT_LOCAL_PORT,
	LWM2M_CLIENT_OPT_FW_DLOAD_STATE,
	LWM2M_CLIENT_OPT_UPDATE_RSRQ,
	LWM2M_CLIENT_OPT_UPDATE_RSRP,
}lwm2m_client_option_type;

typedef enum
{
	LWM2M_CLIENT_INIT = 0,
	LWM2M_CLIENT_PDP_ACTIVE,
	LWM2M_CLIENT_PDP_DEACTIVE,
	LWM2M_CLIENT_DNS_QUERY,
	LWM2M_CLIENT_SOC_NEW,
    LWM2M_CLIENT_DTLS,
    LWM2M_CLIENT_BOOTSTRAPING,
	LWM2M_CLIENT_BOOTSTRAP,
	LWM2M_CLIENT_REGISTERING,
	LWM2M_CLIENT_READY,
	LWM2M_CLIENT_DEREG ,
}lwm2m_client_stat;


typedef enum
{
	LWM2M_CLIENT_STATE_CHG_IND = 1,
	LWM2M_CLIENT_OPT_OBJ_IND = 2,
	LWM2M_CLIENT_FW_DLOAD_IND = 3,
	LWM2M_CLIENT_FW_UPDATE_IND = 4,
	LWM2M_CLIENT_REBOOT_IND = 5,
	LWM2M_CLIENT_UPDATE_IND = 6,
}lwm2m_client_event_type;

/*
+QLWURC: "initial","successfully"/"failed"
+QLWURC: "pdpact","successfully"/"failed",<apn>
+QLWURC: "dns","successfully"/"failed",<ssid>
+QLWURC: "dtls handshark","successfully"/"failed",<ssid>
+QLWURC: "bootstraping"
+QLWURC: "bootstraped", "successfully"/, <ssid>
+QLWURC: "registering",
+QLWURC: "registered", "successfully"/"failed",<ssid>
+QLWURC: "update","successfully"/"failed",<ssid>
+QLWURC: "deregister",<ssid>
+QLWURC: "fota/pkgurl",<url>
+QLWURC: "read",<uri>
+QLWURC: "write",<uri>
+QLWURC: "delete",<uri>
+QLWURC: "obersve",<uri>
+QLWURC: "exec",<uri>
*/

typedef struct{
	int 	result;
	int 	state;
	int 	short_server_id;
	char    apn[256];
}lwm2m_state_event_info;

typedef struct{
	int    opt_code;
	int    obj_id;
	int    instance_id;
	int    resource_id;
}lwm2m_obj_opt_event_info;

typedef union{
	lwm2m_state_event_info     state_info;
	lwm2m_obj_opt_event_info   opt_info;
	char                       pkg_url[256];
}lwm2m_event_info;


typedef void (*lwm2m_client_listener)(int lwm2m_client_hdl, int event_id, lwm2m_event_info *event_info_ptr);

/*
description: allocate  new lwm2m client
*/
int  lwm2m_client_new(lwm2m_client_listener      cb);

int  lwm2m_client_set_option(int client_hdl, int opt_name, void *val, int val_len);

int  lwm2m_client_object_update(int client_hdl, int opt,int obj_id);

int  lwm2m_client_register(int       client_hdl);

int  lwm2m_client_update(int       client_hdl,int short_server_id, boolean with_obj);

int  lwm2m_client_deregister(int       client_hdl);

int  lwm2m_client_free(int       client_hdl);

boolean lwm2m_check_is_vzw_netwrok();

// add by herry.Geng on 20190411 for ATT spn
boolean lwm2m_check_is_att_netwrok();

int  lwm2m_client_get_fw_dload_url(int client_hdl, char *url);

boolean lwm2m_client_handle_wap_sms(int client_hdl, uint8 *data, uint16 data_len);

int lwm2m_client_create_object_instance(int client_hdl, int obj_id, lwm2m_data_t *instance_data);

int lwm2m_client_delete_object_instance(int client_hdl, int obj_id, int instance_id);

int lwm2m_client_read_object_instance(int client_hdl, int obj_id, int instance_id, int res_id,lwm2m_data_t **instance_data);

int lwm2m_client_write_object_resource(int client_hdl, int obj_id, int instance_id, int res_id, void *res_data);

int lwm2m_client_reset_object_info();

#endif

