#ifndef LWM2M_CONFIGURE_H
#define LWM2M_CONFIGURE_H

#define LWM2M_MAX_SERVER_CNT  4

#define Bootstrap_Server_ID  	0
#define DM_Server_ID         	1
#define Diagnostics_Server_ID   2
#define Repository_Server_ID    3

typedef struct
{
	boolean  valid;
	int      server_id;
	char     server_url[256];
	boolean  bootstrap;
	int      security_mode;
	char     psk_id[64];
	char     psk_key[128];
	int      life_time;
	int      period_min;
	int      period_max;
	int      disable_timeout;
	boolean  storing;
	char     binding[4];	
}lwm2m_server_info_t;

typedef struct{
	int     mask;//LSB0:apn_name;LSB1:estab_time; LSB2: estab_result;LSB3:estab_result;LSB4:estab_rej_cause;LSB5:release_time
	char    apn_name[256];
	int     estab_time;
	int     estab_result;
	int     estab_rej_cause;
	int     release_time;
}lwm2m_apn_conn_info_t;

typedef struct{
	char   unique_id[128];
	char   manufacture[128];
	char   model[128];
	char   sw_version[128];
}lwm2m_hostdevice_info_t;

void lwm2m_load_server_configuration();

lwm2m_server_info_t *lwm2m_get_server_info(int server_idx);

boolean lwm2m_update_server_info(int server_idx,lwm2m_server_info_t  *server_info_ptr);

void lwm2m_delete_server_info(int server_idx);

void lwm2m_save_apn_conn_info(lwm2m_apn_conn_info_t *apn_conn_info);

boolean lwm2m_load_apn_conn_info(lwm2m_apn_conn_info_t *apn_conn_info);

void lwm2m_save_hostdevice_info(int id, lwm2m_hostdevice_info_t *dev_info);

boolean lwm2m_load_hostdevice_info(int id, lwm2m_hostdevice_info_t *dev_info);

#endif
