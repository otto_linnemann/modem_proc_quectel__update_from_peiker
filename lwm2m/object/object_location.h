#ifndef OBJECT_LOCATION_H
#define OBJECT_LOCATION_H

// Resource Id's:
#define RES_M_LATITUDE     0
#define RES_M_LONGITUDE    1
#define RES_O_ALTITUDE     2
#define RES_O_RADIUS       3
#define RES_O_VELOCITY     4
#define RES_M_TIMESTAMP    5
#define RES_O_SPEED        6

#define VELOCITY_OCTETS                      5  // for HORITZOL_VELOCITY_WITH_UNCERTAINTY 
#define DEG_DECIMAL_PLACES                   6  // configuration: degree decimals implementation


typedef struct location_data_
{
  uint16_t instanceId;
  double    latitude; //"359.12345" frag=5, 9+1=10! degrees +\0
  double    longitude;
  double    altitude;
  float    radius;
  uint8_t  velocity[VELOCITY_OCTETS];        //3GPP notation 1st step: HORITZOL_VELOCITY_WITH_UNCERTAINTY
  uint64_t timestamp;
  float     speed;
} location_data_t;

lwm2m_object_t * get_object_location(void);

void free_object_location(lwm2m_object_t * object);

#endif

