#ifndef _OBJECT_SERVER_H
#define _OBJECT_SERVER_H

#define LWM2M_SERVER_RESOURCE_INST_IS_REGISTERED                  0
#define LWM2M_SERVER_RESOURCE_INST_IS_CLIENT_HOLD_OFF_TIMER       1
#define MAX_CUST_30000_RES_INSTANCES 10


typedef struct _server_instance_
{
  struct _server_instance_ * next;   // matches lwm2m_list_t::next
  uint16_t    instanceId;            // matches lwm2m_list_t::id
  uint16_t    shortServerId;
  uint32_t    lifetime;
  uint32_t    defaultMinPeriod;
  uint32_t    defaultMaxPeriod;
  uint32_t    disableTimeout;
  boolean     storing;
#ifdef FACTORY_BOOTSTRAPPING
  char        *binding;
  uint32_t    bindingLen;
#else
  char        binding[4];
#endif
  resource_instance_int_list_t*    custom30000List;

} server_instance_t;

#ifdef FACTORY_BOOTSTRAPPING
/**
 * @fn  lwm2m_object_t * get_server_object()
 * @brief   This function is used to create an empty server object
 *          (No instantiation)
 *
 * @return returns pointer to server object created if successful, 
 *         else return null
 */
lwm2m_object_t * get_server_object(void);
#else
/**
 * @fn  lwm2m_object_t * get_server_object()
 * @brief   This function is used to get the inited server object
 *
 * @param   serverId    Short Server ID of the lwm2m server
 * @param   binding     Binding mode to be used to connect to the lwm2m server
 * @param   lifetime    Lifetime period of the registration with this server
 * @param   storing     Indicates if the notifies needs to be stored if 
 *                      server disabled
 *
 * @return returns pointer to server object created if successful, 
 *         else return null
 */
lwm2m_object_t * get_server_object(int serverId, const char* binding, int lifetime,
                 bool storing);
#endif

/**
 * @fn  void clean_server_object()
 * @brief   Deletes all instances of server object
 *
 * @param   object  handle to server object
 *
 * @return  void
 */
void clean_server_object(lwm2m_object_t * object);

/**
 * @fn void display_server_object()
 * @brief   Displays all instances of server object
 *
 * @param   object  handle to server object
 *
 * @return  void
 */
void display_server_object(lwm2m_object_t * objectP);

/**
 * @fn void copy_server_object();
 * @brief   Creates of copy of server object containing a copy of each 
 *          server instance
 *
 * @param   object  handle to server object
 *
 * @return  void
 */
void copy_server_object(lwm2m_object_t * objectDest, 
     lwm2m_object_t * objectSrc);

int get_server_lifetime(lwm2m_object_t * objectP, uint16_t InstID);

int get_server_ssid(lwm2m_object_t * objectP, uint16_t InstID);

     
int load_server_persistent_info(lwm2m_object_t *serObjP);

int dump_server_persistent_info(lwm2m_object_t *serObjP);
void update_isRegistered_for_server (lwm2m_object_t * serv_object,uint16_t ssid, uint8 value);

#endif

