#ifndef OBJECT_CONNECTIVITY_MONI_H
#define OBJECT_CONNECTIVITY_MONI_H


#include "liblwm2m.h"
// Resource Id's:
#define RES_M_NETWORK_BEARER            0
#define RES_M_AVL_NETWORK_BEARER        1
#define RES_M_RADIO_SIGNAL_STRENGTH     2
#define RES_O_LINK_QUALITY              3
#define RES_M_IP_ADDRESSES              4
#define RES_O_ROUTER_IP_ADDRESS         5
#define RES_O_LINK_UTILIZATION          6
#define RES_O_APN                       7
#define RES_O_CELL_ID                   8
#define RES_O_SMNC                      9
#define RES_O_SMCC                      10
#define RES_O_APN_30000                 30000
#define RES_O_APN_CLASS2                 0
#define RES_O_APN_CLASS3                 1
#define RES_O_APN_CLASS6                 2
#define RES_O_APN_CLASS7                 3


//TODO: Need to take the APN as per Carrier requirement
#define VALUE_APN_1				"ims.carrier.com"
#define VALUE_APN_2         	"cda.vodafone.de"

#define MAX_AVAIL_BEARER	15

#define NUM_OF_IP_ADDR          4 	// Limited to 2
#define NUM_OF_ROUTER_ADDR      4	//Limited to 2
#define IP_ADDR_LEN             16

/*	For m2m_get_connectivity_info*/
#define MASK_SIGNAL_STRENGTH    0x01
#define MASK_LINK_QUALITY       0x02
#define MASK_CELL_ID            0x04
#define MASK_MCC                0x08
#define MASK_MNC                0x0A

typedef struct conn_m_data_
{
  uint16_t instanceId;
  uint16_t pdp_cid; 
  resource_instance_string_list_t*    ipAddresses;
  resource_instance_string_list_t*    routerIpAddresses;
  int64_t cellId;
  int signalStrength;
  int linkQuality;
  int32_t mcc;
  int32_t mnc;
  int linkUtilization;
  uint16_t networkBearer;
  uint8_t availNetworkBearerLength;
  uint16_t availNetworkBearer[MAX_AVAIL_BEARER];
  resource_instance_string_list_t*    apnList;
} conn_m_data_t;

void get_connectivity_info(conn_m_data_t *conn_obj, unsigned int *ret_status);


lwm2m_object_t * get_object_conn_m(int16 pdp_cid);


void free_object_conn_m(lwm2m_object_t * objectP);

int populate_carrier_apn_info_to_conn_m_obj(lwm2m_context_t * context);


#endif
