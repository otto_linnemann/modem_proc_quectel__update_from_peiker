#ifndef OBJECT_FIRMWARE_H
#define OBJECT_FIRMWARE_H

lwm2m_object_t * get_object_firmware(void);

void free_object_firmware(lwm2m_object_t * objectP);

#endif
