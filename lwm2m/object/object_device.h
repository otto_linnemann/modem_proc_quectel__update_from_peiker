#ifndef OBJECT_DEVICE_H
#define OBJECT_DEVICE_H

lwm2m_object_t * get_object_device(void *user_data);

void free_object_device(lwm2m_object_t * objectP);

#endif
