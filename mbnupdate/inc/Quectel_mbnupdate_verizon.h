
/*!  
  @file
  Quectel_mbnupdate_verizon.h

  @brief
  This file defines declarations for MBN-UPDATE VERIZON use

  @see
  Create this file by QCM9X00211C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/13/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MBNUPDATE_VERIZON_H__
#define __QUECTEL_MBNUPDATE_VERIZON_H__

#include "quectel_mbnupdate_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

extern boolean MBNUPDATE_VERIZON_FuncRefresh(void);
extern boolean MBNUPDATE_VERIZON_FuncDeactivate(void);

#endif  // #ifndef __QUECTEL_MBNUPDATE_VERIZON_H__
