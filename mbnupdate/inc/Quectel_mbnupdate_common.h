
/*!  
  @file
  Quectel_mbnupdate_common.h

  @brief
  This file defines declarations for MBN-UPDATE COMMON use

  @see
  Create this file by QCM9X00211C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/13/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MBNUPDATE_COMMON_H__
#define __QUECTEL_MBNUPDATE_COMMON_H__

#include "quectel_mbnupdate_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
extern MBNUPDATE_ItemStruct g_asMBNUPDATEElemListProject[];

extern MBNUPDATE_ElemStruct g_asMBNUPDATEElemListVerizon[];

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean QL_MBNUPDATE_McfgRefresh(void);
extern boolean QL_MBNUPDATE_MbnDeactivate(void);
extern boolean QL_MBNUPDATE_GetCurVersion(int iOperator,MBNUPDATE_VersionStruct *pVersion);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////

#endif  // #ifndef __QUECTEL_MBNUPDATE_COMMON_H__
