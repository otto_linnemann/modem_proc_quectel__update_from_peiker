
/*!  
  @file
  Quectel_mbnupdate_base.h

  @brief
  This file defines declarations for MBN-UPDATE BASE use

  @see
  Create this file by QCM9X00211C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/13/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MBNUPDATE_BASE_H__
#define __QUECTEL_MBNUPDATE_BASE_H__

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define MBNUPDATE_OPERATOR_DEFAULT   (-3) //-1 is FUNC_NET_OPER_MAX
#define MBNUPDATE_OPERATOR_NONE   (-2)

#define QUEC_EFS_MBNUPDATE_VERSION_PREFIX  "mbnupdate_version_"

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef enum
{
    MBNUPDATE_TYPE_NV,
    MBNUPDATE_TYPE_EFS,
    MBNUPDATE_TYPE_FUNC,
    MBNUPDATE_TYPE_VERSION,
    //<2019/02/15-QCM9X00211C001-P04-Vicent.Gao, <[MBNUPDATE] Segment 4==> Base code submit.>
    MBNUPDATE_TYPE_EFS_PART,
    //>2019/02/15-QCM9X00211C001-P04-Vicent.Gao

    //Warning!==>Please add new MBN-UPDATE TYPE upper this line.
    MBNUPDATE_TYPE_MAX
} MBNUPDATE_TypeEnum;

typedef struct
{
    uint32 uOperator; //For operator
    uint32 uProject; //For project
    uint32 aRev[6];
} MBNUPDATE_VersionStruct;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

typedef boolean MBNUPDATE_Func(void);

typedef struct
{
    uint8 uType; // 'MBNUPDATE_TypeEnum' data
    void *pData; // NV; EFS; FUNC;
    char *pStr;
    //<2019/02/15-QCM9X00211C001-P04-Vicent.Gao, <[MBNUPDATE] Segment 4==> Base code submit.>
    char *pPart;
    //>2019/02/15-QCM9X00211C001-P04-Vicent.Gao
} MBNUPDATE_ElemStruct;

typedef struct
{
    int iOperator;
    MBNUPDATE_ElemStruct *pElemList;
} MBNUPDATE_ItemStruct;

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
extern const char mob_sw_rev[];

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean QL_EFSDEF_WriteNvItemByMcfgStr(nv_items_enum_type eItem,char *pStr);
extern boolean QL_EFSDEF_WriteFileStr(char *pEfs,char *pStr);

extern MBNUPDATE_ElemStruct* QL_MBNUPDATE_GetElem(MBNUPDATE_ElemStruct *pElemList,int iElemType);
extern boolean QL_MBNUPDATE_GetVersionFromEfs(int iOperator,MBNUPDATE_VersionStruct *pVersion);
extern boolean QL_MBNUPDATE_SaveVersionToEfs(int iOperator,MBNUPDATE_VersionStruct *pVersion);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define MBNUPDATE_TRACE_0(f)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBNUPDATE_" f,__FUNCTION__,__LINE__)
#define MBNUPDATE_TRACE_1(f,g1)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBNUPDATE_" f,__FUNCTION__,__LINE__,g1)
#define MBNUPDATE_TRACE_2(f,g1,g2)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2)
#define MBNUPDATE_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_5(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3)
#define MBNUPDATE_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_6(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4)
#define MBNUPDATE_TRACE_5(f,g1,g2,g3,g4,g5)  MSG_SPRINTF_7(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5)
#define MBNUPDATE_TRACE_6(f,g1,g2,g3,g4,g5,g6)  MSG_SPRINTF_8(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBNUPDATE_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5,g6)

#endif  // #ifndef __QUECTEL_MBNUPDATE_BASE_H__
