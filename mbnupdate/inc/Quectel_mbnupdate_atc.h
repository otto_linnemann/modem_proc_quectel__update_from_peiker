
/*!  
  @file
  Quectel_mbnupdate_atc.h

  @brief
  This file defines declarations for MBN-UPDATE ATC use

  @see
  Create this file by QCM9X00211C002-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
11/26/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MBNUPDATE_ATC_H__
#define __QUECTEL_MBNUPDATE_ATC_H__

#include "quectel_mbnupdate_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean QL_QMBNUPDATE_SendResponseS3S4(void);
extern boolean QL_MBNUPDATE_GetCurVersion(int iOperator,MBNUPDATE_VersionStruct *pVersion);

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern dsat_result_enum_type quectel_exec_qmbnupdate_cmd
(
    dsat_mode_enum_type eMode,             /*  AT command mode:            */
    const dsati_cmd_type *pParseTab,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *pTok,    /*  Command tokens from parser  */
    dsm_item_type *pResBuf           /*  Place to put response       */
);

#endif  // #ifndef __QUECTEL_MBNUPDATE_ATC_H__
