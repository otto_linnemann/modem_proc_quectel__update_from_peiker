#ifndef QUECTEL_SMTP_APP_H
#define  QUECTEL_SMTP_APP_H

#if defined(QUECTEL_SMTP_SUPPORT)


#define QUECTEL_SMTP_DEL_ALL_DST                         0
#define QUECTEL_SMTP_DEL_DST_BY_TYPE                 1
#define QUECTEL_SMTP_DEL_DST_BY_ADDR                 2
#define QUECTEL_SMTP_DEL_ALL_ATTACHMENT           0
#define QUECTEL_SMTP_DEL_ATTACHMENT_BY_ID       1
#define QUECTEL_SMTP_DEL_ATTACHMENT_BY_NAME  2

typedef enum
{
	QUECTEL_SMTP_MAIL_DST_READ,
	QUECTEL_SMTP_MAIL_ATTACHMENT_READ,
	QUECTEL_SMTP_MAIL_SEND_RSP
}quectel_smtp_event_enum;

typedef struct
{
 	Q_INT32            result;
	Q_BOOL            begin;
	Q_BOOL            end;
	Q_INT32            dst_type;
	Q_UINT8           dst_addr[255];
}quectel_smtp_mail_dst_info;


typedef struct
{
	Q_INT32            result;
	Q_BOOL             begin;
	Q_BOOL             end;
	Q_INT32            id;
	Q_UINT8            file_name[255];
	Q_UINT32          file_size;
}quectel_smtp_mail_attachment_info;

typedef struct
{
	Q_INT32          result;
	Q_INT32          protocol_result;
}quectel_smtp_mail_send_result;



typedef enum
{
	QUECTEL_SMTP_ACCOUNT_CFG = 0,
	QUECTEL_SMTP_SENDER_CFG,
	QUECTEL_SMTP_SERVER_CFG,
	QUECTEL_SMTP_AUTH_TYPE_CFG,
	QUECTEL_SMTP_SSL_CTX_ID_CFG,
	QUECTEL_SMTP_PDP_ID_CFG
}quectel_smtp_common_option;


typedef struct
{
 	Q_UINT8    *username;
	Q_UINT8    *password;
}quectel_smtp_account;


typedef struct
{
	Q_UINT8    *sender_name;
	Q_UINT8    *sender_addr;
}quectel_smtp_sender;

typedef struct
{
	Q_UINT8    *server_addr;
	Q_UINT16    server_port;
}quectel_smtp_server;

typedef void (*quectel_smtp_event_cb)(Q_INT32 smtp_ctx, quectel_smtp_event_enum event, void *event_ptr);

Q_INT32  quectel_smtp_ctx_get
(
quectel_smtp_event_cb   cb
);

Q_INT32  quectel_smtp_common_configuration_set
(
Q_INT32      smtp_ctx,
Q_UINT8     opt_name,
void            *opt_val
);

Q_INT32  quectel_smtp_common_configuration_get
(
Q_INT32      smtp_ctx,
Q_UINT8     opt_name,
void            *opt_val
);

Q_INT32  quectel_smtp_mail_receiver_add
(
Q_INT32     smtp_ctx,
Q_INT32     receiver_type,
Q_UINT8     *receiver_addr 
);

Q_INT32  quectel_smtp_mail_receiver_delete
(
Q_INT32     smtp_ctx,
Q_INT32    delete_type,
Q_INT32    receiver_type,
Q_UINT8  *receiver_addr 
);


Q_INT32  quectel_smtp_mail_receiver_read
(
Q_INT32     smtp_ctx
);

Q_INT32  quectel_smtp_mail_subject_set
(
Q_INT32              smtp_ctx,
Q_UINT32             charset,
Q_UINT32              encode_type,
dsm_item_type  *subject
);

Q_INT32  quectel_smtp_mail_subject_get
(
Q_INT32                 smtp_ctx,
Q_UINT32                *charset,
dsm_item_type        **subject
);

Q_INT32  quectel_smtp_mail_attachment_add
(
Q_INT32     smtp_ctx,
Q_INT32     id,
Q_UINT8     *file_name 
);

Q_INT32  quectel_smtp_mail_attachment_delete
(
Q_INT32     smtp_ctx,
Q_INT32    delete_type,
Q_INT32    id,
Q_UINT8  *file_name 
);


Q_INT32  quectel_smtp_mail_attachment_read
(
Q_INT32     smtp_ctx
);


Q_INT32 quectel_smtp_mail_body_add
(
Q_INT32    smtp_ctx,
Q_INT32    charset,
dsm_item_type  *body
);


Q_INT32  quectel_smtp_mail_discard
(
Q_INT32 smtp_ctx
);

Q_INT32  quectel_smtp_mail_send
(
Q_INT32          smtp_ctx,
Q_UINT32      send_timeout
);

Q_INT32 quectel_remove_dir_info
(
Q_UINT8 *no_pre_name
);

#endif//QUECTEL_SMTP_SUPPORT
#endif//QUECTEL_SMTP_APP_H
