#ifndef QUECTEL_BASE64_H
#define QUECTEL_BASE64_H

extern int quectel_EncodeBase64(const char* pcIn, int nInLen, char* pszOut);
extern int quectel_DecodeBase64(const char* cpszIn, char* pcOut);
#endif
