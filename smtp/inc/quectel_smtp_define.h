#ifndef QUECTEL_SMTP_DEFINE_H
#define  QUECTEL_SMTP_DEFINE_H

#include "comdef.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"

#if defined(QUECTEL_SMTP_SUPPORT)

#define MAX_APP_SMTP_RECIPIENT_NUM         (20)
#define MAX_APP_SMTP_ATTACHMENT_NUM     (10)


/*reply codes from the SMTP server*/
#define SMTP_REPLY_SUCCESS            (200)
#define SMTP_REPLY_SYS_STATUS         (211)
#define SMTP_REPLY_HELP_MSG           (214)
#define SMTP_REPLY_SERVICE_READY      (220) 
#define SMTP_REPLY_SERVICE_CLOSE      (221)
#define SMTP_REPLY_AUTH_SUCCESS       (235)
#define SMTP_REPLY_MAIL_COMPLETE      (250)
#define SMTP_REPLY_NOT_LOCAL          (251)
#define SMTP_REPLY_NEED_AUTH          (334)
#define SMTP_REPLY_START_MAIL         (354)
#define SMTP_REPLY_SERVICE_FAIL       (421) 
#define SMTP_REPLY_MAILBOX_UNAVAIL    (450)
#define SMTP_REPLY_REQUEST_ABORTED    (451)
#define SMTP_REPLY_STORAGE_FULL       (452) 
#define SMTP_REPLY_COMMAND_ERROR      (500) 
#define SMTP_REPLY_PARAM_ERROR        (501)
#define SMTP_REPLY_COMMAND_NOT_IMPLE  (502)
#define SMTP_REPLY_BAD_SEQUENCE       (503)
#define SMTP_REPLY_PARAM_NOT_IMPLE    (504)
#define SMTP_REPLY_SERVER_UNACCEPT    (521)
#define SMTP_REPLY_ACCESS_DENIED      (530)
#define SMTP_REPLY_AUTHEN_FAILED      (535)
#define SMTP_REPLY_MAILBOX_UNAVAIL_2  (550)   /*similar with 450*/
#define SMTP_REPLY_NOT_LOCAL_TRY_FOR  (551)   /*similar with 251*/
#define SMTP_REPLY_STORAGE_EXCEED     (552)   /*similar with 452*/
#define SMTP_REPLY_MAILBOX_NAME_ERROR (553)
#define SMTP_REPLY_TRANSACTION_FAILED (554)


typedef enum
{
 	SMTP_STATUS_IDLE = 0,
 	SMTP_STATUS_PDP_ACTIVING,
 	SMTP_STATUS_DNS_PARSE,
 	SMTP_STATUS_CONNECTING_TO_SERVER,
 	SMTP_STATUS_CONNECTED,
 	SMTP_STATUS_SSL_HANDSHARK,
	SMTP_STATUS_MAIL_EHLO,
	SMTP_STATUS_MAIL_TLS,
	SMTP_STATUS_MAIL_AUTH,
	SMTP_STATUS_AUTH_PWD,
	SMTP_STATUS_AUTH_OVER,
	SMTP_STATUS_MAIL_FROM,
	SMTP_STATUS_MAIL_RCPT_TO,
	SMTP_STATUS_MAIL_DATA,
	SMTP_STATUS_MAIL_EOM,
	SMTP_STATUS_MAIL_QUIT,
 	SMTP_STATUS_CLOSING,
 	SMTP_STATUS_CLOSED = SMTP_STATUS_IDLE
}smtp_service_state;

typedef enum 
{
 	SMTP_ENCODING_7BIT,
 	SMTP_ENCODING_BASE64,
 	SMTP_ENCODING_QPRINT,
 	SMTP_ENCODING_NUMS
}smtp_encoding_type;

typedef enum 
{
 	SMTP_CONTYPE_TEXT_PLAIN,
 	SMTP_CONTYPE_OCTET_STREAM,
 	SMTP_CONTYPE_NUMS
}smtp_content_type;

typedef enum
{
	SMTP_DATA_PART_HEADER=0,
	SMTP_DATA_PART_BODY,
	SMTP_DATA_PART_ATTACHMNET,
	SMTP_DATA_PART_NONE
}smtp_data_type;

typedef enum 
{
 	SMTP_CHARSET_ASCII,
 	SMTP_CHARSET_UTF8,
 	SMTP_CHARSET_GB2312,
  	SMTP_CHARSET_BIG5,
	SMTP_CHARSET_NUMS	
}smtp_charset_type;

typedef enum 
{
	SMTP_DST_NONE,
	SMTP_DST_RECP,
	SMTP_DST_COPY,
	SMTP_DST_SECRET,
	SMTP_DST_NUMS
}smtp_dst_type;

typedef enum
{
 	SMTP_SSL_NONE = 0,
  	SMTP_SSL,
 	SMTP_SSL_TLS,
    	SMTP_SSL_MAX
}smtp_ssl_type;

typedef enum 
{
  	SMTP_COMMAND_HELO,
	SMTP_COMMAND_EHLO,
  	SMTP_COMMAND_AUTH,
  	SMTP_COMMAND_MAIL,      /* set the address of the sender */
  	SMTP_COMMAND_RCPT,      /* set the address of the receivers */
  	SMTP_COMMAND_DATA,      /* start to transfer the content of the email */
  	SMTP_COMMAND_EOM,
  	SMTP_COMMAND_QUIT,      /* quit the session of SMTP */
  	SMTP_COMMAND_TLS,
  	SMTP_COMMAND_NUM
}smtp_command_enum;



typedef struct
{
	Q_UINT8      login_user[255];
	Q_UINT8      login_pwd[255];
 	Q_UINT8      sender_name[255];
  	Q_UINT8      sender_mail[255];
  	Q_UINT8      smtp_server_addr[255];
  	Q_UINT16    smtp_server_port;
  	Q_UINT8      smtp_ssl_type;
}smtp_configure;

typedef struct
{
	smtp_charset_type		charset;
	smtp_encoding_type      encoding;
	dsm_item_type              *subject;
}smtp_mail_subject;

typedef struct
{
	smtp_charset_type        charset;
	smtp_encoding_type      encoding;
	dsm_item_type              *body;
}smtp_mail_body;


typedef struct
{
	smtp_dst_type            type;
	Q_UINT8                    receiver_addr[255];
}smtp_mail_receiver;

typedef struct
{
	smtp_charset_type        charset;
	smtp_encoding_type      encoding;
	smtp_content_type        content_type;
	Q_UINT32                     id;
	Q_UINT8                       filename[255];
	Q_UINT32                     file_size;
}smtp_mail_attachment;

typedef struct
{
 	smtp_mail_subject          subject;
	smtp_mail_body             body;
	smtp_mail_receiver        receiver_info[MAX_APP_SMTP_RECIPIENT_NUM];
	smtp_mail_attachment    attachment_info[MAX_APP_SMTP_ATTACHMENT_NUM];
	Q_UINT32                      receiver_total_cnt;
	Q_UINT32                      attachment_total_cnt;
	Q_UINT32                      attachment_total_size;
}smtp_mail_configure;

typedef struct
{
	Q_UINT8                   		   auth_mask;
	Q_UINT8                                 receiver_index;
	Q_UINT32                               smtp_body_len;
	Q_UINT32                               smtp_att_size;
	Q_UINT32                               att_index;
	Q_INT32                                 att_file_handle;
	Q_UINT8                                 smtp_data_type;
}smtp_mail_send_info;

typedef enum 
{
	SMTP_AUTH_UNKNOWN   = 0x00,
	SMTP_AUTH_LOGIN     = 0x01,
	SMTP_AUTH_PLAIN     = 0x02,
	SMTP_AUTH_CRAM_MD5  = 0x04
}smtp_auth_type;


typedef struct
{
 	smtp_service_state                  state;
 	Q_UINT8                                 pdp_id;
  	Q_INT32                                 sockfd;
#if defined(QUECTEL_CYASSL_SUPPORT)
 	Q_INT32                                 ssl_ctx_id;
 	Q_INT32                                 ssl_session_handle;
#endif
	Q_BOOL                                  soc_write_block;
 	smtp_configure                        common_cfg;
  	smtp_mail_configure                mail_cfg; 
	apptcpip_timer_id                    mail_send_timer;
	Q_UINT32                               mail_send_timeout;
	Q_INT32                                 smtp_reply_code;
	dsm_item_type                        *smtp_server_msg;
	dsm_item_type                        *smtp_client_data;
	smtp_mail_send_info                smtp_send_info;
	quectel_smtp_event_cb            event_cb;
}smtp_service_context;


typedef enum
{
     APP_SMTP_OK = 0,
     APP_SMTP_ERROR = -1,
     APP_SMTP_WOULDBLOCK = -2,
     APP_SMTP_ERROR_BUSY = -3,
     APP_SMTP_ERROR_DNS = -4,       /*failed to parse domain name*/
     APP_SMTP_ERROR_NETWORK = -5,   /*failed to establish socket or the network is deactivated*/
     APP_SMTP_ERROR_UNSUPPORT = -6,
     APP_SMTP_PEER_CLOSE = -7,
     APP_SMTP_BEARER_FAIL = -8,  
     APP_SMTP_ERROR_TIMEOUT =-9,
     APP_SMTP_ERROR_NO_RCPT = -10,
     APP_SMTP_ERROR_SEND_FAIL = -11,
     APP_SMTP_ERROR_OPEN_FILE  = -12,
     APP_SMTP_ERROR_NO_MEMORY = -13,
     APP_SMTP_ERROR_SAVE_FAIL = -14,
     APP_SMTP_ERROR_PARAM = -15,
     APP_SMTP_SSL_AUTH_FAIL = -16,
     APP_SMTP_REPLY_SERVICE_FAIL = -17,
     APP_SMTP_REPLY_MAILBOX_UNAVAIL = -18,
     APP_SMTP_REPLY_REQUEST_ABORTED = -19,
     APP_SMTP_REPLY_STORAGE_FULL = -20,
     APP_SMTP_REPLY_COMMAND_ERROR = -21,
     APP_SMTP_REPLY_PARAM_ERROR = -22,
     APP_SMTP_REPLY_COMMAND_NOT_IMPLE = -23,
     APP_SMTP_REPLY_BAD_SEQUENCE = -24,
     APP_SMTP_REPLY_PARAM_NOT_IMPLE = -25,
     APP_SMTP_REPLY_SERVER_UNACCEPT = -26,
     APP_SMTP_REPLY_ACCESS_DENIED = -27,
     APP_SMTP_REPLY_AUTHEN_FAILED = -28,
     APP_SMTP_REPLY_MAILBOX_UNAVAIL_2 = -29,
     APP_SMTP_REPLY_NOT_LOCAL_TRY_FOR = -30,
     APP_SMTP_REPLY_STORAGE_EXCEED = -31,
     APP_SMTP_REPLY_MAILBOX_NAME_ERROR = -32,
     APP_SMTP_REPLY_TRANSACTION_FAILED = -33,
}smtp_error_code;

#endif//QUECTEL_SMTP_SUPPORT

#endif//QUECTEL_SMTP_DEFINE_H
