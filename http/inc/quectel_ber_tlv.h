/******************************************************************************
*        
*     Copyright (c) 2013 Quectel Ltd.       
*        
*******************************************************************************
*  file name:          Quectel_ber_tlv.h
*  author:              Kim
*  date:                2013.1.11
*  file description:   
******************************************************************************/

#ifndef __QUECTEL_BER_TLV_H__
#define __QUECTEL_BER_TLV_H__

#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"
#include "quectel_triangle_locator.h"
#include "time_types.h"

#ifdef FEATURE_QUECTEL_LOCATOR_APP

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
基本信息相关的结构体定义，基本信息不参与加密操作
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/* 服务类型*/
#define Q_SERV_TYPE_LOC		  0x01
#define Q_SERV_TYPE_LPOS       0x02
#define Q_SERV_TYPE_APS	      0x03
#define Q_SERV_TYPE_USER		  0x04
/* 服务类型字符串*/
#define Q_SERV_STR_LOC		    ("q_loc")
#define Q_SERV_STR_LPOS       ("q_lpos")
#define Q_SERV_STR_APS	        ("q_agps")
#define Q_SERV_STR_USER		("q_user")
/* URI*/
#define Q_URI_LOC                     ("/location/QLOC")
#define Q_URI_LPOS                   ("/location/QLPOS")
#define Q_URI_APS                     ("/location/QAPS")
#define Q_URI_USER                   ("/location/QUSER")
/* 数据类型*/
#define Q_CONT_TYPE_JSON           0x01
#define Q_CONT_TYPE_BERTLV       0x02
#define Q_CONT_TYPE_TRIANGLE     0x03
/* 数据类型字符串 版本号*/
#define Q_CONT_STR_JSON       ("01")
#define Q_CONT_STR_BERTLV   ("02")
#define Q_CONT_STR_TRIANGLE  ("03") //jonathan,14/2/13,
/* 加密类型*/
#define Q_ENCRIPT_XOR     0x01
/* 返回信息是否包含地址*/
#define Q_ADDR_NONE       0x01
#define Q_ADDR_INCLUDE  0x02


/*Begin - 2013.6.6 Kim 增加分步式服务器重定向功能*/
#define Q_LOC_DEFAULT_SERVER                      ("www.queclocator.com")
#define Q_LOC_DEFAULT_SERVER_LEN             (strlen(Q_LOC_DEFAULT_SERVER))
#define Q_LOC_DEFAULT_PORT                          (80)
#define MAX_REDIRECT_LIST_SIZE                     (3)
#define MAX_SERVER_LIST_SIZE                         (10)
#define MAX_ERR_REP_LIST_SIZE                     (3)
#define MAX_ADDRESS_SIZE                               (256)
#define MAX_SERVER_LIST_VERSION_SIZE       (20)
#define MAX_HOST_ADDR_SIZE                           (32)
//#define MAX_DNS_SERVER_IP_LIST                    SOC_MAX_A_ENTRY
#define  IP_ADDR_LEN                                           (4)    
#define MAX_QUECLOCATOR_PRO_LEN              (20)
#define MAX_QUECLOCATOR_PROVALUE_LEN   (50)
#define QUECLOCATOR_MINIMUM_TIMEOUT      (1)     /* unit: second */
#define QUECLOCATOR_MAXIMUM_TIMEOUT      (300)   /* unit: second */

/* 定位方法: at命令的第一个参数*/
typedef enum QLocType_Tag
{
    Q_LOC_TYPE_BEGIN                       = 0,
    Q_LOC_TYPE_NORMAL                   = 1,
    Q_LOC_TYPE_CLIENT_TRIANGLE  =           2,
    Q_LOC_TYPE_INPUT                       = 3,
    Q_LOC_TYPE_SERVER_TRIANGLE = 4,
    Q_LOC_TYPE_MUTI                         =5,
    Q_LOC_TYPE_NUM
}QLocType_e;

/* 定位方法: 发给服务器的locMethod*/
typedef enum QLocMethod_Tag
{
    Q_LOC_METHOD_BEGIN                         = 0,
    Q_LOC_METHOD_NORMAL                     = 1,
    Q_LOC_METHOD_MUTI                           =2,
    Q_LOC_METHOD_SERVER_TRIANGLE   = 3,
    Q_LOC_METHOD_CLIENT_TRIANGLE    = 4,
    Q_LOC_METHOD_NUM
}QLocMethod_e;

typedef enum QLocErrorCode_Tag
{
    QLOC_ERROR_CONNECT_FAIL = 1,
    QLOC_ERROR_NO_POSITION  = 2,
    QLOC_ERROR_DECODE_ERR    = 3,
    QLOC_ERROR_TIMEOUT_ERR   = 4,
}QLocErrCode_e;

typedef struct QLocServerAddress_Tag
{
    /*
    !!!此结构体如有修改，需同时修改nvram_ef_queclocator_struct
    *注意4 字节对齐
    */
    Q_UINT16                   hostport;
    Q_UINT8                     isDomainName;       /*0 -IP, 1 - DomainName*/
    Q_UINT8                     reserve;                   /*保留，保持4 byte对齐*/
    union{
        Q_UINT8                 hostip[MAX_HOST_ADDR_SIZE];
        Q_UINT8                 hostname[MAX_HOST_ADDR_SIZE];
    }addr;
}QLocServerAddress;

typedef struct QLocRedirectServer_Tag
{
    Q_BOOL                    isValid;           /*标记当前地址是否有效*/
    Q_UINT8                   failTimes;        /*记录失败次数*/
    QLocServerAddress  serverAddr;
}QLocRedirectServer_t;

typedef struct QLocServerList_Tag
{
    /*
    !!!此结构体如有修改，需同时修改nvram_ef_queclocator_struct
    *注意4 字节对齐
    */
    Q_UINT8                  version[MAX_SERVER_LIST_VERSION_SIZE];/*eg: v20130603082356*/
    QLocServerAddress  serverAddr[MAX_SERVER_LIST_SIZE];
}QLocServerList_t;

#define QLOC_SERVER_LIST_SIZE            (sizeof(QLocServerList_t))

/*定位请求时，服务器地址从哪里来*/
typedef enum SrvFromType_Tag
{
    SRV_FROM_TYPE_REDIRECT  = 1,
    SRV_FROM_TYPE_CONFIG = 2,
    SRV_FROM_TYPE_SRVLIST = 3,
    //SRV_FROM_TYPE_DNS,
    SRV_FROM_TYPE_DEFAULT,
}SrvFromType_e;

#define SERVER_IP_SIZE                (4)
typedef struct QLocErrReport_Tag
{
    Q_UINT16 reason;                                  /*失败原因*/
    Q_UINT8   failTimes;                              /*失败次数*/
    Q_UINT8   serverIP[SERVER_IP_SIZE];  /*服务器IP */
}QLocErrReport_t;
#define MAX_ERR_REPORT_SIZE  7 /*单个ERROR REPORT 字节数*/
/*End - 2013.6.6 Kim 增加分步式服务器重定向功能*/

#define TLV_ASSERT(x)  ASSERT(x)
#define SAFE_FREE(p)			{if(p){apptcpip_mem_free(p); (p)=NULL;}}  
#define MIN_VALUE(a,b) ((a)<(b))? (a):(b)
//jonathan,14-2-18,update code
#define MAX_VALUE(a,b) ((a)>(b))? (a):(b)

#define BER_CONSTRUCTOR	                  (0x20)
#define BER_LONG_LEN                         (0x80)
#define BER_MORE_TAG                         (0x80)
#define BER_EXTENSION_ID                  (0x1F)

#define BER_LEN_ALLOC_SIZE(x)          ((x)<0x80 ? 1 : (2+(x)/256))

/* 短整型大小端互换*/
#define BigLittleSwap16(A)  ((((Q_UINT16)(A) & 0xff00) >> 8) | (((Q_UINT16)(A) & 0x00ff) << 8))

/* 长整型大小端互换*/
#define BigLittleSwap32(A)  ((((Q_UINT32)(A) & 0xff000000) >> 24) | \
													(((Q_UINT32)(A) & 0x00ff0000) >> 8) | \
													(((Q_UINT32)(A) & 0x0000ff00) << 8) | \
													(((Q_UINT32)(A) & 0x000000ff) << 24))

#define PARASE_RECURSION_TREE_ERROR (65535)
#define MAX_SIGLE_TLV_LEN  (2046) /*单个TLV长度最大为2046字节*/

#define MAX_RAW_KEY_NUM  (8) /*加扰key的总个数*/
#define XOR_KEY_LEN (8)/*异或加解密的KEY长度*/

typedef enum Radio_Type_Tag
{
    RADIO_TYPE_GSM  = 1,
    RADIO_TYPE_WCDMA = 2,
    RADIO_TYPE_LTE,
    RADIO_TYPE_MAX
}RadioType_e;

/*基站定位请求TAG宏定义*/
#define BER_LOC_TAG	                  0x01	/* 基站定位请求，嵌套类型*/
#define BER_LOC_BASIC_TAG		  0x01  /* 基本信息*/
#define BER_LOC_AUTH_TAG		  0x02  /* 鉴权信息*/
#define BER_LOC_CELL1_TAG		  0x03  /* 基站信息1*/
#define BER_LOC_CELL2_TAG		  0x04  /* 基站信息2*/
#define BER_LOC_CELL3_TAG		  0x05  /* 基站信息3*/
#define BER_LOC_CELL4_TAG		  0x06  /* 基站信息4*/
#define BER_LOC_CELL5_TAG		  0x07  /* 基站信息5*/
#define BER_LOC_CELL6_TAG		  0x08  /* 基站信息6*/
#define BER_LOC_MD5_TAG		      0x09  /* MD5*/
/*Begin - 2013.6.13 Kim 增加分步式服务器重定向功能*/
#define BER_LOC_SRV_LIST_VER_TAG     0x0A /*固化服务器表version*/
#define BER_LOC_ERROR_REPORT1_TAG 0x0B /*前一次访问出错上报*/
#define BER_LOC_ERROR_REPORT2_TAG 0x0C /*前一次访问出错上报*/
#define BER_LOC_ERROR_REPORT3_TAG 0x0D /*前一次访问出错上报*/
/*End - 2013.6.13 Kim 增加分步式服务器重定向功能*/

/*返回位置信息*/
#define BER_POS_TAG                              0x02	/* 复合结构*/
#define BER_POS_STAT_TAG                   0x01	
#define BER_POS_BASIC_TAG                 0x02
#define BER_POS_RAND_TAG                  0x03
#define BER_POS_POS1_TAG                  0x04 
#define BER_POS_POS2_TAG                  0x05
#define BER_POS_POS3_TAG                  0x06
#define BER_POS_POS4_TAG                  0x07 
#define BER_POS_POS5_TAG                  0x08 
#define BER_POS_POS6_TAG                  0x09 
#define BER_POS_MD5_TAG                    0x0A  /* MD5*/
/*Begin - 2013.6.7 Kim 增加分步式服务器重定向功能*/
#define BER_POS_REDIRECT1_TAG         0x0B /*重定向表数据1*/
#define BER_POS_REDIRECT2_TAG         0x0C /*重定向表数据2*/
#define BER_POS_REDIRECT3_TAG         0x0D /*重定向表数据3*/
#define BER_POS_SRVLIST_VER_TAG     0x0E /*固化服务器表版本信息*/
#define BER_POS_SRVLIST1_TAG           0x0F /*固化服务器表数据1*/
#define BER_POS_SRVLIST2_TAG           0x10
#define BER_POS_SRVLIST3_TAG           0x11
#define BER_POS_SRVLIST4_TAG           0x12
#define BER_POS_SRVLIST5_TAG           0x13
#define BER_POS_SRVLIST6_TAG           0x14
#define BER_POS_SRVLIST7_TAG           0x15
#define BER_POS_SRVLIST8_TAG           0x16
#define BER_POS_SRVLIST9_TAG           0x17
#define BER_POS_SRVLIST10_TAG         0x18 /*固化服务器表数据10*/
#define BER_POS_ADDRESS_TAG            0x19 /*街道信息*/
/*End - 2013.6.7 Kim 增加分步式服务器重定向功能*/
//jonathan,14/2/18
#define BER_POS_TRIANGLE_FRE_TAG   0x1A/*获取服务器返回的三角定位访问频率*/
#define BER_TLV_CELL_MAX  6


typedef enum QuectelBerClass_Tag{
	BER_TAG_CLASS_UNIVERSAL        = 0,      /* 0b00 */
	BER_TAG_CLASS_APPLICATION    = 1,      /* 0b01 */
	BER_TAG_CLASS_CONTEXT            = 2,      /* 0b10 */
	BER_TAG_CLASS_PRIVATE             = 3	       /* 0b11 */
}ber_class_tag;

typedef enum QuectelBerConstruct_Tag{
	BER_TAG_PRIMITIVE,
	BER_TAG_CONSTRUCTED
}ber_construct_tag;

typedef struct QuectelBerTlv_Tag
{
	Q_UINT32 tag_value;
	ber_construct_tag construct_tag;
	ber_class_tag class_tag;
}ber_tlv_tag;

/*status: 
0 = 成功定位
1 = 用户信息验证失败
2 = 请求格式不正确
3= 基站信息未发现*/
/* 返回的状态类型*/
typedef enum{
    POS_POS_NUM_0 =0,
    POS_POS_NUM_1,
    POS_POS_NUM_2,
    POS_POS_NUM_3,
    POS_POS_NUM_4,
    POS_POS_NUM_5,
    POS_POS_NUM_6,
}QTlvRetPosStat;


/*
*Begin - 2013.3.26 Kim add 增加cache 机制
*/
/*
*定位模式
*/
typedef enum{
    LOC_MODE_DATE   = 0,          /*形式日期和时间的形式*/
    LOC_MODE_CACHE = 1,         /*本地带有缓存的形式*/
}QLocMode_Tag;

/*
*对缓存的操作
*/
typedef enum{
    LOC_OPER_ADD = 0,   /*向缓存中添加数据*/
    LOC_OPER_DEL = 1,   /*删除缓存中某条记录*/
    LOC_OPER_SEARCH = 2, /*在缓存中查找记录*/
}QLocOperate_Tag;

/*End - 2013.3.26 Kim add 增加cache 机制*/
typedef enum QLocAddrCharset_tag
{
    Q_LOC_ADDR_UTF8 = 0,
    Q_LOC_ADDR_GBK,
    Q_LOC_ADDR_ASCII,
    Q_LOC_ADDR_NUM,
}QLocAddrCharset_e;

///////////////////////////////////////////////////////////////
/*TLV结构体*/
typedef struct QTLVNode_Tag {
    q_link_type  link_ptr;
	ber_tlv_tag tag;                     /*标记值*/
	Q_INT32 length;                   /* 数据长度*/
	Q_UINT8* value;                  /* 数据*/
	struct QTLVNode_Tag* next;                  /*兄弟*/
	struct QTLVNode_Tag* child;                 /*孩子*/

	struct QTLVNode_Tag*(*AddBrother)(struct QTLVNode_Tag* curNode, struct QTLVNode_Tag* newNode);
	struct QTLVNode_Tag*(*AddChild)(struct QTLVNode_Tag* curNode, struct QTLVNode_Tag* newNode);
}QTLVNode;

/* TLV打包解包类*/
typedef struct QuectelCTlvPacket_Tag
{
	/* 根节点，注意该节点是树根，不包括任何值。*/
	QTLVNode* root;
	/* 重新分配内存*/
	Q_UINT8* (*realloc_memory)(Q_UINT8* buf1,Q_UINT32 buf1Len,Q_UINT8* buf2,Q_UINT32 buf2Len);
	/* 解析TLV字符串*/
	Q_INT32 (*ParseTlvBuffer)(struct QuectelCTlvPacket_Tag **packet, const Q_UINT8* buffer, Q_INT32 bufSize);
	/*构建TLV简单字符串*/
	Q_INT32 (*SerializeTlvBuffer)(const ber_tlv_tag* tlvtag, Q_INT32 tlvlen,const Q_UINT8* tlvvalue, Q_INT32 valuesize,Q_UINT8** retbuf);
	/*删除内存*/
	void (*FreeBuffer)(struct QuectelCTlvPacket_Tag *packet);
	/* 按层次遍历，从startNode节点开始寻找Tlv Tag值为tag的节点*/
	QTLVNode* (*FindByLevelTraverse)(QTLVNode* startNode,Q_UINT32 tag);
	/* 前序遍历查找*/
	const QTLVNode* (*FindByPreOrder)(const QTLVNode* startNode,Q_UINT32 tag);
}QTlvPacket;

//////////////////////////////////////////////////////////////
/*请求的原始数据打包解包*/
#define AUTH_NAME_LEN 16
#define AUTH_PWD_LEN 16
#define AUTH_IMEI_LEN 15

#define AUTH_FIXINFO_LEN        17	/* 固定长度：IMEI(15)+RAND(2)=17*/
#define MAX_AUTH_INFO_SIZE  (AUTH_NAME_LEN+AUTH_PWD_LEN+AUTH_FIXINFO_LEN)
#define MAX_AUTH_REQ_SIZE    (AUTH_NAME_LEN+AUTH_FIXINFO_LEN)
#define MAX_SCELL_INFO_SIZE 19/*17 为三角定位增加bcch*/ /*单个cell占的字节数*/

#define AUTH_MD5_LEN   8 /*MD5值长度*/

typedef struct  QTlvAuthInfo_Tag
{
	Q_UINT8 name[AUTH_NAME_LEN+1];                 /*用户名*/
	Q_UINT8 pwd[AUTH_PWD_LEN+1];                    /*密码*/
	Q_UINT8 imei[AUTH_IMEI_LEN+1];                            /* IMEI*/
	Q_INT16 rand;                                                     /*  随机数*/
}QTlvAuthInfo;

    
/*Cell Info*/
typedef struct
{
    Q_UINT8  radio_type;//0:GSM,1:WCDMA,2:LTE
    Q_UINT16 mcc;
    Q_UINT16 mnc;
    Q_UINT32 lac;
    Q_UINT32 cellId;
    Q_INT16 signal;
    Q_UINT16 timead;
    Q_UINT16 bcch; /*2013.9.23 add for 三角定位*/
}tlv_cellinfo_struct;

/*retun loc*/
typedef struct
{
    Q_UINT8 serve_type;       /*服务类型*/
    Q_UINT8 encript_type;     /*加密类型*/
    Q_UINT8 key_index;         /*密钥索引*/
    Q_UINT8 pos_data_type;  /*数据包类型*/
    Q_UINT8 loc_method;        /*定位类型*/
}tlv_baseinfo_struct;

#define MAX_BASIC_INFO_SIZE 5

/*返回的位置信息结构体*/
typedef struct
{
    Q_BOOL isValid;         /*是否是有效的位置信息*/
    float longitude;            /*经度*/
    float latitude;               /*纬度*/
    Q_UINT16 accuracy;   /*精度*/
    Q_UINT8  uCellFlag;    /*0-正常基站，1 - 表示此基站为无效基站*/
}QCellLocPositonInfo;

//#define MAX_SPOS_INFO_SIZE	  (sizeof(Q_UINT8)+sizeof(float)*2+sizeof(Q_UINT16))/*返回的每个位置信息的大小*/

/*
*Begin - 2013.3.26 Kim add 增加cache 机制
*/
#define MAX_lOCATION_CACHE_SIZE              12 //jonathan,14/2/18,updata code

typedef struct CellLocCacheUnitTag{
    Q_BOOL                   bOccupied;  /*该位置是否已占用*/
    Q_INT8                    reqCounter;  /*请求该小区位置信息的次数*/
    tlv_cellinfo_struct     cellInfo;/*小区信息*/
    QCellLocPositonInfo location;/*位置信息*/
    QLocAddrCharset_e    addrCharset_ret;/*街道信息Charset   : 返回*/
    Q_UINT16                addrInfoLen;                                /*街道信息长度*/
    //Q_UINT8                 addrInfo[MAX_ADDRESS_SIZE];/*街道信息*/
    Q_UINT8*                 addrInfo;/*街道信息 。kim,2013.11.25 改成动态分配*/
}CellLocCacheUnit_t;

typedef struct CellLocCacheTag{
    //tlv_cellinfo_struct     preCellInfo;/*保存前一次请求时的主小区信息*/ //jonathan,14/2/18,del
    CellLocCacheUnit_t  cacheUnit[MAX_lOCATION_CACHE_SIZE];/*位置信息*/
}CellLocCache_t;
/*End - 2013.3.26 Kim add 增加cache 机制*/

typedef struct{
    QLocMode_Tag  locMode;/*定位模式*/
    Q_UINT32 type;
    Q_UINT32 version;
    QTlvAuthInfo authInfo;
    Q_UINT8 radioType;/*RadioType_e*/
    Q_UINT8 cellNum;/*发送请求时，发送的小区数*/
    Q_UINT8 locNum;/*返回的位置的个数*/
    tlv_cellinfo_struct cellInfo[BER_TLV_CELL_MAX];
    Q_UINT8 *encodedBuffer;/*发送请求的数据，已编码*/
    Q_UINT32 encodedBufLen;
    Q_UINT32 curIndex;
    Q_UINT8 *recvBuffer;/*收到的socket数据*/
    Q_UINT32 recvBufLen;
    QCellLocPositonInfo positionInfo_ret[BER_TLV_CELL_MAX];
    QTlvRetPosStat retStat;
    Q_UINT8 md5Value[AUTH_MD5_LEN]; /*保存请求或返回的MD5值*/
    /*Base Information*/
    tlv_baseinfo_struct baseinfo_req;/*请求时的baseInfo*/
    tlv_baseinfo_struct baseinfo_ret;/*返回的baseInfo*/
    //Q_BOOL bUseSetURL;                  /*是否使用AT+QHTTPURL设置的URL*/
    CellLocCache_t locCache;             /*缓存机制*/
	CellLocCache_t test_print_info;     //jonathan,14/4/1,打印场测数据
	Q_BOOL         b_test; //jonathan,14/4/1,打印场测数据的开关
	Q_UINT32       test_count;
    /*Begin - 2013.6.6 Kim 增加分步式服务器重定向功能*/
    QLocRedirectServer_t    redirectList[MAX_REDIRECT_LIST_SIZE];/*重定向列表*/
    Q_INT8                       srvListIndex;/*固化的服务器列表当前索引*/
    //soc_dns_a_struct       dnsSrvList[MAX_DNS_SERVER_IP_LIST];
    //Q_UINT8                     dnsListIndex;
    //Q_UINT8                     dnsSrvNum;
    Q_BOOL                      bCfgSrvFailed;
    SrvFromType_e           srvFromType;/*定位请求时，服务器地址从哪里来*/
    Q_UINT8                     userCfgSrv[MAX_HOST_ADDR_SIZE+1];
    //Q_UINT32                   srv_addr;
    Q_UINT16                   userCfgPort;
    Q_UINT16                   timeoutValue;
    apptcpip_ip_addr_struct    srv_ip_addr;
    Q_UINT8                    srv_addr_format;
    Q_BOOL                       bLastReqError;
    QLocErrReport_t          errorReport[MAX_ERR_REP_LIST_SIZE];
    QLocAddrCharset_e    addrCharset_req;/*街道信息Charset  : 请求*/
    QLocAddrCharset_e    addrCharset_ret;/*街道信息Charset   : 返回*/
    Q_UINT8                     addrInfo[MAX_ADDRESS_SIZE];   /*街道信息*/
    Q_UINT16                   addrInfoLen;                                /*街道信息长度*/
    Q_UINT8                     iNeedAddr; 
    Q_INT16                     loc_socket;//jonathan add @2013.10.25
    Q_UINT8                     context_id;
    //2013.11.11,CLIENT TRIANGER
    Q_UINT8                     triangulationMode;
    BSPoint                       BSPointArray[BER_TLV_CELL_MAX];
    Q_UINT8                     BSPointArrayNum;

    //JONATHAN,14/2/18,UPDATA CODE
    time_julian_type        lastTime;/*2013.12.3 记录最后一次定位的时间*/
    time_julian_type        lastTriangleTime;/*2013.12.3 记录模块端三角定位最后一次访问Server 的时间*/
    time_julian_type        curTime;/*本次定位的时间*/
    Q_UINT16                   triangleFrequency;/*单位秒,三角定位访问服务器的最短时间间隔,取服务器返回值与模块本地值的较大者*/
    Q_UINT16                   triangleFreLocal;/*模块本地默认的时间还AT设置的时间*/
    Q_UINT16                   triangleFreSrv;/*Server 返回的时间*/
    Q_INT8                       iFirstRetPos;  /*记录返回的第一个POS, 用于校验Server的MD5*/
    Q_BOOL                      bEnableRedirect;/*是否使用重定向功能*/
    //JONATHAN,14/2/18
    /*AT+QLOCCFG="server",...*/
    /*End - 2013.6.6 Kim 增加分步式服务器重定向功能*/
    /* 设置基本信息*/
    void (*set_basicInfo)(tlv_baseinfo_struct* pBaseInfo, Q_UINT8 servType,Q_UINT8 encriptType,Q_UINT8 keyIndex,Q_UINT8 posDataType,Q_UINT8 locMethod);
}TlvCellLoc_t;


extern Q_UINT32 QTlv_Serialize_BaseInfo_Buf(tlv_baseinfo_struct* pBaseInfo, Q_UINT8 *retBuffer);
extern Q_UINT32 QTlv_Serialize_AuthInfo_Buf(Q_BOOL bWithPwd, Q_UINT8 *retBuffer);
extern Q_UINT32 QTlv_Serialize_CellInfo_Buf(tlv_cellinfo_struct *cellInfo, Q_UINT8 *retBuffer);
extern Q_UINT8 QTlv_make_md5_value(tlv_baseinfo_struct *pBaseInfo, Q_UINT8 *retMD5Value);
extern TlvCellLoc_t* ptrToBerTlvContext(void);
extern void QTlv_init_loc_req_context(void);
extern Q_UINT8* tlv_realloc_memory(Q_UINT8* buf1,Q_UINT32 buf1Len,Q_UINT8* buf2,Q_UINT32 buf2Len);
extern Q_INT32 QTlv_LocReq_Encode(Q_UINT8 **retBuffer);
extern Q_INT32 QTlv_LocReq_Decode_Recv_Body(void);
/*Begin - 2013.3.26 Kim add 增加cache 机制*/
extern void QTlv_clear_loc_data(void);
extern CellLocCache_t* ptrToLocCache(void);
extern Q_BOOL qcellloc_update_cache(QLocOperate_Tag operate, Q_INT8 *unitIndex,Q_UINT8  uCellIndex);
extern Q_BOOL qcellloc_check_need_send_req(void);
extern void qcellloc_cache_log(void);
/*End - 2013.3.26 Kim add 增加cache 机制*/
/*Begin - 2013.6.6 Kim 增加分步式服务器重定向功能*/
extern void qcellloc_get_current_server_address(Q_UINT8* hostIp, Q_UINT16 ipMaxLen, Q_UINT8* hostName, Q_UINT16 nameMaxLen, Q_UINT16 *port);
extern void qcellloc_error_handler(Q_UINT8* hostIP, Q_UINT16 errCode);
/*End - 2013.6.6 Kim 增加分步式服务器重定向功能*/
extern TlvCellLoc_t* ptrToBerTlvContext(void);
extern  void convert_hex_value_2_str(uint8 *val, uint8 *str, uint8 length);
//2013.11.11, CLIENT TRIANGER
extern Q_BOOL qcellloc_triangle_check_in_cache(void);
extern Q_BOOL qcellloc_triangle_Locate(void);

//jonathan 14/2/18,update code
extern Q_BOOL qcellloc_triangle_check_in_cache(void);
extern void qcellloc_control_invalid_cells_in_cache(void);
#endif
#endif
