#ifndef __QUECTEL_TRIANGLE_LOCATOR_H__
#define __QUECTEL_TRIANGLE_LOCATOR_H__

#include "quectel_apptcpip_util.h"

#ifdef FEATURE_QUECTEL_LOCATOR_APP


/**************************************************************************************************
文件名称: TriangleLocation
功能: 实现模块端三角定位算法
***************************************************************************************************/
#define M_PI					3.14159265358979323846
#define M_TA				63
#define M_MAX_BS		6
#define M_REF_RATE	0.666

#define EARTH_RADIUS    6378.137   /*地球半径*/

#define LAT_INIT_VALUE	-1
#define LNG_INIT_VALUE	-1

#define BAND0_124(BCCH) (935+(BCCH)*0.2)
#define BAND128_251(BCCH) (869.2+0.2*(BCCH-128))
#define BAND259_293(BCCH) (460.6+0.2*(BCCH-259))
#define BAND306_340(BCCH) (489+0.2*(BCCH-306))
#define BAND438_511(BCCH) (777.2+0.2*(BCCH-438))
#define BAND512_885(BCCH) (1805.2+0.2*(BCCH-512))
#define BAND955_1023(BCCH) (935+0.2*(BCCH-1024))


//经纬度坐标结构体
typedef struct BSPoint_Tag{
    float x;			// 经度
    float y;			// 纬度
    float fre;		// 频率
    float dbm;		// 信号强度
    Q_UINT8 bFillWithData;
}BSPoint;

typedef struct TriangleLoc_Tag
{
	// 定义最大6个基站
	BSPoint* BSPointArray[M_MAX_BS];
	int totalBSNum;
	BSPoint* legalBSPointArray[M_MAX_BS];
	int legalBSNum;
	BSPoint* invalidBSPointArray[M_MAX_BS];
	// 有效基站索引
	int refBSIndex;
#if 0
	// 设置模块端传进的指针
	// 此处使用指针传递，主要是为了考虑效率。但是，因为使用指针，需要自行维护指针的生命周期。
	void SetBSArray(BSPoint* BSArray[],int num);
	BSPoint** GetBSArray();
	int GetBSNum(){return totalBSNum_;}

	BSPoint** GetLegalBSArray();
	int GetLegalBSNum(){return legalBSNum_;}
	
	BSPoint** GetInvalidBSArray();
	int GetInvalidBSNum(){return totalBSNum_-legalBSNum_;}

	// 先排序，再调用Locate函数进行三角定位
	bool TriangleLocate(BSPoint *ret);

	// 根据相邻基站相隔距离不可能超过某个临界值计算
	bool IsExceptionBSPoint(BSPoint* refPoint,BSPoint* checkPoint);
	// 判断是否有异常，如果有任何异常，则直接退出
	// 暂时没有考虑概率法
	bool IsExistExceptionPoint(BSPoint* inPoints[], int num);
	//三角定位函数
	bool locate(BSPoint *ret);

	// 选择基准基站信息，该函数暂时不使用
	bool SelectRefBSPoint();
	// 选取基准基站和合法基站
	void SelectLegalBSPoint();

	//计算距离比函数
	float distanceRatio(float dbm1, float dbm2, float fre1, float fre2);
	//两点平均值函数
	void average(BSPoint *p1, BSPoint *p2, BSPoint *ret) {
		ret->x = (p1->x + p2->x) / 2;
		ret->y = (p1->y + p2->y) / 2;
	}
	//计算两点距离函数
	float distance(BSPoint *p1, BSPoint *p2);

	//排序算法，按照距离的远近，从近到远排序
	// 该算法在找出合法点之后，再进行一次排序，对最优点进行排序，以满足locate算法的需求。
	//对于一个Point数组list排序
	void SortBSPoints();
    #endif
}TriangleLoc_t;

extern void TriangleLoc_InitTriangleLoc(void);
extern Q_BOOL TriangleLoc_GetFreFromBcch(int bcch, float* fre);
extern void TriangleLoc_SetBSArray(BSPoint* BSArray, int num);
extern BSPoint** TriangleLoc_GetBSArray(void);
extern Q_BOOL TriangleLoc_TriangleLocate(BSPoint *ret);
Q_BOOL TriangleLoc_TriangleLocate_3g(BSPoint *ret);

#endif
#endif