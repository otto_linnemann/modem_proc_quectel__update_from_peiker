/******************************************************************************
*        
*     Copyright (c) 2013 Quectel Ltd.       
*        
*******************************************************************************
*  file name:          Quectel_queue.h
*  author:              Kim
*  date:                2013.1.11
*  file description:   
******************************************************************************/
#ifndef __QUECTEL_QUEUE_H__
#define __QUECTEL_QUEUE_H__

#include "quectel_apptcpip_util.h"
#ifdef FEATURE_QUECTEL_LOCATOR_APP

typedef struct mynode{
       struct mynode* next;
       void *elem;
}QNode;

typedef struct{
   QNode* item;
   Q_UINT32 elem_size;   /*元素大小*/
   Q_UINT32 length;  /*队列长度*/
   void (*freefn)(void *); /*自定义内存释放函数，释放外部动态申请的内存*/
}Q_Queue;

/*初始化队列*/
extern void quectel_initQueue(Q_Queue *s,Q_UINT32 size, void (*freefn)(void*));

/*判断队列为空*/
extern int quectel_emptyQueue(Q_Queue *q);

/*求队列长度*/
extern int quectel_sizeQueue(Q_Queue *q);

/*获得队列头部元素*/
extern void* quectel_frontQueue(Q_Queue *q);

/*获得队列尾部元素*/
extern void* quectel_backQueue(Q_Queue *q);

/*弹出（删除）队列头部元素*/
extern void quectel_popQueue(Q_Queue *q);

/*元素入队列尾部*/
extern void quectel_pushQueue(Q_Queue *q,void *value);

/*删除队列*/
extern void quectel_destoryQueue(Q_Queue *q);
#endif

#endif
