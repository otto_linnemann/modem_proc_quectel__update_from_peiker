#ifndef __QUEC_MD5_H__
#define __QUEC_MD5_H__

#include "comdef.h"
#include "customer.h"
#include "err.h"
#include "sys.h"
#include "cm.h"
#include "QuecOEM_feature.h"

#ifdef FEATURE_QUECTEL_LOCATOR_APP

/* MD5.H - header file for MD5C.C
 */

/* Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
rights reserved.

License to copy and use this software is granted provided that it
is identified as the "RSA Data Security, Inc. MD5 Message-Digest
Algorithm" in all material mentioning or referencing this software
or this function.

License is also granted to make and use derivative works provided
that such works are identified as "derived from the RSA Data
Security, Inc. MD5 Message-Digest Algorithm" in all material
mentioning or referencing the derived work.

RSA Data Security, Inc. makes no representations concerning either
the merchantability of this software or the suitability of this
software for any particular purpose. It is provided "as is"
without express or implied warranty of any kind.


These notices must be retained in any copies of any part of this
documentation and/or software.
 */

/* MD5 context. */
typedef struct {
  unsigned long state[4];                                   /* state (ABCD) */
  unsigned long count[2];        /* number of bits, modulo 2^64 (lsb first) */
  unsigned char buffer[64];                         /* input buffer */
} MD_CTX;

void MDInit (MD_CTX *);
void MDUpdate 
  (MD_CTX *, unsigned char *, unsigned int);
void MDFinal (unsigned char [16], MD_CTX *);

void MDString (char *, unsigned int, unsigned char [16]);

#endif

#endif
