#ifndef QUECTEL_HTTP_CLIENT_H
#define  QUECTEL_HTTP_CLIENT_H

#include "comdef.h"
#include "quectel_apptcpip_util.h"
#include "quectel_http_util.h"
#include "quectel_http_client_def.h"

#if defined(QUECTEL_HTTP_APP_SUPPORT)

typedef struct
{
	ds3g_siolib_port_e_type              serial_port;
	Q_INT32                                    url_len;
	Q_INT32                                    url_read_timeout;
}http_client_set_access_url_param;


typedef struct
{
  ds3g_siolib_port_e_type              serial_port;
  Q_BOOL                                     default_header;
  Q_INT32                                    msg_len;
  Q_INT32                                    msg_read_timeout;
  Q_INT32                                    cmd_rsp_timeout;
}http_client_get_cmd_param;


typedef struct
{
   ds3g_siolib_port_e_type              serial_port;
   Q_BOOL                                     default_header;
	Q_INT32                                    post_type;
	Q_INT32                                    msg_len;
    Q_INT32                                    msg_read_timeout;
    Q_INT32                                    cmd_rsp_timeout;
}http_client_post_cmd_param;

typedef struct
{
   ds3g_siolib_port_e_type              serial_port;
   Q_BOOL                                     output_header;
	Q_INT32                                    read_type;
	Q_INT32                                    soc_read_interval_timeout;
}http_client_read_cmd_param;

typedef struct
{
	ds3g_siolib_port_e_type              serial_port;
	Q_INT32                                     reason;
}http_client_cancel_cmd_param;


void quectel_http_client_init
(
void
);

Q_INT32 quectel_http_client_get_common_cfg
(
http_client_cfg_type   cfg_type,
http_client_common_cfg_param  *cfg_param
);

Q_INT32 quectel_http_client_set_common_cfg
(
http_client_cfg_type   cfg_type,
http_client_common_cfg_param  *cfg_param
);

Q_INT32  quectel_http_client_get_access_url
(
dsm_item_type **url
);

// add by herry.Geng on 20190408 for supporting lwm2m
Q_INT32 quectel_http_client_set_access_url_ex
(
char *url
);


Q_INT32 quectel_http_client_set_access_url
(
http_client_set_access_url_param  *cmd_param
);

Q_INT32  quectel_http_client_get_cmd
(
http_client_get_cmd_param  *cmd_param
);

Q_INT32  quectel_http_client_post_cmd
(
http_client_post_cmd_param *cmd_param
);

Q_INT32  quectel_http_client_read_cmd
(
http_client_read_cmd_param *cmd_param
);

Q_INT32  quectel_http_client_cancel_cmd
(
http_client_cancel_cmd_param *cmd_param
);

Q_INT32  quectel_http_client_receive_sio_data
(
dsm_item_type  *data_ptr
);

void quectel_http_client_read_rsp_data
(
dsm_item_type **data_ptr
);

extern void quectel_change_http_cmd_to_none(void);

#endif

#endif/*QUECTEL_HTTP_CLIENT_H*/
