#ifndef QUECTEL_HTTP_CLIENT_CB_H
#define  QUECTEL_HTTP_CLIENT_CB_H

#include "ds3gsiolib.h"
#include "quectel_apptcpip_util.h"

#if defined(QUECTEL_HTTP_APP_SUPPORT)
typedef  struct
{
    ds3g_siolib_port_e_type   serial_port;
	 Q_INT8                           http_cmd;
}quectel_http_client_enter_data_mode_ind_param;

typedef  struct
{
    ds3g_siolib_port_e_type   serial_port;
	 Q_INT8                           http_cmd;
}quectel_http_client_exit_data_mode_ind_param;

typedef struct
{
	 ds3g_siolib_port_e_type   serial_port;
	 Q_INT32                         result;
}quectel_http_client_url_cfg_result_param;

typedef struct
{
	ds3g_siolib_port_e_type   serial_port;
	Q_BOOL  ds_err_result;
	Q_INT32  result;
	Q_INT32  http_rsp_code;
	Q_BOOL  content_len_exist;
	Q_INT32  content_length;
}quectel_http_client_get_cmd_result_param;

typedef struct
{
	ds3g_siolib_port_e_type   serial_port;
	Q_BOOL  ds_err_result;
	Q_INT32  post_type;
	Q_INT32  result;
	Q_INT32  http_rsp_code;
	Q_BOOL   content_len_exist;
	Q_INT32  content_length;
}quectel_http_client_post_cmd_result_param;

typedef struct
{
	ds3g_siolib_port_e_type   serial_port;
	Q_BOOL  ds_err_result;
	Q_INT32  result;
}quectel_http_client_read_cmd_result_param;

typedef struct
{
	ds3g_siolib_port_e_type   serial_port;
	Q_INT32  result;
}quectel_http_client_response_data_income_ind_param;

#ifdef FEATURE_QUECTEL_LOCATOR_APP
typedef struct
{
    Q_INT16  result;
    Q_UINT8  uart_port;
    Q_UINT32   http_rsp_code;
    dsm_item_type *locator_rsp;
}quectel_http_locaotor_event_info_struct;
void  quectel_http_locator_rsp_ind_cb(quectel_http_locaotor_event_info_struct *ind_param);
 #endif
 
void quectel_http_client_enter_data_mode_ind(quectel_http_client_enter_data_mode_ind_param *ind_param);

void  quectel_http_client_exit_data_mode_ind(quectel_http_client_exit_data_mode_ind_param *ind_param);

void  quectel_http_client_url_cfg_result_rsp(quectel_http_client_url_cfg_result_param *rsp_param);

void  quectel_http_client_get_cmd_result_rsp(quectel_http_client_get_cmd_result_param *rsp_param);

void  quectel_http_client_post_cmd_result_rsp(quectel_http_client_post_cmd_result_param *rsp_param);

void  quectel_http_client_read_cmd_result_rsp(quectel_http_client_read_cmd_result_param *rsp_param);

void  quectel_http_client_response_data_income_ind(quectel_http_client_response_data_income_ind_param *ind_param);

void  quectel_http_client_sio_receive_ind();
typedef struct
{
   void( *quectel_http_client_enter_data_mode_ind)(quectel_http_client_enter_data_mode_ind_param *ind_param);
	void( *quectel_http_client_exit_data_mode_ind)(quectel_http_client_exit_data_mode_ind_param *ind_param);
   void(*quectel_http_client_url_cfg_result_rsp)(quectel_http_client_url_cfg_result_param *rsp_param);
	void(*quectel_http_client_get_cmd_result_rsp)(quectel_http_client_get_cmd_result_param *rsp_param);
	void(*quectel_http_client_post_cmd_result_rsp)(quectel_http_client_post_cmd_result_param *rsp_param);
	void(*quectel_http_client_read_cmd_result_rsp)(quectel_http_client_read_cmd_result_param *rsp_param);
	void(*quectel_http_client_response_data_income_ind)(quectel_http_client_response_data_income_ind_param *ind_param);
	void(*quectel_http_client_sio_receive_ind)();
}quectel_http_client_cb_tbl;

#endif/*QUECTEL_HTTP_APP_SUPPORT*/

#endif/*QUECTEL_HTTP_CLIENT_CB_H*/
