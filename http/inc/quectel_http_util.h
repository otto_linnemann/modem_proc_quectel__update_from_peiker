#ifndef QUECTEL_HTTP_UTIL_H
#define  QUECTEL_HTTP_UTIL_H

#include "quectel_apptcpip_util.h"
#include "dsm.h"


#if defined(QUECTEL_HTTP_APP_SUPPORT)

#define HTTP_DEFAULT_PORT                   80
#define HTTPS_DEFAULT_PORT                 443
#define HTTP_TAG_VALUE_LENGTH                       700
 
#define HTTP_URL_LENGTH                        700
#define HTTP_DOMAIN_NAME_LENGTH      150

#define  HTTP_BOUNDARY  "7dquecteldfffffffkfsjgfjkfdkjl7d33a816d302b6"
#define  HTTP_CONTENT_DISPOSITION  "Content-Disposition: form-data; name=\"file\"; filename=\"1.txt\""


typedef  enum
{
	HTTP_HEADERFIELD_END = 0,
	HTTP_HEADERFIELD_RESPONSEHEAD,
	HTTP_HEADERFIELD_GETMOTHED,
	HTTP_HEADERFIELD_POSTMOTHED,
	HTTP_HEADERFIELD_URL,
	HTTP_HEADERFIELD_PROTOL,
	HTTP_HEADERFIELD_HOST,
	HTTP_HEADERFIELD_ACCEPT,
	HTTP_HEADERFIELD_ACCEPT_CHARSET,
	HTTP_HEADERFIELD_CACHE_CONTROL,
	HTTP_HEADERFIELD_PRAGMA,
	HTTP_HEADERFIELD_CONNECTION,
	HTTP_HEADERFIELD_USERAGENT,
	HTTP_HEADERFIELD_ACCEPT_LANGUAGE,
	HTTP_HEADERFIELD_X_WAP_PROFILE,
	HTTP_HEADERFIELD_LINESYMBOL,
	HTTP_HEADERFIELD_CONTENT_TYPE,
	HTTP_HEADERFIELD_CONTENT_LENGTH,
	HTTP_HEADERFIELD_TRANSFER_ENCODING_CHUNKED,
	HTTP_HEADERFIELD_LOCATION,
	HTTP_HEADERFIELD_DATE,
	HTTP_HEADERFIELD_BOUNDARY,
	HTTP_HEADERFIELD_CONTENT_DISPOSITION,
	HTTP_HEADERFIELD_MDN,
	END_OF_HTTP_HEADERFILELD
}http_header_tag_type;


typedef enum
{
	HTTP_CODERESULT_OK                 = 0,
	HTTP_CODERESULT_END               = 1,
	HTTP_CODERESULT_BUFFERFULL  = 2,
	HTTP_CODERESULT_DATAABSENT = 3,
	HTTP_CODERESULT_ERROR            = -1,
	HTTP_CODERESULT_WODBLOCK     = -2,
	HTTP_CODERESULT_TIMEOUT       = -3,
}http_code_result;



typedef struct 
{
    Q_INT32     httpresponse;/*HTTP回应值*/
    Q_UINT8    *urladdress;/*指向url address, 不分配空间*/
    Q_UINT16   urladdress_length;/*不包含\0的长度*/
    Q_UINT8     hostnameorip[50];                   /*其它的都不用内存分配，指向一个存在的内存*/
    Q_UINT8    *accept;
    Q_UINT8   *content_type;
    Q_UINT16   hostport;
    Q_UINT32   content_length; /*上传时作为mms head + body 大小，由计算分析得来*/
                                                    /*包含mms head 和mms body的长度, 发送前准备好*/

    Q_BOOL     contentLength_exist_InHttpResponse;/*在HttpResponse 中是否存在ContentLength, 有的情况下么有这个字段和TRANSFER_ENCODING_CHUNKED 编码*/
    Q_BOOL     transfer_encoding_Is_chunked;  /*当 Transfer-Encoding: chunked  时无ContentLength长度*/
    
    Q_UINT8   *connection;    //  Keep-Alive
    Q_UINT8   *user_agent;   // QUECTEL_MODULE
    Q_UINT8   *accept_charset;//"utf-8, us-ascii"
    Q_UINT8   *cache_control;//"no-cache"
    Q_UINT8   *x_wap_profile;
    Q_UINT8   *x_vzw_mdn;
    Q_UINT8   *pragma;//"no-cache"
    Q_UINT8   *accept_language; //"en"
    Q_BOOL     is_redirect;
    Q_UINT8    redirect_url_address[700]; //2013.08.05 chris
    Q_UINT8    date[50]; //格式 2010/05/18,20:15:30 //jay add 20100519
    Q_BOOL     dateorg; /*Date[50]获取HTTP中的原始数据，不进行解析*/
}http_header_struct;

typedef struct 
{
    Q_INT32     *encode_arrange;/*编码的数组*/
    Q_UINT16    encodestep;
    Q_UINT32    param1;
    Q_UINT32    param2;
    Q_BOOL      encodeend;
}http_encode_info;

typedef struct 
{
    Q_UINT32    param1;
    Q_UINT32    param2;
}http_decode_info;



typedef struct 
{
    http_header_tag_type       filedtype;
    Q_UINT8                          valuetype;
    Q_UINT32                        valuelength;
    Q_UINT8                          value[HTTP_TAG_VALUE_LENGTH];
}http_header_line_info;

 typedef enum 
 {
	STATUS_CHUECK_BLOCK_LENGTH_1,
	STATUS_CHUECK_BLOCK_LENGTH_2,
	STATUS_CHUECK_BLOCK_LF,
	STATUS_CHUECK_BLOCK_DATA,
	STATUS_CHUECK_BLOCK_DATA_CR,
	STATUS_CHUECK_BLOCK_DATA_LF,
	STATUS_CHUECK_BLOCK_FOOTER,
	STATUS_CHUECK_BLOCK_FOOTER_LF,
}http_chunk_status_e_type;

typedef struct 
{
	http_chunk_status_e_type   status;
	Q_UINT32                          total_length;
	Q_UINT32                          block_length;
	Q_UINT32                          block_count;
}http_chunk_decode_info;


typedef struct
{
    Q_UINT8                         server_addr_buf[HTTP_DOMAIN_NAME_LENGTH+1];
    apptcpip_ip_addr_struct   server_addr;
    Q_UINT16                       server_port;
    Q_UINT8                         ip_type; //0: IPv4 ; 1: IPV6
    Q_UINT8                         addr_format;//0:ip addr 1:host name
    Q_UINT8                         security_type;    //0: htttp  1:https
    Q_BOOL                         default_port;
}http_server_info;


Q_INT16  http_encode_header
(
http_header_struct   *httpheader, 
http_encode_info     *encoder, 
Q_UINT8                *encode_buffer,
Q_INT32                  encode_buffer_length, 
Q_INT32                *encode_length
);


Q_INT16  http_decode_header
(
http_header_struct   *httpheader, 
http_decode_info     *decoder, 
Q_UINT8                *decode_buffer, 
Q_INT32                  decode_buffer_length, 
Q_INT32                *decode_length
);

Q_BOOL   http_decode_url
(
Q_UINT8              *url, 
Q_UINT32            *url_length, 
http_server_info   *server_info
);



void http_init_chucked_decode
(
http_chunk_decode_info  *decoder
);

Q_INT32   http_procee_chucked_decode
(
http_chunk_decode_info *decoder, 
Q_UINT8                         ch
);

Q_BOOL http_header_line_is_complete
(
Q_UINT8 *buffer, 
Q_INT32  length
);
#ifdef FEATURE_QUECTEL_LOCATOR_APP
extern Q_INT8 http_post_cellloc_head_req_init(void);
Q_INT16 http_recv_rsp_head();
Q_INT16  http_recv_rsp_body(Q_UINT32 *recv_len);
#endif
#endif/*QUECTEL_HTTP_APP_SUPPORT*/

#endif/*QUECTEL_HTTP_UTIL_H*/
