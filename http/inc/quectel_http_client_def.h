#ifndef QUECTEL_HTTP_CLIENT_DEF_H
#define  QUECTEL_HTTP_CLIENT_DEF_H

#include "comdef.h"
#include "quectel_apptcpip_util.h"
#include "quectel_apptcpip_timer.h"
#include "quectel_http_util.h"
#include "ds3gsiolib.h"

#if defined(QUECTEL_HTTP_APP_SUPPORT)

#define QUECTEL_HTTP_POST_FROM_COM    1
#define QUECTEL_HTTP_POST_FROM_FILE    2
#define QUECTEL_HTTP_POST_CELL_LOC      3

#define QUECTEL_HTTP_READ_TO_COM         1
#define QUECTEL_HTTP_READ_TO_FILE         2

typedef enum
{
	QUECTEL_HTTP_CLIENT_PDP_CID = 0,
	QUECTEL_HTTP_CLIENT_CUST_REQ_HDR,
	QUECTEL_HTTP_CLIENT_STORE_RSP_HDR,
	QUECTEL_HTTP_CLIENT_SSL_CTX_ID,
	QUECTEL_HTTP_CLIENT_CONTENT_TYPE,
	QUECTEL_HTTP_CLIENT_INVALID_CFG_TYPE
}http_client_cfg_type;

typedef enum
{
    HTTP_NONE_CMD = 0,
    HTTP_CFG_CMD,
    HTTP_URL_CMD,
    HTTP_HEADE_CMD,
    HTTP_GET_CMD,
    HTTP_POST_CMD,
    HTTP_READ_CMD,
   #ifdef FEATURE_QUECTEL_LOCATOR_APP
   HTTP_CELL_LOC_CMD,
   #endif

// add by herry.Geng on 20190408 for supporting lwm2m
#if defined(QUECTEL_LWM2M_SUPPORT)
    HTTP_FOTA_PKG_DLOAD_CMD,
#endif

    HTTP_MAX_CMD
}http_cmd_e_type;

typedef enum 
{
  HTTP_METHOD_IDLE,   
  HTTP_METHOD_GET,   
  HTTP_METHOD_POST,
  END_OF_HTTP_METHOD
}http_method_e_type;

typedef enum
{
	HTTP_CONTENT_TYPE0, // application/x-www-form-urlencoded
	HTTP_CONTENT_TYPE1, //text/plain
	HTTP_CONTENT_TYPE2, //application/octet-stream
	HTTP_CONTENT_TYPE3 //multipart/form-data
}http_content_type_e_type;

typedef enum 
{
  HTTP_STATE_IDLE = 0,          /*idle state. Allow any operation*/
  HTTP_STATE_URL_CONFIG, // 1
  HTTP_STATE_PREPARE_CONECT_TO_SERVER,// 2
  HTTP_STATE_SERVER_NAME_DNS_PARSE, // 3
  HTTP_STATE_SERVER_CONNECTING, // 4
  HTTP_STATE_SERVER_CONNECTED, // 5
  HTTP_STATE_REQ_HEADER_SENDING, // 6
  HTTP_STATE_REQ_BODY_SENDING, // 7
  HTTP_STATE_RSP_HEADER_READING, // 8
  HTTP_STATE_RSP_BODY_READING, // 9
  HTTP_STATE_RSP_READ_COMPLETE, //10
  END_OF_HTTP_STATE
}http_state_e_type;


typedef enum 
{
   HTTP_PROTOCOLPROCESS_IDLE,   

   HTTP_PROTOCOLPROCESS_WRITE_POSTHEADER, 
   HTTP_PROTOCOLPROCESS_WRITE_POSTBODY,   

   HTTP_PROTOCOLPROCESS_WRITE_GETHEADER, 

   HTTP_PROTOCOLPROCESS_READ_HEADER, 
   HTTP_PROTOCOLPROCESS_READ_BODY,   


  END_OF_HTTP_PROTOCOLPROCESS
}http_protocol_process_state_e_type;

typedef enum
{
   HTTP_UART_NORMAL_MODE = 0,
   HTTP_UART_URL_READ_MODE,
   HTTP_UART_POST_BODY_READ_MODE,
   HTTP_UART_RSP_INFO_WRITE_MODE,
   HTTP_UART_REQ_INFO_READ_MODE
}http_uart_port_state_e_type;

typedef enum
{
     HTTP_READ_TO_COM = 0,
	  HTTP_READ_TO_FILE
}http_read_mode_e_type;

typedef enum
{
    HTTP_UART_ESCAPE_EVENT = 0,
	 HTTP_READ_TO_FILE_FAILED
}http_abort_reason_e_type;

typedef enum
{
     HTTP_SOC_IDLE = 0,
	  HTTP_SOC_PDP_ACTIVING,
	  HTTP_SOC_INIT,
	  HTTP_SOC_CONNECTING,
	  HTTP_SOC_SSL_HANDSHARK,
	  HTTP_SOC_CONNECTED,
	  HTTP_SOC_CLOSING,
	  HTTP_SOC_CLOSED = HTTP_SOC_IDLE
}http_socket_state_e_type;


typedef union
{
	Q_INT32   pdp_cid;
	Q_BOOL    cust_req_hdr;
	Q_BOOL    store_rsp_hdr;
	Q_INT32   ssl_ctx_id;
	Q_INT32   content_type;

	// add by herry.Geng on 20190408 for supporting lwm2m
	Q_BOOL    resp_auto_out;
	Q_BOOL    closed_ind;
}http_client_common_cfg_param;


typedef struct
{
	ds3g_siolib_port_e_type         serial_port;
	Q_UINT32                             sio_read_len;
	Q_UINT32                             sio_current_read_len;
	Q_UINT32                             sio_read_timeout;
	apptcpip_timer_id                  sio_read_timer_id;
}http_client_sio_read_param;

typedef struct
{
	Q_BOOL                               msg_from_uart;
   Q_UINT32                             cmd_rsp_timeout;
	apptcpip_timer_id                  cmd_rsp_timer_id;	
}http_client_request_cmd_param;

#ifdef FEATURE_QUECTEL_LOCATOR_APP
typedef  struct
{
    Q_UINT8            cellNum;
    Q_UINT8            locMethod;
    Q_UINT8            dateType;
    Q_UINT8            uart_port;
}quectel_apptcpip_qcellloc_cmd_info;
#endif

typedef struct
{
	ds3g_siolib_port_e_type         serial_port;
	Q_UINT32                             rsp_read_interval_timeout;
	apptcpip_timer_id                  rsp_read_interval_timer_id;
	Q_UINT32                             rsp_body_len;
   Q_BOOL                                read_complete;
}http_client_response_read_param;

typedef struct
{
   ds3g_siolib_port_e_type         serial_port;
   http_uart_port_state_e_type   serial_port_state;
   Q_INT32                               pdp_cid;
#if defined(QUECTEL_CYASSL_SUPPORT)
   Q_INT32                               ssl_ctx_id;
   Q_INT32                               ssl_session_handle;
#endif
   Q_INT32                               sock_fd;
   Q_INT32                               sock_state;
	Q_BOOL                               sock_write_enable;
   Q_UINT8			                      url[HTTP_URL_LENGTH+1];
   Q_UINT32			                  url_length;
   http_server_info                    server_info;
   http_cmd_e_type                   http_cmd;
    
   Q_BOOL                                cust_req_header;
   Q_BOOL                                output_rsp_header;
   http_header_struct                 http_req_header;
   http_header_struct                 http_rsp_header;
   http_encode_info                    http_encoder;
   http_decode_info                    http_decoder;
   http_chunk_decode_info          http_chunk_decoder;
	Q_UINT32                              http_req_info_len;
    
   http_state_e_type                   http_state;
   http_method_e_type               http_method;
	Q_UINT32                              content_type;

   // add by herry.Geng on 20190408 for supporting lwm2m
   Q_BOOL                                 auto_out_resp;
   Q_BOOL                                 closed_ind;

	Q_BOOL                                         req_msg_high_level;
 	dsm_item_type                               *req_msg_item_ptr;
   dsm_watermark_type                       req_msg_wm_ptr;
	q_type                                            req_msg_q;
	Q_BOOL                                         rsp_msg_high_level;
	dsm_watermark_type                      rsp_header_wm_ptr;
	q_type                                            rsp_msg_hdr_q;
   dsm_watermark_type                       rsp_body_wm_ptr;
	q_type                                            rsp_msg_body_q;
    
	http_client_response_read_param  rsp_read_ptr;
	http_client_sio_read_param           sio_read_ptr;
	http_client_request_cmd_param     req_ptr;
    
     #ifdef FEATURE_QUECTEL_LOCATOR_APP
    apptcpip_timer_id       locator_timer;
    quectel_apptcpip_qcellloc_cmd_info cellloc_cmd;
    http_protocol_process_state_e_type               http_write_state;
    http_protocol_process_state_e_type               http_read_state;
    Q_UINT32           http_head_len;
    Q_UINT32           http_body_len;
    http_header_struct        http_header;
    Q_BOOL              read_complete;
    Q_UINT32                         content_length;
     #endif
     
}http_client_context;
#endif/*QUECTEL_HTTP_APP_SUPPORT*/

#endif/*QUECTEL_HTTP_CLIENT_DEF_H*/
