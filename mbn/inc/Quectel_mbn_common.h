
/*!  
  @file
  Quectel_mbn_common.h

  @brief
  This file defines declarations for MBN COMMON use

  @see
  Create this file by QCM9X00205C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
09/12/2018   vicent        Init
=================================================================================*/
 
#ifndef __QUECTEL_MBN_COMMON_H__
#define __QUECTEL_MBN_COMMON_H__

#include "quectel_mbn_base.h"

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    MBN_OPERATOR_CTCC = 0,
    MBN_OPERATOR_CMCC = 1,
    //<2018/12/12-QCM9X00205C001-P02-Vicent.Gao, <[MBN] Segment 2==> Base code submit.>
    MBN_OPERATOR_CUCC = 2,
    //>2018/12/12-QCM9X00205C001-P02-Vicent.Gao

    //Waring! ==> Please add new MBN OPERATOR upper this line
    MBN_OPERATOR_MAX
} MBN_OperatorEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
extern boolean QL_MBN_IsDirectPlmnMatch(const mcfg_trl_carrier_mcc_mnc_s_type *pUimPlmn, const mcfg_trl_carrier_mcc_mnc_s_type *pConfigPlmn);

#endif  // #ifndef __QUECTEL_MBN_COMMON_H__
