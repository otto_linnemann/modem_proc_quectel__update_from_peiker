
/*!  
  @file
  Quectel_mbn_base.h

  @brief
  This file defines declarations for MBN BASE use

  @see
  Create this file by QCM9X00205C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
09/12/2018   vicent        Init
=================================================================================*/

#ifndef __QUECTEL_MBN_BASE_H__
#define __QUECTEL_MBN_BASE_H__

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
#define MBN_TRACE_0(f)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBN_" f,__FUNCTION__,__LINE__)
#define MBN_TRACE_1(f,g1)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBN_" f,__FUNCTION__,__LINE__,g1)
#define MBN_TRACE_2(f,g1,g2)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBN_" f,__FUNCTION__,__LINE__,g1,g2)
#define MBN_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_5(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBN_" f,__FUNCTION__,__LINE__,g1,g2,g3)
#define MBN_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_6(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBN_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4)
#define MBN_TRACE_5(f,g1,g2,g3,g4,g5)  MSG_SPRINTF_7(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBN_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5)
#define MBN_TRACE_6(f,g1,g2,g3,g4,g5,g6)  MSG_SPRINTF_8(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),MBN_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5,g6)

#endif  // #ifndef __QUECTEL_MBN_BASE_H__
