
/*!  
  @file
  Quectel_mbn_common.c

  @brief
  This file defines interfaces for MBN COMMON use

  @see
  Create this file by QCM9X00205C001-P01
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
09/12/2018   vicent        Init
=================================================================================*/

#include "dsatme.h"
#include "dsati.h"
#include "Mcfg_trl.h"

#include "quectel_mbn_common.h"

#if defined(QUECTEL_MBN_CONFIG)

/////////////////////////////////////////////////////////////
// MACRO CONSTANTS DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// GLOBAL DATA DEFINITION
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Static functions
// NOTE: Static functions ONLY are used internal in this file.
/////////////////////////////////////////////////////////////
/*========================================================================
  FUNCTION:  QL_MBN_sGetOperator
=========================================================================*/
/*! @brief 
     Get operator name from MBN PLMN LIST

    @param
     <pConfigPlmn>  [IN] The pointer of 'mcfg_trl_carrier_mcc_mnc_s_type' data

    @return
     'MBN_OperatorEnum' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static int QL_MBN_sGetOperator(const mcfg_trl_carrier_mcc_mnc_s_type *pConfigPlmn)
{
    int iOperator = MBN_OPERATOR_MAX;

    if(NULL == pConfigPlmn)
    {
        MBN_TRACE_2("FAIL! pConfigPlmnList is NULL.",0,0);
        return MBN_OPERATOR_MAX;
    }

    if(0)
    {}
    else if((460 == pConfigPlmn->mcc) && (11 == pConfigPlmn->mnc))
    {
        iOperator = MBN_OPERATOR_CTCC;
    }
    else if((460 == pConfigPlmn->mcc) && (0 == pConfigPlmn->mnc))
    {
        iOperator = MBN_OPERATOR_CMCC;
    }
    //<2018/12/12-QCM9X00205C001-P02-Vicent.Gao, <[MBN] Segment 2==> Base code submit.>
    else if((460 == pConfigPlmn->mcc) && (1 == pConfigPlmn->mnc))
    {
        iOperator = MBN_OPERATOR_CUCC;
    }
    //>2018/12/12-QCM9X00205C001-P02-Vicent.Gao
    else
    {
        iOperator = MBN_OPERATOR_MAX;
    }

    return iOperator;
}

/*========================================================================
  FUNCTION:  QL_MBN_sIsDirectPlmnMatchCTCC
=========================================================================*/
/*! @brief 
     Check if UIM-PLMN is direct plmn match - CTCC use

    @param
     <pUimPlmn>  [IN] The pointer of 'mcfg_trl_carrier_mcc_mnc_s_type' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MBN_sIsDirectPlmnMatchCTCC(const mcfg_trl_carrier_mcc_mnc_s_type *pUimPlmn)
{
    boolean bResult = FALSE;

    if(NULL == pUimPlmn)
    {
        MBN_TRACE_2("FAIL! pUimPlmn is NULL.",0,0);
        return FALSE;
    }

    if(0)
    {}
    else if((999 == pUimPlmn->mcc) && (4 == pUimPlmn->mnc)) //CTCC International simcard
    {
        bResult = TRUE;
    }
    else
    {
        bResult = FALSE;
    }

	return bResult;
}

/*========================================================================
  FUNCTION:  QL_MBN_sIsDirectPlmnMatchCMCC
=========================================================================*/
/*! @brief 
     Check if UIM-PLMN is direct plmn match - CMCC use

    @param
     <pUimPlmn>  [IN] The pointer of 'mcfg_trl_carrier_mcc_mnc_s_type' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MBN_sIsDirectPlmnMatchCMCC(const mcfg_trl_carrier_mcc_mnc_s_type *pUimPlmn)
{
    boolean bResult = FALSE;

    if(NULL == pUimPlmn)
    {
        MBN_TRACE_2("FAIL! pUimPlmn is NULL.",0,0);
        return FALSE;
    }

    if(0)
    {}
    else if((460 == pUimPlmn->mcc)  && (13 == pUimPlmn->mnc))
    {
        bResult = TRUE;
    }
    else
    {
        bResult = FALSE;
    }

	return bResult;
}

//<2018/12/12-QCM9X00205C001-P02-Vicent.Gao, <[MBN] Segment 2==> Base code submit.>
/*========================================================================
  FUNCTION:  QL_MBN_sIsDirectPlmnMatchCUCC
=========================================================================*/
/*! @brief 
     Check if UIM-PLMN is direct plmn match - CUCC use

    @param
     <pUimPlmn>  [IN] The pointer of 'mcfg_trl_carrier_mcc_mnc_s_type' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
static boolean QL_MBN_sIsDirectPlmnMatchCUCC(const mcfg_trl_carrier_mcc_mnc_s_type *pUimPlmn)
{
    boolean bResult = FALSE;

    if(NULL == pUimPlmn)
    {
        MBN_TRACE_2("FAIL! pUimPlmn is NULL.",0,0);
        return FALSE;
    }

    if(0)
    {}
    else if((460 == pUimPlmn->mcc)  && (6 == pUimPlmn->mnc))
    {
        bResult = TRUE;
    }
    else
    {
        bResult = FALSE;
    }

	return bResult;
}
//>2018/12/12-QCM9X00205C001-P02-Vicent.Gao

/////////////////////////////////////////////////////////////
// FUNCTION DEFINITIONS ==> Extern functions
/////////////////////////////////////////////////////////////

/*========================================================================
  FUNCTION:  QL_MBN_IsDirectPlmnMatch
=========================================================================*/
/*! @brief 
     Check if UIM-PLMN is direct plmn match

    @param
     <pUimPlmn>  [IN] The pointer of 'mcfg_trl_carrier_mcc_mnc_s_type' data
     <pConfigPlmn>  [IN] The pointer of 'mcfg_trl_carrier_mcc_mnc_s_type' data

    @return
     'boolean' value

    @dependencies
     NONE

    @see
     
*/
/*=======================================================================*/
boolean QL_MBN_IsDirectPlmnMatch(const mcfg_trl_carrier_mcc_mnc_s_type *pUimPlmn, const mcfg_trl_carrier_mcc_mnc_s_type *pConfigPlmn)
{
    int iOperator = MBN_OPERATOR_MAX;
    boolean bResult = FALSE;

    if((NULL == pUimPlmn) || (NULL == pConfigPlmn))
    {
        MBN_TRACE_2("FAIL! Parameter is INVALID. pUimPlmn:%p,pConfigPlmn:%p",pUimPlmn,pConfigPlmn);
        return FALSE;
    }

    iOperator = QL_MBN_sGetOperator(pConfigPlmn);
    
    if(0)
    {}
    else if(MBN_OPERATOR_CTCC == iOperator)
    {
        bResult = QL_MBN_sIsDirectPlmnMatchCTCC(pUimPlmn);
    }
    else if(MBN_OPERATOR_CMCC == iOperator)
    {
        bResult = QL_MBN_sIsDirectPlmnMatchCMCC(pUimPlmn);
    }
    //<2018/12/12-QCM9X00205C001-P02-Vicent.Gao, <[MBN] Segment 2==> Base code submit.>
    else if(MBN_OPERATOR_CUCC == iOperator)
    {
        bResult = QL_MBN_sIsDirectPlmnMatchCUCC(pUimPlmn);
    }
    //>2018/12/12-QCM9X00205C001-P02-Vicent.Gao
    else
    {
        bResult = FALSE;
    }

    return bResult;
}

#endif //#if defined(QUECTEL_MBN_CONFIG)
