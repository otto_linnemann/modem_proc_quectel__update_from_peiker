#ifndef QUECTEL_URC_H
#define QUECTEL_URC_H

#include "ds3gsiolib.h"

#if defined(QUECTEL_URC_SUPPORT)

#define  	QUECTEL_MAX_URC_SUPPORT    15
/*
A type URC :only report on quectel urc port
B type URC: only report on port which can enter in data mode
C type URC: only report on port which relevant AT command executed
D type URC: report on all ports
*/
typedef enum
{
    DSAT_A_TYPE_URC = 0, //URC仅在QURCCFG设置的urc port上上报
    DSAT_B_TYPE_URC,     //URC仅在可以进入DATA mode的口上上报
    DSAT_C_TYPE_URC,     //URC仅在指定的urc port上上报
    DSAT_D_TYPE_URC,     //URC在所有口上都上报
    DSAT_MAX_TYPE_URC
}dsat_urc_e_type;

/*
DSAT_RI_PULL_NONE: not pull ri
DSAT_RI_PULL_BURST: pull xxms burst
DSAT_RI_PULL_ALWAYS: always pull
*/
typedef enum
{
    DSAT_RI_PULL_NONE = 0,
    DSAT_RI_PULL_BURST,
    DSAT_RI_PULL_ALWAYS
}dsat_ri_pull_e_type;

/*
urc_type: URC的类型，A类型/B类型/C类型/D类型
ri_pull_type: RI引脚的行为,不拉/拉一个脉冲/长拉
discard: 当上报urc的口，当前处于data mode/Async cmd mode时，要不要缓存URC fasle:缓存
port_id: 当URC为C类URC时，指定上报的口
*/
typedef struct
{
    dsat_urc_e_type              urc_type;
    dsat_ri_pull_e_type          ri_pull_type;
    boolean                      discard;
    ds3g_siolib_port_e_type      port_id;
#ifdef QUEC_TCPIP_USING_URC_MODE_TO_DIR_PUSH	
	uint32                       tcp_direct_push_magic; //lory 2016/12/23 tcp direct push indicate magic
#endif	
}dsat_urc_ind_struct;

void dsat_queue_urc_init
(
void
);

void dsat_send_urc_ext
(
  dsm_item_type *item_ptr,
                                            dsat_rsp_enum_type  rsp_type,
  dsat_urc_ind_struct   *urc_ind_opt
);

void dsat_flush_urc
(
  ds3g_siolib_port_e_type  port_id
);

ds3g_siolib_port_e_type dsat_get_urc_port_by_urc_type
(
    dsat_urc_e_type  urc_type,
    ds3g_siolib_port_e_type urc_sio_port
);

//<2016/11/03-QCWXM9X00019-P01-wenxue.sheng, <[QCFG] Segment 1==> Add AT+QCFG="urc/cache" function.>
#if defined(QUECTEL_URC_SUPPORT)
extern boolean QL_QCFG_UrcCacheGetEnable(void);
#endif // #if defined(QUECTEL_URC_SUPPORT)
//>2016/11/03-QCWXM9X00019-P01-wenxue.sheng

#endif/*QUECTEL_URC_SUPPORT*/

#endif/*QUECTEL_URC_H*/
