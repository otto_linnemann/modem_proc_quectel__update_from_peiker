/*===========================================================================

                           QUECTEL_WMS_ATCTAB . H

DESCRIPTION
  wms atc table function

REFERENCES

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
03/29/2013 wms atc table declare add by clark.li 

===========================================================================*/
#ifndef __QUECTEL_WMS_ATCTAB_H__
#define __QUECTEL_WMS_ATCTAB_H__

#include "dsati.h"

#ifdef QUECTEL_WMS_ATC
extern const dsati_cmd_type dsat_quectel_wms_action_table[];
extern const unsigned int dsat_quectel_wms_table_size;

extern const dsati_cmd_ex_type dsat_quectel_wms_action_table_ex[];

#endif
#endif
