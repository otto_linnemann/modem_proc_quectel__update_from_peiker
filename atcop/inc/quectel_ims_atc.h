/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL IMS ATC use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Aaron LI
 |  Coded    by     :   Aaron LI
 |  Tested   by     :   Aaron LI
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/09/19       Aaron LI        Create this file by QCM9X00139-P01
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_IMS_ATC_H__
#define __QUECTEL_IMS_ATC_H__

#include "dstask.h"
#include "dsati.h"

#if defined(QUECTEL_AT_COMMON_SUPPORT)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
//<2017/09/13-QCM9X00140-P01-Aaron.Li, <[QIMSCFG] Segment 1==> Add new sub command AT+QIMSCFG="user_agent".>
#define QIMSCFG_UA_MAX_LEN  (1024)
#define QIMSCFG_EFS_NV69689  "/nv/item_files/ims/ims_user_agent"
//>2017/09/13-QCM9X00140-P01-Aaron.Li

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang, <[QMI] Segment 1==> Add new AT:AT+QIMSCFG="speech_codec".>
typedef struct
{
    int network_mode;
    int speech_codec;
    int speech_enc_samp_freq;
    uint8  call_id;
}QIMSCFG_SPEECH_CODEC_INFO_STRUCT;

QIMSCFG_SPEECH_CODEC_INFO_STRUCT sQIMSCFG_SpeechCodecInfo;
//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////
//<2017/10/17-QCM9X00146-P01-Laurence.Yin, <[QIMSCFG] Segment 1==> Add new sub command AT+QIMSCFG="ims".>
extern dsat_num_item_type value_ims;
//>2017/10/17-QCM9X00146-P01-Laurence.Yin

/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern dsat_result_enum_type quectel_exec_qimscfg_cmd
(
    dsat_mode_enum_type eMode,             /*  AT command mode:            */
    const dsati_cmd_type *pParseTab,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *pTok,    /*  Command tokens from parser  */
    dsm_item_type *pResBuf           /*  Place to put response       */
);

//<2017/09/13-QCM9X00140-P01-Aaron.Li, <[QIMSCFG] Segment 1==> Add new sub command AT+QIMSCFG="user_agent".>
extern dsat_result_enum_type quectel_exec_qimscfg_user_agent_cmd
(
    dsat_mode_enum_type eMode, 
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok, 
    dsm_item_type *pResBuf
);
//>2017/09/13-QCM9X00140-P01-Aaron.Li

//<2017/10/17-QCM9X00146-P01-Laurence.Yin, <[QIMSCFG] Segment 1==> Add new sub command AT+QIMSCFG="ims".>
extern uint8 QCFG_IMS_IsVoLTEReady(void);
extern dsat_result_enum_type write_IMS_by_set(void);

dsat_result_enum_type quectel_exec_qimscfg_ims_cmd
(
    dsat_mode_enum_type eMode, 
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok, 
    dsm_item_type *pResBuf
);
//>2017/10/17-QCM9X00146-P01-Laurence.Yin

//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang, <[QMI] Segment 1==> Add new AT:AT+QIMSCFG="speech_codec".>
extern dsat_result_enum_type quectel_exec_qimscfg_speech_codec_cmd
(
    dsat_mode_enum_type eMode,             /*  AT command mode:            */
    const dsati_cmd_type *pParseTab,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *pTok,    /*  Command tokens from parser  */
    dsm_item_type *pResBuf           /*  Place to put response       */
);
//<2017/12/27-QCM9X00151C010-P01-Yosef.Zhang

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////


#endif // #if defined(QUECTEL_AT_COMMON_SUPPORT)

#endif //#ifndef __QUECTEL_IMS_ATC_H__
