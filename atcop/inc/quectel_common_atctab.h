#ifndef QUECTEL_COMMON_ATCTAB_H
#define QUECTEL_COMMON_ATCTAB_H

#include "dstask.h"
#include "dsati.h"

#ifdef QUECTEL_AT_COMMON_SUPPORT
#if 0
extern const dsati_cmd_type dsat_quectel_common_table[];
extern const unsigned int dsat_quectel_common_table_size;

extern const dsati_cmd_ex_type dsat_quectel_common_table_ex [];
#endif
#endif

#endif/*QUECTEL_COMMON_ATCTAB_H*/
