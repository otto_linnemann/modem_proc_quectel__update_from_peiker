#ifndef QUECTEL_HTTP_ATC_H
#define  QUECTEL_HTTP_ATC_H

#include "dsati.h"
#include "dsat_v.h"

#if defined(QUECTEL_HTTP_APP_SUPPORT)
dsat_result_enum_type dsat_http_qhttpcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_http_qhttpurl_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_http_qhttpget_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_http_qhttppost_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_http_qhttpread_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_http_qhttppostfile_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_http_qhttpreadfile_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#ifdef FEATURE_QUECTEL_LOCATOR_APP
#define LOC_CFG_STR_LEN   16
#define LOC_CFG_SERVER_LEN 50
typedef enum
{
    LOC_CFG_SERVER,
    LOC_CFG_ADDR,
    LOC_CFG_TIMEOUT,
    LOC_CFG_CHARSET,
    LOC_CFG_CACHE,
    LOC_CFG_FREQUENCY,//JONATHAN,14/2/19,UPDATE
    LOC_CFG_REDIRECT,
    LOC_CFG_VERSION,
    LOC_CFG_CTXID,//JONATHAN,14/2/27
    LOC_CFG_TEST,//JONATHAN,14/4/2
    LOC_CFG_MAX
}quec_loc_cfg_type;
dsat_result_enum_type dsat_http_loccfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
dsat_result_enum_type dsat_http_cellloc_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif
#endif

#endif/*QUECTEL_HTTP_ATC_H*/
