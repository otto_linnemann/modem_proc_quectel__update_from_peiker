#ifndef QUECTEL_RI_H
#define  QUECTEL_RI_H

#include "dsati.h"
#include "quectel_urc.h"
#include "ds3gtimer.h"

#ifndef QUECTEL_NANWANG_NETLIGET
#define QUECTEL_RI_FEATURE_GPIO       (75)//Ramos.zhang 201501205  add for EC25  RI gpio 75
#else
#define QUECTEL_RI_FEATURE_GPIO       (79) //(75) Ramos 20170107 准捷南网 要使用RI 做系统灯，所以这里指定一个模块没有拉出来的GPIO
#endif
//#define QUECTEL_RI_FEATURE_CFG        DAL_GPIO_CFG(QUECTEL_RI_FEATURE_GPIO, 0, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP,DAL_GPIO_2MA)//

#define PULSE_DURATION_MIN  1
#define WAVE_DURATION_MIN  1
#define PULSE_DURATION_MAX  2000
#define WAVE_DURATION_MAX  10000
#define PULSE_COUNT_MIN 1
#define PULSE_COUNT_MAX 5

#define OFF_ON_NUM  2
#define RI_TYPE_NAME_BUFFER_LENGTH  20

//<2017/01/10-QCWXM9X00026-P01-wenxue.sheng, <[ATC] Segment 1==>Add AT+QCFG="urc/ri/pin".>
#if defined(QUECTEL_URC_SUPPORT)
#define QCFG_URC_RI_PIN_NAME_MAX_LEN   15
#define QCFG_URC_RI_PIN_UART1_RI_GPIO  75  //UART1_RI_GPIO_NUM
#define QCFG_URC_RI_PIN_UART1_DCD_GPIO  38  //UART1_DCD_GPIO_NUM
#endif // #if defined(QUECTEL_URC_SUPPORT)
//>2017/01/10-QCWXM9X00026-P01-wenxue.sheng

typedef enum
{
    DSAT_QUEC_URC_RI_TYPE_NONE = -1,
    DSAT_QUEC_URC_RI_TYPE_RING,
    DSAT_QUEC_URC_RI_TYPE_SMS_INCOMING,
    DSAT_QUEC_URC_RI_TYPE_OTHER,
    DSAT_QUEC_URC_RI_TYPE_MAX
}dsat_quec_urc_ri_type_enum_type;

typedef enum
{
    DSAT_QUEC_RI_OUTPUT_TYPE_RESPECTIVE,
    DSAT_QUEC_RI_OUTPUT_TYPE_PHYSICAL,
    //DSAT_QUEC_RI_OUTPUT_TYPE_BREAK,
    //DSAT_QUEC_RI_OUTPUT_TYPE_WAKEUPOUT,
    DSAT_QUEC_RI_OUTPUT_TYPE_MAX
}dsat_quec_ri_output_type_enum_type;

typedef enum
{
    DSAT_QUEC_RI_SIGNAL_TYPE_OFF,
    DSAT_QUEC_RI_SIGNAL_TYPE_PULSE,
    DSAT_QUEC_RI_SIGNAL_TYPE_ALWAYS,
    DSAT_QUEC_RI_SIGNAL_TYPE_AUTO,
    DSAT_QUEC_RI_SIGNAL_TYPE_WAVE,
    DSAT_QUEC_RI_SIGNAL_TYPE_MAX
}dsat_quec_ri_signal_type_enum_type;

typedef struct
{
    dsat_quec_ri_signal_type_enum_type ri_signal_type;
    uint32 pulse_duration;//ms default 120
    uint8  pulse_count;//default 1
}dsat_quec_ri_element_struct;

typedef struct
{
/*当DSAT_QUEC_RI_OUTPUT_TYPE_PHYSICAL等方式时，RI跳变和URC上报不在同一个口
这里用于记录RI跳变对应的URC上报口*/
    ds3g_siolib_port_e_type ri_urc_port;

/*RI改变次数*/
    uint8 ri_change_count;

/*RI改变次数上限= pulse_count * 2 */
    uint8 max_count;

/*脉冲宽度=脉冲间隔*/
    uint32 duration;
}dsat_quec_ri_dual_pulse_struct;


typedef struct
{
    dsat_quec_ri_output_type_enum_type ri_output_type;
    dsat_quec_ri_element_struct ri_info[DSAT_QUEC_URC_RI_TYPE_MAX];//ri 信号特征设置
    boolean ri_active_level;//默认低电平激活
    boolean ring_auto_wave_no_disturbing;//
    uint32  ring_wave_active_duration;
    uint32  ring_wave_inactive_duration;
    boolean urc_delay;

    dsat_quec_ri_dual_pulse_struct dual_pulse[DS3G_SIOLIB_PORT_MAX - DS3G_SIOLIB_DATA_PORT];
}dsat_quec_ri_info_struct;

//<2017/01/10-QCWXM9X00026-P01-wenxue.sheng, <[ATC] Segment 1==>Add AT+QCFG="urc/ri/pin".>
#if defined(QUECTEL_URC_SUPPORT)
typedef struct
{
    uint32 uNum;
    char aName[QCFG_URC_RI_PIN_NAME_MAX_LEN + 1];
} QCFG_URCRIPINInfoStruct;
#endif // #if defined(QUECTEL_URC_SUPPORT)
//>2017/01/10-QCWXM9X00026-P01-wenxue.sheng



#if defined(QUECTEL_RI_SUPPORT)

//-----------------------function------------------------------

boolean quectel_ri_urc_action
(
    dsat_ri_pull_e_type  ri_pull_type,
    dsat_quec_urc_ri_type_enum_type  urc_ri_type,
    ds3g_siolib_port_e_type port_id
);

dsat_result_enum_type quectel_ri_load_ri_info_from_nv( void );

void quectel_ri_init_gpio_config( void );

uint8 quectel_ri_is_ringing( void );

ds3g_siolib_port_e_type quectel_ri_get_ri_port(dsat_urc_e_type  urc_type);

dsat_quec_ri_info_struct* quectel_ri_get_ri_info( void );

dsat_result_enum_type quectel_ri_stop_timer
(
  ds3g_siolib_port_e_type actual_port_id
);

dsat_result_enum_type quectel_ri_active
(
  boolean is_active,
  ds3g_siolib_port_e_type port_id
);

void quectel_ri_time_out_hdlr(ds3g_timer_enum_type timer_id);


dsat_result_enum_type quectel_exec_qcfg_ri_ring_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

dsat_result_enum_type quectel_exec_qcfg_ri_smsincoming_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

dsat_result_enum_type quectel_exec_qcfg_ri_other_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

dsat_result_enum_type quectel_exec_qcfg_risignaltpye_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

//<2016/08/29-QCWXM9X00012-P01-wenxue.sheng, <[QCFG] Segment 1==> Add AT+QCFG="urc/delay".>
dsat_result_enum_type quectel_exec_qcfg_urc_delay_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
//>2016/08/29-QCWXM9X00012-P01- wenxue.sheng

//<2017/01/10-QCWXM9X00026-P01-wenxue.sheng, <[ATC] Segment 1==>Add AT+QCFG="urc/ri/pin".>
#if defined(QUECTEL_RI_PIN)
extern boolean QCFG_URCRIPIN_Init(void);
extern dsat_result_enum_type dsat_exec_qcfg_urc_ri_pin_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);
#endif // #if defined(QUECTEL_RI_PIN)
//>2017/01/10-QCWXM9X00026-P01-wenxue.sheng

#ifdef QUECTEL_REDIRECT_RI_FUNC
dsat_result_enum_type quectel_exec_qcfg_ri_pin_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
)
#endif

#else
uint8 quectel_ri_is_ringing(void);

#endif/*QUECTEL_RI_SUPPORT*/

#endif/*QUECTEL_RI_H*/
