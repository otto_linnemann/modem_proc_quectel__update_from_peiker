#ifndef QUECTEL_COMMON_ATC_H
#define  QUECTEL_COMMON_ATC_H

#include "dstask.h"
#include "dsati.h"

#if defined(QUECTEL_AT_COMMON_SUPPORT)

//<2017/02/27-QCM9XTVG00060-P01-Vicent.Gao, <[QCFG] Segment 1==> Add cmd: QCFG-imsreg/iptype.>
#define QCFG_EFS_IMS_REG_IP_TYPE  "/nv/item_files/ims/qp_ims_reg_config_db"
//>2017/02/27-QCM9XTVG00060-P01-Vicent.Gao

//<2017/03/28-QCM9XTVG00066-P01-Vicent.Gao, <[EFS] Segment 1==>Add new cmd: QEFSVER>
#define QEFSVER_PROJECT_NAME_MAX_LEN    40
#define QEFSVER_SUB_VER_MAX             60000
#define QEFSVER_RELEASE_DATE_MAX        220010209
#define QEFSVER_FLASH_OEM_MAX_LEN       40
#define QEFSVER_FLASH_TYPE_MAX_LEN      20
//>2017/03/28-QCM9XTVG00066-P01-Vicent.Gao
//<2017/05/31-QCM9X00091-P01-Vicent.Gao, <[COMMON] Segment 1==> IMEISV-SVN value specify architecture.>
//<2017/07/14-QCM9X00091-P02-Yosef.Zhang, <[COMMON] Segment 2==> IMEISV-SVN value specify architecture.>
//#define IMEISV_SVN_NOT_CHANGE   0xFF    
#define IMEISV_SVN_GET_SW_REV   0xFF
#define IMEISV_SVN_NOT_CHANGE   0xFE
//>2017/07/14-QCM9X00091-P02-Yosef.Zhang
//>2017/05/31-QCM9X00091-P01-Vicent.Gao

//<2017/06/01-QCM9X00092-P01-Aaron.LI, <[MBN] Segment 1==> Add new command: AT+QMBNHWCFG>
#if defined(QUECTEL_MBN_CONFIG)
#define QMBNHWCFG_REC_NUM_MAX   (4)
#endif // #if defined(QUECTEL_MBN_CONFIG)
//>2017/06/01-QCM9X00092-P01-Aaron.Li

//<2017/02/27-QCM9XTVG00060-P01-Vicent.Gao, <[QCFG] Segment 1==> Add cmd: QCFG-imsreg/iptype.>
typedef enum
{
    QCFG_IMS_REG_IP_TYPE_V6 = 0,
    QCFG_IMS_REG_IP_TYPE_V4 = 1,

    //Warning!==>Please add new QCFG IMS-REG IP-TYPE upper this line.
    QCFG_IMS_REG_IP_TYPE_INVALID = 0xFF
} QCFGIMSREG_IpTypeEnum;
//>2017/02/27-QCM9XTVG00060-P01-Vicent.Gao

//<2017/03/13-QCM9XTVG00063-P01-Vicent.Gao, <[MBN] Segment 1==>Fix QMBNCFG common issues.>
#if defined(QUECTEL_MBN_CONFIG)
typedef enum
{
	MCFG_TRL_TLV_TYPE_FORMAT_VERSION = 0,
	MCFG_TRL_TLV_TYPE_CONFIG_VERSION = 1,
	MCFG_TRL_TLV_TYPE_CARRIER_MCC_MNC = 2,
	MCFG_TRL_TLV_TYPE_CARRIER_NAME = 3,
	MCFG_TRL_TLV_TYPE_IIN_LIST = 4,
	MCFG_TRL_TLV_TYPE_BASE_VERSION = 5,
	MCFG_TRL_TLV_TYPE_PLMN_LIST = 6,
	MCFG_TRL_TLV_TYPE_CATEGORY = 7,
	
	//Warning!==>Please add new MCFG TRL TLV TYPE upper this line.
	MCFG_TRL_TLV_TYPE_MAX
} MCFG_TrlTlvTypeEnum;
#endif // #if defined(QUECTEL_MBN_CONFIG)
//>2017/03/13-QCM9XTVG00063-P01-Vicent.Gao

//<2017/03/28-QCM9XTVG00066-P01-Vicent.Gao, <[EFS] Segment 1==>Add new cmd: QEFSVER>
typedef struct
{
    char aProjectName[QEFSVER_PROJECT_NAME_MAX_LEN + 1];
    dsat_num_item_type uSubVer;
    dsat_num_item_type uReleaseDate;
    char aFlashOem[QEFSVER_FLASH_OEM_MAX_LEN + 1];
    char aFlashType[QEFSVER_FLASH_TYPE_MAX_LEN + 1];
} QEFSVER_CntxStruct;
//>2017/03/28-QCM9XTVG00066-P01-Vicent.Gao

//<2017/07/20-QCM9X00122-P01-Yosef.Zhang, <[COMMON] Segment 1==> Add new command:AT+QSVN.>
typedef struct
{
    char aData[QSVN_VALUE_FIX_LEN + 1]; //This is a string end of '\0'
} QSVN_ElemStruct;
//>2017/07/20-QCM9X00122-P01-Yosef.Zhang

//>2018/04/08-[Internal Request][Tracking Number :RQ0000025][URC][New Command:AT+QCFG="Feature_Switch_Flag"]-P02-jarvis.feng --Add new Command AT+QCFG="Feature_Switch_Flag" to solve whether report ETWS URC Begin
#if defined(QUECTEL_AT_COMMON_SUPPORT)

/*该全局变量的不同bit 为对应不同的开关*/
extern unsigned int  Quectel_Feature_Switch_Flag;

/*枚举值为各个功能开关的 bit map ,后续其他开关可直接找其中没有使用的 bit 位即可   */
typedef enum
{
	QMCFG_FEATURE_SWITCH_ETWS_URC_FLAG = 0x01,
	
	
	//Warning!==>Please add new Feature Switch upper this line.
	QMCFG_FEATURE_SWITCH_MAX
} QMCFG_FeatureSwitchFlagEnum;
#endif
//>2018/04/08-[Internal Request][Tracking Number :RQ0000025][URC][New Command:AT+QCFG="Feature_Switch_Flag"]-P02-jarvis.feng --Add new Command AT+QCFG="Feature_Switch_Flag" to solve whether report ETWS URC End

dsat_result_enum_type  quectel_exec_egmr_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#ifdef QUECTEL_MBN_CONFIG
dsat_result_enum_type quectel_exec_qmbncfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

//<2017/06/01-QCM9X00092-P01-Aaron.LI, <[MBN] Segment 1==> Add new command.: AT+QMBNHWCFG >
dsat_result_enum_type quectel_exec_qmbnhwcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
//>2017/06/01-QCM9X00092-P01-Aaron.Li

#endif
dsat_result_enum_type quectel_exec_csub_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_cversion_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qdmem_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

//2016/08/19 porting by chao.zhou for landi
dsat_result_enum_type dsat_exec_qoem_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#ifdef QUECTEL_RI_SUPPORT/*2013-7-31-laguna*/
/*===========================================================================

FUNCTION  dsat_exec_qrir_cmd

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QRIR command.
  This command restores all RI pins to inactive. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsat_exec_qrir_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);
#endif/*QUECTEL_RI_SUPPORT*/

#if defined(QUECTEL_URC_SUPPORT)
dsat_result_enum_type  quectel_exec_qurccfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type  quectel_exec_qindcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#if defined(QUECTEL_NV_RW_SUPPORT)
dsat_result_enum_type  quectel_exec_qnvw_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type  quectel_exec_qnvr_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type  quectel_exec_qnvfw_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qnvfr_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qnvfd_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif/*QUECTEL_NV_RW_SUPPORT*/

#ifdef QUECTEL_FLY_MODE//2014,10,20 francis
dsat_result_enum_type dsat_exec_qcfg_airplanecontrol_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_NITZ
dsat_result_enum_type dsatetsime_exec_quectest_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif /*QUECTEL_NITZ*/

/* 2015.07.15 added by rex for configure hotswap */
#ifdef QUECTEL_SIM_HOTSWAP
dsat_result_enum_type dsat_exec_qcfg_sim_hotswap_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,
	dsm_item_type *res_buff_ptr
);
#endif

#if defined(QUECTEL_NET_LED)&& !defined(QUECTEL_GW_NETLIGHT_CONTROL)
dsat_result_enum_type dsat_exec_qcfg_ledmode_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,
    dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_CUSTOMER_BONSON

dsat_result_enum_type dsat_exec_hwinfo_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);


dsat_result_enum_type dsat_exec_qcfg_bonson_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif//QUECTEL_CUSTOMER_BONSON

#ifdef QUECTEL_CUSTOMER_BONSON
dsat_result_enum_type dsat_exec_qati_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_qgmi_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_qgmm_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_qgmr_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#if defined(QUECTEL_RF_BURST_TX)
dsat_result_enum_type dsat_exec_qrftest_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_qrftestmode_cmd
(
	dsat_mode_enum_type mode,			/*  AT command mode: 		   */
	const dsati_cmd_type *parse_table,  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr		   	/*  Place to put response	   */
);
#endif/*QUECTEL_RF_BURST_TX*/

#ifdef QUECTEL_USB_CONFIG_ATC //will.shao, for modify usb vid/pid
dsat_result_enum_type quectel_exec_qcfg_usbid_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

dsat_result_enum_type dsat_exec_qcfg_usbnet_cmd(QUEC_CMD_HDLR_PARAM); //maxcodeflag20161013

#endif

#ifdef QUECTEL_USB_ENABLE_OR_DISABLE
dsat_result_enum_type quectel_exec_qcfg_usbcfg_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

dsat_result_enum_type quectel_exec_qcfg_usbee_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

#ifdef QUECTEL_NV_OPERATOR
dsat_result_enum_type dsat_exec_qcfg_volte_operator_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,
    dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_VRM_FOTA

dsat_result_enum_type quectel_exec_qfotadl_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif/*QUECTEL_VRM_FOTA*/

#ifdef QUECTEL_OMA_DM
dsat_result_enum_type quectel_exec_qfumo_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
dsat_result_enum_type quectel_exec_qfumocfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif/*QUECTEL_OMA_DM*/

#ifdef QUECTEL_GPS_SUPPORT
dsat_result_enum_type quectel_exec_qcfg_agps_string_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_BT_FETURE
dsat_result_enum_type quectel_exec_qbtaddr_cmd(dsat_mode_enum_type mode, const dsati_cmd_type *parse_table, const tokens_struct_type *tok_ptr, dsm_item_type *res_buff_ptr);
#endif

#ifdef QUECTEL_CDMA_RUIM_CTRL
dsat_result_enum_type dsat_exec_qcfg_cmda_ruim_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

void quectel_mcfg_sys_restart( const byte *rsp, 
                                        unsigned int length, 
                                        void *param);


#ifdef QUECTEL_CMUX_URC
dsat_result_enum_type dsat_exec_qcfg_cmux_urcport_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif


#ifdef QUECTEL_MODEM_RESTART_LEVEL
dsat_result_enum_type dsat_exec_qcfg_modem_restart_level_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_APPS_RESTART_LEVEL
dsat_result_enum_type dsat_exec_qcfg_ap_restart_level_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif


#ifdef QUECTEL_AUDIO_AT_CMD
dsat_result_enum_type dsat_exec_qcfg_pcmclk_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

dsat_result_enum_type dsat_exec_qcfg_tone_incoming_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_PPP_AUTH_FEATURE
dsat_result_enum_type quectel_exec_auth_cmd(QUEC_CMD_HDLR_PARAM);
#endif

#ifdef QUECTEL_NETWORK_FEATURE //maxcodeflag20160711
dsat_result_enum_type dsat_exec_qcfg_no_auth_check_cmd(QUEC_CMD_HDLR_PARAM);
#endif

#ifdef QUEC_OOS_TIMER

dsat_result_enum_type quectel_exec_qcfg_oos_timer_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_DIVERSITY_FEATURE
dsat_result_enum_type dsat_exec_qcfg_diversity_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif
//2016/07/28-QCM9X07FZ00002-P01-Frederic.Zhang
//add AT+QCFG="IMS" command 
#ifdef QUECTEL_IMS_CMD_SUPPORT
dsat_result_enum_type dsat_exec_qcfg_ims_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

//<2017/05/26-QCM9X00088-P01-Vicent.Gao, <[QCFG] Segment 1==> Fix QCFG-IMS issues.>
extern uint8 QCFG_IMS_IsVoLTEReady(void);
//>2017/05/26-QCM9X00088-P01-Vicent.Gao

#endif // QUECTEL_IMS_CMD_SUPPORT
//2016/07/28-QCM9XFZ00002-P01-Frederic.Zhang


#ifdef QUECTEL_RFTX_POWER_CONTROL_FEATURE
dsat_result_enum_type dsat_exec_qrftxpwr_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif
#ifdef QUEC_3GPP_RRC_MANUAL_RELEASE
extern dsat_result_enum_type quectel_exec_qw2lfst_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

//<2017/02/27-QCM9XTVG00060-P01-Vicent.Gao, <[QCFG] Segment 1==> Add cmd: QCFG-imsreg/iptype.>
extern dsat_result_enum_type dsat_exec_qcfg_imsreg_iptype_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *pResBuf
);
//>2017/02/27-QCM9XTVG00060-P01-Vicent.Gao

#endif/*QUECTEL_AT_COMMON_SUPPORT*/

//<2016/09/05-QCM9XTVG00052-P01-Vicent.Gao,<[SIM] Segment 1==>Copy QCFG-sim/recovery from UC20.>
#if defined(QUECTEL_SIM_RECOVERY)
extern void quectel_load_qcfg_sim_recovery_from_nv(void);
extern void quectel_sim_auto_detect_timer_init(void);

extern dsat_result_enum_type dsat_exec_qcfg_sim_recovery_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif //#if defined(QUECTEL_SIM_RECOVERY)
//>2016/09/05-QCM9XTVG00052-P01-Vicent.Gao

//<2016/10/10-QCWXM9X00017-P01-wenxue.sheng, <[QCFG] Segment 1==> Add AT+QCFG="amrcodec".>
#ifdef QUECTEL_AMRCODEC_CONFIG
extern dsat_result_enum_type dsat_exec_qcfg_amrcodec_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);
#endif // #if defined(QUECTEL_AMRCODEC_CONFIG)
//>2016/10/10-QCWXM9X00017-P01-wenxue.sheng


//<2016/10/18-QCWXM9X00018C003-P01-wenxue.sheng, <[SKT] Segment 1==> Copy common function code from UC20.>

extern dsat_result_enum_type quectel_exec_qmodemcpu_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
extern dsat_result_enum_type quectel_exec_qlinuxcpu_cmd
(
	dsat_mode_enum_type mode,			/*  AT command mode: 		   */
	const dsati_cmd_type *parse_table,  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr		   	/*  Place to put response	   */
);

//<2017/01/12-QCM9XTVG00076-P01-Vicent.Gao, <[VTS] Segment 1==> Add cmd: QCFG-vts/async.>
#if defined(QUECTEL_CALL_FEATURE)
extern dsat_result_enum_type dsat_exec_qcfg_vts_async_cmd
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *pResBuf
);
#endif // #if defined(QUECTEL_CALL_FEATURE)
//>2017/01/12-QCM9XTVG00076-P01-Vicent.Gao

extern boolean Quectel_SaveWifiMacToNV(uint8 *pMac);

//<2016/11/03-QCWXM9X00019-P01-wenxue.sheng, <[QCFG] Segment 1==> Add AT+QCFG="urc/cache" function.>
#if defined(QUECTEL_URC_SUPPORT)
extern dsat_result_enum_type   dsat_exec_qcfg_urc_cache_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:          */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,
    dsm_item_type *res_buff_ptr
);
#endif // #if defined(QUECTEL_URC_SUPPORT)
//>2016/11/03-QCWXM9X00019-P01-wenxue.sheng

#ifdef QUECTEL_QDSP6_FREQ_LIMIT
dsat_result_enum_type dsat_exec_qcfg_qdsp6_freq_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_THERMAL   //maxcodeflag20171206
dsat_result_enum_type dsat_exec_qcfg_thermal_txpwrlmt_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);
#endif


#ifdef QUECTEL_THERMAL_ATC
dsat_result_enum_type dsat_exec_qcfg_thermal_modem_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);

dsat_result_enum_type dsat_exec_qcfg_thermal_limit_rates_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);
#endif

//<2016/12/06-QCM9XAW00006-P01-Andvin.Wang, <[QCFG] Segment 1==>Add QCFG sub-cmd: lte/bandprior.>
#ifdef QUECTEL_NETWORK_SUPPORT
extern dsat_result_enum_type dsat_exec_qcfg_lte_bandprior_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif
//>2016/12/06-QCM9XAW00006-P01-Andvin.Wang

//<2016/10/21-QCM9XAW00002-P01-Andvin.Wang, <[QSAR] Segment 1==>Add QSAR common function definition.>
#ifdef QUECTEL_RF_SAR_FEATURE
extern void quectel_rf_sar_init(void);
#endif
//>2016/10/21-QCM9XAW00002-P01-Andvin.Wang

#ifdef QUECTEL_CODEC_POWSAVE_ATC
dsat_result_enum_type dsat_exec_qcfg_codec_powsave_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);
#endif

//Kenneth 2017/01/05
#ifdef QUECTEL_QMI_SYNC
dsat_result_enum_type dsat_exec_qcfg_qmi_sync_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);
#endif /*End of  QUECTEL_QMI_SYNC*/

/******************************************************************************************
connor-2018/12/06:Description....
Refer to [Issue-Depot].[IS0000382][Submitter:connor.wu@quectel.com,Date:2018-12-05]
<妯″潡娌″紑閫?G涓氬姟鍏ョ綉浠?15	(No suitable cells in tracking area)琚嫆锛屾湡鏈涙ā鍧楃珛鍗虫妸褰撳墠鐨凱LMN鍔犲叆鍒癰ackoff鍒楄〃锛屽垏鍒?G鍔熻兘閲嶅疄鐜?
******************************************************************************************/
#ifdef QUECTEL_CONTROL_3G_REDIR_TO_LTE
dsat_result_enum_type dsat_exec_control_3g_redir_to_lte
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);
void QCFG_CONTROL_3G_REDIR_TO_LTE_InitEFS(void);
boolean QCFG_CONTROL_3G_REDIR_TO_LTE_WriteEFS(int redir_flag);
int QCFG_CONTROL_3G_REDIR_TO_LTE_ReadEFS(void);
int quectel_isDeny3GRedirToLTE(void);
int quectel_GetEpsStorageSupportedValueFromNvOrUsim(boolean eps_storage_supported_sim);
#endif



/*- - - - - - maxcodeflag20161208 begin - - - - - - */
#ifdef QUECTEL_RMNET_FEATURE
typedef struct _TQCRMCallRetInfo
{
    uint8 qcrm_inst;
    uint8 ip_type;
    boolean bActOK;
}TQCRMCallRetInfo;


#define QUECTEL_MAX_NET_DEV 16

typedef struct _TNetDevConnInfo
{
    uint8 state;   //0:DisConnect 1:WaitingConnect 2:Connected
    uint8 ip_type;
    uint8 inst;

}TNetDevConnInfo;

//Kenneth 2017/01/24
#ifdef QUECTEL_IP_ROUTE_CONTROL
extern int quec_list_ip_tuple(char *buf);

dsat_result_enum_type dsat_exec_qcfg_iproute_enable_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);

dsat_result_enum_type dsat_exec_qcfg_iproute_list_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,
  dsm_item_type *res_buff_ptr
);
#endif
//End of Kenneth

typedef struct _TNetDevStatusInfo
{
    boolean bNetDevStatusRpt;
    TNetDevConnInfo ConnInfo[QUECTEL_MAX_NET_DEV];
}TNetDevStatusInfo;

dsat_result_enum_type quectel_exec_qnetdevstatus_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif
/*- - - - - - maxcodeflag20161208 end - - - - - - */

//<2017/02/09-QCM9XTVG00080C002-P01-Joyce.Sun, <[STK] Segment 1==>Add cmd: QCFG-stkauto/setupmenutr.>
#if defined(QUECTEL_STK_SUPPORT)
extern dsat_result_enum_type dsat_exec_qcfg_stkauto_setupmenutr
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *pResBuf
);
extern boolean QCFG_STKAUTO_SetUpMenuTrInit(void);
#endif // #if defined(QUECTEL_STK_SUPPORT)
//>2017/02/09-QCM9XTVG00080C002-P01-Joyce.Sun

//<2017/03/15-QCWXM9X00031-P01-wenxue.sheng, <[ATC] Segment 1==>Add AT+QEMMINFO.>
extern boolean QL_QEMMINFO_GetTimerValue(dword* pT3402Val, dword* pT3412Val);

extern dsat_result_enum_type dsat_exec_qemminfo_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
//>2017/03/15-QCWXM9X00031-P01-wenxue.sheng
#ifdef QUECTEL_AT_DBGCTL_SUPPORT
dsat_result_enum_type dsat_exec_dbgctl_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
void quectel_dbgctl_value_readEFS(void);
extern  unsigned int Printf_enable_Flag;
#endif

#ifdef QUECTEL_AT_QCAUTOCONNECT_SUPPORT
dsat_result_enum_type dsat_exec_qcfg_qcautoconnect_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_SIM_INVALI_RECOVERY
dsat_result_enum_type dsat_exec_sim_invali_recovery_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_THERMAL_ATC //duke 2018 02 22    	
dsat_result_enum_type dsat_exec_qcfg_sarcfg_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

//<2017/03/20-QCWXM9X00032-P01-wenxue.sheng, <[ATC] Segment 1==>Sync QCOPS CMD from 9215.>
#if defined(QUECTEL_QCOPS_FEATURE)
extern void quectel_qcops_flag_nv_init(void);
#endif // #if defined(QUECTEL_QCOPS_FEATURE)
//>2017/03/20-QCWXM9X00032-P01-wenxue.sheng

//<2017/07/01-QCM9X00108-P01-Aaron.Li, <[COMMON] Segment 1==> Add new cmd: AT+QINF.>
extern dsat_result_enum_type dsat_exec_qinf_cmd
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *pResBuf
);

//<2017/03/28-QCM9XTVG00066-P01-Vicent.Gao, <[EFS] Segment 1==>Add new cmd: QEFSVER>
extern boolean QL_QEFSVER_Init(void);

//<2017/07/29-QCM9X00099-P01-Aaron.Li, <[QCFG] Segment 1==> Add sub cmd: AT+QCFG="sim/clk_freg".>
#if defined(QUECTEL_SIM_FEATURE)
extern boolean QCFG_SIMCLFR_Init(void);
extern dsat_result_enum_type dsat_exec_qcfg_clk_freq_cmd
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *pResBuf
);
#endif // #if defined(QUECTEL_SIM_FEATURE)
//>2017/07/29-QCM9X00099-P01-Aaron.Li


extern dsat_result_enum_type quectel_exec_qefsver_cmd
(
  dsat_mode_enum_type mode, 			/*	AT command mode:			*/
  const dsati_cmd_type *parse_table,	/*	Ptr to cmd in parse table	*/
  const tokens_struct_type *tok_ptr,	/*	Command tokens from parser	*/
  dsm_item_type *pResBuf			/*	Place to put response		*/
);
//>2017/03/28-QCM9XTVG00066-P01-Vicent.Gao

//>2017/07/01-QCM9X00108-P01-Aaron.Li


#ifdef QUECTEL_AT_FORCE_RX_FEATURE_SUPPORT

dsat_result_enum_type quec_rx_exec_qrxftm_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
  );

#endif

//<2017/07/20-QCM9X00122-P01-Yosef.Zhang, <[COMMON] Segment 1==> Add new command:AT+QSVN.>
extern boolean QL_QSVN_Init();

extern dsat_result_enum_type dsat_exec_qsvn_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
//>2017/07/20-QCM9X00122-P01-Yosef.Zhang

/* - - - - - maxcodeflag20161108 begin - - - - - */
dsat_result_enum_type dsat_exec_qcfg_cdma_ppp_auth_cmd(QUEC_CMD_HDLR_PARAM);
/* - - - - - maxcodeflag20161108 end - - - - - */

#ifdef QUECTEL_FAST_DORMANCY
dsat_result_enum_type dsat_exec_qcfg_fast_dormancy_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#ifdef QUECTEL_REPLACE_OOS
dsat_result_enum_type dsat_exec_qcfg_quec_oos_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);	
#endif

#ifdef QUECTEL_QABOUT_ATC
dsat_result_enum_type quectel_exec_qabout_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif 
#ifdef QUECTEL_AT_AP_MODEM_NEW_VERSION
int quectel_ap_modem_version_storage (char *ap_version);
dsat_result_enum_type  quectel_exec_qapver_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
dsat_result_enum_type  quectel_exec_qapsub_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
dsat_result_enum_type  quectel_exec_qsub_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif

//<2018.3.26 Added by amos.zhang,Add hplmn search timer get&set function 
#ifdef QUECTEL_HPLMN_SEARCH_TIMER
dsat_result_enum_type dsat_exec_qcfg_hplmn_search_timer_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif
//>end

//<2017/08/30-QCM9X00138-P02-Aaron.Li, <[CALL] Segment 2==> Add new cmd: AT+CIREP.>
#if defined(QUECTEL_CALL_FEATURE)
extern boolean QL_CIREP_Init(void);
#endif // #if defined(QUECTEL_CALL_FEATURE)
//>2017/08/30-QCM9X00138-P02-Aaron.Li

//>2018/04/08-[Internal Request][Tracking Number :RQ0000025][URC][New Command:AT+QCFG="Feature_Switch_Flag"]-P05-jarvis.feng --Add new Command AT+QCFG="Feature_Switch_Flag" to solve whether report ETWS URC Begin
#if defined(QUECTEL_AT_COMMON_SUPPORT)

/*****************************************************************************
 * FUNCTION
 *  QL_QCFG_Feature_Switch_Flag_sWriteNv
 *
 * DESCRIPTION
 *  Write NV for AT+QCFG="Feature_Switch_Flag"
 *  
 * PARAMETERS
 *  void
 *
 * RETURNS
 *  'boolean' value
 *
 * NOTE
 *  
 *****************************************************************************/
static boolean QL_QCFG_Feature_Switch_Flag_sWriteNv(void);

/*****************************************************************************
 * FUNCTION
 *  QL_QCFG_Feature_Switch_Flag_sReadNv
 *
 * DESCRIPTION
 *  Read NV for Quectel Feature Switch(Quectel_Feature_Switch_Flag)
 *  
 * PARAMETERS
 *  void
 *
 * RETURNS
 *  'boolean' value
 *
 * NOTE
 *  
 *****************************************************************************/
static boolean QL_QCFG_Feature_Switch_Flag_sReadNv(void);

/*****************************************************************************
 * FUNCTION
 *  QL_QCFG_Feature_Switch_Flag_Init
 *
 * DESCRIPTION
 *  Initialize Quectel Feature Switch(Quectel_Feature_Switch_Flag)
 *  
 * PARAMETERS
 *  void
 *
 * RETURNS
 *  'boolean' value
 *
 * NOTE
 *  
 *****************************************************************************/
boolean QL_QCFG_Feature_Switch_Flag_Init(void);

 /*****************************************************************************
 * FUNCTION
 *  dsat_exec_qcfg_feature_switch_flag_cmd
 *
 * DESCRIPTION
 *  Command handler of AT+QCFG="Feature_Switch_Flag" to enable or disable some Quectel Feature
 *  
 * PARAMETERS
 *  <Mode>          [IN]
 *  <parse_table>   [IN]
 *  <tok_ptr>       [IN]
 *  <res_buff_ptr>  [OUT]
 *
 * RETURNS
 *  'dsat_result_enum_type' value
 *
 * NOTE
 *  
 *****************************************************************************/
dsat_result_enum_type dsat_exec_qcfg_feature_switch_flag_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);   
#endif
//>2018/04/08-[Internal Request][Tracking Number :RQ0000025][URC][New Command:AT+QCFG="Feature_Switch_Flag"]-P05-jarvis.feng --Add new Command AT+QCFG="Feature_Switch_Flag" to solve whether report ETWS URC End

//<2018/04/27-QCM9X00178C001-P02-Vicent.Gao, <[QVOLTEDBG] Segment 2==>Base code submit.>
/******************************************************************************************
Vicent.Gao-2018/04/28: Add QVOLTEDBG sub-cmd LTENAS
Refer to [Req-Depot].[RQ0000059][Submitter:2018-04-28,Date:2018-04-28]
<MDM9x07>
******************************************************************************************/
#if defined(QUECTEL_QVOLTEDBG_FEATURE)
extern boolean QL_QVOLTEDBG_Init(void);
#endif // #if defined(QUECTEL_QVOLTEDBG_FEATURE)
//>2018/04/27-QCM9X00178C001-P02-Vicent.Gao

//<2018/08/19-QCM9X00202-P01-Vicent.Gao, <[COMMON] Segment 1==> Add QCFG sub-cmd: hardcoded_ecc_list.>
/******************************************************************************************
Vicent.Gao-2018/08/19:Add new QCFG sub-cmd: hardcoded_ecc_list
Refer to [Req-Depot].[RQ0000164][Submitter:2018-08-19,Date:2018-08-19]
<MDM9x07>
******************************************************************************************/
#if defined(QUECTEL_CALL_FEATURE)
extern boolean QCFG_HCECC_Init(void);
extern dsat_result_enum_type dsat_exec_qcfg_hardcoded_ecc_list
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *pResBuf
);
#endif // #if defined(QUECTEL_CALL_FEATURE)
//>2018/08/19-QCM9X00202-P01-Vicent.Gao

#endif/*QUECTEL_COMMON_ATC_H*/
