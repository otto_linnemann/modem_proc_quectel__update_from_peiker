/***************************************************************************************
*
*			File Name:quectel_file_at.h
*
*		       Description:Deal with the file operation
*
*			Author:Gibson Pan(Pan Wei)
*		
*			Creat Time: 12 Nov. 2012
*
***************************************************************************************/


#ifndef __QUECTEL_FILE_AT__

#define __QUECTEL_FILE_AT__

#include "dsati.h"
#include "quectel_apptcpip_util.h"
#include "msg_diag_service.h"
#include "msgcfg.h"
#include "dsat_ext_api.h"
#include "ds3gsiolib_ex.h"
#include "dstask_v.h"
#include "fs_sys_types.h"
//#include "fs_vnode.h"
#include "dsm_queue.h"
#include "quectel_file_common.h"
#if defined(QUECTEL_FILE_FUNC)

#define QUECTEL_MAX_STR_NAME_LEN                10
#define FILE_MAX_NUMBER                         100
#define FILE_SIZE_DEFAULT                       10240//10KB
#define FILE_SIZE_MAX                           0x40000000//1GB,单次传输最大文件大小
#define TIME_OUT_DEFAULT                        5
#define TIME_OUT_MAX                            65535
#define QFUPL_PARAM_NUM					        4
#define QACK_VALUE_LIMIT					    1024
#define QFILE_TAG                               "panwei_file"
#define END_STR                                 "+++"
#define MAX_DWL_NUM                             2
#define MAX_UPL_NUM                             2
#define BYTES_LIMIT                             100
#define QFMOV_PARAM_NUM                         4
#define QFOPEN_PARAM_NUM                        2
#define QFCLOSE_PARAM_NUM                       1
#define QFPOSITION_PARAM_NUM                    1
#define QFFLUSH_PARAM_NUM                       1
#define QFTUCAT_PARAM_NUM                       1
#define QFSEEK_PARAM_NUM                        3
#define QFREAD_PARAM_NUM                        2
#define QFWRITE_PARAM_NUM                       3
#define FD_MAX                                  0x7fffffff
#define QF_WATERMARK_LOW                        204800//200KB，解除流控触发值
#define QF_WATERMARK_HIGH                       0x100000//1MB，使能流控触发值
#define QF_WATERMARK_MAX_LIMIT                  0x500000//5MB//0x7fffffff
#define QF_TX_WATERMARK_MAX_LIMIT               0x500000//5MB//0x7fffffff

//Add by Sundy 2016.3.25 for fix bug#712, add port num
#if defined(FEATURE_DATA_MUX)
#define DS_AT_PORT_NUM                                  9
#else
#define DS_AT_PORT_NUM                                  5
#endif

#define OK_STR	"OK"
#define ERROR_STR	"ERROR"
#define QSUCCESS	(0)
#define QFAILURE    	(-1)

#define READ_C	'r'
#define WRITE_C	'w'
#define INVALID_C '*'

typedef dsat_mode_enum_type (*at_escape_sequence_handler)( void );
typedef void (*dtr_changed_sig_handler_type)(boolean dtr_status);


typedef struct file_base_info
{
	char file_name[QFILE_MAX_PATH_LENGTH];
	dword file_size;
	dword ram_size_allocated;
}FILE_BASE_INFO;

typedef struct qfupl_param
{
    char file_name[QFILE_MAX_PATH_LENGTH];
    dsat_num_item_type file_size;
    //word time_out;
    dsat_num_item_type time_out;
    word ack_mode;	//since call the lib func, so use the unique func.
    dword size_uploaded;
    dword size_write;
    word checksum;
    word odd_byte;
    uint8 odd_flag;
    dword bytes_ack;
    ds3g_siolib_port_e_type port_id;
    q_type q_ptr;
    Q_INT32 fd;
    Q_UINT8 index;
    Q_UINT8 end;
    rex_timer_type timer;
    Q_UINT8 fupl_flag;
    Q_INT8 flow_flag;
    rex_crit_sect_type qfupl_crit_sect; //gibson:since it is used before, it will be abandoned later
    sio_rx_func_ptr_type rcv_func_reserved;	//now it is not used,but reserved
    dsat_mode_enum_type (*at_escape_sequence_handler_reserved)( void );
    void (*at_dtr_changed_sig_handler_reserved)( boolean dtr_status );//add by running,2016-10-13,for dtr exit
    Q_INT64 count;
    QF_FILETYPE fileType;
    dsm_watermark_type           tx_wm_ptr; 
    dsm_watermark_type           rx_wm_ptr; 
    q_type q_ptr_tx;
    q_type q_ptr_rx;
}QFUPL_PARAM;


typedef struct qfdwl_param
{
    Q_CHAR file_name[QFILE_MAX_PATH_LENGTH];
    dword size_downloaded;//已经读取/下载的数据字节数
    dword size;//命令请求的数据字节数
    word checksum;
    word odd_byte;
    uint8 odd_flag;
    Q_INT8 index;
    Q_INT8 end;//结束标志。当文件读到尾或者获取了已经请求的数据大小，或者中途临时退出均要置1
    Q_INT32 fd;
    Q_INT8 fdwl_flag;//0表示使用qfread，1表示使用qfdwl命令
    ds3g_siolib_port_e_type port_id;//传送数据的AT口
    rex_crit_sect_type qfdwl_crit_sect;
    dsat_mode_enum_type (*at_escape_sequence_handler_reserved)( void );
    void (*at_dtr_changed_sig_handler_reserved)(boolean dtr_status);
    dsm_watermark_type           tx_wm_ptr; 
    dsm_watermark_type           rx_wm_ptr; 
    q_type q_ptr_tx;
    q_type q_ptr_rx;
    dsat_cme_error_e_type code;
    QF_FILETYPE fileType;
    //Q_UINT8 sentReq;
}QFDWL_PARAM;

typedef union
{
	QFUPL_PARAM fupl;
	QFDWL_PARAM fdwl;
}FILE_T_PARAM;

typedef struct
{
	Q_CHAR op;
	FILE_T_PARAM fparam;
}DM_PARAM;


typedef enum
{
	QFALSE,
	QTRUE
}BOOL;


typedef struct
{
    ds3g_siolib_port_e_type port_id;
    dsat_mode_enum_type (*at_escape_sequence_handler_qfupl)( void );
    dsat_mode_enum_type (*at_escape_sequence_handler_qfdwl)( void );
    void (*fupl_func_ptr)(dsm_item_type **dsm_para);
    void  (*flush_func_ptr_cb)(void);
}ds_port_datamode_handler;



//Function

/*===========================================================================

FUNCTION DSATVEND_EXEC_QFLDS_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFLDS command.
  AT+QFLDS is used to query the memory space information specified by the parametes. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qflds_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION DSATVEND_EXEC_QFLST_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFLST command.
  AT+QFLST is used to query the files information of the memory space specified by the parametes. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qflst_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


/*===========================================================================

FUNCTION DSATVEND_EXEC_QFUPL_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFUPL command.
  AT+QFUPL is used to upload the file to the modem side. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfupl_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION DSATVEND_EXEC_QFDWL_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFDWL command.
  AT+QFDWL is used to download the file to the modem side. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfdwl_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


/*===========================================================================

FUNCTION DSATVEND_EXEC_QFDEL_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFDEL command.
  AT+QFDWL is used to delete the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfdel_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


#ifdef QUECTEL_FILE_DIR //add by running, 2016-09-02
dsat_result_enum_type dsatvend_exec_qfcwd_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


dsat_result_enum_type dsatvend_exec_qfmkd_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


dsat_result_enum_type dsatvend_exec_qfpwd_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);
#endif

#ifdef QUECTEL_UFS_FILE
/*===========================================================================
FUNCTION DSATVEND_EXEC_QFMOV_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFMOV command.
  AT+QFMOV is used to move the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfmov_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);
#endif
/*===========================================================================
FUNCTION DSATVEND_EXEC_QFOPEN_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFOPEN command.
  AT+QFOPEN is used to open the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfopen_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


/*===========================================================================
FUNCTION DSATVEND_EXEC_QFCLOSE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFCLOSE command.
  AT+QFCLOSE is used to close the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfclose_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================
FUNCTION DSATVEND_EXEC_QFPOSITION_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFPOSITION command.
  AT+QFPOSITON is used to get the offset of file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfposition_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


/*===========================================================================
FUNCTION DSATVEND_EXEC_QFFLUSH_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFFLUSH command.
  AT+QFFLUSH is used to fflush filesystem. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfflush_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================
FUNCTION DSATVEND_EXEC_QFTUCAT_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFTUCAT command.
  AT+QFTUCAT is used to truncate the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qftucat_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


/*===========================================================================
FUNCTION DSATVEND_EXEC_QFSEEK_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFSEEK command.
  AT+QFSEEK is used to seek the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfseek_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================
FUNCTION DSATVEND_EXEC_QFREAD_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFREAD command.
  AT+QFREAD is used to read the data from the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfread_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);


/*===========================================================================
FUNCTION DSATVEND_EXEC_QFWRITE_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+QFWRITE command.
  AT+QFWRITE is used to write the data from the file. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfwrite_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);



/*===========================================================================

FUNCTION dsat_ft_result_handler

DESCRIPTION
  file task notify the ds task with the result of handler.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsat_ft_result_handler
( 
 ds_cmd_type * cmd_ptr 
);

/*===========================================================================
FUNCTION dsatvend_exec_qfdelrootfile_cmd

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+qfdelrootfile command.
  AT+qfdelrootfile is used to delete the file in the rootdir. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qfdelrootfile_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================
FUNCTION dsatvend_exec_qusbdelaytime_cmd

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes at+qusbdelaytime command.
  AT+qusbdelaytime is used to set USB delay time. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK : If the command has been successfully executed
    DSAT_ERROR : If there was any problem in executing the command

SIDE EFFECTS
  None
  
===========================================================================*/
dsat_result_enum_type dsatvend_exec_qusbwaittime_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

Q_INT8 checkStrValid(Q_CHAR *ptr);

#ifdef QUECTEL_BACKUP_CEFS
dsat_result_enum_type dsatvend_exec_qprtpara_cmd
(
    dsat_mode_enum_type mode,                /*  AT command mode:            */
    const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr              /*  Place to put response       */
);
#endif

#endif

#ifdef QUECTEL_CEFS_COMPLETENESS_CHECK
dsat_result_enum_type dsatsimdet_exec_efsDirCheck_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#endif
