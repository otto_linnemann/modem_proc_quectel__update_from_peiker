/*===========================================================================

                           QUECTEL_ECC_ATC . H

DESCRIPTION
  ECC atc function

REFERENCES

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2014 QUECTEL Incorporated.
All rights reserved.
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
09/10/2014 ECC atc function add by Rex
11/10/2018 vicent add 000 as emergency number of no sim, add at+qeccnum=4 to set factory value

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "dsat_v.h"
#include "dsati.h"
#include "../../../datamodem/interface/atcop/src/dsatme.h"
#include "dsat_ext_api.h"

#ifdef QUECTEL_CALL_FEATURE
#define ECC_PROGRAM_MAX_NUM 20
#define ECC_PROGRAM_MAX_ECC_LENGTH 6 /*include '\0'*/

//<2019/03/15-QCM9XTVG00065-P04-Vicent.Gao, <[ECC] Segment 4==>Fix QECCNUM issues.>
#define QECCNUM_EFS_FACTORY_VER       "qeccnum_factory_ver"
#define QECCNUM_EFS_FACTORY_VER_SIZE  10      //EX: 201903151
#define QECCNUM_CUR_FACTORY_VER       "201903151"
//>2019/03/15-QCM9XTVG00065-P04-Vicent.Gao

typedef enum ecc_operater_e
{
    DSAT_ECC_OPERATER_QUERY= 0,
    DSAT_ECC_OPERATER_ADD = 1,
    DSAT_ECC_OPERATER_DELETE= 2,
    DSAT_ECC_OPERATER_REPLACE = 3,
    //<2018/11/09-QCM9XTVG00065-P03-Vicent.Gao, <[ECC] Segment 3==>Fix QECCNUM issues.>
    DSAT_ECC_OPERATER_SET_FACTORY = 4,
    //>2018/11/09-QCM9XTVG00065-P03-Vicent.Gao
    
    DSAT_ECC_OPERATER_MAX
}dsat_ecc_operater_e;

typedef enum ecc_type_e
{
    DSAT_ECC_TYPE_WITH_NO_SIM = 0,
    DSAT_ECC_TYPE_WITH_SIM = 1,
    DSAT_ECC_TYPE_MAX
}dsat_ecc_type_e;

typedef struct quectel_ecc_table
{
    uint8                dial_string[ECC_PROGRAM_MAX_ECC_LENGTH];   //ecc string number.
    uint8                 string_length;  // ecc string length
    boolean             already_in_cache;  //ecc number is in cache or not
}QUECTEL_ECC_TABLE;

/*===========================================================================

FUNCTION QUECTEL_INIT_ECC_TABLE

DESCRIPTION
  This function initiation ecc number table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
void quectel_init_ecc_table(dsat_ecc_type_e ecc_type);

/*===========================================================================

FUNCTION QUECTEL_SET_DEFAULT_ECC_NUM_IN_TABLE

DESCRIPTION
  This function set default ecc number into ecc number table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
void quectel_set_default_ecc_num_in_table(dsat_ecc_type_e ecc_type);

/*===========================================================================

FUNCTION QUECTEL_SET_ECC_TABLE_TO_NV

DESCRIPTION
  This function set ecc numbers into nv.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
boolean quectel_set_ecc_table_to_nv(void);

/*===========================================================================

FUNCTION QUECTEL_SET_NV_ECC_NUM_IN_TABLE

DESCRIPTION
  This function set nv ecc numbers to ecc number table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
void quectel_set_nv_ecc_num_in_table(void);

/*===========================================================================

FUNCTION QUECTEL_ECC_NUMBER_INIT

DESCRIPTION
  This function ecc number init function.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
extern void quectel_ecc_number_init(void);

/*===========================================================================

FUNCTION QUECTEL_GET_ECC_TABLE_SIZE

DESCRIPTION
  This function get length of ecc number table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
uint8 quectel_get_ecc_table_size(QUECTEL_ECC_TABLE *ecc_table);

/*===========================================================================

FUNCTION QUECTEL_JUDGE_ECC_TABLE_SIZE

DESCRIPTION
  This function judge length of ecc number table whether large the max of ecc table or not.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
boolean quectel_judge_ecc_table_size(dsat_ecc_type_e ecc_type, uint8 new_ecc_size);

/*===========================================================================

FUNCTION QUECTEL_GET_ECC_TYPE_ECC_NUM

DESCRIPTION
  This function get ecc numbers of with_sim or with_no_sim type.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
void quectel_get_ecc_type_ecc_num(dsat_ecc_type_e ecc_type, dsm_item_type *res_buff_ptr);

/*===========================================================================

FUNCTION QUECTEL_IS_ECC_NUM_IN_TABLE

DESCRIPTION
  This function judge the input ecc number whether is ecc number in table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
boolean quectel_is_ecc_num_in_table(dsat_ecc_type_e ecc_type, unsigned char * ecc_string);

/*===========================================================================

FUNCTION QUECTEL_ADD_ECC_NUM_INTO_TABLE

DESCRIPTION
  This function add the input ecc number into specific type ecc number table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
boolean quectel_add_ecc_num_into_table(dsat_ecc_type_e ecc_type, unsigned char *ecc_string);

/*===========================================================================

FUNCTION QUECTEL_DELETE_ECC_NUM_INTO_TABLE

DESCRIPTION
  This function delete the input ecc number from specific type ecc number table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
boolean quectel_delete_ecc_num_from_table(dsat_ecc_type_e ecc_type, unsigned char *ecc_string);

/*===========================================================================

FUNCTION quectel_keep_reserved_ecc_num_into_table

DESCRIPTION
  This function clear but keep 112/911 in the table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
boolean quectel_keep_reserved_ecc_num_into_table(dsat_ecc_type_e ecc_type);

/*===========================================================================

FUNCTION DSATETSICALL_EXEC_QECCNUM_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QECCNUM command.
  This command provides the ability to query, add and delete 
  the user programming ecc numbers. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
extern dsat_result_enum_type dsatetsicall_exec_qeccnum_cmd
(
  dsat_mode_enum_type mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,      /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION PBM_ECC_SET_USER_ECC

DESCRIPTION
  This function set user ecc number into pbm.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
extern void pbm_ecc_set_user_ecc(boolean uim_present);

//<2016/08/24-QCM9XTVG00051-P01-Vicent.Gao, <[CALL] Segment 1==>Add EMER-CALL common function definition.>
extern boolean QL_QECCNUM_AddGroup(uint8 *pGp,uint16 uEccMaxSize,uint16 uCnt,uint8 uType);
extern boolean QL_QECCNUM_AddGroupNoSim(uint8 *pGp,uint16 uCnt);
extern boolean QL_QECCNUM_AddGroupWtSim(uint8 *pGp,uint16 uCnt);
//>2016/08/24-QCM9XTVG00051-P01-Vicent.Gao

//<2018/11/09-QCM9XTVG00065-P03-Vicent.Gao, <[ECC] Segment 3==>Fix QECCNUM issues.>
#define ECC_TRACE_0(f)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),ECC_" f,__FUNCTION__,__LINE__)
#define ECC_TRACE_1(f,g1)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),ECC_" f,__FUNCTION__,__LINE__,g1)
#define ECC_TRACE_2(f,g1,g2)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),ECC_" f,__FUNCTION__,__LINE__,g1,g2)
#define ECC_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_5(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),ECC_" f,__FUNCTION__,__LINE__,g1,g2,g3)
#define ECC_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_6(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),ECC_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4)
#define ECC_TRACE_5(f,g1,g2,g3,g4,g5)  MSG_SPRINTF_7(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),ECC_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5)
#define ECC_TRACE_6(f,g1,g2,g3,g4,g5,g6)  MSG_SPRINTF_8(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),ECC_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5,g6)
//>2018/11/09-QCM9XTVG00065-P03-Vicent.Gao

#endif
