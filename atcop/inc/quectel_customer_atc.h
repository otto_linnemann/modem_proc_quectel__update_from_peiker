#ifndef QUECTEL_CUSTOMER_ATC_H
#define QUECTEL_CUSTOMER_ATC_H

#include "dsati.h"
#include "dsat_v.h"

#ifdef QUECTEL_CUSTOMER_DMCUSREX
dsat_result_enum_type dsat_exec_dmcusrex_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_CUSTOMER_MYSYSINFO

extern uint16 quec_get_mysysinfo_sysmode(void);
extern void quec_get_mysysinfo_mnc(uint32 *mcc, uint32 *mnc);

dsat_result_enum_type dsat_exec_mysysinfo_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#endif /* QUECTEL_CUSTOMER_ATC_H */

