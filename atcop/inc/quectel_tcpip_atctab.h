#ifndef QUECTEL_TCPIP_ATCTAB_H
#define  QUECTEL_TCPIP_ATCTAB_H

#include "dsati.h"

#if 0
#if defined(QUECTEL_SOC_APP_SUPPORT)
extern const dsati_cmd_type dsat_tcpip_action_table [];
extern const unsigned int dsat_tcpip_action_table_size;

extern const dsati_cmd_ex_type dsat_tcpip_action_table_ex [];

#endif/*QUECTEL_SOC_APP_SUPPORT*/
#endif

#endif/*QUECTEL_TCPIP_ATCTAB_H*/
