/*
	2015-12-28 created by Jacky.zhang
 */

#ifndef __QUECTEL_GPS_ATC_H__
#define __QUECTEL_GPS_ATC_H__

#include "dsati.h"
#include "QuecOEM_feature.h"

#ifdef QUECTEL_GPS_SUPPORT

#ifdef QUECTEL_GPS_XTRA_SUPPORT
#include "../../../gps/gnss/inc/pdapi.h"
#endif

#define QUECTEL_NONE_PORT            0
#define QUECTEL_USBNMEA_PORT         1
#define QUECTEL_UARTDEBUG_PORT       2

extern dsat_result_enum_type dsat_gps_qgps_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpscfg_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsend_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_qgps_oper_result_handler
(
  ds_cmd_type *cmd_ptr      /* DS Command pointer */
);

extern dsat_result_enum_type dsat_qgps_urc_cmd_handler
(
  ds_cmd_type          *cmd_ptr              /* DS Command pointer         */
);

dsat_result_enum_type dsat_qgps_nmea_port_ctrl_handler
(
  ds_cmd_type          *cmd_ptr              /* DS Command pointer         */
);


dsat_result_enum_type dsat_gps_qgpsxtra_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsxtratime_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsxtradata_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsdel_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:				*/
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table 	  */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 		  */
);

extern dsat_result_enum_type dsat_gps_qgpsxtraurl_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsxtrasntp_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsxtratauto_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsxtraupl_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

#ifdef QUECTEL_GPS_XTRA_SUPPORT
extern void dast_lbs_xtra_events_cb
(
	void *p_data,
	pdsm_xtra_event_type  events,
	const pdsm_xtra_info_s_type *p_xtrainfo
);
#endif
extern dsat_result_enum_type dsat_gps_qgpssuplurl_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpssuplca_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsloc_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsgnmea_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpslock_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_gps_qgpsaccept_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern void qgps_set_agps_string_gsm_encoding(boolean enc);
extern boolean qgps_get_agps_string_gsm_encoding(void);


// add by herry.Geng on 20190408 for supporting lwm2m
#ifdef QUECTEL_LWM2M_SUPPORT
extern void  ql_get_gps_location(float *latitude,float *longitude);
#endif


#endif /* QUECTEL_GPS_SUPPORT */
#endif /*__QUECTEL_GPS_ATC_H__*/
