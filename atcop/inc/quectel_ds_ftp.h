#ifndef __QUECTEL_DS_FTP_H__
#define __QUECTEL_DS_FTP_H__

#include "quectel_apptcpip_util.h"
#include "queue.h"
#include "dsm_queue.h"
#include "ds3gsiolib.h"

#if defined(QUECTEL_FTP_APP_SUPPORT)

typedef enum
{
  DS_FTP_OK              = 0,
  DS_FTP_ERROR           = 601,
  DS_FTP_WOULDBLOCK      = 602,
  DS_FTP_BUSY            = 603,
  DS_FTP_ERROR_DNS       = 604,   /*failed to parse domain name*/
  DS_FTP_ERROR_NETWORK   = 605,   /*failed to establish socket or the network is deactivated*/
  DS_FTP_CTRL_CLOSE      = 606,   /*the ftp session has been closed by the ftp server*/
  DS_FTP_DATA_CLOSE      = 607,   /*the data connection was closed by the ftp server*/
  DS_FTP_BEARER_FAIL     = 608,
  DS_FTP_ERROR_TIMEOUT   = 609,
  DS_FTP_INVALID_PARAM   = 610,
  DS_FTP_FILE_NOT_FOUND  = 611,
  DS_FTP_FILE_POINT_ERR  = 612,
  DS_FTP_FS_FILE_ERROR   = 613,
  DS_FTP_SERVICE_END     = 614,
  DS_FTP_DATA_FAILED     = 615,
  DS_FTP_CLOSE_CONN      = 616,
  DS_FTP_FILE_UNOPER     = 617,
  DS_FTP_REQUEST_ABORT   = 618,
  DS_FTP_OVER_MEMORY     = 619,
  DS_FTP_COMMAND_ERROR   = 620,
  DS_FTP_PARAM_ERROR     = 621,
  DS_FTP_COMMAND_FAILED  = 622,
  DS_FTP_COMMAND_BAD_SEQUENCE = 623,
  DS_FTP_COMMAND_NOT_IMPLENT = 624,
  DS_FTP_ERROR_UNLOGIN   = 625,
  DS_FTP_NO_ACCOUNT      = 626,
  DS_FTP_REQUEST_FAILED  = 627,
  DS_FTP_REQUEST_STOP    = 628,
  DS_FTP_FILEREQ_STOP    = 629,
  DS_FTP_FILENAME_ERROR  = 630,
  DS_FTP_SSL_AUTH_FAIL   = 631,
}quectel_ds_ftp_error_code;

typedef enum
{
    DS_FTP_CMD_NONE = 0,
	 DS_FTP_CMD_QFTPCFG,
    DS_FTP_CMD_QFTPOPEN,
    DS_FTP_CMD_QFTPCLOSE,
    DS_FTP_CMD_QFTPGET,
    DS_FTP_CMD_QFTPPUT,
    DS_FTP_CMD_QFTPSIZE,
    DS_FTP_CMD_QFTPCWD,
    DS_FTP_CMD_QFTPPWD,
    DS_FTP_CMD_QFTPDEL,
    DS_FTP_CMD_QFTPMKDIR,
    DS_FTP_CMD_QFTPRMDIR,
    DS_FTP_CMD_QFTPRENAME,
    DS_FTP_CMD_QFTPLIST,
    DS_FTP_CMD_QFTPNLST,
    DS_FTP_CMD_QFTPMLSD,
    DS_FTP_CMD_QFTPMDTM,
}quectel_ds_ftp_cmd;

typedef enum
{
	DS_FTP_NONE_EVENT = 0,
   DS_FTP_OPEN_EVENT,
	DS_FTP_CLOSE_EVENT,
	DS_FTP_FILE_SIZE_EVENT,
	DS_FTP_ENTER_DATA_MODE_EVENT,
	DS_FTP_FLUSH_SIO_TX_EVENT,
	DS_FTP_EXIT_DATA_MODE_EVENT,
	DS_FTP_OPEN_LOCAL_FILE_EVENT,
	DS_FTP_CLOSE_LOCAL_FILE_EVENT,
	DS_FTP_DL_DATA_IND_EVENT,
	DS_FTP_FILE_GET_EVENT,
	DS_FTP_UL_DATA_IND_EVENT,//10
	DS_FTP_FILE_PUT_EVENT,
	DS_FTP_FILE_DEL_EVENT,
	DS_FTP_SET_DIR_EVENT,
	DS_FTP_GET_DIR_EVENT,
	DS_FTP_MAKE_DIR_EVENT,
	DS_FTP_RM_DIR_EVENT,
	DS_FTP_RENAME_EVENT,
	DS_FTP_MODTIME_EVENT,
	DS_FTP_LIST_EVENT,
	DS_FTP_NLST_EVENT,
	DS_FTP_MLSD_EVENT,
}quectel_ds_ftp_event;


typedef enum
{
	FTP_CFG_ACCOUNT = 0,
	FTP_CFG_TRANSFER_TYPE,
	FTP_CFG_TRANSFER_MODE,
	FTP_CFG_PDP_CID,
	FTP_CFG_TIMEOUT,
	FTP_CFG_SSL_TYPE,
	FTP_CFG_SSL_CTX_ID,
	MAX_FTP_CFG_TYPE
}ds_ftp_cfg_type;


typedef struct
{
	 int32                               result;
	 uint32                             ftp_stat;
	 uint32                             ftp_session_type; // 0: control session 1:data session
	 uint32                             ftp_protocol_reply_code;
	 uint8                               sio_port;
	 uint8                               exit_data_mode_cause;
	 uint32                             file_size;
	 byte                                current_dir[256];
	 byte                                file_modtime[128];
}quectel_ds_ftp_event_info;

typedef struct
{
	ds3g_siolib_port_e_type             sio_port;
	byte                                          username[256];
	byte                                          password[256];
	byte                                          local_file[256];
	Q_INT32                                    local_file_handle;
	int32                                         transfer_type;
	int32                                         transfer_mode;
	int32                                         ftp_timeout;
	int32                                         pdp_cid;
	int32                                         ssl_type;
	int32                                         ssl_ctx_id;
       Q_INT32                                   ftp_ctx;
	boolean                                     ftp_opened;
	int32                                         last_ftp_cmd;
	dsm_watermark_type                 ds_ftp_msg_wm;
       q_type                                      ds_ftp_msg_q;
	uint8                                          escape_cause;
	int32                                          escape_result;
	int32                                          break_ftp_cmd;
	uint32                                         dl_offset;
	uint32                                         current_dl_len;
	uint32                                         ul_offset;
	uint32                                         current_ul_len;
	boolean                                       ul_block;
	dsm_item_type                           *ul_data_ptr;
	dsm_watermark_type                  sio_rx_wm;
	q_type                                        sio_rx_q;
	boolean                                      sio_tx_wm_high;
	dsm_watermark_type                  sio_tx_wm;	
	q_type                                        sio_tx_q;
	boolean                                       wait_ds_result;
	uint32                                         data_transfer_len;
	boolean                                       last_ul_pkt;//fix QC1813
}quectel_ds_ftp_mgr;

dsat_result_enum_type  ds_ftp_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_open
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_close
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_size
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_stat
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_get_transfer_data_len
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);


dsat_result_enum_type  ds_ftp_delete
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_get
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);


dsat_result_enum_type  ds_ftp_put
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_set_current_directory
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_get_current_directory
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_mkdir
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_rmdir
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_rename
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_get_last_modify_time
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_list
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_nlst
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_mlsd
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_ftp_ato_handler
(
void
);

dsat_result_enum_type ds_ftp_signal_handler
(
 dsat_mode_enum_type at_mode
);


#endif
#endif//__QUECTEL_DS_FTP_H__
