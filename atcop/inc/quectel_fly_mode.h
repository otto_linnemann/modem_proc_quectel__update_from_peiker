#ifndef QUECTEL_FLY_MODE_H
#define QUECTEL_FLY_MODE_H

#define TYPE_INTERRUPT_REGISTER                 0   /*注册中断*/
#define TYPE_INTERRUPT_DEREGISTER               1   /*反注册中断*/
#define TYPE_INTERRUPT_REVERSE                  2   /*反转中断极性*/
#define TYPE_INTERRUPT_MASK                     3   /*mask */
#define TYPE_INTERRUPT_UNMASK                   4   /*unmask */
#define TYPE_INTERRUPT_UNMASK_AND_REVERSE                   5   /*unmask and reverse */

typedef enum fly_mode_e
{
    DSAT_FLYMODE_OUT = 0,
    DSAT_FLYMODE_IN = 1,
    DSAT_FLYMODE_MAX
}dsat_fly_mode_e;

#ifdef QUECTEL_DEBOUNCE_FLY_MODE
typedef void (*interrupt_end_handler_type)(boolean is_success, uint32 gpio_encode, boolean stable_value);

typedef struct
{
    interrupt_end_handler_type  interrupt_end_handler;
    uint32 period;
    uint32 max_count;
    uint32 gpio_encode;
    boolean stable_value;
    timer_type  debounce_timer;
    uint32 current_count;

}dsat_quec_sw_debounce_struct;
#endif

extern boolean airplane_status;

extern boolean airplane_control_pin_enable;

extern boolean quectel_flymode_change_end(boolean is_success , boolean is_in_flymode);

extern void *quectel_flymode_isr ( uint32 parm );

extern boolean quectel_flymode_change(boolean fly_mode_enter);

extern void quectel_flymode_nv_init( void );

extern void quectel_send_flymode_request(boolean flymode_entered);

extern dsat_result_enum_type dsat_change_flymode
(
  dsat_fly_mode_e flymode_type
);

dsat_result_enum_type quectel_flymode_change_request_handler
(
 ds_cmd_type * cmd_ptr     /* DS Command pointer */
);

extern void quectel_fly_mode_config_init( void );

extern boolean quectel_get_airplane_control_enable( void );

extern void quectel_set_airplane_control_enable(uint8 airplanecontrol);

extern boolean quectel_get_airplane_status( void );

#ifdef QUECTEL_DEBOUNCE_FLY_MODE //maxcodeflag20170302

void quectel_flymode_interrupt_unmask_with_debouce(void);

boolean quec_sw_start_debounce
(
        interrupt_end_handler_type  interrupt_end_handler,
        uint32 period,
        uint32 max_count,
        uint32 gpio_encode,
        boolean stable_value
);
#endif
#endif//QUECTEL_FLY_MODE_H