#ifndef QUECTEL_DS_STK_H
#define  QUECTEL_DS_STK_H

#if defined(QUECTEL_STK_SUPPORT)


typedef enum
{
	QUECTEL_DS_STK_EVENT_INDICATION= 0,
	QUECTEL_DS_STK_ENVELOPE_CMD_RSP = 1,
	QUECTEL_DS_STK_CMD_TIMEOUT = 2,
	QUECTEL_DS_STK_SEND_SMS_CMD_RSP = 3,
	QUECTEL_DS_STK_SETUP_CALL_CMD_RSP = 4,
	QUECTEL_DS_STK_SEND_SS_CMD_RSP = 5,
	QUECTEL_DS_STK_SEND_USSD_CMD_RSP = 6,

	//<2015/09/30-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> RAW DATA develop init.>
	QUECTEL_DS_STKRAW_SEND_PROACTIVE_CMD_IND = 7,  
	//>2015/09/30-QCM4TVG00004C02-P01-Vicent.Gao
}quectel_ds_stk_msg_id;

typedef dsat_result_enum_type(*ds_stk_msg_handler)
(
quectel_ds_stk_msg_id   msg_id,
dsm_item_type  *msg_data_ptr
);

typedef struct
{
	quectel_ds_stk_msg_id      msg_id;
	ds_stk_msg_handler     msg_handler_fcn;
}ds_stk_msg_handler_tab_type;

void quectel_ds_stk_init
(
);

void quectel_ds_stk_proactive_cmd_ind
(
uint16  stk_pro_cmd_id
);

void quectel_ds_stk_envelope_cmd_rsp
(
uint16  rsp_code
);

void quectel_ds_stk_send_sms_cmd_rsp
(
uint16  rsp_code
);

void quectel_ds_stk_setup_call_cmd_rsp
(
uint16  rsp_code
);

void quectel_ds_stk_send_ss_cmd_rsp
(
uint16  rsp_code
);

void quectel_ds_stk_send_ussd_cmd_rsp
(
uint16  rsp_code
);


void quectel_ds_stk_proactive_cmd_timeout_ind
(
uint16  stk_pro_cmd_id
);

dsat_result_enum_type quectel_ds_stk_sim_card_remove_ind
(
);

dsat_result_enum_type ds_stk_signal_handler
(
 dsat_mode_enum_type at_mode
);

dsat_result_enum_type quectel_ds_stk_cfg
(
 const tokens_struct_type *tok_ptr,
 int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_ds_stk_get_cmd_info
(
 const tokens_struct_type *tok_ptr,
 int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_ds_stk_terminal_cmd
(
 const tokens_struct_type *tok_ptr,
 int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

//<2015/10/06-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> RAW DATA develop init.>
extern dsat_result_enum_type QL_STKRAW_SendTerminalResponse
(
const tokens_struct_type *tok_ptr,
int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type QL_STKRAW_SendEnvelopeCommand
(
const tokens_struct_type *tok_ptr,
int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type QL_STKRAW_GetMainMenu
(
const tokens_struct_type *tok_ptr,
int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
//>2015/10/06-QCM4TVG00004C02-P01-Vicent.Gao

dsat_result_enum_type quectel_ds_stk_get_terminal_profile
(
 const tokens_struct_type *tok_ptr,
 int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_ds_stk_get_state
(
 const tokens_struct_type *tok_ptr,
 int32  arg_index,
dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif/*QUECTEL_STK_SUPPORT*/

#endif/*QUECTEL_DS_STK_H*/
