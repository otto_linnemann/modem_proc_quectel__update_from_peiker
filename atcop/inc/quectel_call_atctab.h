/*===========================================================================

                           QUECTEL_CALL_ATCTAB . H

DESCRIPTION
  CALL atc table function

REFERENCES

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
12/7/2013 CALL atc table declare add by Clark.li 

===========================================================================*/
#ifndef __QUECTEL_CALL_ATCTAB_H__
#define __QUECTEL_CALL_ATCTAB_H__

#include "dsati.h"

#ifdef QUECTEL_CALL_FEATURE
extern const dsati_cmd_type dsat_quectel_call_action_table[];
extern const unsigned int dsat_quectel_call_action_table_size;
extern const dsati_cmd_ex_type dsat_quectel_call_action_table_ex[];
#endif /* QUECTEL_CALL_FEATURE */

#endif /* __QUECTEL_CALL_ATCTAB_H__ */
