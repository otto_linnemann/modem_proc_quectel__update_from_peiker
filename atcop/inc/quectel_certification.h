#ifndef QUECTEL_CERTIFICATION_H
#define QUECTEL_CERTIFICATION_H

/* Modification contents:
   [ID] [Name] [Date] [SearchFlag] : [ForWhat]. just like below:
   [066] [MaxTANG] [2016-06-06] [maxcodeflag20160606] : [solve a major bug that will cause memory leak] 
*/

/* - - - - - - - - - - - - - - Modification Descriptions - - - - - - - - - - - - - - 
[000] [MaxTANG] [2016-06-24] [maxcodeflag20160624] : [添加TELSTRA运营认证时对于PDP激活动作限制功能。详细如下:
法PDP激活被拒且原因码是8/29/32/33时，再次发起激活PDP的动作必须以5S/10S/28S/80S/4MIN/12MIN的间隔为限制。]


*/

#include "dsati.h"

#ifdef QUECTEL_CERTIFICATION_FEATURE

#define QUEC_CERTI_OK       0
#define QUEC_CERTI_FAIL     -1

#ifdef QUECTEL_CERTIFICATION_TELSTRA
/*
  开机初始化时调用，对相关全局变量进行实始化
*/
extern void Quectel_TelstraRuleInit(void);

#ifdef QUECTEL_CERTIFICATION_TELSTRA_PROFILE
/*
 当有PDP激活动作发起时，调用该函数检查当前是否处于TELSRTA rule限制中
*/
extern boolean Quectel_IsPDPActAllowedByTelstra(void);

/*
 当有PDP激活动作有结果时，调用该函数来检查是否需要启动TELSTRA规定的相关timer
*/
extern void Quectel_TelstraPDPActRuleCheck(sm_status_T status);
#endif

#endif

dsat_result_enum_type dsat_exec_qcertiop_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif

#endif
