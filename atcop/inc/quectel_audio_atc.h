/*
 *  AT command handler declarations about AUDIO.
 *
 *  design by	: jun.wu
 *  date      	: 2016-01-11 
 *
 */

#ifndef __QUECTEL_AUDIO_ATC_H__
#define __QUECTEL_AUDIO_ATC_H__

#include "dsati.h"
#include "quectel_ati.h"

//<2016/10/10-QCWXM9X00017-P01-wenxue.sheng, <[QCFG] Segment 1==> Add AT+QCFG="amrcodec".>
#ifdef QUECTEL_AMRCODEC_CONFIG
#define QIPCALL_CODEC_MODE_SET_AMR_WB       "/nv/item_files/ims/qipcall_codec_mode_set_amr_wb"
#define QIPCALL_OCTET_ALIGNED_MODE_AMR_WB   "/nv/item_files/ims/qipcall_octet_aligned_mode_amr_wb"
        
#define QIPCALL_CODEC_MODE_AMR_WB_BIT_MASK         (1 << 4)
#define QIPCALL_OCTET_ALIGNED_MODE_AMR_WB_BIT_MASK (1 << 5)
#endif // #if defined(QUECTEL_AMRCODEC_CONFIG)
//>2016/10/10-QCWXM9X00017-P01-wenxue.sheng

//<2016/10/10-QCWXM9X00017-P01-wenxue.sheng, <[QCFG] Segment 1==> Add AT+QCFG="amrcodec".>
#ifdef QUECTEL_AMRCODEC_CONFIG
extern byte	gsm_wcdma_amr_codec_preference;
#endif // #if defined(QUECTEL_AMRCODEC_CONFIG)
//>2016/10/10-QCWXM9X00017-P01-wenxue.sheng

#ifdef QUECTEL_AUDIO_AT_CMD
extern dsat_result_enum_type dsat_audio_qiic_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qaudloop_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qdai_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_cmut_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_clvl_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qsidet_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qaudmod_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qeec_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qmic_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

dsat_result_enum_type dsat_audio_qrxgain_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qaudrd_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qaudplay_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);

extern dsat_result_enum_type dsat_audio_qaudstop_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);
//end jun.wu

#ifdef QUECTEL_TTS_SUPPORT //2016-06-25 add by running.qian
 dsat_result_enum_type dsat_audio_qtts_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

 dsat_result_enum_type dsat_audio_qttsetup_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_AUDIO_TONE_DTMF
dsat_result_enum_type dsat_audio_qltone_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:				*/
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table 	  */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 		  */
);

dsat_result_enum_type dsat_audio_qldtmf_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:				*/
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table 	  */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 		  */
);

dsat_result_enum_type dsat_audio_qaudcfg_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:				*/
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table 	  */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 		  */
);

dsat_result_enum_type dsat_audio_qtonedet_cmd
(
	dsat_mode_enum_type mode,			  /*  AT command mode:			  */
	const dsati_cmd_type *parse_table,	  /*  Ptr to cmd in parse table   */
	const tokens_struct_type *tok_ptr,	  /*  Command tokens from parser  */
	dsm_item_type *res_buff_ptr 		  /*  Place to put response 	  */
);
#endif
#ifdef QUECTEL_QPCMV_AT_CMD //add by becking.bai,bflag20161215
 dsat_result_enum_type dsat_audio_qpcmv_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif


#endif /* QUECTEL_AUDIO_AT_CMD */

#endif /*__QUECTEL_AUDIO_ATC_H__*/

