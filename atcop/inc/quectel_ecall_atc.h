/*===========================================================================

                           QUECTEL_ECALL_EXT_AT . H

DESCRIPTION
  eCall extern at function

REFERENCES
  3GPP TS 26.267

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary 

EDIT HISTORY FOR MODULE
03/13/2015 Rex add for extern ecall function

===========================================================================*/
#ifndef __QUECTEL_ECALL_EXT_AT_H__
#define __QUECTEL_ECALL_EXT_AT_H__

#include "comdef.h"
#include "customer.h"
#include "dsati.h"
#include "dsat_v.h"
#include "cm.h"
#include "msg_diag_service.h"
#include "ecall_app.h"

#include "dsat_ext_api.h"
#include "ecall_efs.h"
#include "dstask_v.h"
#include "dsatvend.h"

#if defined(QUECTEL_ECALL_EXT)
 typedef  unsigned long long   QUINT64;
 typedef  unsigned long int     QUINT32;
 typedef  unsigned short        QUINT16;
 typedef  unsigned char         QUINT8;
 typedef  signed long long      QINT64;
 typedef  signed long int        QINT32;
 typedef  signed short           QINT16;
 typedef  signed char            QINT8;
 typedef  unsigned char         QBOOL ;
 typedef  signed char            QCHAR;

#define QECALL_TAG            "eCall"



/*===========================================================================

FUNCTION DSATETSICALL_EXEC_QECCFG_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QECCCFG command.
  This command provides the ability to set ecall configuration. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicall_exec_qeccfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);



/*===========================================================================

FUNCTION DSATETSICALL_EXEC_QECMSD_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QECMSDDATA command.
  This command provides the ability to set whole msd data string. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicall_exec_qecmsd_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);




dsat_result_enum_type dsatetsicall_exec_qecpush_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);


/*===========================================================================

FUNCTION DSATETSICALL_EXEC_QECALL_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QECALL command.
  This command provides the ability to make a eCall to the network. 

dsat_qecall_val[0]  -> Start/Stop the eCall 0 - Stop ; 1 - Start
dsat_qecall_val[1]  -> Type of the eCall    0 - Test ; 1 - Emergency 2 - Reconfigure
dsat_qecall_val[2]  -> Activation type      0 - Manual; 1- Automatic

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicall_exec_qecall_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

/*===========================================================================
FUNCTION DSATETSICALL_QECALL_AT_CMD_HANDLER

DESCRIPTION
  This function is handler function for reporting qecall session response.

DEPENDENCIES
  None
  
RETURN VALUE
  DSAT_ASYNC_EVENT: Always Returns ASYNC_EVENT.
 
SIDE EFFECTS
  None
  
======================================================================*/
dsat_result_enum_type dsatetsicall_qecall_at_cmd_handler
(
  ds_cmd_type         * cmd_ptr              /* DS Command pointer         */
);
#endif
#endif

