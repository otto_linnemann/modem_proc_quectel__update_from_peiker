/*==========================================================================
 |              QUECTEL - Build a smart world.
 |
 |              Copyright(c) 2017 QUECTEL Incorporated.
 |
 |--------------------------------------------------------------------------
 | File Description
 | ----------------
 |      This file defines declaration for QUECTEL EFS DEFAULT use
 |
 |--------------------------------------------------------------------------
 |
 |  Designed by     :   Aaron LI
 |  Coded    by     :   Aaron LI
 |  Tested   by     :   Aaron LI
 |--------------------------------------------------------------------------
 | Revision History
 | ----------------
 |  2017/09/29       Aaron LI        Create this file by QCM9X00143-P01
 |  2018/05/18       Vicent GAO      Add common functions
 |  2018/05/22       Vicent GAO      Add common functions
 |  2018/06/12       Vicent GAO      Add common functions
 |  2018/06/14       Vicent GAO      Add common definitions
 |  2018/06/28       Vicent GAO      Add common functions/definitions, architecture for MCFG refresh
 |  2018/07/06       Vicent GAO      Add common functions
 |  2018/09/13       Vicent GAO      Add codes for Openlinux/EC20CETGA project
 |  2018/12/06       Vicent GAO      Add codes for lock function
 |  ------------------------------------------------------------------------
 \=========================================================================*/

#ifndef __QUECTEL_EFS_DEFAULT_H__
#define __QUECTEL_EFS_DEFAULT_H__

#include "dstask.h"
#include "dsati.h"

#if defined(QUECTEL_AT_COMMON_SUPPORT)

/////////////////////////////////////////////////////////////
// MACRO CONSTANT DEFINITIONS
/////////////////////////////////////////////////////////////
#define QUEC_EFSDEF_NV66048  "/nv/item_files/modem/mmode/voice_domain_pref"
#define QUEC_EFSDEF_NV65777  "/nv/item_files/modem/mmode/ue_usage_setting"

//<2017/11/28-QCM9X00143-P03-Joyce.Sun, <[EFSDEF] Segment 3==> EFS Default arch implement.>
#define QUEC_EFSDEF_NV67348  "/nv/item_files/ims/qipcall_config_items"
#define QUEC_EFSDEF_NV67348_SIZE   512
#define QUEC_EFSDEF_NV67348_OFFSET_SRVCC_BIT_MASK   476
//>2017/11/28-QCM9X00143-P03-Joyce.Sun

//<2018/05/17-QCM9X00143C001-P01-Vicent.Gao, <[EFSDEF] Segment 1==> Add common function definition.>
/******************************************************************************************
Vicent.Gao-2018/05/18: Add common function definition for Quectel EFS default.
Refer to [Req-Depot].[RQ0000075][Submitter:vicent.gao,Date:2018-05-18]
<MDM9X07>
******************************************************************************************/
#define QUEC_EFSDEF_NV65633  "/nv/item_files/modem/mmode/lte_bandpref"
//>2018/05/17-QCM9X00143C001-P01-Vicent.Gao

//<2018/06/14-QCM9X00143C002-P01-Vicent.Gao, <[EFSDEF] Segment 1==> base code submit.>
#define QUEC_EFSDEF_NV73680  "/nv/item_files/modem/mmode/lte_bandpref_extn_65_256"
//>2018/06/14-QCM9X00143C002-P01-Vicent.Gao

//<2018/05/22-QCM9X00143C001-P02-Vicent.Gao, <[EFSDEF] Segment 2==> Add common function definition.>
/******************************************************************************************
Vicent.Gao-2018/05/18: Add common function definition for Quectel EFS default.
Refer to [Req-Depot].[RQ0000075][Submitter:vicent.gao,Date:2018-05-18]
<MDM9X07>
******************************************************************************************/
#define QUEC_EFSDEF_LTE_RRC_CAP_FGI           "/nv/item_files/modem/lte/rrc/cap/fgi"
#define QUEC_EFSDEF_LTE_RRC_CAP_FGI_REL9      "/nv/item_files/modem/lte/rrc/cap/fgi_rel9"
#define QUEC_EFSDEF_LTE_RRC_CAP_FGI_R10       "/nv/item_files/modem/lte/rrc/cap/fgi_r10"
#define QUEC_EFSDEF_LTE_RRC_CAP_FGI_TDD       "/nv/item_files/modem/lte/rrc/cap/fgi_tdd"
#define QUEC_EFSDEF_LTE_RRC_CAP_FGI_TDD_REL9  "/nv/item_files/modem/lte/rrc/cap/fgi_tdd_rel9"
#define QUEC_EFSDEF_LTE_RRC_CAP_FGI_TDD_R10   "/nv/item_files/modem/lte/rrc/cap/fgi_tdd_r10"

#define QUEC_EFSDEF_LTE_RRC_CAP_DIFF_FDD_TDD_FGI_ENABLE   "/nv/item_files/modem/lte/rrc/cap/diff_fdd_tdd_fgi_enable"

#define QUEC_EFSDEF_NV69689                   "/nv/item_files/ims/ims_user_agent"
//>2018/05/22-QCM9X00143C001-P02-Vicent.Gao

//<2018/08/14-QCM9X00143C003-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CET default value specify.>
#define QUEC_EFSDEF_RFNV_NV66078           "/nv/item_files/rfnv/00020291"
#define QUEC_EFSDEF_RFNV_NV66732           "/nv/item_files/rfnv/00020807"
//>2018/08/14-QCM9X00143C003-P01-Vicent.Gao

//<2018/08/18-QCM9X00143C004-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CEFDLG default value specify.>
#define QUEC_EFSDEF_RFNV_NV66412           "/nv/item_files/rfnv/00020401"
#define QUEC_EFSDEF_RFNV_NV66811           "/nv/item_files/rfnv/00020865"
#define QUEC_EFSDEF_RFNV_NV67048           "/nv/item_files/rfnv/00021630"
#define QUEC_EFSDEF_RFNV_NV70750           "/nv/item_files/rfnv/00024981"
#define QUEC_EFSDEF_RFNV_NV70815           "/nv/item_files/rfnv/00025046"
//>2018/08/18-QCM9X00143C004-P01-Vicent.Gao

//<2018/09/06-QCM9X00143C006-P01-Vicent.Gao, <[EFSDEF] Segment 1==> EC21VFB default value specify.>
#define QUEC_EFSDEF_NV67264                         "/nv/item_files/ims/qp_ims_reg_config"
#define QUEC_EFSDEF_NV67264_SIZE                    405
#define QUEC_EFSDEF_NV67264_OFFSET_REG_RAT_CONFIG   294
//>2018/09/06-QCM9X00143C006-P01-Vicent.Gao

//<2018/09/30-QCM9X00143C001-P05-Vicent.Gao, <[EFSDEF] Segment 5==> Add common function definition.>
#define QUEC_EFSDEF_BAND_PRIORITY_LIST_V2				"/nv/item_files/modem/lte/rrc/csp/band_priority_list_v2"
#define QUEC_EFSDEF_PDP_Profile1 						"/Data_Profiles/Profile1"
#define QUEC_EFSDEF_NV70263_UTAPNName_FIRSTNET 			"firstnet-phone"
#define QUEC_EFSDEF_NV70263_UTAPNName_NDO 				"ndo"
#define QUEC_EFSDEF_NV70263  							"/nv/item_files/ims/qp_ims_ut_config"
#define QUEC_EFSDEF_NV70263_SIZE   						996
#define QUEC_EFSDEF_NV70263_OFFSET_UTAPNName            1
#define QUEC_EFSDEF_NV70263_UTAPNName_SIZE  			64
//>2018/09/30-QCM9X00143C001-P05-Vicent.Gao

//<2019/01/09-QCM9X00143C008-P01-Vicent.Gao, <[EFSDEF] Segment 1==> EC25AF default value specify.>
#define QUEC_EFSDEF_QCFG_VOLTE_DISABLE         "/nv/item_files/quectel/qcfg_volte_disable"
//>2019/01/09-QCM9X00143C008-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// ENUM TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
typedef enum
{
    //<2017/09/29-QCM9X00144-P01-Aaron.Li, <[EC21AUT] Segment 1==> Set EC21AUT CEFS to default.>
    EFSDEF_PROJECT_EC21AUT = 0,
    //>2017/09/29-QCM9X00144-P01-Aaron.Li

    //<2017/11/28-QCM9X00159-P01-Joyce.Sun, <[EFSDEF] Segment 1==> Set EC21JDCM CEFS to default.>
    EFSDEF_PROJECT_EC21JDCM = 1,    
    //>2017/11/28-QCM9X00159-P01-Joyce.Sun

    //<2017/11/28-QCM9X00160-P01-Joyce.Sun, <[EFSDEF] Segment 1==> Set EC25JDCM CEFS to default.>
    EFSDEF_PROJECT_EC25JDCM = 2,    
    //>2017/11/28-QCM9X00160-P01-Joyce.Sun

    //<2018/08/14-QCM9X00143C003-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CET default value specify.>
    EFSDEF_PROJECT_OL_EC20CET = 3,    
    //>2018/08/14-QCM9X00143C003-P01-Vicent.Gao

    //<2018/08/18-QCM9X00143C004-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CEFDLG default value specify.>
    EFSDEF_PROJECT_OL_EC20CEFDLG = 4,    
    //>2018/08/18-QCM9X00143C004-P01-Vicent.Gao

    //<2018/09/03-QCM9X00143C005-P01-Vicent.Gao, <[EFSDEF] Segment 1==> MDM9640/EP06A default value specify.>
    EFSDEF_PROJECT_MDM9640_EP06A = 5,    
    //>2018/09/03-QCM9X00143C005-P01-Vicent.Gao

    //<2018/09/06-QCM9X00143C006-P01-Vicent.Gao, <[EFSDEF] Segment 1==> EC21VFB default value specify.>
    EFSDEF_PROJECT_EC21VFB = 6,    
    //>2018/09/06-QCM9X00143C006-P01-Vicent.Gao

    //<2018/09/13-QCM9X00143C007-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CETGA default value specify.>
    EFSDEF_PROJECT_OL_EC20CETGA = 7,
    //>2018/09/13-QCM9X00143C007-P01-Vicent.Gao

    //<2019/01/09-QCM9X00143C008-P01-Vicent.Gao, <[EFSDEF] Segment 1==> EC25AF default value specify.>
    EFSDEF_PROJECT_EC25AF = 8,
    //>2019/01/09-QCM9X00143C008-P01-Vicent.Gao

    //Warning!==>Please add new EFSDEF PROJECT upper this line
    EFSDEF_PROJECT_MAX
} EFSDEF_ProjectEnum;

/////////////////////////////////////////////////////////////
// STRUCT TYPE DEFINITIONS
/////////////////////////////////////////////////////////////
//<2018/06/27-QCM9X00143C002-P02-Vicent.Gao, <[EFSDEF] Segment 2==> base code submit.>
typedef struct
{
    int iId;
    void *pData;
    uint16 uLen;
} EFSDEF_NvList;

typedef struct
{
    char *pPath;
    void *pData;
    uint16 uLen;
} EFSDEF_EfsList;
//>2018/06/27-QCM9X00143C002-P02-Vicent.Gao

//<2018/08/18-QCM9X00143C004-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CEFDLG default value specify.>
typedef struct
{
    int iId;
    void *pStr;
} EFSDEF_NvListStr;

typedef struct
{
    char *pPath;
    void *pStr;
} EFSDEF_EfsListStr;
//>2018/08/18-QCM9X00143C004-P01-Vicent.Gao

/////////////////////////////////////////////////////////////
// GLOBAL DATA DECLARATIONS
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS
/////////////////////////////////////////////////////////////
extern boolean nvim_op_get_presence(nv_items_enum_type item);

extern boolean QL_EFSDEF_SetNV66048(uint8 uVdmPref);
extern boolean QL_EFSDEF_SetNV65777(uint8 uUeUsgSet);
extern boolean QL_EFSDEF_DeleteNV6850(void);
extern boolean QL_EFSDEF_SetNV4118(uint8 uHsdpaCat);

extern boolean QL_EFSDEF_IsLock(char *pLock);
extern boolean QL_EFSDEF_Lock(char *pLock);
extern boolean QL_EFSDEF_Unlock(char *pLock);

extern boolean QL_EFSDEF_DeleteProfile(char *pApn);
extern boolean QL_EFSDEF_DeleteProfileIms(void);
extern char* QL_EFSDEF_GetNvItemPath(nv_items_enum_type eItem,byte uIndex,uint16 uContext);
extern boolean QL_EFSDEF_DeleteNvItem(nv_items_enum_type eItem,byte uIndex,uint16 uContext);
extern boolean QL_EFSDEF_DeleteNvItemNum(nv_items_enum_type eItem);
extern boolean QL_EFSDEF_DeleteNvItemNumIndex(nv_items_enum_type eItem,byte uIndex);

//<2017/11/28-QCM9X00143-P03-Joyce.Sun, <[EFSDEF] Segment 3==> EFS Default arch implement.>
extern boolean QL_EFSDEF_SetNV67348SRVCCBitMask(uint8 uSRVCCBitMask);
//>2017/11/28-QCM9X00143-P03-Joyce.Sun

//<2018/05/17-QCM9X00143C001-P01-Vicent.Gao, <[EFSDEF] Segment 1==> Add common function definition.>
/******************************************************************************************
Vicent.Gao-2018/05/18: Add common function definition for Quectel EFS default.
Refer to [Req-Depot].[RQ0000075][Submitter:vicent.gao,Date:2018-05-18]
<MDM9X07>
******************************************************************************************/
extern uint16 nvim_op_get_array_size (nv_items_enum_type item);
extern uint32 nvim_op_get_size(nv_items_enum_type item_code);

extern boolean QL_EFSDEF_SetNV441(uint16 uBand);
extern boolean QL_EFSDEF_SetNV946(uint16 uBand);
extern boolean QL_EFSDEF_SetNV2954(uint32 uBand);
extern boolean QL_EFSDEF_SetNV65633(uint64 uBand);
extern boolean QL_EFSDEF_WriteNvItemByMcfg(nv_items_enum_type eItem,byte *pData,uint16 uDataLen);
//>2018/05/17-QCM9X00143C001-P01-Vicent.Gao

//<2018/05/22-QCM9X00143C001-P02-Vicent.Gao, <[EFSDEF] Segment 2==> Add common function definition.>
/******************************************************************************************
Vicent.Gao-2018/05/18: Add common function definition for Quectel EFS default.
Refer to [Req-Depot].[RQ0000075][Submitter:vicent.gao,Date:2018-05-18]
<MDM9X07>
******************************************************************************************/
extern boolean QL_EFSDEF_SetEFSLteRrcCapFgi(const char *pEfsPath,uint32 uFgi);
extern boolean QL_EFSDEF_SetEFSLteRrcCapDiffFddTddFgiEnable(uint8 uEnable);
extern boolean QL_EFSDEF_SetNV6850(uint8 uCodec);
extern boolean QL_EFSDEF_SetNV69689(const char *pUserAgent);
//>2018/05/22-QCM9X00143C001-P02-Vicent.Gao

//<2017/05/17-QCM9X00170C004-P01-Vicent.Gao, <[JAPAN] Segment 1==> EFS default base code submit.>
/******************************************************************************************
Vicent.Gao-2018/04/14: Only one EC25J version support DCM/KDDI/SBM
Refer to [Req-Depot].[RQ0000018][Submitter:2018-03-30,Date:2018-03-29]
<MDM9x07>
******************************************************************************************/
#if defined(QUECTEL_JAPAN_FEATURE)
extern boolean JP_EFSDEF_MbnDeactivate(void);
#endif // #if defined(QUECTEL_JAPAN_FEATURE)
//>2017/05/17-QCM9X00170C004-P01-Vicent.Gao

//<2018/06/12-QCM9X00143C001-P03-Vicent.Gao, <[EFSDEF] Segment 3==> Add common function definition.>
/******************************************************************************************
Vicent.Gao-2018/05/18: Add common function definition for Quectel EFS default.
Refer to [Req-Depot].[RQ0000075][Submitter:vicent.gao,Date:2018-05-18]
<MDM9X07>
******************************************************************************************/
extern boolean QL_EFSDEF_WriteFile(char *pEfs,byte *pData,uint16 uDataLen);
//>2018/06/12-QCM9X00143C001-P03-Vicent.Gao

//<2018/06/27-QCM9X00143C002-P02-Vicent.Gao, <[EFSDEF] Segment 2==> base code submit.>
extern boolean FUNC_MCFG_GetPlmnFromActiveMbn(char *pPlmn,uint8 uPlmnMaxLen);
extern int QL_EFSDEF_GetOper(void);
//>2018/06/27-QCM9X00143C002-P02-Vicent.Gao

//<2018/08/14-QCM9X00143C003-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CET default value specify.>
extern boolean QL_EFSDEF_SetNV6717(byte uMap0,byte uMap1,byte uMap2,byte uMap3);
extern boolean QL_EFSDEF_SetNV66078(byte uMap0,byte uMap1,byte uMap2,byte uMap3);
extern boolean QL_EFSDEF_SetNV66732(byte uMap0,byte uMap1,byte uMap2,byte uMap3);
//>2018/08/14-QCM9X00143C003-P01-Vicent.Gao

//<2018/07/06-QCM9X00143C001-P04-Vicent.Gao, <[EFSDEF] Segment 4==> Add common function definition.>
extern boolean QL_EFSDEF_WriteNvItemByMcfgWtIndex(nv_items_enum_type eItem,byte *pData,uint16 uDataLen);
//>2018/07/06-QCM9X00143C001-P04-Vicent.Gao

//<2018/08/18-QCM9X00143C004-P01-Vicent.Gao, <[EFSDEF] Segment 1==> OpenLinux/EC20CEFDLG default value specify.>
extern boolean QL_EFSDEF_WriteFileStr(char *pEfs,char *pStr);
extern boolean QL_EFSDEF_WriteNvItemByMcfgStr(nv_items_enum_type eItem,char *pStr);
//>2018/08/18-QCM9X00143C004-P01-Vicent.Gao

//<2018/09/06-QCM9X00143C006-P01-Vicent.Gao, <[EFSDEF] Segment 1==> EC21VFB default value specify.>
extern boolean QL_EFSDEF_SetNV67264RegRATConfig(uint8 uRAT);
//>2018/09/06-QCM9X00143C006-P01-Vicent.Gao

//<2018/09/30-QCM9X00143C001-P05-Vicent.Gao, <[EFSDEF] Segment 5==> Add common function definition.>
extern boolean QL_EFSDEF_SetNV70263utAPNName(const char* pUtAPNName);
//>2018/09/30-QCM9X00143C001-P05-Vicent.Gao

//<2018/12/20-QCM9X00143C001-P06-Vicent.Gao, <[EFSDEF] Segment 6==> Add common function definition.>
extern boolean QL_EFSDEF_WriteNvItemByMcfgS2(nv_items_enum_type eItem,byte *pData,uint16 uDataLen);
extern boolean QL_EFSDEF_WriteNvItemByMcfgStrS2(nv_items_enum_type eItem,char *pStr);
//>2018/12/20-QCM9X00143C001-P06-Vicent.Gao

//<2019/02/15-QCM9X00143C001-P07-Vicent.Gao, <[EFSDEF] Segment 7==> Add common function definition.>
extern uint32 QL_EFSDEF_GetFileSize(char *pEfs);
extern boolean QL_EFSDEF_WriteFilePartStr(char *pEfs,int iOffset,char *pStr);
//>2019/02/15-QCM9X00143C001-P07-Vicent.Gao

/////////////////////////////////////////////////////////////
// MACRO FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////
//<2018/06/27-QCM9X00143C002-P02-Vicent.Gao, <[EFSDEF] Segment 2==> base code submit.>
#define EFSDEF_TRACE_0(f)  MSG_SPRINTF_2(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),EFSDEF_" f,__FUNCTION__,__LINE__)
#define EFSDEF_TRACE_1(f,g1)  MSG_SPRINTF_3(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),EFSDEF_" f,__FUNCTION__,__LINE__,g1)
#define EFSDEF_TRACE_2(f,g1,g2)  MSG_SPRINTF_4(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),EFSDEF_" f,__FUNCTION__,__LINE__,g1,g2)
#define EFSDEF_TRACE_3(f,g1,g2,g3)  MSG_SPRINTF_5(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),EFSDEF_" f,__FUNCTION__,__LINE__,g1,g2,g3)
#define EFSDEF_TRACE_4(f,g1,g2,g3,g4)  MSG_SPRINTF_6(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),EFSDEF_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4)
#define EFSDEF_TRACE_5(f,g1,g2,g3,g4,g5)  MSG_SPRINTF_7(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),EFSDEF_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5)
#define EFSDEF_TRACE_6(f,g1,g2,g3,g4,g5,g6)  MSG_SPRINTF_8(MSG_SSID_DS_ATCOP,MSG_LEGACY_HIGH,"Enter %s(%d),EFSDEF_" f,__FUNCTION__,__LINE__,g1,g2,g3,g4,g5,g6)
//>2018/06/27-QCM9X00143C002-P02-Vicent.Gao

#endif // #if defined(QUECTEL_AT_COMMON_SUPPORT)

#endif //#ifndef __QUECTEL_EFS_DEFAULT_H__
