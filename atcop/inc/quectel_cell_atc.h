/*===========================================================================

                           QUECTEL_CELL_ATC . H

DESCRIPTION
   Cell informations

REFERENCES

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2014 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

===========================================================================*/
#ifndef __QUECTEL_QUECCELL_ATC_H__
#define __QUECTEL_QUECCELL_ATC_H__
#include "comdef.h"
#include "customer.h"
#include "dsat_v.h"
#include "dsati.h"
#include "quectel_wifi_atc.h"

#ifdef QUECTEL_QENG_FEATURE
#include "sys_type.h"
#include "rrcmmif.h"

//#include "Quectel_nw_atc.h"
//#include "l1rrcif.h"
//#include "rr_l1.h"
//#include "rfm_cdma_band_types.h"
//#include "rr_l1_g.h"


#define QUECTEL_RR_IRAT_MEASUREMENTS_PER_RAT 6
#define QUECTEL_MAX_MA_CHANNEL     64

//<2016/12/06-QCM9XAW00006-P01-Andvin.Wang, <[QCFG] Segment 1==>Add QCFG sub-cmd: lte/bandprior.>
#ifdef QUECTEL_AT_COMMON_SUPPORT
#define QCFG_LTE_PRIOR_EFS_LTE_BAND_PRIORITY_LIST    "/nv/item_files/modem/lte/rrc/csp/band_priority_list"
#define QCFG_LTE_PRIOR_BAND_ID_MIN            1
#define QCFG_LTE_PRIOR_BAND_ID_MAX            43
#define QCFG_LTE_PRIOR_BAND_NUM_MAX           3
#endif
//>2016/12/06-QCM9XAW00006-P01-Andvin.Wang

typedef struct
{
  word uarfcn;
  /*!< The UARFCN of the WCDMA cell. */
  
  word scrambling_code;
  /*!< The Scrambling Code of the WCDMA cell. */
  
  boolean diversity;
  /*!< The Diversity flag for the cell. */
  
  int16 rscp;
  /*! The RSCP value measured for this cell. */
  
  int16 ecno;
  /*! The Ec/No value measured for this cell. */

  int16 rssi;
  /*!< The RSSI across the whole WCDMA frequency. */
} quectel_rr_irat_measurement_wcdma_t;

typedef struct
{
  uint16 earfcn;
  uint16 pcid;
  int16 rsrp;
  int16 rsrq;
} quectel_rr_irat_measurement_lte_t;

typedef struct
{
  // No of entries in the surrounding wcdma cell data base
  byte                  no_of_entries;

  union
  {
  // wcdma cell entries
  quectel_rr_irat_measurement_wcdma_t wcdma_entries[QUECTEL_RR_IRAT_MEASUREMENTS_PER_RAT];
  //lte cell entries
  quectel_rr_irat_measurement_lte_t lte_entries[QUECTEL_RR_IRAT_MEASUREMENTS_PER_RAT];
  }entries;
} quectel_rr_irat_measurements_t;

typedef struct
{
   byte                 no_of_items;
   ARFCN_T              channel[QUECTEL_MAX_MA_CHANNEL];
} quectel_frequency_list_T;

typedef struct
{
   channel_type_T       channel_type;
   byte                 subchannel;
   byte                 TN;
   byte                 TSC;
   boolean              hopping_flag;
   byte                 MAIO;
   byte                 HSN;
   quectel_frequency_list_T     quectel_frequency_list;
} quectel_channel_information_T;

typedef struct
{
	uint8  quectel_rxlev_full;
	uint8  quectel_rxlev_sub;
	byte  quectel_rxqual_full;
	byte  quectel_rxqual_sub;
} quectel_rx_lev_qual_information_T;


typedef struct
{
    boolean index_used;
    rrc_plmn_identity_type  plmn_id;
    uint16 lac_id;             
    uint32 cell_id;
    uint32 scr_code;
    uint32 freq;
}QUECTEL_CELL_INFO;

/*===========================================================================

FUNCTION DSAT_EXEC_QENG_CMD

DESCRIPTION
  Command for AT+QENG.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
dsat_result_enum_type dsat_exec_qeng_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif /*QUECTEL_QENG_FEATURE*/
extern int8 quectel_eng_convert_rx_lev_to_dBm( uint16 rxlev );
#define DSDIV10(x)  ((int)((x)/10))
extern dsat_result_enum_type  dsat_qeng_query_lte_ncell_info(dsat_cops_act_e_type  net_act, dsm_item_type *res_buff_ptr);

//2016/08/19 porting by chao.zhou for landi-->Begin
#ifdef QUECTEL_CUSTOMIZATION_FOR_LIANDI
dsat_result_enum_type dsat_exec_smonc_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type  dsat_qeng_query_3gcomm_info(dsm_item_type *res_buff_ptr);
#endif
//2016/08/19 porting by chao.zhou for landi-->End

//<2016/10/21-QCM9XAW00002-P01-Andvin.Wang, <[QSAR] Segment 1==>Add QSAR common function definition.>
#ifdef QUECTEL_RF_SAR_FEATURE
extern dsat_result_enum_type dsat_exec_qsar_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif
//>2016/10/21-QCM9XAW00002-P01-Andvin.Wang
#ifdef QUEC_QCELLLOC_SUPPORT_CDMA
void quectel_get_cdma_base_position(uint32 *latitude,uint32 *longitude);
#endif
//lory 2017/05/31 get nw state 
int quectel_judge_nw_state_is_searching_based_on_qeng(void);

// Amos.zhang-2018/04/04: add quec_lte_get_rsssnr declaration
#ifdef QUECTEL_NETWORK_FEATURE
dsat_result_enum_type quec_lte_get_rsssnr(dsm_item_type *res_buff_ptr);
#endif

#endif

