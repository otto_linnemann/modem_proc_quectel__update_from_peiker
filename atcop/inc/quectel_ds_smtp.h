#ifndef QUECTEL_DS_SMTP_H
#define QUECTEL_DS_SMTP_H

#include "quectel_apptcpip_util.h"
#include "queue.h"
#include "dsm_queue.h"
#include "ds3gsiolib.h"

#if defined(QUECTEL_SMTP_SUPPORT)
typedef enum
{
     DS_SMTP_OK = 0,
     DS_SMTP_ERROR = 650,
     DS_SMTP_WOULDBLOCK = 651,
     DS_SMTP_ERROR_BUSY = 652,
     DS_SMTP_ERROR_DNS = 653,       /*failed to parse domain name*/
     DS_SMTP_ERROR_NETWORK = 654,   /*failed to establish socket or the network is deactivated*/
     DS_SMTP_ERROR_UNSUPPORT = 655,
     DS_SMTP_PEER_CLOSE = 656,
     DS_SMTP_BEARER_FAIL =657,  
     DS_SMTP_ERROR_TIMEOUT =658,
     DS_SMTP_ERROR_NO_RCPT = 659,
     DS_SMTP_ERROR_SEND_FAIL = 660,
     DS_SMTP_ERROR_OPEN_FILE  = 661,
     DS_SMTP_ERROR_NO_MEMORY = 662,
     DS_SMTP_ERROR_SAVE_FAIL = 663,
     DS_SMTP_ERROR_PARAM = 664,
     DS_SMTP_SSL_AUTH_FAIL =665,
     DS_SMTP_REPLY_SERVICE_FAIL =666,
     DS_SMTP_REPLY_MAILBOX_UNAVAIL = 667,
     DS_SMTP_REPLY_REQUEST_ABORTED = 668,
     DS_SMTP_REPLY_STORAGE_FULL = 669,
     DS_SMTP_REPLY_COMMAND_ERROR =670,
     DS_SMTP_REPLY_PARAM_ERROR = 671,
     DS_SMTP_REPLY_COMMAND_NOT_IMPLE = 672,
     DS_SMTP_REPLY_BAD_SEQUENCE = 673,
     DS_SMTP_REPLY_PARAM_NOT_IMPLE = 674,
     DS_SMTP_REPLY_SERVER_UNACCEPT = 675,
     DS_SMTP_REPLY_ACCESS_DENIED = 676,
     DS_SMTP_REPLY_AUTHEN_FAILED = 677,
     DS_SMTP_REPLY_MAILBOX_UNAVAIL_2 = 678,
     DS_SMTP_REPLY_NOT_LOCAL_TRY_FOR = 679,
     DS_SMTP_REPLY_STORAGE_EXCEED = 680,
     DS_SMTP_REPLY_MAILBOX_NAME_ERROR = 681,
     DS_SMTP_REPLY_TRANSACTION_FAILED =682,
}quectel_ds_smtp_error_code;

typedef enum
{
	SMTP_CFG_ACCOUNT = 0,
	SMTP_CFG_SENDER,
	SMTP_CFG_SMTP_SERVER,
	SMTP_CFG_PDP_CID,
	SMTP_CFG_SSL_CTX_ID,
	SMTP_CFG_SSL_TYPE,
	MAX_SMTP_CFG_TYPE
}ds_smtp_cfg_type;

typedef enum
{
	DS_SMTP_NONE_CMD = 0,
	DS_SMTP_CFG_CMD,
	DS_SMTP_ATT_CMD,
	DS_SMTP_DST_CMD,
	DS_SMTP_SUB_CMD,
	DS_SMTP_CLR_CMD,
	DS_SMTP_PUT_CMD
}ds_smtp_cmd;
typedef struct
{
	ds3g_siolib_port_e_type             sio_port;
	ds_smtp_cmd                            last_cmd;
       Q_INT32                                   smtp_ctx;
	dsm_watermark_type                ds_smtp_msg_wm;
	q_type                                      ds_smtp_msg_q;
	int32                                         smtp_body_charset;
	uint32                                       smtp_body_len;
	uint32                                       smtp_body_inputTout;
	dsm_item_type                          *smtp_body_item;
}quectel_ds_smtp_mgr;

typedef enum
{
	DS_SMTP_NONE_EVENT = 0,
	DS_SMTP_RCPT_INFO_EVENT,
	DS_SMTP_ATT_INFO_EVENT,
	DS_SMTP_ENTER_DATA_MODE_EVENT,
	DS_SMTP_EXIT_DATA_MODE_EVENT,
	DS_SMTP_MAIL_SEND_RSP_EVENT
}quectel_ds_smtp_event;


dsat_result_enum_type  ds_smtp_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);


dsat_result_enum_type  ds_smtp_edit_mail_receiver
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);


dsat_result_enum_type  ds_smtp_edit_mail_subject
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_smtp_edit_mail_attachment
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_smtp_edit_mail_body
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_smtp_discard_mail
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_smtp_send_mail
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type ds_smtp_signal_handler
(
 dsat_mode_enum_type at_mode
);

#endif

#endif//QUECTEL_DS_SMTP_H
