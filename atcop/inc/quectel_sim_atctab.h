/*===========================================================================

                           QUECTEL_SIM_ATCTAB . H

DESCRIPTION
  SIM atc table function

REFERENCES

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved.
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE

===========================================================================*/
#ifndef __QUECTEL_SIM_ATCTAB_H__
#define __QUECTEL_SIM_ATCTAB_H__

#include "dsati.h"
#if 0
#ifdef QUECTEL_SIM_FEATURE
extern const dsati_cmd_type dsat_quectel_sim_action_table[];
extern const unsigned int dsat_quectel_sim_action_table_size;
#endif
#endif
#endif
