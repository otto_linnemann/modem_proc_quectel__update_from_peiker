#ifndef QUECTEL_DS_MMS_H
#define QUECTEL_DS_MMS_H

#include "quectel_apptcpip_util.h"
#include "queue.h"
#include "dsm_queue.h"
#include "ds3gsiolib.h"

#if defined(QUECTEL_MMS_SUPPORT)

typedef enum
{
	DS_MMS_RESULT_OK =0,
	DS_MMS_RESULT_UNKNOW_ERROR = 751,
	DS_MMS_RESULT_ERROR_URL_LENGTHERROR =752,
       DS_MMS_RESULT_ERROR_URL_ERROR = 753,       /*failed to parse domain name*/
       DS_MMS_RESULT_ERROR_PROXYTYPE_NOSUPPORT = 754,   /*failed to establish socket or the network is deactivated*/
       DS_MMS_RESULT_ERROR_PROXYADDR_ERROR = 755,
       DS_MMS_RESULT_ERROR_PARAM_ERROR = 756,
       DS_MMS_RESULT_ERROR_PARAM_TOADDR_FULL = 757,  
       DS_MMS_RESULT_ERROR_PARAM_CCADDR_FULL = 758,
       DS_MMS_RESULT_ERROR_PARAM_BCCADDR_FULL = 759,
       DS_MMS_RESULT_ERROR_PARAM_BODYFILE_FULL = 760,
       DS_MMS_RESULT_ERROR_FILE = 761,
       DS_MMS_RESULT_ERROR_PARAM_EMTPY_TOADDR =762,
       DS_MMS_RESULT_ERROR_FILE_NOT_FOUND = 763,
       DS_MMS_RESULT_ERROR_MMS_BUSY = 764,
       DS_MMS_RESULT_ERROR_HTTP_RESPONSE_FAILED =765,
       DS_MMS_RESULT_ERROR_MMS_POST_RESPONS_INVALIDMSG =766,
       DS_MMS_RESULT_ERROR_MMS_POST_RESPONSE_REPORTERROR = 767,
       DS_MMS_RESULT_ERROR_NETWORK_OPENFAILED =768,
       DS_MMS_RESULT_ERROR_NETWORK_ERROR = 769,
       DS_MMS_RESULT_ERROR_SOC_CREATEFAILED = 770,
       DS_MMS_RESULT_ERROR_SOC_CONNECTFAILED =771,
       DS_MMS_RESULT_ERROR_SOC_READFAILED = 772,
       DS_MMS_RESULT_ERROR_SOC_WRITEFAILED = 773,
       DS_MMS_RESULT_ERROR_SOC_CLOSE =774,
       DS_MMS_RESULT_ERROR_TIMEOUT =775,   
       DS_MMS_RESULT_ERROR_ENCODE_DATAFAILED =776,  
       DS_MMS_RESULT_ERROR_HTTP_DECODEERROR = 777,
       /*
       DS_MMS_RESULT_ERROR_DECODE_DATAFAILED =778, 
       DS_MMS_RESULT_ERROR_PDU_INPUTALIGNERROR = 779,
       DS_MMS_RESULT_ERROR_PDU_INPUTCHARERROR =780, 
       DS_MMS_RESULT_ERROR_MMS_NOTEXIST =781,
       DS_MMS_RESULT_ERROR_INVALIDADDRESS =782,
       DS_MMS_RESULT_ERROR_VOICEBUSY = 783,
       DS_MMS_RESULT_ERROR_ALLOC_MEMORY= 784,
       DS_MMS_RESULT_ERROR_ENCODE_GETMMSLENTHFAILED = 785,
       DS_MMS_RESULT_ERROR_SMS_DECODEERROR = 786,
       DS_MMS_RESULT_ERROR_MMS_RECEIVEFULL =787,
       */
}quectel_ds_mms_error_code;

typedef enum
{
	DS_MMS_NONE_CMD = 0,
	DS_MMS_CFG_CMD,
	DS_MMS_EDIT_CMD,
	DS_MMS_SEND_CMD,
}ds_mms_cmd;

typedef enum
{
	DS_MMS_NONE_EVENT = 0,
	DS_MMS_ADDR_READ_EVENT,
	DS_MMS_TITLE_READ_EVENT,
	DS_MMS_ATT_READ_EVENT,
	DS_MMS_SEND_RSP_EVENT
}quectel_ds_mms_event;

typedef struct
{
	ds3g_siolib_port_e_type             sio_port;
	Q_INT32                                   mms_ctx;
	int32                                         last_mms_cmd;
	dsm_watermark_type                 ds_mms_msg_wm;
	q_type                                      ds_mms_msg_q;
}quectel_ds_mms_mgr;


typedef enum
{
	MMS_CFG_PDP_CID = 0,
	MMS_CFG_MMSC,
	MMS_CFG_PROXY,
	MMS_CFG_CHARSET,
	MMS_CFG_SEND_PARAM,
#ifdef QUECTEL_MMS_ADD_X_UP_CALLING_ID_FIELD
    MMS_CFG_SUPPORT_FIELD,
#endif
	MMS_CFG_MAX
}quectel_ds_mms_cfg_type;

dsat_result_enum_type  ds_mms_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);

dsat_result_enum_type  ds_mms_edit
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);


dsat_result_enum_type  ds_mms_send
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
dsm_item_type *res_buff_ptr
);


dsat_result_enum_type ds_mms_signal_handler
(
 dsat_mode_enum_type at_mode
);
#ifdef QUECTEL_MMS_ADD_X_UP_CALLING_ID_FIELD
int quectel_mms_get_support_field_x_up_calling_id(void);
dsat_result_enum_type  ds_mms_cfg_support_field
(
 const tokens_struct_type *tok_ptr,
 uint32 arg_index,
 dsm_item_type *res_buff_ptr	  
);

#endif
#endif//QUECTEL_MMS_SUPPORT
#endif//QUECTEL_DS_MMS_H
