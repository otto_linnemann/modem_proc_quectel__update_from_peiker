/*===========================================================================

                           QUECTEL_SIM_ATC . H

DESCRIPTION
  quectel sim atc function

REFERENCES

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2013 QUECTEL Incorporated.
All rights reserved.
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
17/4/2015 quectel sim atc function declare add by Francis.tang

===========================================================================*/
#ifndef __QUECTEL_SIM_ATC_H__
#define __QUECTEL_SIM_ATC_H__

#include "comdef.h"
#include "customer.h"
#include "dsat_v.h"
#include "dsati.h"
//<2017/06/04-QCM9X00093-P01-Vicent.Gao, <[SIM] Segment 1==> Remove PLMN from FPLMN list design>
#include "reg_sim.h"
//>2017/06/04-QCM9X00093-P01-Vicent.Gao

#ifdef QUECTEL_SIM_FEATURE

//<2016/09/05-QCM9XTVG00052-P01-Vicent.Gao,<[SIM] Segment 1==>Copy QCFG-sim/recovery from UC20.>
#if defined(QUECTEL_SIM_RECOVERY)
#define MIN_SIM_RECOVERY_COUNT        3
#define MAX_SIM_RECOVERY_COUNT        300
#define MIN_SIM_AUTO_DETECT_PERIOD    5
#define MAX_SIM_AUTO_DETECT_PERIOD    300
#define MAX_SIM_AUTO_DETECT_COUNT     300
#endif //#if defined(QUECTEL_SIM_RECOVERY)
//>2016/09/05-QCM9XTVG00052-P01-Vicent.Gao

//<2017/07/29-QCM9X00099-P01-Aaron.Li, <[QCFG] Segment 1==> Add sub cmd: AT+QCFG="sim/clk_freg".>
#define QCFG_SIMCLFR_EFS_NV67330            "/nv/item_files/modem/uim/uimdrv/uim_features_status_list"
#define QCFG_SIMCLFR_EFS_NV67330_OFFSET     (19)
#define QCFG_SIMCLFR_3_8_HZ   (0)
#define QCFG_SIMCLFR_4_8_HZ   (1)
#define QCFG_SIMCLFR_DEFAULT  (QCFG_SIMCLFR_3_8_HZ)
//>2017/07/29-QCM9X00099-P01-Aaron.Li
typedef enum dsat_me_qinistat_e
{
  DSAT_QINISTAT_NOT_INIT     =      0x00,
  DSAT_QINISTAT_PIN1_INIT    =      0x01,
  DSAT_QINISTAT_SMS_INIT     =      0x02,
  DSAT_QINISTAT_PHB_INIT     =      0x04
} dsat_me_qinistat_e_tupe;


#ifdef QUECTEL_SIM_HOTSWAP_DETECT

#define UIM_FEATURE_SUPPORT_HOTSWAP_FILE            "/nv/item_files/modem/uim/uimdrv/feature_support_hotswap"
#define UIM_HOTSWAP_CARD_INSERTED_LEVEL_NV_EF  "/nv/item_files/modem/uim/uimdrv/hotswap_card_inserted_level"

enum {
  QUE_UIM_DRV_SLOT1,
  QUE_UIM_DRV_SLOT2,
  QUE_UIM_NUM_DRV_SLOTS
};

//<2017/06/11-QCM9XTVG00097-P01-Vicent.Gao, <[SIM] Segment 1==> Add cmd: AT+QPLMNLIST.>
typedef enum
{
    QPLMNLIST_TYPE_HPLMN = 0,
    QPLMNLIST_TYPE_EHPLMN = 1,
    QPLMNLIST_TYPE_FPLMN = 2,

    //Warning!==>Please add new QPLMNLIST TYPE upper this line.
    QPLMNLIST_TYPE_MAX
} QPLMNLIST_TypeEnum;
//>2017/06/11-QCM9XTVG00097-P01-Vicent.Gao

typedef struct {
  uint8    version;
  /* version 0: NEW FEATURE ITEMS = 1; TOTAL CURRENT FEATURE ITEMS = 1. */
  boolean  feature_support_hotswap;
  /* version 0: RESERVED_FOR_FUTURE_USE size is 10 - 1 = 9 */
  uint8    reserved_for_future_use[9];
} quec_simdet_feature_support_hotswap_type;


//AT+QSIMDET parameters
typedef struct
{
    uint8 enable;
    uint8 sim_inserted_level;
}quectel_simdet_option_struct;


//AT+QSIMSTAT parameters
typedef struct
{
    uint8 reporting_enable;
    uint8 inserted_status;
}quectel_simstat_option_struct;

#endif//QUECTEL_SIM_HOTSWAP_DETECT

/*===========================================================================

FUNCTION dsatsim_qinistat_change

AUTHOR Laguna.XU

DATE 2013-5-29

DESCRIPTION
  This function changes qinistat.

DEPENDENCIES
  None

RETURN VALUE
    qinistat

SIDE EFFECTS
  None

===========================================================================*/
uint32 dsatsim_qinistat_change(uint8 op, dsat_me_qinistat_e_tupe  change_tag);

/*===========================================================================

FUNCTION dsatsim_exec_qinistat_cmd

AUTHOR Laguna.XU

DATE 2013-5-29

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes +QINISTAT command.
  This command can query initialization status of SIM/USIM card,like phonebook and SMS initialization is done or not.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_OK :       if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatsim_exec_qinistat_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION dsatsim_exec_qpinc_cmd

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes +QPINC command.
  This command query SIM PIN reminder counter.

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :      if there was any problem in executing the command
    DSAT_OK :         if it is a success.
    DSAT_CMD_ERR_RSP: if +CME ERROR is being generated
    DSAT_ASYNC_CMD :  excuting the action command and waiting for the
                      callback function be called.

SIDE EFFECTS
  None

===========================================================================*/
/* ARGSUSED */
/*2015-4-17-francis-for QPINC*/
dsat_result_enum_type dsatsim_exec_qpinc_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION dsatsim_qpinc_event_handler

DESCRIPTION
  This command process event from mmgsdi cause by +QPINC.

DEPENDENCIES
  None

RETURN VALUE
  Returns the command running status.
    DSAT_CMD_ERR_RSP : if the data parameter was not correct
    DSAT_ERROR :       if there was any problem in executing the command
    DSAT_OK :          if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatsim_qpinc_event_handler
(
   ds_at_cmd_status_type *cmd_info_ptr
);

#ifdef QUECTEL_SIM_HOTSWAP_DETECT

dsat_result_enum_type dsatsimdet_exec_qsimdet_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
void quectel_simdet_save_config_to_nv(void);
void quectel_get_simdet_config_from_nv(void);
void quectel_get_simdet_info(uint8 *enable_p, uint8 *sim_inserted_level_p);


dsat_result_enum_type dsatsimdet_exec_qsimstat_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

void quectel_simdet_report_qsimstat_urc (void);

//<2016/03/02-QCM9XTVG00032-P01-Vicent.Gao, <[SIM] Segment 1==>Fix SIM-HOTSWAP issue.>
extern boolean QL_SIMHTSW_ReadSimdetParaFromEfs(uint8 slot,quectel_simdet_option_struct *pOpt);
extern boolean QL_SIMHTSW_SaveSimdetParaToEfs(uint8 slot,quectel_simdet_option_struct *pOpt);
//>2016/03/02-QCM9XTVG00032-P01-Vicent.Gao

#endif//QUECTEL_SIM_HOTSWAP_DETECT

//<2016/09/05-QCM9XTVG00052-P01-Vicent.Gao,<[SIM] Segment 1==>Copy QCFG-sim/recovery from UC20.>
#if defined(QUECTEL_SIM_RECOVERY)
extern dsat_result_enum_type dsat_exec_qcfg_sim_recovery_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

extern void quectel_load_qcfg_sim_recovery_from_nv(void);
extern void quectel_sim_auto_detect_timer_init(void);
extern void quectel_sim_auto_detect_timer_start(void);
extern void quectel_sim_auto_detect_timer_stop(void);

extern boolean QL_SIMREC_HotSwapSetSignal(int32 eUimInst,int32 eUimCardSwap);
#endif //#if defined(QUECTEL_SIM_RECOVERY)
//>2016/09/05-QCM9XTVG00052-P01-Vicent.Gao


//Ramos20160911 add for QDSIM common
#ifdef QUECTEL_QDSIM
extern dsat_num_item_type Quectel_ReadQdsim_numfromNV(void);
extern dsat_result_enum_type dsatsim_exec_qdsim_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif

//<2017/06/14-QCM9X00090-P01-Vicent.Gao, <[SIM] Segment 1==> Add new cmd: AT+QFPLMNCFG.>
extern dsat_result_enum_type quectel_exec_qfplmncfg_cmd
(
    dsat_mode_enum_type eMode,             /*  AT command mode:            */
    const dsati_cmd_type *pParseTab,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *pTok,    /*  Command tokens from parser  */
    dsm_item_type *pResBuf           /*  Place to put response       */
);
//>2017/06/14-QCM9X00090-P01-Vicent.Gao

//<2017/06/04-QCM9X00093-P01-Vicent.Gao, <[SIM] Segment 1==> Remove PLMN from FPLMN list design>
extern boolean reg_sim_find_plmn_in_list
(
    sys_plmn_id_s_type        plmn,
    const reg_sim_plmn_list_s_type* plmn_list_p,
    uint32*                   position_p
);
//>2017/06/04-QCM9X00093-P01-Vicent.Gao

//<2017/06/11-QCM9XTVG00097-P01-Vicent.Gao, <[SIM] Segment 1==> Add cmd: AT+QPLMNLIST.>
extern dsat_result_enum_type quectel_exec_qplmnlist_cmd
(
    dsat_mode_enum_type eMode,             /*  AT command mode:            */
    const dsati_cmd_type *pParseTab,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *pTok,    /*  Command tokens from parser  */
    dsm_item_type *pResBuf           /*  Place to put response       */
);
//>2017/06/11-QCM9XTVG00097-P01-Vicent.Gao

#endif/*QUECTEL_SIM_FEATURE*/

#ifdef QUECTEL_ANS1_ENCODE_ERR_FEATURE
extern boolean QL_QLOCI_Clear(void);
#endif

#endif/*__QUECTEL_SIM_ATC_H__*/
