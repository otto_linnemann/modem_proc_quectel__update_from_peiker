#ifndef QUECTEL_NW_ATC_H
#define  QUECTEL_NW_ATC_H

#include "dsati.h"
#include "ds3gmgrint.h"
#include "sys.h"

//<2017/03/20-QCWXM9X00032-P01-wenxue.sheng, <[ATC] Segment 1==>Sync QCOPS CMD from 9215.>
#ifdef QUECTEL_QCOPS_FEATURE
#include "cm.h"
#include "Geran_dual_sim.h"
#endif // #if defined(QUECTEL_QCOPS_FEATURE)
//>2017/03/20-QCWXM9X00032-P01-wenxue.sheng

#if defined(QUECTEL_NETWORK_SUPPORT)

enum que_nas_bc_e
{
  QUE_NAS_BC_BC_0                     = 0,  //CDMA
  QUE_NAS_BC_BC_1                     = 1,
  QUE_NAS_BC_BC_3                     = 3,
  QUE_NAS_BC_BC_4                     = 4,
  QUE_NAS_BC_BC_5                     = 5,
  QUE_NAS_BC_BC_6                     = 6,
  QUE_NAS_BC_BC_7                     = 7,
  QUE_NAS_BC_BC_8                     = 8,
  QUE_NAS_BC_BC_9                     = 9,
  QUE_NAS_BC_BC_10                    = 10,
  QUE_NAS_BC_BC_11                    = 11,
  QUE_NAS_BC_BC_12                    = 12,
  QUE_NAS_BC_BC_13                    = 13,
  QUE_NAS_BC_BC_14                    = 14,
  QUE_NAS_BC_BC_15                    = 15,
  QUE_NAS_BC_BC_16                    = 16,
  QUE_NAS_BC_BC_17                    = 17,
  QUE_NAS_BC_BC_18                    = 18,
  QUE_NAS_BC_BC_19                    = 19,
  QUE_NAS_BC_GSM_450                  = 40, //GSM
  QUE_NAS_BC_GSM_480                  = 41,
  QUE_NAS_BC_GSM_750                  = 42,
  QUE_NAS_BC_GSM_850                  = 43,
  QUE_NAS_BC_GSM_900_EXTENDED         = 44,
  QUE_NAS_BC_GSM_900_PRIMARY          = 45,
  QUE_NAS_BC_GSM_900_RAILWAYS         = 46,
  QUE_NAS_BC_GSM_1800                 = 47,
  QUE_NAS_BC_GSM_1900                 = 48,
  QUE_NAS_BC_WCDMA_2100               = 80,  //WCDMA
  QUE_NAS_BC_WCDMA_PCS_1900           = 81,
  QUE_NAS_BC_WCDMA_DCS_1800           = 82,
  QUE_NAS_BC_WCDMA_1700_US            = 83,
  QUE_NAS_BC_WCDMA_850                = 84,
  QUE_NAS_BC_WCDMA_800                = 85,
  QUE_NAS_BC_WCDMA_2600               = 86,
  QUE_NAS_BC_WCDMA_900                = 87,
  QUE_NAS_BC_WCDMA_1700_JAPAN         = 88,
  QUE_NAS_BC_WCDMA_1500               = 90,
  QUE_NAS_BC_WCDMA_850_JAPAN          = 91,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_1  = 120, //LTE
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_2  = 121,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_3  = 122,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_4  = 123,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_5  = 124,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_6  = 125,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_7  = 126,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_8  = 127,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_9  = 128,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_10 = 129,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_11 = 130,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_12 = 131,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_13 = 132,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_14 = 133,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_17 = 136,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_18 = 137,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_19 = 138,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_20 = 139,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_21 = 140,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_23 = 142,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_24 = 143,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_25 = 144,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_26 = 145,
  //<2017/01/24-QCM9XAW00008-P01-Andvin.Wang, <[QNWINFO] Segment 1==>Fix issues of QNWINFO.>
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_28 = 147,
  //>2017/01/24-QCM9XAW00008-P01-Andvin.Wang
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_33 = 152,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_34 = 153,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_35 = 154,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_36 = 155,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_37 = 156,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_38 = 157,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_39 = 158,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_40 = 159,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_41 = 160,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_42 = 161,
  QUE_NAS_BC_E_UTRA_OPERATING_BAND_43 = 162,
  QUE_NAS_BC_TDSCDMA_BAND_A           = 200, //TDS
  QUE_NAS_BC_TDSCDMA_BAND_B           = 201,
  QUE_NAS_BC_TDSCDMA_BAND_C           = 202,
  QUE_NAS_BC_TDSCDMA_BAND_D           = 203,
  QUE_NAS_BC_TDSCDMA_BAND_E           = 204,
  QUE_NAS_BC_TDSCDMA_BAND_F           = 205,
  QUE_NAS_BC_MAX                      = 0xFFFF
};

//<2017/03/02-QCM9XTVG00061-P01-Vicent.Gao, <[NW] Segment 1==> Fix AT+CGDCONTEX issues.>
#if defined(QUECTEL_3GPP2_PROFILE_CONFIG)
typedef enum
{
    CGDCONTEX_IP_TYPE_V4 = 0,
    CGDCONTEX_IP_TYPE_V6 = 1,
    CGDCONTEX_IP_TYPE_V4V6 = 2,
    CGDCONTEX_IP_TYPE_UNSPEC = 3,
    
    //Warning!==>Please add new CGDCONTEX IP TYPE upper this line.
    CGDCONTEX_IP_TYPE_MAX
} CGDCONTEX_IpTypeEnum;
#endif // #if defined(QUECTEL_3GPP2_PROFILE_CONFIG)
//>2017/03/02-QCM9XTVG00061-P01-Vicent.Gao

typedef struct que_active_band_description_type
{
    uint16 band;
    char band_description[20];
}que_band_description_type;



typedef struct quectel_nas_modem_stats_info
{
  sys_gsm_information_s_type    gsm_info;   /* GSM structure */
  sys_wcdma_information_s_type  wcdma_info; /*RRC  structure */
}quectel_nas_modem_stats_info_s;

#if defined(QUECTEL_NWINFO_FEATURE)
#define QUECTEL_SIGNAL_STR_CHANGE_MASK (0x00            | \
                                     CM_SS_RSSI_MASK | \
                                     CM_SS_ECIO_MASK   | \
                                     CM_SS_RSCP_MASK | \
                                     CM_SS_SINR_MASK | \
                                     CM_SS_RSRQ_MASK  | \
                                     CM_SS_RSRP_MASK | \
                                     CM_SS_CDMA_ECIO_MASK | \
                                     CM_SS_HDR_RSSI_MASK | \
                                     CM_SS_HDR_ECIO_MASK | \
                                     CM_SS_HDR_SINR_MASK)
                                     
#define QUECTEL_SIGNAL_TW_STR_CHANGE_MASK (0x00            | \
                                     CM_SS_RSSI_MASK  | \
                                     CM_SS_ECIO_MASK  | \
                                     CM_SS_RSCP_MASK)

#define QUECTEL_SIGNAL_L_STR_CHANGE_MASK (0x00            | \
                                     CM_SS_RSSI_MASK  | \
                                     CM_SS_SINR_MASK  | \
                                     CM_SS_RSRQ_MASK  | \
                                     CM_SS_RSRP_MASK)


#define QUECTEL_SIGNAL_CDMA_STR_CHANGE_MASK (0x00            | \
                                     CM_SS_RSSI_MASK | \
                                     CM_SS_CDMA_ECIO_MASK | \
                                     CM_SS_HDR_RSSI_MASK | \
                                     CM_SS_HDR_ECIO_MASK | \
                                     CM_SS_HDR_SINR_MASK)                                    

#define QUECTEL_SIGNAL_HDR_STR_CHANGE_MASK (0x00            | \
                                     CM_SS_HDR_RSSI_MASK | \
                                     CM_SS_HDR_ECIO_MASK | \
                                     CM_SS_HDR_SINR_MASK)   

typedef struct 
{
	 uint64                                  changed_fields ;
    uint64                                  changed_fields2 ;
    uint64                                  signal_strength_changed_fields ;
    sys_plmn_id_s_type plmn_id;
    sys_sys_mode_e_type sys_mode;
    sys_sys_id_s_type                       sys_id ;

    uint16 active_band;

    sys_srv_status_e_type      srv_status;
      /**< Service status (SERVICE/NO SERVICE) of the system. Mask used is
            CM_SS_SRV_STATUS_MASK. */  
    sys_roam_status_e_type     roam_status;
       /**< Indicates the current hybrid HDR roaming status (FEATURE_HDR_HYBRID).  
            Mask used is CM_SS_HDR_ROAM_STATUS_MASK. */
    sys_channel_num_type        active_channel;
      /**< Report the active channel of the current serving system. */
      
    sys_srv_domain_e_type      srv_capability;
      /**< System's service capability. Mask used is
           CM_SS_SRV_CAPABILITY_MASK. */

    sys_active_prot_e_type     hdr_active_prot;
        /**< HDR active protocol revision information. Mask used is
             CM_SS_HDR_ACTIVE_PROT_MASK. Must define CM_API_HDR_ACTIVE_PROT before
             accessing this field. */
         
    sys_personality_e_type     hdr_personality;
      /**< HDR personality information. Mask used is CM_SS_HDR_PERSONALITY_MASK. */

    int                     rssi;
      /**< RSSI in positive dBm. 75 means -75 dBm. The ranges are as follows:
           - AMPS:      -110 to -89 dBm.
           - 800 CDMA:  -105 to -90 dBm.
           - 1900 CDMA: -108 to -93 dBm.
           - GSM/WCDMA: -105 to -60 dBm. 
           - TD-SCDMA:  -120 to -25 dBm. */

    int16                      ecio;
      /**< Ec/Io in negative 0.5 dBm. 63 means -31.5 dBm. */

    int                        io;
      /**< IO field is valid only when in HDR mode-only operation. */

    byte                       sinr;
      /**< Signal-to-Interface plus Noise Ratio (SINR)  SINR is applicable 
           when sys_mode = HDR or LTE

           For HDR SINR: range is from 0 to 8,where Level 8 represents 
                        the highest SINR.
           For LTE SINR: 10xdB values which are from -200 to 300 
                        are mapped to range 0 - 250. -20 db maps to 0, 
                        -19.8 db maps to 1, -19.6 maps to 2 ....
                        30 db maps to 250 */

    int16                      rscp;
      /**< Range of Received Signal Code Power (RSCP) is
           from -28 to -121 dBm. */
   int16                      gw_ecio;
	int16                      rsrp;  
      /**< Current Reference Signal Received Power (RSRP) in dBm as measured
           by L1. The range is -44 to -140 dBm. */
    int8                       rsrq;  
      /**< Current Reference Signal Receive Quality (RSRQ) as measured
           by L1. Quantities are in dB. The range is -20 to -3 dB. */ 
/* HDR rssi information*/
#ifdef FEATURE_HDR_HYBRID
    boolean                    hdr_hybrid;
      /**< Indicates whether the system is hybrid (FEATURE_HDR_HYBRID). */
	   uint16                     hdr_rssi;
      /**< HDR RSSI in positive dBm. 75 means -75dBm (FEATURE_HDR_HYBRID). 
           The range is -105 to -90 dBm. */
    
    int16                      hdr_ecio;
      /**< HDR Ec/Io values (used when mobile is in hybrid operation)
           (FEATURE_HDR). HDR Ec/Io is in negative 0.5 dBm i.e. 63 means -31.5dBm. */
    
    int                        hdr_io;
      /**< HDR IO values (used when mobile is in hybrid operation) (FEATURE_HDR).   
           IO range is from -106 dBm to -21 dBm. */
    
    byte                       hdr_sinr;
      /**< SINR range is from 0 to 8, where Level 8 represents the highest SINR. */
#endif
    
}quectel_nwinfo_type;

#if defined(QUECTEL_NITZ)
typedef struct
{
  boolean                                   univ_time_and_time_zone_avail;
    /**< Whether the universal time is available. */
  boolean                                   time_zone_avail;
    /**< Whether the timezone is available. */
  boolean                                   lsa_identity_avail;
    /**< Whether the LSA ID is available. */
  boolean                                   daylight_saving_adj_avail;
    /**< Whether daylight saving information is available. */
  sys_time_and_time_zone_s_type             univ_time_and_time_zone;
    /**< Universal Time Coordinated (UTC) time zone information. */
  sys_time_zone_type                        time_zone;
    /**< Current time zone information. */
  sys_lsa_identity_s_type                   lsa_identity;
    /**< LSA ID. */
  sys_daylight_saving_adj_e_type            daylight_saving_adj;

   uint64                                              nitz_sync_time_stamp;
}quectel_nitz_info;
#endif
boolean quectel_is_cdma_mode();

#define QUECTEL_IS_CDMA_MODE() \
	              quectel_is_cdma_mode()

#endif

typedef struct 
{
    uint64 u64PreItemRxCnt;
    uint64 u64PreItemTxCnt;
    uint64 u64ItemRxInc;
    uint64 u64ItemTxInc;
    cm_call_id_type CallId;
}QuecDataCntItem;

typedef struct
{
    uint64 u64TotalRxCnt;
    uint64 u64TotalTxCnt;
    QuecDataCntItem item[DS3GI_MAX_NUM_CALLS];
}QuecDataCntInfoType;

#define QUEC_MIN_AUTO_GDCNT_INTERVAL 30 //unit: second
#define QUEC_MAX_AUTO_GDCNT_INTERVAL 65535 //unit: second

#define QUEC_DEFAULT_AUTO_GDCNT_INTERVAL 0

#define QUEC_LOG_OUTPUT_THRESHOLD_BY_INC (64) //每接收或发送这么多 数据时，push一条log到qxdm中，以便debug (建议值10K)

#ifdef QUECTEL_REMOTE_IP_FEATURE
typedef enum
{
    QUECTEL_REMOTE_IP_NULL  = 0,
    QUECTEL_REMOTE_IP_DUMMY = 1,
}quectel_remote_ip_e_type;

#endif

dsat_result_enum_type quectel_exec_qcfg_gprsattach_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);


dsat_result_enum_type quectel_exec_qcfg_nwscanmode_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_qcfg_nwscan_mode_ex_cmd(QUEC_CMD_HDLR_PARAM); //maxcodeflag20160713

dsat_result_enum_type quectel_exec_qcfg_nwscanseq_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_servicedomain_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_band_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_roamservice_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_pdpduplicatechk_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_rrcrelease_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_sgsnr99_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_mscr99_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_hsdpa_cat_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcfg_hsupa_cat_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#if defined(QUECTEL_NWINFO_FEATURE)
dsat_result_enum_type quectel_exec_qnwinfo_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type quectel_exec_qcsq_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

/*
*	Amos.zhang-2018/04/03:Add at+qnetinfo cmd
*/

/*===========================================================================
FUNCTION: quec_netinfo_tab_init
============================================================================*/
/**
	@brief
	init quec_netinfo_tab global val
*/
/*==========================================================================*/
void quec_netinfo_tab_init(void);
/*============================================================================
FUCTION: quectel_exec_qnetinfo_cmd
=============================================================================*/
/**
	@brief
	AT+QNETINFO=<rat>,<bitmsk>
	rat:0:GSM 1:WCDMA 2:LTE
	bitmsk:
	if rat = GSM
	   ...
	if rat = WCDMA
	   ...
	if rat = LTE
	   bit0  - LTE_NETINFO_RSSSNR
	   ...
	@dependencies
	none
*/
/*============================================================================*/
dsat_result_enum_type quectel_exec_qnetinfo_cmd
(
  dsat_mode_enum_type mode, 			/*	AT command mode:			*/
  const dsati_cmd_type *parse_table,	/*	Ptr to cmd in parse table	*/
  const tokens_struct_type *tok_ptr,	/*	Command tokens from parser	*/
  dsm_item_type *res_buff_ptr			/*	Place to put response		*/
);

#endif

#ifdef QUECTEL_NW_LOCK
dsat_result_enum_type quectel_exec_qnwlock_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

dsat_result_enum_type quectel_exec_qgdcnt_cmd
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *res_buff_ptr
);

/******************************************************************************************
connor-2019/03/05:Description....
Refer to [Issue-Depot].[IS0000481][Submitter:connor.wu@quectel.com,Date:2019-03-05]
<褰撻厤缃负voice centric 鐢变簬sim鍗″紓甯稿鑷碨R LTE涓嶅彲鐢紝褰撴ā鍧楁病鏈夋敞鍐屽湪CDMA鏃讹紝鑰屾敞鍐屽湪LTE鏃讹紝
鐢变簬鐢典俊鐨凩TE鏄疎PS only涓嶆敮鎸乿oice call 浼氬鑷存ā鍧楀皢鐢典俊鐨凩TE鍔犲叆鍒癰ackoff鍒楄〃锛岃�屽鑷存ā鍧楁棤娉曟敞缃戣繘琛屼笟鍔°�
******************************************************************************************/

#ifdef QUECTEL_ATCOP_DISABLE_BACKOFF_LTE
dsat_result_enum_type quectel_exec_qcfg_disable_backoff_lte_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif


//<2016/06/22-QCM9XTCZ00002-P01-Chao.Zhou [Comment: add for fix build error]
dsat_result_enum_type quectel_exec_qpdpgdcnt_cmd
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *res_buff_ptr
);
//<2016/06/22-QCM9XTCZ00002-P01-Chao.Zhou 

dsat_result_enum_type quectel_exec_qaugdcnt_cmd
(
    dsat_mode_enum_type mode, 
    const dsati_cmd_type *parse_table,
    const tokens_struct_type *tok_ptr, 
    dsm_item_type *res_buff_ptr
);

#endif

#if defined(QUECTEL_CDMA_FEATURE)
dsat_result_enum_type quectel_exec_qctpwdcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#if defined(QUECTEL_TDS_CSQ_FEATURE)

dsat_result_enum_type quectel_exec_qcfg_tds_csq_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#if defined(QUECTEL_DRX_CONFIG)
dsat_result_enum_type quectel_exec_qcfg_drx_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#if defined(QUECTEL_REMOTE_IP_FEATURE)
dsat_result_enum_type quectel_exec_qcfg_remote_ip_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif
#if defined(QUECTEL_EHRPD_SET)
dsat_result_enum_type quectel_exec_qcfg_ehrpd_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif

#if defined(QUECTEL_NITZ)
dsat_result_enum_type quectel_exec_qlts_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);
#endif
/*
AT^NETCFG=<inst>,<tech_pref>,<cdma_profile>,<umts_profile>,<ip_family>
,<mcast>,<call_type>,<apn>,<auth_type>[,<user>[,<passwd>]]
*/
#if defined(QUECTEL_CUSTOMER_BONSON_NETCFG) || defined(QUECTEL_PPP_HUNGUP_EXT)//add by chao.zhou 2016/07/22 for porting pdpdrop cmd

typedef enum
{
    IP_TYPE_IPV4 = 4,
    IP_TYPE_IPV6 = 6,
    IP_TYPE_IPV4V6 = 10 
}bonson_ip_version_type;
#endif
#ifdef QUECTEL_CUSTOMER_BONSON_NETCFG

dsat_result_enum_type dsat_exec_bonson_netcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_bonson_qdatamode_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_bonson_qdatadown_cmd
(
  dsat_mode_enum_type mode,                /*  AT command mode:            */
  const dsati_cmd_type *parse_table,       /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,       /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

#endif
#ifdef QUECTEL_3GPP2_PROFILE_CONFIG
dsat_result_enum_type quectel_exec_cgdcontex_cmd
(
  dsat_mode_enum_type mode, 			/*	AT command mode:			*/
  const dsati_cmd_type *parse_table,	/*	Ptr to cmd in parse table	*/
  const tokens_struct_type *tok_ptr,	/*	Command tokens from parser	*/
  dsm_item_type *res_buff_ptr			/*	Place to put response		*/
);
#endif

#ifdef QUECTEL_PPP_HUNGUP_EXT//add by chao.zhou 2016/07/22 for porting pdpdrop cmd
dsat_result_enum_type dsat_exec_qpppdrop_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_QTRYRAT_FEATURE
extern int g_try_rat_value;
extern int quectel_rat_first;
extern boolean quectel_try_operate(uint8 value);

extern dsat_result_enum_type dsat_exec_qtryrat_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

typedef enum
{
 CHINA_UNICOM,
 CHINA_MOBILE,
 CHINA_TELECOM,
 OPERATOR_UNKNOWN
}que_mode_enum_type;
extern que_mode_enum_type quectel_get_reg_nw_operator(void);
#endif
#if 0//defined(QUECTEL_IOT_RPT_SUPPORT)

void  quectel_IOT_report_modem_info(); 

void  quectel_IOT_report_timer_cb(void);
#endif
//<2017/03/20-QCWXM9X00032-P01-wenxue.sheng, <[ATC] Segment 1==>Sync QCOPS CMD from 9215.>
#ifdef QUECTEL_QCOPS_FEATURE
dsat_result_enum_type dsat_exec_qcops_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_exec_qcopscfg_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type quectel_qcops_scan_rsp_handler
(
  ds_cmd_type         *cmd_buf             /* DS Command pointer         */
);

extern boolean qcops_get_running_state(void);

extern dsat_result_enum_type quec_qcops_copy_plmn_rats
(
	dsat_sys_detailed_plmn_list_s_type *list_ptr
);
extern dsat_result_enum_type quec_qcops_start_scan(void);

extern void quec_qcops_scan_band
(
    cm_band_pref_e_type               band_pref,
    /**< Switch to this band preference. */
    
    cm_band_pref_e_type               lte_band_pref,
    /**< Switch to this LTE Band preference. */
    
    cm_band_pref_e_type               tds_band_pref,
    /**< Switch to this TDS Band preference. */

    sys_plmn_id_s_type                *plmn,
    
    sys_sys_mode_e_type               rat
);

extern boolean rr_gprs_get_gprs_indicator_info(const gas_id_t gas_id);

extern int8 quectel_eng_convert_rx_lev_to_dBm( uint16 rxlev );

extern void queccell_update_sync_set_info(void);
extern void queccell_update_cset_info(void);

#endif
//>2017/03/20-QCWXM9X00032-P01-wenxue.sheng

#ifdef QUECTEL_NW_SRCH_OPTIMIZE//add by chao.zhou 2016/07/22 for porting nwoptmz/acq cmd->[begin]

#define QUEC_ONLY_CHECK_REGION

typedef enum
{
	QUEC_RPLMN_ACT_TYPE_UMTS,
	QUEC_RPLMN_ACT_TYPE_GSM,
	QUEC_RPLMN_ACT_TYPE_AUTO,
}QUEC_RPLMN_ACT_TYPE;

#define QUEC_IS_RPLMN_ACT_UMTS(rplmnact) ((rplmnact.act[0] & 0x80) && (0x00 == rplmnact.act[1]))
#define QUEC_SET_RPLMN_ACT_UMTS(rplmnact) \
{ \
	rplmnact.act[0] = 0x80; \
	rplmnact.act[1] = 0x00; \
}

#define QUEC_IS_RPLMN_ACT_GSM(rplmnact)  ((rplmnact.act[1] & 0x80) && (0x00 == rplmnact.act[0]))
#define QUEC_SET_RPLMN_ACT_GSM(rplmnact) \
{ \
	rplmnact.act[0] = 0x00; \
	rplmnact.act[1] = 0x80; \
}

#define QUEC_IS_RPLMN_ACT_AUTO(rplmnact)  ((0X00 == rplmnact.act[1]) && (0x00 == rplmnact.act[0]))
#define QUEC_SET_RPLMN_ACT_AUTO(rplmnact) \
{ \
	rplmnact.act[0] = 0x00; \
	rplmnact.act[1] = 0x00; \
}

#define Quectel_IsCoveredByChinaCell(plmn_id) ((4 == (plmn_id.identity[0] & 0x0F)) && (6 == (plmn_id.identity[0] / 0x10)) && (0 == (plmn_id.identity[1] & 0x0F)))


#define    QUEC_ACQ_DB_UPT_FLG_NONE           0x00
#define    QUEC_ACQ_DB_UPT_FLG_REG_SIG        0x01
#define    QUEC_ACQ_DB_UPT_FLG_GSM_FREQ       0x02
#define    QUEC_ACQ_DB_UPT_FLG_WCDMA_FREQ     0x04
#define    QUEC_ACQ_DB_UPT_FLG_INNER_REG_SIG  0x08

#define    QUEC_ACQ_DB_UPT_FLG_ALL (QUEC_ACQ_DB_UPT_FLG_REG_SIG  \
                                       | QUEC_ACQ_DB_UPT_FLG_GSM_FREQ \
                                       | QUEC_ACQ_DB_UPT_FLG_WCDMA_FREQ \
                                       | QUEC_ACQ_DB_UPT_FLG_INNER_REG_SIG \
                                       )

#define QUEC_ACQ_SAVE_OPT_NONE 0x00
#define QUEC_ACQ_SAVE_OPT_GSM 0x01
#define QUEC_ACQ_SAVE_OPT_WCDMA 0x02
#define QUEC_ACQ_SAVE_OPT_ALL (QUEC_ACQ_SAVE_OPT_GSM | QUEC_ACQ_SAVE_OPT_WCDMA)

#define QUEC_ACQDB_CHK_INTERVAL_DEFAULT 600 //unit: second
#define QUEC_ACQDB_CHK_INTERVAL_MIN 60 
#define QUEC_ACQDB_CHK_INTERVAL_MAX 0xFFFFF0 

extern void Quectel_AttentionToACQInfo(void);

extern void Quectel_ACQDBOptimizeCheck(uint32 uiUpdateFlag);

extern boolean Quectel_IsACQOptEnable(void);

extern void Quectel_ACQDBOptimizeEnable(void);

extern void Quectel_ACQDBOptimizeDisable(void);

extern boolean Quectel_ACQDBOptimizeSetChkInterval(uint32 uiValue);

extern uint32 Quectel_ACQDBOptimizeGetChkInterval(void);

extern boolean Quectel_ACQDBOptimizeChkIntervalValid(uint32 uiValue);

extern dsat_result_enum_type Quectel_AttentionToACQInfo_handler
( 
    ds_cmd_type * cmd_ptr     /* DS Command pointer */
);

extern dsat_result_enum_type Quectel_FWD_RegSigToDS_handler
( 
    ds_cmd_type * cmd_ptr     /* DS Command pointer */
);

dsat_result_enum_type dsat_exec_qcfg_nw_optmz_acq_info_cmd
(
dsat_mode_enum_type mode,             /*  AT command mode:            */
const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
const tokens_struct_type *tok_ptr,
dsm_item_type *res_buff_ptr
);
#endif//add by chao.zhou 2016/07/22 for porting nwoptmz/acq cmd->[end]


#ifdef QUECTEL_PPP_HUNGUP_EXT//add by chao.zhou 2016/07/22 for porting ppp/termframe cmd
dsat_result_enum_type dsat_exec_qcfg_ppp_term_frame_cmd
(
dsat_mode_enum_type mode,             /*  AT command mode:            */
const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
const tokens_struct_type *tok_ptr,
dsm_item_type *res_buff_ptr
);
#endif
#ifdef QUECTEL_PPP_V4V6_CHECK_FEATURE //add by duke.xin 2017/07/22 
dsat_result_enum_type dsat_exec_qcfg_ppp_v4v6_cmd
(
dsat_mode_enum_type mode,             /*  AT command mode:            */
const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
const tokens_struct_type *tok_ptr,
dsm_item_type *res_buff_ptr
);
extern unsigned int   ppp_v4v6_flag;
#endif
#ifdef QUECTEL_SET_RSSI_THRESHOLD
extern boolean quec_set_rssi_threshold(uint8 rssi_change);

extern uint8 quec_get_rssi_threshold(void);

extern dsat_result_enum_type dsat_exec_qcfg_rssi_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,
    dsm_item_type *res_buff_ptr
);
#endif
#ifdef QUECTEL_DYNAMIC_AP_READY//2014,10,31 francis
extern  dsat_result_enum_type   dsat_exec_qcfg_ap_ready_cmd
  (
    dsat_mode_enum_type mode,             /*  AT command mode:          */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,
    dsm_item_type *res_buff_ptr
);

extern dsat_result_enum_type quectel_qcfg_get_ap_ready
(
  dsm_item_type *res_buff_ptr
);

extern dsat_result_enum_type quectel_qcfg_set_ap_ready
(
  const tokens_struct_type *tok_ptr
);

#endif

//lory 2016/09/23
#ifdef QUECTEL_SLEEP_IND
dsat_result_enum_type dsat_exec_qcfg_sleepind_level_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

#endif
//2016/09/20 for wakeupin
#ifdef QUECTEL_WAKEUP_IN
dsat_result_enum_type dsat_exec_qcfg_wakeupin_level_cmd
(
 dsat_mode_enum_type mode, 
 const dsati_cmd_type *parse_table,
 const tokens_struct_type *tok_ptr, 
 dsm_item_type *res_buff_ptr
);

#endif

#ifdef QUECTEL_NW_SRCH_OPTIMIZE
void Quectel_ResetAcqInfo(void);
#endif

//<2016/11/18-QCWXM9X00020-P01-wenxue.sheng, <[ATC] Segment 1==> Add AT+QRXPWR function.>
#if defined(QUECTEL_AT_COMMON_SUPPORT)
extern dsat_result_enum_type dsat_exec_qrxpwr_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,
    dsm_item_type *res_buff_ptr
);
#endif // #if defined(QUECTEL_AT_COMMON_SUPPORT)
//>2016/11/18-QCWXM9X00020-P01-wenxue.sheng

//<2016/12/15-QCWXM9X00021-P02-wenxue.sheng, <[QCFG] Segment 2==> Copy AT+QCFG="disrplmn" from EC20.>
#if defined(QUECTEL_RPLMN_CONTROL_FEATURE)
extern uint8 quectel_get_rplmn_disable_ctrl_value_from_nv(void);
extern dsat_result_enum_type dsat_exec_qcfg_disrplmn_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:            */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
    const tokens_struct_type *tok_ptr,
    dsm_item_type *res_buff_ptr
);
#endif // #if defined(QUECTEL_RPLMN_CONTROL_FEATURE)
//>2016/12/15-QCWXM9X00021-P02-wenxue.sheng

//lory 2017/05/03 enable multi-pdn same apn 
#ifdef QUECTEL_ENABLE_MULTI_PDP_SAME_APN
void quectel_enable_multi_pdn_same_apn_init(void);
#endif

boolean quectel_band_ind_isValid(sys_sys_mode_e_type sys_mode ,int band_ind);
boolean quectel_isInvalidLTE_par(void);
uint16 quectel_get_network_uarfcn(sys_sys_mode_e_type sys_mode);
int quectel_gsm_band_arfcn_map(uint16 arfcn);
int quectel_tdscdma_band_arfcn_map(uint16 arfcn);
int quectel_nw_mode_map(sys_sys_mode_e_type sys_mode);
uint16 quectel_get_network_active_band(sys_sys_mode_e_type sys_mode);
void  get_the_mcc_mnc_for_1xlte(uint32 *mcc,uint32 *mnc,boolean *mnc_includes_pcs_digit_3);

#ifdef QUECTEL_ANS1_ENCODE_ERR_FEATURE
extern boolean Quectel_ANS1ErrEncodeHandleCmd(boolean ans1_err);
extern void Quectel_ANS1ErrEncodeHandler(const ds_cmd_type *cmd_ptr);
#endif

#endif/*QUECTEL_NW_ATC_H*/
