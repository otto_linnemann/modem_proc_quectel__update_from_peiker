/*===========================================================================

                           QUECTEL_WMS_ATC . H

DESCRIPTION
  quectel wms atc function

REFERENCES

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
04/1/2013 quectel wms atc function declare add by clark.li 

===========================================================================*/
#ifndef __QUECTEL_WMS_ATC_H__
#define __QUECTEL_WMS_ATC_H__

#include "comdef.h"
#include "customer.h"
#include "dsat_v.h"
#include "dsati.h"

//<2016/10/10-QCM9XTVG00059C001-P01-Vicent.Gao,<[LTESMS] Segment 1==>Add QCFG sub-cmd: ltesms/format.>
#if defined(QUECTEL_SMS_FEATURE)
#define QCFG_LTE_SMS_FORMAT_CDMA     (0)
#define QCFG_LTE_SMS_FORMAT_GSM      (1)
#endif //#if defined(QUECTEL_SMS_FEATURE)
//>2016/10/10-QCM9XTVG00059C001-P01-Vicent.Gao

//<2016/12/05-QCM9XTVG00039-P01-Vicent.Gao, <[SMS] Segment 1==>Con-SMS merge read design.>
#define CSBUF_CON_SMS_BUF_MAX_CNT   (1)
#define CSBUF_CON_SMS_SEG_MAX_CHAR  (160)
#define CSBUF_CON_SMS_SEG_MAX_BYTE  (CSBUF_CON_SMS_SEG_MAX_CHAR)
#define CSBUF_CON_SMS_MAX_SEG       (11)
//>2016/12/05-QCM9XTVG00039-P01-Vicent.Gao

//<2016/12/06-AUTH0005C004-P01-Vicent.Gao, <[VZW] Segment 1==>Fix OMADM issues.>
#if defined(FEATURE_CERT_OMADM_WAP_PUSH)
#define OMADM_CSBUF_CON_SMS_BUF_MAX_CNT     (1)
#define OMADM_CON_SMS_INTACT_DATA_BUF_SIZE  (CSBUF_CON_SMS_SEG_MAX_BYTE * CSBUF_CON_SMS_MAX_SEG)
#endif // #if defined(FEATURE_CERT_OMADM_WAP_PUSH)
//>2016/12/06-AUTH0005C004-P01-Vicent.Gao

#ifdef QUECTEL_WMS_CONCATENATED_FEATURE
typedef struct concatenated_sms_udh
{
    uint8    msg_refer;
    uint8     seq_num; 
    uint8     total_sms;
}quectel_con_sms_udh;
#endif /* QUECTEL_WMS_CONCATENATED_FEATURE */

//<2016/12/05-QCM9XTVG00039-P01-Vicent.Gao, <[SMS] Segment 1==>Con-SMS merge read design.>
typedef struct
{
    uint8 aData[CSBUF_CON_SMS_SEG_MAX_BYTE];
    uint16 uLen;
} CSBUF_ConSMSSegStruct;

typedef struct
{
    uint16 uMsgRef;
    uint8 uMsgTot;

    CSBUF_ConSMSSegStruct asSeg[CSBUF_CON_SMS_MAX_SEG];
    boolean abSegValid[CSBUF_CON_SMS_MAX_SEG];
} CSBUF_ConSMSStruct;

typedef struct
{
    uint16 msgRef;
    uint8 msgSeg;
    uint8 msgTot;
} CSBUF_UdhStruct;
//>2016/12/05-QCM9XTVG00039-P01-Vicent.Gao

extern dsat_num_item_type dsat_qcmgo_val;

/*===========================================================================

FUNCTION    QUECTEL_DELETE_MESSAGES_EXT

DESCRIPTION
  This function is used for +QMGDA command when a <stat_val> is present.
  Tries to get the list of message with that particular <stat_val> in memory.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type quectel_delete_messages_ext
(
    byte * stat_val,
    boolean type,
    wms_memory_store_e_type mem_store
);

/*===========================================================================

FUNCTION DSATETSISMS_EXEC_QMGDA_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QMGDA command.
  This command provides the ability to delete different combination type of sms. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
extern dsat_result_enum_type dsatetsisms_exec_qmgda_cmd
(
  dsat_mode_enum_type mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,      /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION DSATETSISMS_EXEC_QCMGO_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QCMGO command.
  This command provides the ability to open/close URC of the overflow of the sms buffer. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
extern dsat_result_enum_type dsatetsisms_exec_qcmgo_cmd
(
  dsat_mode_enum_type mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,      /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

#ifdef QUECTEL_WMS_CONCATENATED_FEATURE
/*===========================================================================

FUNCTION DSATETSISMS_EXEC_QCMGENREF_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QCMGENREF command.
  This command can be used to generate a new message reference. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
extern dsat_result_enum_type dsatetsisms_exec_qcmgenref_cmd
(
  dsat_mode_enum_type mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,      /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION DSATETSISMS_EXEC_QCMGS_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QCMGS command.
  This command can be used to send long sms to Network. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
extern dsat_result_enum_type dsatetsisms_exec_qcmgs_cmd
(
  dsat_mode_enum_type mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,      /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION DSATETSISMS_EXEC_QCMGR_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QCMGR command.
  This command can be used to read long sms. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
extern dsat_result_enum_type dsatetsisms_exec_qcmgr_cmd
(
  dsat_mode_enum_type mode,              /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,      /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr              /*  Place to put response       */
);

/*===========================================================================

FUNCTION    DSATETSISMSI_FILL_CONCATENATE_SMS_UDH_16

DESCRIPTION
  This function fill concatenate sms 16bit udh information.
  
DEPENDENCIES
  None

RETURN VALUE
  None
  
SIDE EFFECTS
  None

===========================================================================*/
void dsatetsismsi_fill_Concatenate_SMS_udh_8
( 
  wms_gw_pp_ts_data_s_type * msg_ptr  /* TPDU data pointer */
);

#endif /* QUECTEL_WMS_CONCATENATED_FEATURE */
/*===========================================================================

FUNCTION    QUECTEL_SMS_EXT_CHARACTER_LEN

DESCRIPTION
  This function is used for calculating the length of the externed character.
  
DEPENDENCIES
  None

RETURN VALUE
  length of the externed character

SIDE EFFECTS
  None

===========================================================================*/
extern uint8 quectel_sms_ext_character_len(char* str, int sms_length);

//<2016/12/06-AUTH0005C004-P01-Vicent.Gao, <[VZW] Segment 1==>Fix OMADM issues.>
//<2017/08/04-QCM9X00128-P01-Joyce.Sun, <[ETWS] Segment 1==> Fix ETWS issues.>
#if defined(FEATURE_CERT_OMADM_WAP_PUSH) || defined(QUECTEL_ETWS_FEATURE)
//>2017/08/04-QCM9X00128-P01-Joyce.Sun
extern void *wms_mem_malloc(size_t size);
extern void wms_mem_free(void *mem_ptr);
#endif // #if defined(FEATURE_CERT_OMADM_WAP_PUSH)
//>2016/12/06-AUTH0005C004-P01-Vicent.Gao

#endif

