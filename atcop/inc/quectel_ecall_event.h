/*===========================================================================

                           QUECTEL_ECALL_EVENT . H

DESCRIPTION
  eCall URC event process function  

REFERENCES
  3GPP TS 26.267

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
03/13/2015  Rex add eCall URC event process.
03/15/2019  Rambo    Add ecall event ECALL_EVENT_SF_RECEIVED,
                           ECALL_EVENT_CLEAR_DOWN_FALLBACK_TIMEOUT, 
                           ECALL_EVENT_IVS_INITIATION_TIMEOUT 
===========================================================================*/
#ifndef __QUECTEL_ECALL_EVENT_H__
#define __QUECTEL_ECALL_EVENT_H__

#include "dsat_ext_api.h"
#include "dsat_v.h"
#if defined(QUECTEL_ECALL_EXT)
#include "ecall_app.h"

typedef enum
{
	ECALL_EVENT_SF_RECEIVED,
	ECALL_EVENT_START_RECEIVED_NO_MSD,
	ECALL_EVENT_START_RECEIVED_MSD,
	ECALL_EVENT_MSD_SENT,
	ECALL_EVENT_SESSION_COMPLETED,
	ECALL_EVENT_SESSION_RESET,
	ECALL_EVENT_MSD_FAST_MODE,
	ECALL_EVENT_MSD_ROBUST_MODE,
	ECALL_EVENT_ACK_RECEIVED,
	ECALL_EVENT_NACK_RECEIVED,
	ECALL_EVENT_IVS_ERR,								//10
	ECALL_EVENT_TX_COMPLETED,
	ECALL_EVENT_HLACK_RECEIVED,
	ECALL_EVENT_CLEARDOWN_RECEIVED,
	ECALL_EVENT_IVS_LOSE_SYNC,
	ECALL_EVENT_NACK_OUT_OF_ORDER,						//15
  	ECALL_EVENT_ACK_OUT_OF_ORDER,
	ECALL_EVENT_PSAP_END_RECEIVED,
	ECALL_EVENT_ORIGINATE_OK,
	ECALL_EVENT_ORIGINATE_FAIL,
	ECALL_EVENT_WAIT_START_TIMEOUT,
	ECALL_EVENT_TRANSMIT_TIMEOUT,
	ECALL_EVENT_WAIT_HALK_TIMEOUT,
	ECALL_EVENT_REQURE_UPDATE_MSD,
	ECALL_EVENT_UPDATING_MSD_EVENT,
	ECALL_EVENT_UPDATE_MSD_TIMEOUT,

	ECALL_EVENT_ORIGINATE_FAIL_AND_REDIAL, 
	ECALL_EVENT_CALL_DROP_AND_REDIAL,
	ECALL_EVENT_CLEAR_DOWN_FALLBACK_TIMEOUT,
	ECALL_EVENT_IVS_INITIATION_TIMEOUT,
    ECALL_EVENT_T9_TIMEOUT,
    ECALL_EVENT_T10_TIMEOUT,

	ECALL_EVENT_MAX
}ecall_event_e_type;

typedef enum
{
	ECALL_EVENT_FAILS_IND_TYPE      = 0,
	ECALL_EVENT_PROCESS_IND_TYPE    ,
	ECALL_EVENT_MSDUPDATE_IND_TYPE  ,
	ECALL_EVENT_ESTABLISH_IND_TYPE  ,
	ECALL_EVENT_HACKCODE_IND_TYPE   ,
	ECALL_EVENT_ORI_FAIL_IND_TYPE   ,
	ECALL_EVENT_DROP_IND_TYPE       ,
	ECALL_EVENT_EXTEND_STATE_IND_TYPE,
	ECALL_EVENT_MAX_IND_TYPE
}ecall_event_ind_type;

typedef dsat_result_enum_type(* ecall_msg_event_f_type)
(
  dsat_mode_enum_type mode,
  quec_ecall_cb_data_type *event_info
);

typedef struct
{
    ecall_event_e_type event;
    ecall_msg_event_f_type handler;
}ecall_event_handler_tab_type;



dsat_result_enum_type dsatetsiecall_signal_handler
(
  dsat_mode_enum_type mode
);


void dsat_quec_ecall_init(void);

void quec_ecall_qmi_to_mcm_urc_ind(char *urc_event,uint8 urc_evnet_len);

void quec_ecall_qmi_to_mcm_event_ind(		ecall_event_ind_type ind_type,	uint16	ind_value_1,uint16	ind_value_2);

#endif


#endif 
