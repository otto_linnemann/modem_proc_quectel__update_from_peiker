#ifndef QUECTEL_ATCMD_TAB_H
#define  QUECTEL_ATCMD_TAB_H

#include "dstask.h"
#include "dsati.h"

#ifdef QUECTEL_AT_COMMON_SUPPORT


extern const dsati_cmd_type dsat_quectel_cmd_table[];
extern const unsigned int dsat_quectel_cmd_table_size;

extern const dsati_cmd_ex_type dsat_quectel_cmd_table_ex [];
#endif

#endif/*QUECTEL_ATCMD_TAB_H*/
