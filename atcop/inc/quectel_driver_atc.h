#ifndef _QUECTEL_DRIVER_ATC_H_
#define _QUECTEL_DRIVER_ATC_H_
#include "comdef.h"
#include "customer.h"
#include "dsat_v.h"
#include "dsati.h"


//Ramos 20160215 add for AT+QNAND CMD
#ifdef QUECTEL_QNAND
typedef enum
{
    QUECTEL_NAND_ITEM_INVALID = -1,
    QUECTEL_NAND_ITEM_WBLOCK = 0,
    QUECTEL_NAND_ITEM_READ_RECOVERY_CNT = 1,
   QUECTEL_NAND_ITEM_GBBLOCK = 2,
   QUECTEL_NAND_ITEM_EBLOCK = 3,
   QUECTEL_NAND_ITEM_FLASH_STRESS_TEST = 4,
   QUECTEL_NAND_ITEM_FLASH_ID =5,
    QUECTEL_NAND_ITEM_MAX
}EQNAND_Item;
#endif

#ifdef QUECTEL_DRIVER_ATC

//modify by clark at 2013.6.8 move macro from quectel_driver_atc.c to here 
#ifdef QUECTEL_AT_COMMON_SUPPORT
typedef enum
{
  HW_CFG_VBATT_THRESHOLD,
  HW_CFG_VBATT_ENABLE,
  HW_CFG_TEMPERATURE_THRESHOLD,
  HW_CFG_TEMPERATURE_ENABLE,
} hw_cfg_type;
#endif

dsat_result_enum_type dsat_exec_qhwcfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

atoi_enum_type quec_atoi(int *val_arg_ptr, const byte *s);

#endif

//Ramos 20160215 add for AT+QNAND CMD
#ifdef QUECTEL_QNAND
/*===========================================================================

FUNCTION DSAT_EXEC_QNAND_CMD

DESCRIPTION
Configure the special feature.

DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  None

===========================================================================*/

dsat_result_enum_type dsat_exec_qnand_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

void Quectel_QNAND_List(void);
EQNAND_Item Quectel_QNAND_Get_Item(dsat_string_item_type *p);

dsat_result_enum_type dsat_exec_qnand_wblock_cmd(  const tokens_struct_type *tok_ptr,  dsm_item_type *res_buff_ptr);
dsat_result_enum_type dsat_exec_qnand_read_recovery_cnt_cmd(  const tokens_struct_type *tok_ptr,  dsm_item_type *res_buff_ptr);
dsat_result_enum_type dsat_exec_qnand_read_GBBlock_cmd(const tokens_struct_type *tok_ptr, dsm_item_type *res_buff_ptr);
dsat_result_enum_type dsat_exec_qnand_eblock_cmd(const tokens_struct_type *tok_ptr, dsm_item_type *res_buff_ptr);
dsat_result_enum_type dsat_exec_qnand_Flash_StrTest_cmd(  const tokens_struct_type *tok_ptr,  dsm_item_type *res_buff_ptr);
#endif
#if defined(QUECTEL_GPIO)
#define GSM_PWR_TYPE_NUM (24)
#define GSM_PWR_TYPE_INVALID_GAIN (0xFFFF)

typedef enum
{
  QUEC_GSM_RF_BAND_850  = 850,
  QUEC_GSM_RF_BAND_900  = 900,
  QUEC_GSM_RF_BAND_1800 = 1800,
  QUEC_GSM_RF_BAND_1900 = 1900,
} quec_gsm_rf_band_type;

typedef enum
{
  QUEC_GSM_GMSK,
  QUEC_GSM_8PSK,
} quec_gsm_modulation_type;
typedef struct
{
  nv_items_enum_type nv_item;
  quec_gsm_rf_band_type rf_band;
  quec_gsm_modulation_type modulation;
  uint8 tx_slot;
  int default_pwr;
  int gain;
  
} quec_gsm_multislot_type;

extern dsat_result_enum_type dsat_qgpio_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsat_qgpiocfg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsat_qgpios_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsat_qgpior_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsat_qgpiow_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsat_qgpiol_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif
#ifdef QUECTEL_AT_Q_VERSION//2017.12.04 add command AT+QVERSION by wayne.wei
dsat_result_enum_type dsat_qversion_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_QTEMP
dsat_result_enum_type dsat_qtemp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_qtempdbg_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_qtempdbglvl_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#endif

