/*===========================================================================

                           QUECTEL_CALL_ATC . H

DESCRIPTION
  quectel call atc function

REFERENCES

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2013 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
12/7/2013 quectel call atc function declare add by clark.li

===========================================================================*/
#ifndef __QUECTEL_CALL_ATC_H__
#define __QUECTEL_CALL_ATC_H__

#include "comdef.h"
#include "customer.h"
#include "dsat_v.h"
#include "dsati.h"
#include "cm.h"

#ifdef QUECTEL_CALL_FEATURE

#define NO_CAUSE_REASON     0x0

#define CM_CS_CALL_ID_MAX  7

//<2017/01/12-QCM9XTVG00076-P01-Vicent.Gao, <[VTS] Segment 1==> Add cmd: QCFG-vts/async.>
#define QCFG_VTS_ASYNC_ENABLE   (1)
#define QCFG_VTS_ASYNC_DISABLE  (0)
//>2017/01/12-QCM9XTVG00076-P01-Vicent.Gao

typedef enum qhup_cause_e_type
{
    DSAT_QHUP_NORMAL_CALL_CLEARING = 0,
    DSAT_QHUP_UNASSIGNED_NUMBER,
    DSAT_QHUP_USER_BUSY,
    DSAT_QHUP_NO_USER_RESPONDING,
    DSAT_QHUP_CALL_REJECTED,
    DSAT_QHUP_DESTINATION_OUT_OF_ORDER,
    DSAT_QHUP_NORMAL_UNSPECIFED,
    DSAT_QHUP_INCOMPATIBLE_DESTINATION,
    DSAT_QHUP_CAUSE_MAX
}dsat_qhup_cause_e_type;

extern dsat_num_item_type dsat_qhup_val[2];
extern uint8 g_iQHUP_cause_reason;
extern uint8 g_vtd_interval;

/*===========================================================================

FUNCTION QUECTEL_QHUP_CAUSE_SRCH_LIST

DESCRIPTION
  This function search the hang up cause reason from the table.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
param_srch_enum_type quectel_qhup_cause_srch_list
    (dsat_num_item_type qhup_cause, uint8*qhup_cause_type, uint8 *index);

/*===========================================================================

FUNCTION QUECTEL_GET_HUP_CAUSE_REASON

DESCRIPTION
  This function get hang up cause reason.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
uint8 quectel_get_hup_cause_reason(dsat_qhup_cause_e_type qhup_cause);

/*===========================================================================

FUNCTION QUECTEL_GET_CALL_ID_NUM

DESCRIPTION
  This function get the number of call and relevant call id.
  
DEPENDENCIES
  None

RETURN VALUE

SIDE EFFECTS
  
===========================================================================*/
uint8 quectel_get_call_id_num(cm_call_id_type* call_id_ptr);

/*===========================================================================

FUNCTION DSATETSICALL_EXEC_QHUP_CMD

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+QHUP command.
  This command provides the ability to hand up calls indicating a
  specific release cause. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicall_exec_qhup_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
/*===========================================================================

FUNCTION  dsatetsicall_exec_vtd_cmd

DESCRIPTION
  This function takes the result from the command line parser
  and executes it. It executes AT+VTD command.
  This command sets duration and interval of VTS. 

DEPENDENCIES
  None

RETURN VALUE
  returns an enum that describes the result of the command execution.
  possible values:
    DSAT_ERROR :    if there was any problem in executing the command
    DSAT_OK :         if it is a success.

SIDE EFFECTS
  None

===========================================================================*/
dsat_result_enum_type dsatetsicall_exec_vtd_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

//<2017/01/12-QCM9XTVG00076-P01-Vicent.Gao, <[VTS] Segment 1==> Add cmd: QCFG-vts/async.>
extern uint8 QCFG_VTSASYNC_GetEnable(void);
//>2017/01/12-QCM9XTVG00076-P01-Vicent.Gao

//<2017/08/28-QCM9X00138-P01-Vicent.Gao, <[CALL] Segment 1==> Add new cmd: AT+CIREP.>
extern uint8 QL_CIREP_GetImsVops(void);

extern dsat_result_enum_type dsat_exec_cirep_cmd
(
    dsat_mode_enum_type eMode, 
    const dsati_cmd_type *pParseTab,
    const tokens_struct_type *pTok, 
    dsm_item_type *pResBuf
);
//>2017/08/28-QCM9X00138-P01-Vicent.Gao

#endif /* QUECTEL_CALL_FEATURE */
#endif /* __QUECTEL_CALL_ATC_H__ */

