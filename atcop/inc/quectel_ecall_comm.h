/*!  
  @file
  Quectel_ecall_comm.h

  @brief
  This file defines declarations for QUECTEL ECALL COMMON use

  @see
  NONE
*/

/*===============================================================================
  Copyright (c) 2018 Quectel Wireless Solution, Co., Ltd.  All Rights Reserved.
  Quectel Wireless Solution Proprietary and Confidential.
=================================================================================*/
/*===============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

WHEN         WHO           WHAT, WHERE, WHY
----------   ----------    -----------------------------------------------------------
05/26/2018   vicent        Add code for ECALL debug info
03/15/2019   Rambo         Add ecall timer value
03/28/2019   Rambo         Add era_glonass ecall timer value
=================================================================================*/

#ifndef _QUECTEL_ECALL_COMM_
#define _QUECTEL_ECALL_COMM_



#include "../../../datamodem/interface/atcop/inc/dsat_v.h"
#include "ecall_efs.h"
#define QECALL_FAILURE       	0
#define QECALL_SUCCESS      	1
#define QECALL_FALSE           	0
#define QECALL_TRUE             1
#define QECALL_QUERYPARAM 		0
#define QECALL_SETPARAM     	1
#define ECALL_DIALNUM_NORMAL    0
#define ECALL_DIALNUM_OVERRIDE 	1
#define QECALL_STOP 			0
#define QECALL_START 			1
#define DS_MAX_ECALL_DIGITS_STRING_LEN 				18
#define DS_MAX_ECALL_SIZEOF_VIN_LEN 				17
#define DS_MAX_ECALL_SIZEOF_SERVICE_PROVIDER_LEN 	16
#define DS_MAX_ECALL_MSD_LEN 			140
#define DS_MAX_ECALL_HEX_MSD_LEN 		280

#define ECALL_MSD_STRING_TYPE   		0
#define ECALL_MSD_CANNED_TYPE 	 		1

#define ECALL_MODE_ONLY 				1
#define ECALL_MODE_DEFAULT 				2
#define DS_MAX_ECALL_DIGITS_STRING_LEN 	18
#define DS_ECALL_EFS_MAX_REC_LEN  		1024
#define ECALL_EFS_MAX_ARRAY_SIZE 		50
#define ECALL_EFS_MAX_STRING_SIZE 		32




typedef enum qecall_e_type
{
  DSAT_QECALL_TEST    = 0,
  DSAT_QECALL_RECONF  = 1,
  DSAT_QECALL_MANUAL  = 2,
  DSAT_QECALL_AUTO    = 3,
  DSAT_QECALL_STOP = 4,
  DSAT_QECALL_MAX     
}dsat_qecall_e_type;



typedef enum
{
	DSAT_QECALL_ENABLE = 0,
    DSAT_QECALL_VOC_CFG,
    DSAT_QECALL_MSD_TYPE,
    DSAT_QECALL_MOD_TYPE,
    DSAT_QECALL_PROCESS_INFO,
    DSAT_QECALL_T5,
    DSAT_QECALL_T6,
    DSAT_QECALL_T7,
    DSAT_QECALL_MOFAIL_REDIAL,
    DSAT_QECALL_DROP_REDIAL,
    DSAT_QECALL_DIALNUM_TYPE,
    DSAT_QECALL_CFG_MAX
}quec_ecall_cfg_e_type;


typedef struct
{
	uint8	enable;
	boolean voc_config;
	uint8   msd_type;
	uint8   dialnum_type;
	uint8   digits[DS_MAX_ECALL_DIGITS_STRING_LEN];
	uint8   mode;
	uint8   default_mode;
	uint8   process_info;

	uint8 	t5_value;
	uint8 	t6_value;
	uint8 	t7_value;

	uint8 	orig_fail_redial_value;
	uint8 	dropped_redial_value;

    uint16 	t2_value;
	uint16 	t3_value;
	uint16 	t9_value;
    uint16  t10_value;

    uint8   enable_era_glonass_system;
    uint16  era_glonass_redial_timer_value;
    uint16  era_glonass_auto_answer_timer_value;
}quec_ecall_cfg_s_type;


typedef struct{

	uint8 requrie_upd_msd;
	int mt_push_flg;   
	int mt_flg;
	int pull_flg;
	
	quec_ecall_cfg_s_type quec_ecall_cfg;
}quec_ecall_comm_s_type;


void  quec_ecall_set_require_upd_msd(uint8 flg);
uint8 quec_ecall_get_require_upd_msd(void);



void quec_ecall_set_mt_push_flg(int flg);
int  quec_ecall_get_mt_push_flg(void);

void quec_ecall_set_mt_flg(int flg);
int  quec_ecall_get_mt_flg(void);

void quec_ecall_set_pull_flg(int flg);
int  quec_ecall_get_pull_flg(void);


/* quectel cfg related interface  */
int    quec_ecall_get_orig_fail_redail_value(void);
int    quec_ecall_get_dropped_redail_value(void);
uint8* quec_ecall_get_digits(void);
void   quec_ecall_set_dialnum_digits(uint8 type,uint8 *digits);

uint16 quec_ecall_get_cfg(quec_ecall_cfg_e_type cfg_type, uint8* val_ptr);
uint16 quec_ecall_set_cfg(const tokens_struct_type *tok_ptr, quec_ecall_cfg_e_type cfg_type);

uint16 quec_ecall_set_ext_msd(byte *hex_str);



void  quec_ecall_comm_obj_init(void);


#endif
