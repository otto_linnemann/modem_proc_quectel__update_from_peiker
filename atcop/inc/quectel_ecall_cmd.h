/*===========================================================================

                           QUECTEL_ECALL_CMD . H

DESCRIPTION
  eCall cmd function process statement

REFERENCES
  3GPP TS 26.267

EXTERNALIZED FUNCTIONS
 
INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright(c) 2012 QUECTEL Incorporated.
All rights reserved. 
QUECTEL Confidential and Proprietary

EDIT HISTORY FOR MODULE
01/10/2013  Clark.li add qecall_session_stop/ecall_session_get_ecall_info and some ds cmd callback 

===========================================================================*/
#ifndef QUECTEL_ECALL_CMD_H
#define QUECTEL_ECALL_CMD_H

#include "comdef.h"

#include "msg_diag_service.h"

#if defined(QUECTEL_ECALL_EXT)



/*==========================================================================

  FUNCTION ECALL_SESSION_GET_ECALL_INFO
    This function will queue up a cmd to ecall_app_task and monitor the eCall
    session call info

  DESCRIPTION

  DEPENDENCIES
    None.

  RETURN VALUE
    Boolean - 1 = Command Queued Up Successfully
              0 = Command Queued Up Failed
    
  SIDE EFFECTS
    None

===========================================================================*/
boolean ecall_session_get_ecall_info
(
  ecall_session_ecall_info_cb_type call_info_cb
);



/*===========================================================================
FUNCTION  DSATETSICALL_QECALLINFO_CB_FUNC

DESCRIPTION
Start ecall style and end status command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
void dsatetsicall_qecallinfo_cb_func(ecall_type_of_call type_of_call,
                                                            ecall_activation_type activation_type, ecall_end_status_type end_status);

/*===========================================================================
FUNCTION  DSATETSICALL_QECALLSTART_CB_FUNC

DESCRIPTION
  ECALL session status command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
void dsatetsicall_qecallstart_cb_func(ecall_session_status_type session_status);

/*===========================================================================
FUNCTION  DSATETSICALL_QECALLSTOP_CB_FUNC

DESCRIPTION
  ECALL stop status command callback function

DEPENDENCIES
  None

RETURNS
  None

SIDE EFFECTS
  Adds command in DS command buffer

===========================================================================*/
void dsatetsicall_qecallstop_cb_func(ecall_end_status_type end_status);
#endif
#endif

