#ifndef QUECTEL_PPP_AUTH_H
#define  QUECTEL_PPP_AUTH_H

#include "dstask.h"
#include "dsati.h"

#ifdef QUECTEL_PPP_AUTH_FEATURE

#define QUEC_MAX_3GPP_PROFILE_NUM 24

typedef enum
{
	QUEC_PPP_AUTH_MIN = 0,
	QUEC_PPP_AUTH_AUTO = QUEC_PPP_AUTH_MIN,
	QUEC_PPP_AUTH_PAP , 
	QUEC_PPP_AUTH_CHAP,
	QUEC_PPP_AUTH_NONE,
	QUEC_PPP_AUTH_MAX = QUEC_PPP_AUTH_NONE,//对none这种情况，暂时不支持，将其附成MAX
}eQuecPPPAuthType;

typedef struct _TQuecPPPAuthParam
{
	boolean bThirsty[QUEC_MAX_3GPP_PROFILE_NUM];
	uint8 u8DefaultAuthType[QUEC_MAX_3GPP_PROFILE_NUM];
	uint8 u8LastValidType[QUEC_MAX_3GPP_PROFILE_NUM];
}TQuecPPPAuthParam;

#define QUEC_IS_VALID_PPP_AUTH(X) ((X >= QUEC_PPP_AUTH_MIN) && (X < QUEC_PPP_AUTH_MAX))

void Quectel_PPPAuthPowerInit(void);
boolean Quectel_SetPPPAuthType(uint32 uiProfile, boolean bDef, eQuecPPPAuthType eAuthType);
eQuecPPPAuthType Quectel_GetPPPAuthType(uint32 uiProfile, boolean bDef);
void Quectel_UpdatePPPAuthParamState(uint32 uiProfile, boolean bValue);
void Quectel_CorrectPPPAuthParamByResult(boolean bGotIP);



dsat_result_enum_type quectel_exec_auth_cmd(QUEC_CMD_HDLR_PARAM);

#endif

#endif
