#ifndef  QUECTEL_DS_TCPIP_H
#define   QUECTEL_DS_TCPIP_H

#include "quectel_ds_tcpip_cb.h"
#include "dsm_item.h"
#include "quectel_apptcpip_util.h"

#if defined(QUECTEL_SOC_APP_SUPPORT)

#define dsat_pdp_cid_valid(cid)               (((cid) >= 1)&&((cid) <=16))
#define dsat_socket_id_valid(id)              (((id) >= 0)&&((id) <=11))

#define DSAT_TCPIP_ERR_START                 550

#define DSAT_TCPIP_PDP_ACTIVE               0
#define DSAT_TCPIP_PDP_DEACTIVE           1

#define DSTCPIP_MIN_VAL(x,y) 	((x)<(y)?(x):(y))

typedef enum
{
	DSAT_TCPIP_SEND_OK = 17,
	DSAT_TCPIP_SEND_FAILE = 18
}dsat_tcpip_result_enum_type;

typedef  enum
{
	DSAT_TCPIP_OK = 0,
	DSAT_TCPIP_ERR_FAILURE = DSAT_TCPIP_ERR_START,
    DSAT_TCPIP_ERR_WODBLOCK,
    DSAT_TCPIP_ERR_INVALID_PARAM,
    DSAT_TCPIP_ERR_OUT_OF_MEMORY,
    DSAT_TCPIP_ERR_NEW_SOCKET_FAILURE,
    DSAT_TCPIP_ERR_NOT_SUPPORT,
    DSAT_TCPIP_ERR_SOC_BIND_FAILURE,
    DSAT_TCPIP_ERR_SOC_LISTEN_FAILURE,
    DSAT_TCPIP_ERR_SOC_WRITE_FAILURE,
    DSAT_TCPIP_ERR_SOC_READ_FAILURE,
    DSAT_TCPIP_ERR_SOC_ACCEPT_FAILURE,
    DSAT_TCPIP_ERR_OPEN_PDP_FAILURE,
    DSAT_TCPIP_ERR_CLOSE_PDP_FAILURE,
    DSAT_TCPIP_ERR_SOC_INDEX_USED,
    DSAT_TCPIP_ERR_DNS_BUSY,
    DSAT_TCPIP_ERR_DNS_FAILURE,
    DSAT_TCPIP_ERR_SOC_CONNECT_FAILURE,
    DSAT_TCPIP_ERR_SOC_CLOSED,
    DSAT_TCPIP_ERR_OPT_BUSY,
    DSAT_TCPIP_ERR_OPT_TIMEOUT,
    DSAT_TCPIP_ERR_NET_DOWN,
    DSAT_TCPIP_ERR_CANCEL_SEND,
    DSAT_TCPIP_ERR_NOT_ALLOWED,
    DSAT_TCPIP_ERR_PORT_BUSY,
    DSAT_TCPIP_ERR_PDP_ACTIVED,

	
    DSAT_TCPIP_ERR_SSL_DECRYPT_FAILURE,
    DSAT_TCPIP_ERR_SSL_ENCRYPT_FAILURE,
    DSAT_TCPIP_ERR_SSL_DATA_VERIFY_FAILURE,
    DSAT_TCPIP_ERR_SSL_RESET,

    DSAT_TCPIP_ERR_MISSING_PARAM,
	DSAT_TCPIP_LAST_ERR_CODE
	
}dsat_tcpip_error_code;

typedef enum
{
   DSAT_TCPIP_CMD_NONE = 0,
	DSAT_TCPIP_CMD_QIACT,
	DSAT_TCPIP_CMD_QIDEACT,
	DSAT_TCPIP_CMD_QIDNSGIP,
	DSAT_TCPIP_CMD_QNTP,
	DSAT_TCPIP_CMD_QIOPEN,
	DSAT_TCPIP_CMD_QISEND,
	DSAT_TCPIP_CMD_QIRD,
	DSAT_TCPIP_CMD_QSSLRECV,
	DSAT_TCPIP_CMD_QISWTMD,
	DSAT_TCPIP_CMD_QISINFO,
	DSAT_TCPIP_CMD_QSSLSTATE,
	DSAT_TCPIP_CMD_QICLOSE,
	DSAT_TCPIP_CMD_ATO
}dsat_tcpip_async_cmd;

typedef enum
{
	TEXT_MODE = 0,
	 HEX_MODE
}dsat_tcpip_data_format;

typedef enum
{
	URC_FORMAT_3G = 0, /*hdeader \r\n data*/
	URC_FORMAT_2G  /*header,data*/
}dsat_tcpip_data_URC_format;

typedef enum
{
	TCPIP_CMD_SET = 0,
	ONLINE_PKT_SIZE,
	ONLINE_WAIT_TM,
	WR_DATA_FORMAT,
	DATA_URC_FORMAT,
	TCP_RETRANS_CFG,
	DNS_CACHE_CFG,
#if defined(QUECTEL_QISEND_TIMEOUT)			
	QISEND_TIMEOUT_CFG,
#endif

//<2016/11/21-QCWXM9X00021-P01-wenxue.sheng, <[QICFG] Segment 1==> Copy AT+QICFG="passiveclosed" from EC20.>
#if defined(QUECTEL_AT_COMMON_SUPPORT)
    TCP_CLOSED_CFG,
#endif // #if defined(QUECTEL_AT_COMMON_SUPPORT)
//>2016/11/21-QCWXM9X00021-P01-wenxue.sheng

	MAX_CFG_TYPE
}dsat_tcpip_cfg_type;

typedef enum
{
	DSAT_3G_TCPIP = 0,
	DSAT_2G_TCPIP,
}dsat_tcpip_cmdset_type;

typedef  struct
{
	uint32       					         online_send_pkt_size;
	uint32        					         online_send_interval_tm;
	dsm_item_type                   *online_rx_data_ptr;
	dsm_watermark_type         online_rx_wm;
	q_type                               online_rx_q;
	boolean                             online_tx_wm_high;
	dsm_watermark_type         online_tx_wm;	
	q_type                              online_tx_q;
	uint8                                 send_data_format;
	uint8                                 recv_data_format;
	uint8                                 data_urc_format;
	int32                                 current_online_socket_index;
	int32                                 last_online_socket_index[DS3G_SIOLIB_PORT_MAX];
	int32                                 pending_buffer_socket_index;
	int32                                 pending_buffer_read_len;
	int32                                 pending_push_socket_index_flag;
#if defined(QUECTEL_QISEND_TIMEOUT)			
	int32                                 qisend_timeout;
#endif
	int32                                 qisend_echo;
	boolean                             qisend_fixlen;
   uint32                                 qisend_send_len;
   uint32                                 qisend_send_index;
	//int32                                qisend_send_to_addr_v4; //v4
	//int64                                qisend_send_to_addr_v6[2];
	apptcpip_ip_addr_struct         qisend_send_to_addr;
	uint32                                qisend_send_to_port;
//<2016/11/21-QCWXM9X00021-P01-wenxue.sheng, <[QICFG] Segment 1==> Copy AT+QICFG="passiveclosed" from EC20.>
#if defined(QUECTEL_AT_COMMON_SUPPORT)
    uint8                                 abnormal_closed;
#endif // #if defined(QUECTEL_AT_COMMON_SUPPORT)
//>2016/11/21-QCWXM9X00021-P01-wenxue.sheng
}dsat_sockets_mgr;



void  dsat_tcpip_init
(
void
);

int32 dsat_tcpip_get_cmdset
(
void
);
void  dsat_tcpip_set_last_error_code
(
int32    error_code
);


int32 dsat_tcpip_get_integer
(
  dsat_num_item_type *val_arg_ptr,      /*  value returned  */
  const byte *s,                        /*  points to string to eval  */
  unsigned int r                        /*  radix */
);

int32  dsat_tcpip_get_string
(
  const byte * quoted_str, 
  byte * out_str, 
  word str_len
);

boolean dsat_tcpip_args_is_parse_complete
(
const byte *args
);

dsat_result_enum_type   dsat_tcpip_get_pdp_profile
(
uint32  pdp_cid,
dsm_item_type *res_buff_ptr    
);

dsat_result_enum_type   dsat_tcpip_set_pdp_profile
(
	uint32  pdp_cid,
	const tokens_struct_type *tok_ptr,
	uint32 arg_index
);
/* - - - maxcodeflag20160818 begin - - -*/
dsat_result_enum_type   dsat_tcpip_set_pdp_profile_for_xgauth
(
	uint32  pdp_cid,
	const tokens_struct_type *tok_ptr,
	uint32 arg_index
);

dsat_result_enum_type   dsat_tcpip_get_pdp_profile_for_xgauth
(
uint32  pdp_cid,
dsm_item_type *res_buff_ptr    
);
/* - - - maxcodeflag20160818 end - - - */

dsat_result_enum_type   dsat_tcpip_pdp_action
(
	uint32  pdp_cid,
	uint32  action
);

dsat_result_enum_type   dsat_tcpip_read_pdp_state
(
dsm_item_type *res_buff_ptr    
);

dsat_result_enum_type  dsat_tcpip_get_dns_server_address
(
uint32  pdp_cid,
dsm_item_type *res_buff_ptr    
);

dsat_result_enum_type   dsat_tcpip_set_dns_server_address
(
	uint32  pdp_cid,
	const tokens_struct_type *tok_ptr,
	uint32 arg_index
);

dsat_result_enum_type   dsat_tcpip_get_ipaddr_by_hostname
(
	uint32  pdp_cid,
	const tokens_struct_type *tok_ptr,
	uint32 arg_index
);

dsat_result_enum_type   dsat_tcpip_ntp_sync_time
(
	uint32  pdp_cid,
	const tokens_struct_type *tok_ptr,
	uint32 arg_index
);

dsat_result_enum_type  dsat_tcpip_read_ntp_client_info
(
dsm_item_type *res_buff_ptr 
);

dsat_result_enum_type   dsat_tcpip_ping
(
	uint32  pdp_cid,
	const tokens_struct_type *tok_ptr,
	uint32 arg_index
);

dsat_result_enum_type   dsat_tcpip_socket_open
(
	uint32  pdp_cid,
	const tokens_struct_type *tok_ptr,
	uint32 arg_index
);

dsat_result_enum_type  dsat_tcpip_socket_access_mode_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index
);


dsat_result_enum_type  dsat_tcpip_socket_read
(
 const tokens_struct_type *tok_ptr,
uint32 arg_index
);

dsat_result_enum_type  dsat_tcpip_socket_send_start
(
const tokens_struct_type *tok_ptr,
uint32 arg_index
);

boolean dsat_tcpip_socket_send_end
(
dsm_item_type *data_item,
dsat_result_enum_type end_code
);

dsat_result_enum_type dsat_tcpip_socket_send_with_no_edit_mode
(
const tokens_struct_type *tok_ptr,
uint32 arg_index
);

dsat_result_enum_type  dsat_tcpip_socket_close
(
const tokens_struct_type *tok_ptr,
uint32 arg_index
);

dsat_result_enum_type  dsat_tcpip_socket_state_read
(
const tokens_struct_type *tok_ptr,
uint32 arg_index
);

dsat_result_enum_type  dsat_tcpip_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);


dsat_result_enum_type  dsat_tcpip_get_last_error_code
(
 dsm_item_type *res_buff_ptr      
);

dsat_result_enum_type  dsat_tcpip_ato_handler
(
void
);

dsat_result_enum_type  dsat_tcpip_send_echo_cfg
(
 const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);

//ssl atc
#if defined(QUECTEL_SSL_AT_SUPPORT)
dsat_result_enum_type  dsat_tcpip_ssl_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);

dsat_result_enum_type  dsat_tcpip_ssl_open
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);

dsat_result_enum_type  dsat_tcpip_ssl_recv
(
 const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);

dsat_result_enum_type  dsat_tcpip_ssl_send
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);

dsat_result_enum_type  dsat_tcpip_ssl_close
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);

dsat_result_enum_type  dsat_tcpip_ssl_state
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buff_ptr      
);
#endif
//cb handler function


dsat_result_enum_type  dsat_tcpip_pdp_active_rsp_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_pdp_deactive_rsp_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_pdp_auto_deactive_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_dnsgip_result_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);
#if defined(QUECTEL_NTP_APP_SUPPORT)
dsat_result_enum_type  dsat_tcpip_ntp_sync_result_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);
dsat_result_enum_type  dsat_tcpip_ntp_read_server_info_rsp_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);
#endif
#if defined(QUECTEL_PING_APP_SUPPORT)
dsat_result_enum_type  dsat_tcpip_ping_echo_reply_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);
dsat_result_enum_type  dsat_tcpip_ping_finish_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);
#endif


dsat_result_enum_type  dsat_tcpip_socket_open_result_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_socket_access_mode_cfg_rsp_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_socket_exit_online_data_mode_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_socket_data_read_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_online_tx_wm_low_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_online_rx_wm_read_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

void dsat_tcpip_flush_pending_rpt_socket_data
(
void
);

dsat_result_enum_type  dsat_tcpip_socket_cmd_write_end_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_socket_close_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_socket_accept_ind_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_tcpip_socket_info_read_rsp_handler
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

void dsat_tcpip_socket_set_port_tx_high_flag
(
ds3g_siolib_port_e_type  port_id,
boolean tx_wm_high
);

extern boolean quectel_IsTCPIP_SSLservice_type(Q_INT32  socket_id);
#endif/*QUECTEL_SOC_APP_SUPPORT*/
#endif/*QUECTEL_DS_TCPIP_H*/
