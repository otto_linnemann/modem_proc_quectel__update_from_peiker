#ifndef QUECTEL_DS_HTTP_H
#define  QUECTEL_DS_HTTP_H

#include "quectel_ds_http_cb.h"
#include "dsm_item.h"
#if defined(QUECTEL_HTTP_APP_SUPPORT)


typedef struct
{
   int32                                   http_cmd;
	 
	dsm_item_type                   *online_rx_data_ptr;
	dsm_watermark_type         online_rx_wm;
	q_type                               online_rx_q;
	boolean                             online_tx_wm_high;
	dsm_watermark_type         online_tx_wm;	
	q_type                              online_tx_q;
    
    uint32                               post_type;
    int32                                 post_file_handle;
	 byte                                 post_filename[256];
		
	uint32                               read_type;
	int32                                 read_file_handle;
	byte                                  read_filename[256];
    #ifdef FEATURE_QUECTEL_LOCATOR_APP
    dsm_item_type *rsp_locator;
    #endif
}dsat_http_client_mgr;


extern const dsat_http_msg_handler_tab_type  dsat_http_msg_handler_tab[] ;
extern const uint32 dsat_http_msg_handler_tab_size;

void  dsat_http_client_init
(
void
);
dsat_result_enum_type  dsat_http_client_common_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buffer_ptr
);

dsat_result_enum_type  dsat_http_client_url_cfg
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buffer_ptr
);

dsat_result_enum_type  dsat_http_client_get_cmd
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buffer_ptr
);

dsat_result_enum_type dsat_http_client_post_cmd
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buffer_ptr
);

dsat_result_enum_type  dsat_http_client_read_cmd
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buffer_ptr
);


dsat_result_enum_type dsat_http_client_postfile_cmd
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buffer_ptr
);

dsat_result_enum_type  dsat_http_client_readfile_cmd
(
const tokens_struct_type *tok_ptr,
uint32 arg_index,
 dsm_item_type *res_buffer_ptr
);
//cb handler
dsat_result_enum_type  dsat_http_enter_data_mode_ind_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_exit_data_mode_ind_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_url_cmd_result_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_get_cmd_result_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_post_cmd_result_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_read_cmd_result_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_rsp_data_ind_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_sio_read_ind_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_sio_write_ind_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

dsat_result_enum_type  dsat_http_sio_tx_flush_ind_handler
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

extern void quectel_change_http_cmd_to_none(void);
#endif
#ifdef FEATURE_QUECTEL_LOCATOR_APP
dsat_result_enum_type   dsat_http_locator_result_event_handler(dshttp_msg_id  msg_id,dsm_item_type *msg_data_ptr);
#endif
#endif/*QUECTEL_DS_HTTP_H#*/
