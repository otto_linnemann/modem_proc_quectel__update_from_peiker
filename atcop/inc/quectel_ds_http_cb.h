#ifndef QUECTEL_DS_HTTP_CB_H
#define QUECTEL_DS_HTTP_CB_H

#include "dsm_item.h"

#if defined(QUECTEL_HTTP_APP_SUPPORT)


typedef enum
{
	DSHTTP_MIN_MSG_CMD = 0,
    DSHTTP_ENTER_DATA_MODE_IND_MSG,
    DSHTTP_EXIT_DATA_MODE_IND_MSG,
    DSHTTP_URL_CMD_RSP_MSG,
    DSHTTP_GET_CMD_RSP_MSG,
    DSHTTP_POST_CMD_RSP_MSG,
    DSHTTP_READ_CMD_RSP_MSG,
    DSHTTP_RSP_DATA_IND_MSG,
    DSHTTP_SIO_READ_IND_MSG,
    DSHTTP_SIO_WRITE_IND_MSG,
    DSHTTP_SIO_FLUSH_IND_MSG,
    #ifdef FEATURE_QUECTEL_LOCATOR_APP
    HTTP_EVENT_LOCATOR_RSP,
    #endif
	DSHTTP_MAX_MSG_CMD
}dshttp_msg_id;

typedef dsat_result_enum_type(*dsat_http_msg_handler)
(
dshttp_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

typedef struct
{
	dshttp_msg_id                  msg_id;
	dsat_http_msg_handler     msg_handler_fcn;
}dsat_http_msg_handler_tab_type;


void  dsat_http_msg_queue_init
(
void
);

dsat_result_enum_type dsat_http_signal_handler
(
 dsat_mode_enum_type at_mode
);


void dsat_http_client_sio_read_ind_cb
(
void
);

void dsat_http_client_sio_write_ind_cb
(
void
);


void dsat_http_client_sio_tx_flush_ind_cb
(
void
);

#endif
#endif
