#ifndef QUECTEL_CUSTOMER_ATCTAB_H
#define QUECTEL_CUSTOMER_ATCTAB_H

#include "dsati.h"

#ifdef QUECTEL_CUSTOMER_VENDOR_ATC
extern const dsati_cmd_type dsat_quectel_customer_vendor_table[];
extern const dsati_cmd_ex_type dsat_quectel_customer_vendor_table_ex [];
extern const unsigned int dsat_quectel_customer_vendor_table_size;
#endif

#endif

