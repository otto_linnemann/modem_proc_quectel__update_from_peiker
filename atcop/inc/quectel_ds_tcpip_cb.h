#ifndef  QUECTEL_DS_TCPIPI_H
#define   QUECTEL_DS_TCPIPI_H

#include "dsm_item.h"

#if defined(QUECTEL_SOC_APP_SUPPORT)

#define  ESCAPE_EXIT            0
#define  ABNORMAL_EXIT       1

typedef enum
{
	DSTCPIP_MIN_MSG_CMD = 0,
	DSTCPIP_NET_ACT_RSP_MSG,
	DSTCPIP_NET_DEACT_RSP_MSG,
	DSTCPIP_NET_AUTO_DEACT_IND_MSG,
	DSTCPIP_NET_DNSGIP_RESULT_IND_MSG,
	DSTCPIP_NTP_SYNC_FINISH_IND_MSG,
	DSTCPIP_NTP_SYNC_SERVER_INFO_RSP_MSG,
	DSTCPIP_PING_ECHO_REPLY_IND_MSG,
	DSTCPIP_PING_FINISH_IND_MSG,
	DSTCPIP_SOCKET_OPEN_RESULT_IND_MSG,
	DSTCPIP_SOCKET_ACCESS_MODE_CFG_RSP_MSG,
	DSTCPIP_EXIT_ONLINE_DATA_MODE_MSG,
	DSTCPIP_SOCKET_DATA_IND_MSG,
	DSTCPIP_ONLINE_TX_WM_LOW_IND_MSG,
	DSTCPIP_ONLINE_RX_WM_READ_IND_MSG,
	DSTCPIP_SOCKET_CMD_WRITE_END_MSG,
	DSTCPIP_SOCKET_CLOSE_IND_MSG,
	DSTCPIP_SOCKET_ACCEPT_IND_MSG,
	DSTCPIP_SOCKET_INFO_READ_RSP_MSG,
	DSTCPIP_MAX_MSG_CMD
}dstcpip_msg_id;

typedef dsat_result_enum_type(*dsat_tcpip_msg_handler)
(
dstcpip_msg_id  msg_id,
dsm_item_type *msg_data_ptr
);

typedef struct
{
	dstcpip_msg_id                  msg_id;
	dsat_tcpip_msg_handler     msg_handler_fcn;
}dsat_tcpip_msg_handler_tab_type;


void  dsat_tcpip_msg_queue_init();

dsat_result_enum_type dsat_tcpip_signal_handler
(
 dsat_mode_enum_type at_mode
);

void dsat_tcpip_complete_online_cmd_switch_cb(void);

void dsat_tcpip_complete_cmd_switch_cb(void);

void dsat_tcpip_online_sio_rx_read_cb(void);

void dsat_tcpip_online_tx_sio_low_cb(void);

void dsat_tcpip_socket_write_end_cb(int32  end_result);


#endif
#endif/*QUECTEL_DS_TCPIPI_H*/
