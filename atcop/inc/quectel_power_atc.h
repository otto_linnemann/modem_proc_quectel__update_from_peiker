#ifndef _QUECTEL_POWER_ATC_H_
#define _QUECTEL_POWER_ATC_H_

#include "dsati.h"

#ifdef QUECTEL_POWER_ATC
extern dsat_result_enum_type dsat_qpowd_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

#endif/*QUECTEL_POWER_ATC*/


#ifdef QUECTEL_FASTBOOT_ATC
/* 2016/5/11 added by sundy */
dsat_result_enum_type dsat_qfastboot_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_SDMOUNT_ATC
/* 2016/8/4 added by sundy */
dsat_result_enum_type dsat_exec_qsdauto_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

/* 2015/3/3 added by tommy */
#ifdef QUECTEL_LINUX_PRINT_ATC
dsat_result_enum_type dsat_qprint_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

/* 2016/6/13 added by sundy */
#ifdef QUECTEL_DIAG_PORT_SWITCH
extern dsat_result_enum_type dsat_qdiagport_cmd
(
    dsat_mode_enum_type mode,             /*  AT command mode:              */
    const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table       */
    const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
    dsm_item_type *res_buff_ptr           /*  Place to put response           */
);

extern dsat_result_enum_type dsat_qdiagport_result_handler
( 
    ds_cmd_type * cmd_ptr     /* DS Command pointer */
);

#endif /* QUECTEL_DIAG_PORT_CHANGE */


#ifdef QUECTEL_DRIVER_ATC_QSCLK
dsat_result_enum_type dsat_exec_qsclk_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#ifdef QUECTEL_ADC  //Added by running 2016-6-6, for implemeting "at + qadc" command
 dsat_result_enum_type dsat_qadc_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

 //Added by sundy 2016-8-27, for implemeting "at + qadctemp" command
 dsat_result_enum_type dsat_qadctemp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
#endif

#endif/*_QUECTEL_POWER_ATC_H_*/
