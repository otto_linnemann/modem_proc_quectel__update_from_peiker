#ifndef QUECTEL_ATI_H
#define QUECTEL_ATI_H

#include "ds3gsiolib.h"
#include "ds3gtimer.h"
#include "dsati.h"
#include "geran_eng_mode_read_api.h"
#include "rrcmmif.h"
#if defined(QUECTEL_AT_COMMON_SUPPORT)

#define IS_LOWER(x)   (((x) >= 'a') && ((x) <= 'z'))

#define SPECIAL_QUECTEL_CUST_CMD     0x86

#define QEGMR_QUERY   0
#define QEGMR_SET        1

typedef enum egmr_type_e
{
    DSAT_EGMR_BASEBAND_CHIPSET = 0,
	DSAT_EGMR_DSP_CODE = 1,
	DSAT_EGMR_DSP_PATCH = 2,
	DSAT_EGMR_MCU_SW = 3,
	DSAT_EGMR_MD_BOARD = 4,
    DSAT_EGMR_SN = 5,
    DSAT_EGMR_IMEI = 7,
  #ifdef QUECTEL_EGMR_MEID_GSN
    DSAT_EGMR_MEID_GSN = 8,
  #endif
    DSAT_EGMR_IMEISV_SVN = 9,
    DSAT_EGMR_IMEI_SVN = 10,		//IMEI is the first 15 bytes of IMEI.
    DSAT_EGMR_MAX
}dsat_egmr_type_e;


#if defined(QUECTEL_URC_SUPPORT)

#define  QUECTEL_USB_AT_PORT_IND           	0x0001
#define  QUECTEL_USB_MODEM_PORT_IND      0x0002
#define  QUECTEL_USB_UART1_PORT_IND       0x0004
#ifdef QUECTEL_URC_PORT_SPI
#define  QUECTEL_USB_SPI_PORT_IND            0x0008
#endif

#ifdef QUECTEL_FEATURE_OPENLINUX
#define  QUECTEL_USB_SMD_PORT_IND            0x0010
#endif

#define  QUECTEL_MAX_PORT_IND                   0x8000

typedef enum
{
    QUECTEL_URC_PORT_CFG = 0,
    QUECTEL_URC_MAX_CFG
}quectel_urc_cfg_e_type;

typedef enum
{
    QUECTEL_USB_AT_PORT = 0,
    QUECTEL_USB_MODEM_PORT,
    QUECTEL_UART1,
#ifdef QUECTEL_URC_PORT_SPI
    QUECTEL_SPI_PORT,
#endif
#ifdef QUECTEL_FEATURE_OPENLINUX
    QUECTEL_SMD_PORT,
#endif
    QUECTEL_MAX_URC_PORT
}quectel_urc_port_e_type;


typedef struct
{
    uint32                     urc_ind_port;
    ds3g_siolib_port_e_type    sio_port;
    quectel_urc_port_e_type    port_index;
}dsat_urc_port_struct;


typedef enum
{
    QUECTEL_URC_ALL_IND = 0,
    QUECTEL_URC_CSQ_IND,
    QUECTEL_URC_SMS_FULL_IND,
    QUECTEL_URC_RING_IND,
    QUECTEL_URC_SMS_INCOMING_IND,
    QUECTEL_URC_ACTCHGRPT_IND,//add by chao.zhou 2016/07/22 for porting act cmd
    QUECTEL_URC_MAX_IND,
}quectel_urc_ind_cfg_e_type;
#endif/*QUECTEL_URC_SUPPORT*/



#undef QUECTEL_ATCMD
#define QUECTEL_ATCMD(cmd_name,attrib, special, compound,cmd_id,def_lim_ptr,proc_func) cmd_id,
typedef enum
{
 QUECTEL_AT_INDEX_START = 18000,
#include "quectel_atcmd_def.h"

#ifdef QUECTEL_WMS_ATC
    DSAT_QUECTEL_WMS_QMGDA_IDX,
    DSAT_QUECTEL_WMS_QCMGO_IDX,
#endif //QUECTEL_WMS_ATC

#ifdef QUECTEL_WMS_CONCATENATED_FEATURE
  DSAT_QUECTEL_WMS_QCMGENREF_IDX,
  DSAT_QUECTEL_WMS_QCMGS_IDX,
  DSAT_QUECTEL_WMS_QCMGR_IDX,
#endif /* QUECTEL_WMS_CONCATENATED_FEATURE */

#if defined(QUECTEL_CALL_FEATURE)
  DSAT_QUECTEL_CALL_QHUP_IDX,
  DSAT_QUECTEL_CALL_VTD_IDX,
#endif

#ifdef QUECTEL_CUSTOMER_MYSYSINFO
    QUECTEL_CUSTOMER_MYSYSINFO_IDX,
#endif
#ifdef QUECTEL_CUSTOMER_DMCUSREX
    DSAT_EXT_CUSTOMER_DMCUSREX_IDX,
#endif


//<2016/07/25-QCM9X07FZ00001-P01-Frederic.Zhang, <[CTCC] Segment 1==> Add cmd: AT+HWVER.>
#if defined(QUECTEL_CTCC_COMMON)
    DSAT_CTCC_ATC_HWVER_IDX,
#endif // #if defined(QUECTEL_CTCC_COMMON)
//>2016/07/12-QCM9X07FZ00001-P01-Frederic.Zhang

 QUECTEL_AT_INDEX_END 
}quectel_at_index_enum_type;



typedef enum
{
	QUECTEL_NW_AUTO = 0,
	QUECTEL_NW_GSM_ONLY = 1,
	QUECTEL_NW_WCDMA_ONLY= 2,
	QUECTEL_NW_LTE_ONLY = 3,
	QUECTEL_NW_TDS_ONLY =4,
	QUECTEL_NW_TDS_WCDMA_ONLY =5,
	QUECTEL_NW_CDMA_ONLY = 6,
	QUECTEL_NW_HDR_ONLY= 7,
	QUECTEL_NW_CDMA_HDR_ONLY = 8,

	QUECTEL_NW_MAX_SCAN_MODE = QUECTEL_NW_CDMA_HDR_ONLY,
}quectel_nwscan_mode_enum_type;



typedef struct
{
	 char *name;
	 dsat_result_enum_type (*proc_func)( dsat_mode_enum_type,
                                      							const struct dsati_cmd_struct*,
                                      							const tokens_struct_type*,
                                     							dsm_item_type* );
}quectel_common_cfg_cmd_type;


#if defined(QUECTEL_URC_SUPPORT)
ds3g_siolib_port_e_type  quectel_get_urc_sio_port
(
void
);

boolean quectel_read_urc_ind_flag
(
quectel_urc_ind_cfg_e_type flag
);

#endif

void  quectel_ds_process_timer_expired_cmd(ds3g_timer_enum_type timer_id);

atoi_enum_type quectel_util_ato64(uint64* des, byte* src);

void quectel_nv_init();

boolean quectel_eng_gsm_get_mcc_mnc_string_by_plmn(eng_mode_plmn_id_t  plmn, char *str);

boolean quectel_eng_wcdma_plmn_id_organizer(rrc_plmn_identity_type *p_plmn_id_info, char *str);

void quectel_convert_cdma_mnc_2_ascii_mnc( sys_mcc_type *p_mnc, byte imsi_11_12);

void quectel_convert_cdma_mcc_2_ascii_mcc( sys_mcc_type *p_mcc, word mcc );


#endif/*QUECTEL_AT_COMMON_SUPPORT*/
#if defined(QUECTEL_MBN_CONFIG)||defined(QUECTEL_NDIS_FEATURE)
/*IVAN 20160226 add for MBN select by AT Command*/
#include "mcfg_common.h"
#include "mcfg_refresh.h"
extern uint32 mcfg_utils_list_configs
(
  mcfg_config_type_e_type type,
  int                     max_list_size,
  mcfg_config_id_s_type  *id_list
);
extern void mcfg_utils_print_config
(
  mcfg_config_type_e_type type,
  mcfg_config_id_s_type  *id
);
extern boolean mcfg_utils_deactivate_config
(
  mcfg_config_type_e_type type,
  mcfg_sub_id_type_e_type sub,
  boolean                 activating
);
extern boolean mcfg_utils_set_selected_config
(
  mcfg_config_type_e_type type,
  mcfg_config_id_s_type  *id,
  mcfg_sub_id_type_e_type sub
);
extern boolean mcfg_utils_get_selected_config
(
  mcfg_config_type_e_type type,
  mcfg_config_id_s_type  *id,
  mcfg_sub_id_type_e_type sub
);
extern boolean mcfg_utils_get_active_config
(
  mcfg_config_type_e_type type,
  mcfg_config_id_s_type  *id,
  mcfg_sub_id_type_e_type sub
);
extern boolean mcfg_utils_get_config_info
(
  mcfg_config_type_e_type  type,
  mcfg_config_id_s_type   *id,
  mcfg_config_info_s_type *info
);
extern boolean mcfg_trl_decode_tlv
(
  const mcfg_config_info_s_type *config_info,
  uint8                          tlv_id,
  void                          *decoded_data,
  uint32                         decoded_data_len
);
//<2019/05/09-QCM96A00014-P02-Vicent.Gao, <[MBN] Segment 2==> Fix issues of AT+QMBNCFG.>
#if 1
extern mcfg_error_e_type mcfg_utils_delete_config
(
  mcfg_config_type_e_type type,
  mcfg_config_id_s_type  *id,
  uint8                   storage_mask
);
#else //#if 1
extern mcfg_error_e_type mcfg_utils_delete_config
(
  mcfg_config_type_e_type type,
  mcfg_config_id_s_type  *id
);
#endif // #if 1
//>2019/05/09-QCM96A00014-P02-Vicent.Gao
//<2019/04/03-QCM96A00014-P01-Vicent.Gao, <[MBN] Segment 1==> Fix issues of AT+QMBNCFG="add".>
#if 1
extern mcfg_error_e_type mcfg_multi_mbn_add_config
(
    mcfg_config_type_e_type  type,
    mcfg_config_s_type      *config,
    mcfg_storage_mode_e_type storage,
    boolean replace_config_based_on_version
);
#else //#if 1
extern mcfg_error_e_type mcfg_multi_mbn_add_config
(
  mcfg_config_type_e_type  type,
  mcfg_config_s_type      *config
);
#endif // #if 1
//>2019/04/03-QCM96A00014-P01-Vicent.Gao
extern boolean mcfg_utils_parse_config_info
(
  mcfg_config_s_type      *config,
  mcfg_config_info_s_type *info
);
extern boolean mcfg_trl_decode_tlv
(
  const mcfg_config_info_s_type *config_info,
  uint8                          tlv_id,
  void                          *decoded_data,
  uint32                         decoded_data_len
);
#endif

#if defined(QUECTEL_MBN_CONFIG)
extern boolean FUNC_MCFG_Reset(void);
extern boolean FUNC_MCFG_Debug(void);
extern uint16 FUNC_MCFG_GetRecList(mcfg_config_type_e_type eType,mcfg_config_id_s_type *pRecList,uint16 uMaxNum);
extern uint16 FUNC_MCFG_GetRecListSw(mcfg_config_id_s_type *pRecList,uint16 uMaxNum);
extern uint16 FUNC_MCFG_GetRecListHw(mcfg_config_id_s_type *pRecList,uint16 uMaxNum);
extern uint16 FUNC_MCFG_GetTotal(mcfg_config_type_e_type eType);
extern uint16 FUNC_MCFG_GetTotalSw(void);
extern uint16 FUNC_MCFG_GetTotalHw(void);

//<2017/12/14-QCM9XTVG00095-P02-Vicent.Gao, <[FUNC] Segment 2==> Add MCFG common function.>
extern boolean FUNC_MCFG_GetRec(mcfg_config_type_e_type eType,mcfg_config_id_s_type *pRec,uint16 uIndex);
extern boolean FUNC_MCFG_GetRecSw(mcfg_config_id_s_type *pRec,uint16 uIndex);
extern boolean FUNC_MCFG_GetRecHw(mcfg_config_id_s_type *pRec,uint16 uIndex);
//>2017/12/14-QCM9XTVG00095-P02-Vicent.Gao

#endif // #if defined(QUECTEL_MBN_CONFIG)

#endif/*QUECTEL_ATI_H*/
