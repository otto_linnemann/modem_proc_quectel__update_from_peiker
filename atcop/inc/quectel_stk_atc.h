#ifndef QUECTEL_STK_ATC_H
#define  QUECTEL_STK_ATC_H

#if defined(QUECTEL_STK_SUPPORT)

//<2017/02/09-QCM9XTVG00080C002-P01-Joyce.Sun, <[STK] Segment 1==>Add cmd: QCFG-stkauto/setupmenutr.>
#define QCFG_STKAUTO_SETUPMENU_TR_ENABLE  1
#define QCFG_STKAUTO_SETUPMENU_TR_DISABLE 0
//>2017/02/09-QCM9XTVG00080C002-P01-Joyce.Sun

dsat_result_enum_type dsat_stk_qstk_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_stk_qstkpd_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);


dsat_result_enum_type dsat_stk_qstkstate_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_stk_qstkgi_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

dsat_result_enum_type dsat_stk_qstkrsp_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

//<2015/10/05-QCM4TVG00004C02-P01-Vicent.Gao,<[STK] Segment 1 ==> RAW DATA develop init.> 
extern dsat_result_enum_type dsat_stk_qstktr_cmd 
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsat_stk_qstkenv_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);

extern dsat_result_enum_type dsat_stk_qstkmm_cmd
(
  dsat_mode_enum_type mode,             /*  AT command mode:            */
  const dsati_cmd_type *parse_table,    /*  Ptr to cmd in parse table   */
  const tokens_struct_type *tok_ptr,    /*  Command tokens from parser  */
  dsm_item_type *res_buff_ptr           /*  Place to put response       */
);
//>2015/10/05-QCM4TVG00004C02-P01-Vicent.Gao

#endif/*QUECTEL_STK_SUPPORT*/

#endif/*QUECTEL_STK_ATC_H*/
