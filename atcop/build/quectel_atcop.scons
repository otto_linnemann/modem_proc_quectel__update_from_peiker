#===============================================================================
#
# Modem wrapper script
#
# GENERAL DESCRIPTION
#    build script to load modem data software units
#
# Copyright (c) 2014 by QUECTEL, Incorporated.
# All Rights Reserved.
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 13/11/15   Chris     Initial revision
#
#===============================================================================

Import('env')

env = env.Clone()

from glob import glob
from os.path import join, basename
#turn off debug if requested
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG = "")


#turn on debug if requested
if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG = "-g --dwarf2")
    env.Replace(HEXAGON_DBG = "-g")
    env.Replace(GCC_DBG = "-g")

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_DS",
])

#-------------------------------------------------------------------------------
# Necessary Core Public API's
#-------------------------------------------------------------------------------
CORE_PUBLIC_APIS = [
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'WIREDCONNECTIVITY',
    'STORAGE',
    'SECUREMSM',
    'BUSES',
    'IODEVICES',
    'DAL',

    # needs to be last also contains wrong comdef.h
    'KERNEL',
    ]
env.RequirePublicApi(CORE_PUBLIC_APIS, area='core')
env.RequirePublicApi(['IMSVOLTEPUBLIC'], area='IMS_VOLTE')
env.RequirePublicApi(['NAS','AUTH','CSVT','DMS','WDA','WDS','OTDT','IMSDCM', 'IMSRTP'], area='QMIMSGS')
env.RequirePublicApi(['IMSPUBLIC'], area='IMS')
#-------------------------------------------------------------------------------
# Necessary Modem Public API's
#-------------------------------------------------------------------------------
MODEM_PUBLIC_APIS = [
    'MMODE',
    'DATAMODEM',
    'UIM',
    'MCS',
    'ONEX',
    'DATA',
    'HDR',
    'WMS',
    'PBM',
    'NAS',
    'GERAN',
    'WCDMA',
    'UTILS',
    'RFA',
    'GPS',
    'ECALL',
    ]

env.RequirePublicApi(MODEM_PUBLIC_APIS)

segment_load_public_api_list = [
       ('MCFG',                'MCFG'),
       ('GPS',                 'GPS'),
       ('RFA',                 'CDMA'),
       ('RFA',                 'MEAS'),
       ('FW',                  'GERAN'),
       ('RFA',                 'LM'),
       ('RFA',                 'GSM'),
       ('RFA',                 'GNSS'),
       ('RFA',                 'LTE'),
       ('FW',                  'RF'),
       ('FW',                  'COMMON'),
       ]

for api_area,api_name in segment_load_public_api_list:
    env.RequirePublicApi([api_name], area=api_area)
#-------------------------------------------------------------------------------
# Necessary Modem Restricted API's
#-------------------------------------------------------------------------------
MODEM_RESTRICTED_APIS = [
    'MODEM_DATA',
    'MODEM_DATACOMMON',
    'DATAMODEM',
    'MMCP',
    'MCS',
    'ONEX',
    'NAS',
    'HDR',
    'MMODE',
    'RFA',
    'GERAN',
    'UIM',
    'WCDMA',
    'UTILS',
    'MDSP',
    'GPS',
    'FW',
    'LTE',
    ]

env.RequireRestrictedApi(MODEM_RESTRICTED_APIS)
#-------------------------------------------------------------------------------
# Necessary Multimedia Public API's
#-------------------------------------------------------------------------------
MM_PUBLIC_APIS = [
    'AUDIO',
    'MVS',
    ]

env.RequirePublicApi(MM_PUBLIC_APIS, area='multimedia')

#-------------------------------------------------------------------------------
# Necessary Modem Restricted API's
#-------------------------------------------------------------------------------
MODEM_PROTECTED_APIS = [
    'DATA_ATCOP',
    ]
env.RequireProtectedApi(MODEM_PROTECTED_APIS)

#-------------------------------------------------------------------------------
# Required external APIs not built with SCons (if any)
# e.g. ['BREW',]
#-------------------------------------------------------------------------------
REQUIRED_NON_SCONS_APIS = [
    'WCONNECT', 
    'BREW',
    ]

if REQUIRED_NON_SCONS_APIS != []:
  env.RequireExternalApi(REQUIRED_NON_SCONS_APIS)

env.RequirePublicApi(['COMPRESSED_HEAP', ], area='PERF')
env.RequirePublicApi(['QUECTEL_ACT'],area='quectel')

#add by Herry.geng on 20190318
env.RequirePublicApi(['QUECTEL_CJSON'],area='quectel')

env.RequirePublicApi(['QUECTEL_NV_OPERATOR'],area='quectel')
env.RequirePublicApi(['DATACOMMON'], area='datamodem')
env.RequirePublicApi(['QUECTEL_LED'],area='quectel')
env.RequirePublicApi(['DATAMODEM'], area='datamodem')
env.RequirePublicApi(['UIM'], area='uim')
env.RequirePublicApi(['QUECTEL_COMMON_QMI'],area='quectel')

#add by Herry.geng on 20190318
env.RequirePublicApi(['QUECTEL_LWM2M'],area='quectel')

env.RequirePublicApi(['STORAGE'], area='CORE')

#-------------------------------------------------------------------------------
# Necessary Modem Restricted API's
#-------------------------------------------------------------------------------
MODEM_PROTECTED_APIS = [
    'DATA_ATCOP',
    ]
env.RequireProtectedApi(MODEM_PROTECTED_APIS)
#-------------------------------------------------------------------------------
# Required external APIs not built with SCons (if any)
# e.g. ['BREW',]
#-------------------------------------------------------------------------------
REQUIRED_NON_SCONS_APIS = [
    'WCONNECT',
    'BREW',
    ]
if REQUIRED_NON_SCONS_APIS != []:
  env.RequireExternalApi(REQUIRED_NON_SCONS_APIS)
#-------------------------------------------------------------------------------
# Non-compliant Private Header Include Paths (Must be removed for CRM builds)
#-------------------------------------------------------------------------------
#if ARGUMENTS.get('SCONS_VIOLATIONS_LEVEL',0) > 99:
print "SCONS VIOLATIONS enabled"

env.PublishPrivateApi('VIOLATIONS',[
    '${INC_ROOT}/data/1x/707/src',
    '${INC_ROOT}/data/1x/bcmcs/src',
    '${INC_ROOT}/core/systemdrivers/clk/inc',
    '${INC_ROOT}/rfa/rf/common/rf/nv/src',
    '${INC_ROOT}/rfa/rf/common/rf/core/src',
    '${INC_ROOT}/rfa/rf/common/rf/rfc/src',
    '${INC_ROOT}/rfa/rf/device/rtr8600_1x/inc',
    '${INC_ROOT}/rfa/rf/device/rfdev_intf/inc',
    '${INC_ROOT}/rfa/rf/hal/p2_1x/inc',
    '${INC_ROOT}/rfa/rf/hal/common/inc',
    '${INC_ROOT}/rfa/rf/wcdma/rf/mc/inc',
    '${INC_ROOT}/rfa/rf/wcdma/rf/nv/inc',
    '${INC_ROOT}/rfa/rf/gsm/rf/core/src',
	'${INC_ROOT}/rfa/variation/inc',
	'${INC_ROOT}/rfa/rf/common/ftm/inc',
    '${INC_ROOT}/utils/oss/oss_asn1_rvds21/include',
    '${INC_ROOT}/gps/gnss/inc',
    '${INC_ROOT}/lte/api/',
    '${INC_ROOT}/core/cust/inc',
    '${INC_ROOT}/uim/api/',
    '${INC_ROOT}/uim/common/inc/',
    '${INC_ROOT}/uim/gstk/src',
    '${INC_ROOT}/uim/pbm/src',
    '${INC_ROOT}/uim/pbm/inc',
		'${INC_ROOT}/uim/variation/inc',
	  '${INC_ROOT}/uim/cust/inc/',
    '${INC_ROOT}/mmcp/nas/mm/src/',
    '$(INC_ROOT}/mmcp/variation/inc',
    '$(INC_ROOT)/mmcp/mmode/sd/inc',
    '$(INC_ROOT)/mmcp/mmode/sd/src',
	"${INC_ROOT}/mmcp/ecall/emodem/inc",
    "${INC_ROOT}/mmcp/ecall/emodemlib/inc",
    '${INC_ROOT}/datamodem/protocols/api',
    '${INC_ROOT}/datamodem/interface/atcop/inc',
    '${INC_ROOT}/datamodem/interface/atcop/src',
    '${INC_ROOT}/datamodem/interface/sysapi/inc',
	  '${INC_ROOT}/datamodem/interface/dsprofiledb/inc',
 	  '${INC_ROOT}/datamodem/interface/rmifacectls/inc',
 	  '${INC_ROOT}/datamodem/interface/qmicore/inc',
 	  '${INC_ROOT}/datamodem/interface/qmidata/inc',
 	  '${INC_ROOT}/datamodem/interface/utils/inc',
 	  '${INC_ROOT}/datamodem/interface/netiface/inc',
 	  '${INC_ROOT}/datamodem/interface/tasks/inc',
	  '${INC_ROOT}/datamodem/3gpp/rmsm/inc',
	  '${INC_ROOT}/datamodem/3gpp/ps/inc',
 	  '${INC_ROOT}/datamodem/3gpp/dsprofile/inc',
    '${INC_ROOT}/datamodem/variation/inc',
    '${INC_ROOT}/datamodem/cust/inc',
    '${INC_ROOT}/datamodem/interface/api',
    '${INC_ROOT}/datamodem/3gpp2/dsmgr/inc',
    '${INC_ROOT}/datamodem/protocols/linklayer/inc',
    '$(INC_ROOT)/perf/compressed_heap/api',
    '$(INC_ROOT)/core/services/nv/src',
    '$(INC_ROOT)/core/memory/dsm/src',
    '$(INC_ROOT)/core/systemdrivers/tlmm/src',
    '$(INC_ROOT)/core/storage/flash/src/dal',
	'${INC_ROOT}/mcfg_fwk/mcfg_proc/inc',
	'${INC_ROOT}/mcfg_fwk/mcfg_auth/inc',
	'${INC_ROOT}/mcfg_fwk/mcfg_nv/inc',
	'${INC_ROOT}/mcfg_fwk/mcfg_gen/inc',
	'${INC_ROOT}/mcfg_fwk/mcfg_qmi/inc',
	'${INC_ROOT}/mcfg_fwk/mcfg_sel/inc',
	'${INC_ROOT}/mcfg_fwk/mcfg_utils/inc',
	'${INC_ROOT}/mcfg_fwk/mcfg_diag/inc',
   ])

env.RequirePrivateApi('VIOLATIONS')

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = ".."

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)
#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------
#code shipped as source
MODEM_QUECTEL_ATCOP_SOURCES = []
ATCOP_COMMON_SOURCES = [
												'${BUILDPATH}/src/quectel_common_atc.c',
		                    '${BUILDPATH}/src/quectel_common_atctab.c',
		                    '${BUILDPATH}/src/quectel_urc.c',
		                    '${BUILDPATH}/src/quectel_ri.c',
 		                    '${BUILDPATH}/src/quectel_nw_atc.c',
 		                    '${BUILDPATH}/src/quectel_ds_tcpip.c',
 		                    '${BUILDPATH}/src/quectel_ds_tcpip_cb.c',
 		                    '${BUILDPATH}/src/quectel_tcpip_atc.c',
		                    '${BUILDPATH}/src/quectel_tcpip_atctab.c',
		                    '${BUILDPATH}/src/quectel_ds_gwtcpip.c',
 		                    '${BUILDPATH}/src/quectel_gwtcpip_atc.c',
                        '${BUILDPATH}/src/Quectel_ecc_atc.c',
	                    '${BUILDPATH}/src/quectel_ftp_atc.c',
 		                    '${BUILDPATH}/src/quectel_ftp_atctab.c',
 		                    '${BUILDPATH}/src/quectel_ds_ftp.c',
							'${BUILDPATH}/src/quectel_ds_gwftp.c',
 		                    '${BUILDPATH}/src/quectel_gwftp_atc.c',
 		                    '${BUILDPATH}/src/quectel_gwftp_atctab.c',
 		                    '${BUILDPATH}/src/quectel_smtp_atc.c',
 		                    '${BUILDPATH}/src/quectel_ds_smtp.c',
 		                    '${BUILDPATH}/src/quectel_smtp_atctab.c',
 		                    '${BUILDPATH}/src/quectel_mms_atc.c',
 		                    '${BUILDPATH}/src/quectel_mms_atctab.c',
 		                    '${BUILDPATH}/src/quectel_ds_mms.c',
 		                    '${BUILDPATH}/src/quectel_http_atctab.c',
												'${BUILDPATH}/src/quectel_http_atc.c',
												'${BUILDPATH}/src/quectel_ds_http.c',
							  			  '${BUILDPATH}/src/quectel_ds_http_cb.c',
                            '${BUILDPATH}/src/Quectel_sim_atc.c',
                            '${BUILDPATH}/src/Quectel_sim_atctab.c',
							  			  '${BUILDPATH}/src/quectel_ssl_atc.c',
                            '${BUILDPATH}/src/Quectel_fly_mode.c',
                            '${BUILDPATH}/src/Quectel_file_at.c',
							'${BUILDPATH}/src/Quectel_gwatc.c',
                            '${BUILDPATH}/src/quectel_ecall_atc.c',
                            '${BUILDPATH}/src/quectel_ecall_cmd.c',
                            '${BUILDPATH}/src/quectel_ecall_event.c',
							'${BUILDPATH}/src/quectel_ecall_comm.c',
							'${BUILDPATH}/src/quectel_atcmd_tab.c',
                            '${BUILDPATH}/src/Quectel_ds_stk.c',
                            '${BUILDPATH}/src/Quectel_stk_atc.c',
                            '${BUILDPATH}/src/Quectel_stk_atctab.c',
							'${BUILDPATH}/src/Quectel_driver_atc.c',
							'${BUILDPATH}/src/quectel_power_atc.c',
							'${BUILDPATH}/src/quectel_gps_atc.c', 
							'${BUILDPATH}/src/quectel_audio_atc.c',
 		                    '${BUILDPATH}/src/quectel_cell_atc.c',                            
                            '${BUILDPATH}/src/quectel_wms_atc.c', 
                            '${BUILDPATH}/src/quectel_wms_atctab.c',
                            '${BUILDPATH}/src/quectel_call_atc.c',
                            '${BUILDPATH}/src/quectel_wifi_atc.c',
                            '${BUILDPATH}/src/quectel_call_atctab.c',
                            '${BUILDPATH}/src/quectel_customer_atc.c',
                            '${BUILDPATH}/src/quectel_customer_atctab.c',
                            '${BUILDPATH}/src/quectel_certification.c',
                            '${BUILDPATH}/src/quectel_psm_atc.c',
                            '${BUILDPATH}/src/quectel_psm_nvram.c',
                            '${BUILDPATH}/src/quectel_efs_default.c',
#add by Herry.geng on 20190318
			'${BUILDPATH}/src/quectel_lwm2m_atc.c',
                        ]
ATCOP_LIB_FILES = [
								'${BUILDPATH}/src/quectel_common_atc.c',
		            '${BUILDPATH}/src/quectel_common_atctab.c',
		            '${BUILDPATH}/src/quectel_urc.c',
		            '${BUILDPATH}/src/quectel_ri.c',
                '${BUILDPATH}/src/quectel_nw_atc.c',
                '${BUILDPATH}/src/quectel_ds_tcpip.c',
                '${BUILDPATH}/src/quectel_ds_tcpip_cb.c',
                '${BUILDPATH}/src/quectel_tcpip_atc.c',
		            '${BUILDPATH}/src/quectel_tcpip_atctab.c',
		            '${BUILDPATH}/src/quectel_ds_gwtcpip.c',
 		            '${BUILDPATH}/src/quectel_gwtcpip_atc.c',
                    '${BUILDPATH}/src/Quectel_ecc_atc.c',
                '${BUILDPATH}/src/quectel_ftp_atc.c',
                  '${BUILDPATH}/src/quectel_ftp_atctab.c',
                  '${BUILDPATH}/src/quectel_ds_ftp.c',
				  '${BUILDPATH}/src/quectel_ds_gwftp.c',
                  '${BUILDPATH}/src/quectel_gwftp_atc.c',
                  '${BUILDPATH}/src/quectel_gwftp_atctab.c',
                  '${BUILDPATH}/src/quectel_smtp_atc.c',
                  '${BUILDPATH}/src/quectel_ds_smtp.c',
                  '${BUILDPATH}/src/quectel_smtp_atctab.c',
                  '${BUILDPATH}/src/quectel_mms_atc.c',
                  '${BUILDPATH}/src/quectel_mms_atctab.c',
                  '${BUILDPATH}/src/quectel_ds_mms.c',
                  '${BUILDPATH}/src/quectel_http_atctab.c',
									'${BUILDPATH}/src/quectel_http_atc.c',
									'${BUILDPATH}/src/quectel_ds_http.c',
				  			  '${BUILDPATH}/src/quectel_ds_http_cb.c',
                            '${BUILDPATH}/src/Quectel_sim_atc.c',
                            '${BUILDPATH}/src/Quectel_sim_atctab.c',
				  			  '${BUILDPATH}/src/quectel_ssl_atc.c',
                   '${BUILDPATH}/src/Quectel_fly_mode.c',
                   '${BUILDPATH}/src/Quectel_file_atctab.c',
                   '${BUILDPATH}/src/Quectel_file_at.c',
				   '${BUILDPATH}/src/Quectel_gwatc.c',
                    '${BUILDPATH}/src/quectel_ecall_atc.c',
                    '${BUILDPATH}/src/quectel_ecall_cmd.c',
                    '${BUILDPATH}/src/quectel_ecall_event.c',
					'${BUILDPATH}/src/quectel_ecall_comm.c',
					'${BUILDPATH}/src/quectel_atcmd_tab.c',
                    '${BUILDPATH}/src/Quectel_ds_stk.c',
                    '${BUILDPATH}/src/Quectel_stk_atc.c',
                    '${BUILDPATH}/src/Quectel_stk_atctab.c', 
                    '${BUILDPATH}/src/Quectel_driver_atc.c',
					'${BUILDPATH}/src/quectel_power_atc.c',
					'${BUILDPATH}/src/quectel_gps_atc.c',
					'${BUILDPATH}/src/quectel_audio_atc.c',
                    '${BUILDPATH}/src/quectel_cell_atc.c',
                    '${BUILDPATH}/src/quectel_wms_atc.c', 
                    '${BUILDPATH}/src/quectel_wms_atctab.c',
                    '${BUILDPATH}/src/quectel_call_atc.c',
                    '${BUILDPATH}/src/quectel_wifi_atc.c',
                    '${BUILDPATH}/src/quectel_call_atctab.c',
                    '${BUILDPATH}/src/quectel_customer_atc.c',
                    '${BUILDPATH}/src/quectel_customer_atctab.c',
                    '${BUILDPATH}/src/quectel_certification.c',
                    '${BUILDPATH}/src/quectel_psm_atc.c',
                    '${BUILDPATH}/src/quectel_psm_nvram.c',
                    '${BUILDPATH}/src/quectel_efs_default.c',
#add by Herry.geng on 20190318
		'${BUILDPATH}/src/quectel_lwm2m_atc.c',
                   ]
MODEM_QUECTEL_ATCOP_SOURCES += ATCOP_COMMON_SOURCES

env.AddBinaryLibrary(['MODEM_MODEM', ], '${BUILDPATH}/atcop', [MODEM_QUECTEL_ATCOP_SOURCES,])

#env.LoadSoftwareUnits()
